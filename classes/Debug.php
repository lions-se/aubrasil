<?

class Debug {

	const FILENAME = "log.txt";

	private static $debug_active = false;

	
	public static function EnableDebug($active = false){
		self::$debug_active = $active;
	}

	private static function Timestamp(){

		$d = new DateTime('NOW',  new DateTimeZone('America/Sao_Paulo'));

		return '['.$d->format('H:i:s').']';

	}

	public static function info($message, $data = NULL){

		if(self::$debug_active){

			try {
				$fp = fopen(Debug::FILENAME,"a");
				
				if(is_null($data)){			
					fwrite($fp, self::Timestamp().'INFO: '.$message.PHP_EOL);
				}else {

					$data = vsprintf($message, $data);
					fwrite($fp,  self::Timestamp().'INFO: '.$data.PHP_EOL);
				}


				fclose($fp);


			}catch(Exception $ex){
				die($ex->getMessage());
				 
			}



		}


	}


	public static function warning(){

	}

}


?>