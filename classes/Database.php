<?php


class Database {
	
	private $dbInstance;
	private $conf;
	private $results;

	
	public function Database($connection = NULL, $config = NULL){
		if( empty($connection) && empty($config)){
			print "Banco de Dados Nao Encontrado";
			return;	
		}
		
		$this->conf = $config;
		$this->createConnection($connection);
	}
	
	private function createConnection($db){
		
		$this->dbInstance = $db;
		
	}
	private function stripSQL($sql){
		$allowed = array("\r","\n");
		
		$string = str_replace($allowed,' ',$sql);
		
		return str_replace(array("\'"),"'", $string);	
		
			
	}
	
	public static function DatabaseStatic($db,$conf){
		$s = new Database($db,$conf);
		
		return $s;
	}
	
	private function priv_query($qstring){
		
		//$escape = mysqli_real_escape_string($this->dbInstance, stripslashes($qstring));
		$escape =  mysqli_escape_string($this->dbInstance, $qstring);
		$d = mysqli_query($this->dbInstance, $escape) or trigger_error(mysqli_error($this->dbInstance)); 
		return $d;
	}
	
	public function query($qstring){
			
			
			$d = $this->priv_query($qstring);
		
		if (!$this->resultCheck($d)){
			return;
		}
		
		
		$qp = new QueryProperties($d);
		
		
		return $qp;
		
	}
	


	
	public function resultCheck($result){
		if( $result == 0){
			return false;	
		}
		
		return true;
	}
	
	public function result(){
		return $this->results;
	}
	
	public function delete($table, $where = NULL){
		$d = NULL;
		$temp ="";
		
		if(!is_string($where)) {
			print "Funcao Delete  WHERE Precisa ser String";
			return;
		}
		
		$temp = $where;
		$query = sprintf("DELETE FROM %s WHERE %s", $table, $temp);
		//print $query;
		$data = $this->priv_query($query);
		return $this->resultCheck($data);
	}
	
	public function update($table, $data, $where){
		
		$set = "";
		$temp = array();
		$tempstr = "";
		if(is_array($data)){
			
			foreach($data as $field => $value){
					$tempstr = sprintf("%s = '%s'",$field,$value);
					array_push($temp,$tempstr);
			}
		}
		
		$set = implode(',',$temp);
		$query = sprintf("UPDATE %s SET %s WHERE %s", $table, $set,$where);
		$run = $this->priv_query($query);
		return $this->resultCheck($run);
		
	}
	
	public function insert($table, $data){
		
		$camposlist = array();
		$valoreslist = array();
		
		
		foreach($data as $field => $value){
			array_push($camposlist, $field);
			array_push($valoreslist, sprintf("'%s'", $value));
		}
		
		
		
		$query = sprintf("INSERT INTO %s (%s) VALUE(%s)", $table, implode(',',$camposlist), implode(',',$valoreslist));
		$data = $this->priv_query($query);  
		return $this->resultCheck($data);
	}
	
	
};


class QueryProperties {
	
	private $result;
	private $rows;
	public $querydata = array();
		
	public function QueryProperties($result, $rows){
			$this->result = $result;
			$this->runquerydata();
			
	}
	
	public function setRowsAffected($rows){
		
	}
	
	public function rowsAffected(){
			return $this->rows;
	}
	
	public function runquerydata(){
		
		return "blbla";
		
		
	}
	
	public function data(){
		return  $this->querydata;
	}
	
}





?>