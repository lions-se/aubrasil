<?



abstract class Product  {

	public $id = 0;
	public $qty = 1;
	public $name = ""; // it needs for using search!
	public $price = 0.0;
	public abstract function addQty();
	public abstract function removeQty();
	public abstract function setQty($qty);

}




class Cart implements Iterator, Countable {

	private $productList = array();
	private static $instance;
	private $position = 0;
	private $ids = array();
	private $franquia_id;





	public function __construct(){

		
	}

	public function setFranquiaId($franquia_id){
		$this->franquia_id = $id;
	}

	public function getFranquiaId(){
		return $this->franquia_id;
	}


	public function key(){
		return $this->position;
	}

	public function next(){
		$this->position++;
	
	}

	public function rewind(){
		$this->position = 0;
	}

	public function valid(){

		return (isset($this->$ids[$this->position]));
	}


	public function current(){

		$index = $this->productList[$this->position];

		return $index; 

	}


	public static function InitCart(){

		if(empty(self::$instance)){

			$newclass = __CLASS__;
			self::$instance = new $newclass;
			return self::$instance;
		}

			return self::$instance;
	}

	public function make_seed(){
    	list($usec, $sec) = explode(' ', microtime());
    	return (float) $sec + ((float) $usec * 100000);
	}

	public function Signature(){

		mt_srand(make_seed());

		return  1.0 * mt_rand() ^ 4;

	}



	public function addProduct(Product $prod){
		
		if($this->xsearch($prod->id)){

			foreach ($this->productList as $value) {
				if($prod->id == $value->id){
					$value->qty++;
					return false;
				}
			}

		}else {
			array_push($this->productList, $prod);
			return true;
		}


		
	}


	public function clear(){
		if(count($this->productList) > 0 ){

			$this->productList = array();
		}
	}


	public function showCartData(){

		var_dump($this->productList);
	}


	public function count(){
		return count($this->productList);
	}

	public function all_products(){

		return $this->productList;
	}


	public function indexSearch($key){
		for($i = 0; $i < count($this->productList); ++$i){

				$produto = $this->productList[$i];

				if($produto->name == $key || $produto->id == $key){
					return $i;
				}
				
		}

		return NULL;

	}

	public function search($key){

		for($i = 0; $i < count($this->productList); ++$i){

				$produto = $this->productList[$i];

				if($produto->name == $key || $produto->id == $key){
					return $produto;
				}
				//if($produto->nome)
		}

		return NULL;

	}

	public function addQtyProduct($product_id){

		if($this->xsearch($product_id)){
			
			$productList[$product_id]->addQty();			
		}

	}


	public function removeProduct($product_id){
			
			$index = $this->indexSearch($product_id);
			unset($this->productList[$index]);
			//return $this->productList[$index];
	}


		public function setQtyProduct($product_id, $qty){

		if($this->xsearch($product_id)){
			$productList[$product_id]->setQty($qty);
		}

	}


	public function xsearch($id){

		for($i = 0; $i < count($this->productList); ++$i){

			if($this->productList[$i]->id == $id){
				return true;
			}
		}

		return false;
	}


	public  function generateXMLPagSeguro($data=NULL, $shipping = NULL, $options = NULL){

		$dom = new DomDocument('1.0','UTF-8');
		$dom->formatOutput = true;

		$root = $dom->createElement('checkout');
		$dom->appendChild($root);

		$currency = $dom->createElement("currency", 'BRL');
		$currency = $root->appendChild($currency);


		$items = $dom->createElement('items');
		$items = $root->appendChild($items);

		
		foreach ($this->productList as $value) {
			$item = $dom->createElement('item');
			
			$id = $dom->createElement('id', sprintf("%04d", $value->id));
			$id = $item->appendChild($id);

			$name = $dom->createElement('description', $value->name);
			$name = $item->appendChild($name);

			$priceFactor = isset($_SESSION['descontoFator']) ?  $value->desc_cliente * $value->qty : 0;
			
			//$total_value = ($value->price * $value->qty) - $priceFactor;

			$price = ( ($value->price - $value->desc_produto) * $value->qty) - $priceFactor;
			

			$amount = $dom->createElement('amount', sprintf("%.2f",$price / $value->qty));
			$amount = $item->appendChild($amount);

			$qty = $dom->createElement('quantity', $value->qty);
			$qty = $item->appendChild($qty);

			$weight = $dom->createElement('weight', sprintf("%d",$value->peso * 1000));
			$weight = $item->appendChild($weight);

			$item = $items->appendChild($item);
		}

		if(!is_null($data)){

			 $sender =  $dom->createElement('sender');
			 $sender = $root->appendChild($sender);

			foreach ($data as $key => $value) {
				if(array_key_exists($key, $data)){

					if(!is_array($value)){
						$name_user = $dom->createElement($key, $value);
						$name_user = $sender->appendChild($name_user);
					}
				}

			}


			if(array_key_exists('phone', $data) && is_array($data['phone'])){

				$phone =  $dom->createElement('phone');
				$phone =  $sender->appendChild($phone);

				foreach ($data['phone'] as $key => $value) {
					$phone_user = $dom->createElement($key,$value);
					$phone_user = $phone->appendChild($phone_user);
				}

			}

		}


		if(!is_null($shipping) && is_array($shipping)){

			$shipping_node = $dom->createElement('shipping');
			$shipping_node = $root->appendChild($shipping_node);

			if(array_key_exists('type', $shipping)){
				$type_node = $dom->createElement('type', $shipping['type']);
				$type_node = $shipping_node->appendChild($type_node);
			}

			if( array_key_exists('address', $shipping)  &&  is_array($shipping['address']  )){

				$address_node = $dom->createElement('address');
				$address_node = $shipping_node->appendChild($address_node);



				foreach($shipping['address'] as $key => $value){

					if(!is_array($value)){

						$shipping_type = $dom->createElement($key,$value);
						$shipping_type = $address_node->appendChild($shipping_type);
					}

				}

			}


		}

		if(!is_null($options) && is_array($options)){



			foreach ($options as $key => $value) {
				
				$opt_node = $dom->createElement($key,$value);
				$opt_node = $root->appendChild($opt_node);
			}
		}




		return $dom->saveXML();

		



	}





}


/*
EXEMPLO DE USO


include_once("../config.php");


$p = new Produto();

$p->id = 1;
$p->name = "Produto Teste";
$p->price = 10.50;
$p->descr = "Produto Teste";
$p->peso = 2.0;

$cart = new Cart();


$cart->addProduct($p);

header("content-type: application/xml");
echo $cart-> generateXMLPagSeguro(array('name'=>'José Comprador',
											 'email' => 'nicholasluis@gmail.com',
											 'phone' => array('areaCode'=>12, 'number' => '551236426818')

											),

									  array('address'=> array('street'=>'Rua Soldado Franscisco Rodrigues',
									  						  'number' => 25,
									  						  'complement'=> 'casa',
									  						  'district'=>'Campos Maia',
									  						  'postalCode'=>12422230,
									  						  'city'=>'Pindamonhangaba',
									  						  'state'=>'SP',
									  						  'country'=>'BRA'),
									  		'type'=> 1)
										);		


*/

?>