<?

class WizardPage {

	private $name;
	private $url;

	public function __construct($name,$url){
		$this->name = $name;
		$this->url = $url;
	}

	public function go($id){
		Flight::redirect(sprintf('/%d/%s', $id, $this->url));
	}

}

class Wizard implements Countable, Iterator {

	private $pagelist = array();
	private $position = 0;

	public function count(){
		return count($this->pagelist);
	}

	public function current(){
		return $this->pagelist[$this->position];
	}

	public function next(){
		return $this->position++;
	}

	public function valid(){
		if( in_array($this->current(), $this->pagelist)){
			return true;
		}
		return false;
	}

	public function rewind(){
		$this->position = 0;
	}

	public function key(){
		array_flip($this->pagelist[$this->position]);
	}

}

$t = new Wizard();

?>