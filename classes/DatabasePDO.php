
<?php


class DatabasePDO extends PDO {
	
	const DRIVER_MYSQL = 0x01;
	const DRIVER_PSGRE = 0x02;
	const DRIVER_ORACLE = 0x03;
	const DRIVER_SQLSERVER = 0x04;
	const DRIVER_SQLITE3= 0x05;
	
	const FETCH_LAZY = 0x01;
	const FETCH_OBJECT = 0x02;
	const FETCH_ASSOC = 0x03;

	const DEBUG = false;

	
	private $driver;
	
	private $username;
	private $passwd;
	private $database;

	private $is_debug;
	
	//private $querydata; 
	
	public function __construct($host,$username,$passwd, $port = 3306, $driver = DatabasePDO::DRIVER_MYSQL, $database, $debug = false){
		
		switch($driver){
			case  DatabasePDO::DRIVER_MYSQL:
				$this->driver = 'mysql';
				break;
				
			case  DatabasePDO::DRIVER_PSGRE:
				$this->driver = 'psgre';
				break;

			case DatabasePDO::DRIVER_SQLSERVER:
			case DatabasePDO::DRIVER_ORACLE:
				die("NOT SUPPORT FOR ORACLE AND SQL SERVER YET (NOT TESTED)");
				break;

			case DatabasePDO::DRIVER_SQLITE3:
				$this->driver ='sqlite';
				break;
			
			default:
				$this->driver = 'mysql';
				break;
				
		}

		$this->is_debug = $debug;
		
			if($this->driver == 'sqlite'){

				parent::__construct($this->connectString($host,$port,$database), $username, $passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

			}else {
				parent::__construct($this->connectString($host,$port,$database), $username, $passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
			}
		    

		    $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    		$this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		
		
	}

	public static function initSQLite($path){
		if(!is_file($path)){

			throw new Exception("Não foi Possivel processar o arquivo sqlite", 1);
			
		}

		return new DatabasePDO('','','',0, DatabasePDO::DRIVER_SQLITE3, $path, true);
	}
	
	private function connectString($host, $port, $db){

		if($this->driver == "sqlite"){

			if(is_null($db)) throw new Exception("Deve ter um caminho para o banco de dados SQLITE", 1);

			return sprintf("sqlite:/%s", $path);			

		}else {
		
			return sprintf("%s:host=%s;port=%d;dbname=%s",$this->driver, $host, $port,$db);

		}
		
	}
	
	protected function ExecuteSQL($statement,$param = NULL, $fetch_type = PDO::FETCH_BOTH, $sender = NULL ){
			
			try{
				$this->beginTransaction();
				
				if(!empty($param)){
					$statement->execute($param);
				}

				$statement->execute();


				switch($fetch_type){
					case PDO::FETCH_CLASS:
					case PDO::FETCH_OBJ:
					case PDO::FETCH_CLASSTYPE:
						$statement->setFetchMode(PDO::FETCH_CLASS, 'stdClass');
						break;

					default:
						
						break;
				}

				$data = $statement->setFetchMode($fetch_type);


				switch($sender){

					case 'insert':
						$query = $this->lastInsertId();
						break;

					case 'update':
						$query = true;
						break;

					case 'delete':
						$query = true;
						break;

					default:

						$data = $statement->fetchAll();
						$query = new QueryProperties($data,$statement->rowCount());
						break;
				}


				$statement->closeCursor();
				//print("LAST INSERT ID: ".$this->lastInsertId());
				$this->commit();
				return $query;


			}catch(PDOException $ex){

					printf("Erro: %s", $ex->getMessage());
					$this->rollback();
					return new PDOException();
			}

				
			
		
		
	}
	
	protected function RunQuery($statement, $param = NULL, $fetch_type){
			
			switch($fetch_type){
				case DatabasePDO::FETCH_ASSOC:
					return $this->ExecuteSQL($statement,$param,PDO::FETCH_BOTH);
					break;
				case DatabasePDO::FETCH_LAZY:
				case DatabasePDO::FETCH_OBJECT:
					return $this->ExecuteSQL($statement,$param,PDO::FETCH_CLASS);
					break;
					
				default:
					return $this->ExecuteSQL($statement,$param,PDO::FETCH_ASSOC);
					break;
					
				
			}
			//$this->Execute($statement,$param,$fetch_type);
	}
	
	public function query($sql, $data =  NULL, $fetch_mode = DatabasePDO::FETCH_ASSOC){
			
			if(empty($data) || $data == NULL){
				return $this->priv_query($sql);	
			}
			
			return $this->priv_query($sql,$data,NULL,$fetch_mode);
		//return $this->priv_query($sql,$data,NULL,$fetch_mode);
			
	}


	public function query2($sql, $data = NULL) {

			

		try{


			$stmt = $this->prepare($sql);



			if($this->is_debug){

				//echo "Contagem Parametros: ".count($data);

			}

			if(!empty($data)){
				
				$count = 0;

				foreach ($data as $key => $value) {
					
					if($this->is_debug){
						++$count;
						//printf("Iter: %d<br/>", $count);
						//printf("KEY: %s  VALUE: %s<br/>", $key, $value);

					}

					if(!empty($value) || $value != NULL  || $value != ''){


						$stmt->bindValue($key, $value, PDO::PARAM_INT | PDO::PARAM_STR);	
					}		
				}

			}

			return $this->ExecuteSQL($stmt, $data, PDO::FETCH_BOTH);



		}catch(PDOException $ex){
			throw new PDOException($ex->getMessage());
			
		}



	}

	
	public function priv_query($query,$data = NULL, $param = NULL, $fetch_mode = DatabasePDO::FETCH_BOTH){
		try{
			$stmt = $this->prepare($query);
	
		
			if(!empty($data)){
			
				foreach($data as $key => $value){

					if($this->is_debug){

						printf("KEY: %s   VALUE:%s<br/>", $key, $value);

					}

					$stmt->bindValue($key,$value, PDO::PARAM_INT |  PDO::PARAM_STR);	
				}
			}
			
			//return $this->ExecuteSQL($stmt,$param,$fetch_mode);
			  return $this->RunQuery($stmt, $param, $fetch_mode);	
		}catch(PDOException $ex){
			 throw new PDOException($ex->getMessage());		
		}
			
	}



	
	public function insert($table, $data){
		
		if(empty($data)){
			throw new Exception("Data Vazio");
			
		}
		
		try{

			$cols = implode(',', array_keys($data));
			$rows = ':H_'.implode(',:H_', array_keys($data));

			//$cols = implode(',',array_flip($data));
			//$rows = ':H_'.implode(',:H_',array_flip($data));



		
		}catch(Exception $ex){
			echo $ex->getMessage();
		}
		
		$query = sprintf("INSERT INTO %s (%s) VALUES(%s)", $table, $cols,$rows);
		$stmt = $this->prepare($query); 
		
		if(!$stmt){
			print $this->errorInfo;
		}
		
		if($this->is_debug); print_r($query);
		
		


		foreach($data as $key => $value){
			
			if($this->is_debug){

				printf("Key: %s    Value:%s<br/>", $key, $value);

			}


				if(!empty($value) ||!is_null($value)){
					

					$stmt->bindValue(':H_'.$key,$value, PDO::PARAM_INT | PDO::PARAM_STR );
				}
		

		}

		
		return $this->ExecuteSQL($stmt,NULL,PDO::FETCH_BOTH, 'insert');
		
		
	}

	
	public  function update($table, $data, $where) {
		
	
		$campo_sql = array();
		
		foreach($data as $key => $value){
				
				array_push($campo_sql,$key.' = :'.$key);
		}

		$query = sprintf("UPDATE %s SET %s WHERE %s",$table, implode(',',$campo_sql), $where);
		
		if($this->is_debug) print_r($query);
		
		$stmt = $this->prepare($query);
		
		foreach($data as $key => $value){
			$stmt->bindValue(':'.$key,$value);
		}
		
		return $this->ExecuteSQL($stmt, NULL, PDO::FETCH_BOTH, 'update');
		
	}


	public function update2($table, $data, $wherestr, $param){

		$campo_sql = array();
		
		foreach($data as $key => $value){
				
				array_push($campo_sql,$key.' = :'.$key);
		}

		$query = sprintf("UPDATE %s SET %s WHERE %s",$table, implode(',',$campo_sql), $wherestr);
		
		if($this->is_debug) print_r($query);
		
		$stmt = $this->prepare($query);
		
		foreach($data as $key => $value){
			$stmt->bindValue(':'.$key,$value,  PDO::PARAM_INT | PDO::PARAM_STR);
		}


		//prepara os parametros do WHERE
		//prepare WHERE parameters

		foreach ($param as $key => $value) {
			$stmt->bindValue($key,$value, PDO::PARAM_INT | PDO::PARAM_STR);
		}


		return $this->ExecuteSQL($stmt,NULL,NULL, 'update');

	}
	
	
	public function delete($table, $where){
		
		
		$q = sprintf("DELETE FROM %s WHERE %s",$table, $where);
		
		
		$stmt = $this->prepare($q);
		return $this->ExecuteSQL($stmt);
				
	}


	public function delete2($table, $wherestr, $param){

			$q = sprintf("DELETE FROM %s WHERE %s", $table, $wherestr);
			$stmt = $this->prepare($q);

			if(is_array($param)){

				foreach ($param as $key => $value) {
					$stmt->bindValue($key,$value, PDO::PARAM_INT | PDO::PARAM_STR);
				}


				return $this->ExecuteSQL($stmt);

			}




	}
	
}

class QueryProperties {
	
	private $rows;
	public $querydata;
	private $lastId;

	public function __construct($data,$rows){
		$this->querydata = $this->clean($data);
		$this->rows = $rows;
		
	}

	public function setLastInsertId($id){

		$this->lastId = $id;
		//print($this->lastId);
	}

	public function getLastId(){
		return $this->lastId;
	}

	public function clean($data){
		return $data;
	}
	
	public function rowsAffected(){
		return $this->rows;
		
	}
}



//exemplos de uso

/*
$t = new  DatabasePDO('localhost', 'root', '',3306,  DatabasePDO::DRIVER_MYSQL, 'tabelateste');


//EXEMPLO DE UMA QUERY COMUM
$testquery = $t->query2("SELECT * FROM  tabela WHERE idtabela = :id", array(":id"=> 1)) ;
print_r($testquery);


//EXEMPLO DE LIKE
$testlike = $t->query2("SELECT * FROM  tabela  WHERE nome LIKE :nome", array(":nome"=>'Joao%')) ;
print_r($testlike);



//exemplo de insert


parametro @1:  'nome da tabela'
parametro @2:  'array com os dados a ser inseridos onde  a chave (key) é o nome do campo eo valor (value) conteudo daquele campo  '

$t->insert('categoria', array('NOME' => 'teste',
							  'NOMEINTERNO' => 'testenomeinterno',
							  'URL'   => 'minhaurl'));



 Atenção  os metodos  delete, update  tem 2 versoes

 $t->delete e $t->delete2
 são parecidos porem diferente implementações
 motivo:
 os metodos como delete2 e update2  é  o jeito correto de implementar  evitando  SQL Injection porem para evitar quebras no codigo e refazer todas as queries
 foi criada uma nova função para cada uma delas

 recomendo o uso dela pois serão removida posteriormente 


FUNCAO  NAO RECOMENDADA USAR
USE delete2
parametro @1 nome da tabela
parametro @2 id  ou qualquer condição de deletar

$t->delete('tabela', "id = 1");



parametro @1 nome da tabela
parametro @2 id  ou qualquer condição de deletar ser digita como statement
parametro @3 conteudo onde vai ser substituido pelo :id

$t->delete2('tabela', "id = :id", array(':id' => 1));



FUNCAO  NAO RECOMENDADA USAR
USE update2
parametro @1 nome da tabela
parametro @2 conteudo a ser substituido onde nomecampo é o campo do banco de dados o valor  o conteudo a ser substituido
parametro @3 string de where

$t->update('nometabela', array('nomecampo' => 'valor', 'id = 1'));



parametro @1 nome da tabela
parametro @2 conteudo a ser substituido onde nomecampo é o campo do banco de dados o valor  o conteudo a ser substituido
parametro @3 string de where controlado pelo statement do PDO
parametro @4 conteudo do where

$t->update2('nometabela', array('nomecampo' => 'valor', 'id = :id', array(':id'=> 1)));


*/


?>