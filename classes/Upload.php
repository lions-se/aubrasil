<?php

//require_once("Image.class.php");
//require_once("Imageresizer.php");

class Upload {

	private $path_dest;
	private $imgnames = array();
	private $options = array(

			'ext_allowed' => array('jpg','png','jpeg'),
			'max_size' => "500K"
		);


	private $files;

	public function Upload($dest, $options = NULL, $files = NULL){
			
			$this->path_dest = $dest;

			if(!is_dir($this->path_dest)){

					mkdir($this->path_dest,0755);
			}

			



			if($options != NULL){
				$this->options = $options;
			}

			//if($files != NULL){
			if(isset($_FILES)){
				$this->files = $_FILES;
			}




	}

	public  function path(){
		return $this->path_dest;
	}


	private function parseSize($size){

		$finalsize = NULL;

		
		if( is_string($size)){

			if( strstr($size, 'M')){

				$num = explode('M',$size);
				$numint = intval($num[0]);

				return $this->calculateMB($numint);


			}else if(strstr($size, 'k')){

				$num = explode("k", $size);
				$numint =   intval($num[0]);

				return $this->calculateKB($numint);
				

			}

		}else if(is_int($size)){

			$finalsize = $size;

			return $finalsize;

		}
	}

	private function calculateMB($megabytes, $precision = 2){

		$base = log($megabytes) / log(1024);

		$suffix = array("M","k")[floor($base)];

		return round(pow(1024, $base - floor($base), $precision)).$suffix;

	}

	private function calculateKB($kilobytes){
		
		$num = 1 << 10;

		return $num * $kilobytes;
	}

	private function checkExtensions($name){

		$file_ext = pathinfo($name);  


		if(in_array($file_ext['extension'], $this->options['ext_allowed'])){
			return true;
		}

		//print $file_ext['extension'];

		return false;
	}

	private function config_exists($key){
		if(!empty($this->options)){
		
			if(array_key_exists($key, $this->options)){
				return true;
			}
		}

		return false;
	}


	private function copy($data, $output = NULL){

		
		$ext = extensao($data['name']);

		if($output == NULL){

			$output = sha1(uniqid(rand(), true)).'.'.$ext;

		}

		$final_path = realpath($this->path_dest).DIR.'arq'.DIR.$output;


		

		if(!function_exists("move_uploaded_file")){
			copy($data['tmp_name'], $final_path);

		}else {
			move_uploaded_file($data['tmp_name'], $final_path);
		}


		return basename($final_path);
		



	}


	private function copy_resize($data,$output=NULL, $w=100,$h=100){

		if($data['error'] != 4 ){		
			$img = new Image($data['tmp_name']);
			$resizer = ImageResizer::ImageFactory($img);
			$resizer->Thumbnail($w,$h);


			if($output  == NULL){
				$name = sha1(uniqid(rand(), true)).'.jpg';
			}else {
				$name = $output;
			}


			$thumbdir = $this->path_dest.'/thumbs/';
			$original = $this->path_dest.'/images/';

			if(!is_dir($thumbdir)){
				mkdir($thumbdir,0755);
			}

			if(!is_dir($original)){
				mkdir($original,0755);
			}


			$resizer->Save($thumbdir.$name,100);
			$resizer->SaveOriginal($original.$name,100);
			$resizer->Close();

			return $name;

	}
} 


	public function doUpload($filename, $output = NULL){


		$data = $this->files[$filename];
		
		

		if($this->checkExtensions($data['name']) && $data['error'] != 4 ){

				return $this->copy($data,$output);
		}	
		

	}


	public function doUploadResize($filename,$output=NULL, $width=100,$height=100){

		$data = $this->files[$filename];

		
		 $name = $this->copy_resize($data, $output, $width, $height);


		return $name;

	}


	public function multipleUploads(){	

		foreach ($this->files as $key => $value) {

			$this->doUpload($key);
		}


	}


	public function multipleUploadsResize($width,$height){

		$nomes = array();

		foreach($this->files as $key => $value){

			$dados = $this->files[$key];

			if($dados['error'] != 4){
				$nomes[$key]  = $this->doUploadResize($key);
			}

		}


		return $nomes;
	}


}


?>