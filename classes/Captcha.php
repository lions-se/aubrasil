<?

class Captcha {

	private $image;
	private $text;

	private $width;
	private $height;
	private $words;

	private $font;
	private $security;

	//private $salt = ""

	public function  __construct($font  = NULL, $width=200,$height=200, $words=4, $security = 5){

		if(is_null($font) && is_file($font)){

			throw new Exception("__construct(): Need Font", 1);
			
		}

		$this->width  = $width;
		$this->height = $height;
		$this->words = $words;

	
		$this->font = $font;
		$this->security = $security;

	}

	public function generateRandomString(){	


		$str = rtrim(base64_encode(md5(microtime())),"=");
		
		return substr($str, 0,$this->words);
		//return  substr(  rtrim(base64_encode(md5(microtime())),"=")),  $this->words)  ;
	}

	public function generateRandomColor(){

		$color = 0;
		$newcolor = rand(0,255);
		return ($newcolor < 255) ? $newcolor : ($newcolor > 0) ? $newcolor : 0;


	}




	public function generateRGB($tmp_image){

	
		return imagecolorallocate($tmp_image, $this->generateRandomColor()  , $this->generateRandomColor(), $this->generateRandomColor());

	}

	public function rgb($r,$g,$b){

		//return imagecolorallocate($this>, red, green, blue)
	}

	public function make_seed(){
    	list($usec, $sec) = explode(' ', microtime());
    	return (float) $sec + ((float) $usec * 100000);
	}

	public function noiseSquares($image){
		$noise = $this->security * 2;

		$gray = imagecolorallocate($image, 180, 180, 180);

		for($i = 0; $i < $noise; ++$i){

			$cx = (int) rand(0,($this->width / 2));
			$cy = (int) rand(0,$this->height);
			$h = $cx + (int) rand(0,($this->height / 5));
			$w = $cx + (int) rand($this->width / 3, $this->width);

			imagefilledrectangle($image, $cx, $cy, $w, $h, $gray);

		}

		
	}

	public function noiseEllipse($image){

		$ellipses = $this->security * 2;
		$black = imagecolorallocate($image, 0, 0,0);

		for($i = 0; $i < $ellipses; ++$i){

  			$cx = (int)rand(-1*($this->width/2), $this->width + ($this->width/2));
  			$cy = (int)rand(-1*($this->height/2), $this->height + ($this->height/2));
  			$h  = (int)rand($this->height/2, 2*$this->height);
  			$w  = (int)rand($this->height/2, 2*$this->height);
			imageellipse($image, $cx, $cy, $w, $h, $black);

		}

	}

	public function generateImage(){

		try{
		
		$this->image = imagecreate($this->width, $this->height);
		//$color = $this->generateRGB($);

		$text = $this->generateRGB($this->image);

		$black = imagecolorallocate($this->image, 0, 0, 0);
		//imagestring($this->image,6,  10 , ($this->height / 2) , $this->generateRandomString()  ,  $black  );
		
		$this->noiseSquares($this->image);
		$this->noiseEllipse($this->image);
		

		//generate crazy letters

		$string = $this->generateRandomString();
		//$len = strlen($string);


		
		
		$size = 20;
		$random_y = ($this->height / 2) + ($size / 2);
			
		//print 'generated '.$string;

		for ($i = 0; $i <= strlen($string); ++$i ) {
			mt_srand($this->make_seed());

			$random_angle = rand(0,15);
			$pos_x  += 5 * ($size / 5 );
			//$random_x = rand(10, $this->width + $last_pos);
			
			$last_pos = $random_x;


			
			imagettftext($this->image, $size,$random_angle, $pos_x, $random_y, $black, $this->font, $string[$i]);
		}


		$this->storeSession($string);


		imagettftext($this->image, 25,0, 200, 200, $black, $this->font, "Real: ".$string);

	}catch(Exception $ex){

		print 'generateImage(): '.$ex->getMessage();
	}

	}


	private function storeSession($string){
		if(session_status() == PHP_SESSION_ACTIVE){
			//mt_srand($this->make_seed());


		
			//print $salt;
			$_SESSION['m_keycaptcha'] = password_hash($string,PASSWORD_BCRYPT);

		}
		//if(sessio)
	}

	public static function check($post){
		
		if(password_verify($post,$_SESSION['m_keycaptcha'] )){

		 return true;

		}

		return false;
	
	}



	public function show(){

		
		imagepng($this->image);
		imagedestroy($this->image);
	}



}


?>