<?


class BCrypt {

	protected static $_saltPrefix = '2a';
	protected static $_cost = 8;
	protected static $_saltlength = 22;



	public static function hash($string, $cost= NULL){

		if(empty($cost)){
			$cost = self::$_cost;
		}

		$salt = self::__generateRandomSalt();

		$hash = self::__genHashString((int) $cost, $salt);

		return crypt($string, $hash);


	}


	public static function  check($str, $hash){

		return (crypt($str, $hash) == $hash);
	}


	public static function __generateRandomSalt(){

		$seed = uniqid(mt_rand(), true);

		$salt = base64_encode($seed);
		$salt = str_replace('+', '.', $salt);

		return substr_replace($salt, 0, self::$_saltlength);

	}



	private static function  __genHashString($cost, $salt){
		return sprintf("$%s$%02d$%s$", self::$_saltPrefix, $cost, $salt);

	} 



};



?>