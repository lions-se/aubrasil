<?php

class Pagination {

	private $pdo;
	private $pages_total;
	private $entries_per_page = 10; //page rows
	private $rows = 0;
	private $qstring = "page";
	private $currentpage = 1;


	//const LAST_PAGE =  1 << 2;
	//const FIRST_PAGE = 1 << 4;

	//private $flags =  Pagination::LAST_PAGE;

	public function __construct($db_instance, $max_entries = NULL, $querystring = 'pages'){

		$this->pdo = $db_instance;
		
		if($max_entries != NULL) $this->entries_per_page = $max_entries;
		

		if($querystring != NULL) $this->qstring = $querystring;

	}


	
	public function getTotalEntries($table, $param = NULL, $data = NULL){

		$d = NULL;

		if($param != NULL && $data != NULL){

				$query = sprintf("SELECT COUNT(*) AS TOTAL from %s  %s", $table, $param);
				$d = $this->pdo->query2($query,$data);
		}else {

				$query = sprintf("SELECT COUNT(*) AS TOTAL from %s", $table);
				$d = $this->pdo->query($query);
		}
	
		

		$this->rows = intval($d->querydata[0]['TOTAL']);
		//print "ROWS ". $this->rows;
		return $this->rows;

	}

	public function getTotalEntriesSQL($query, $param = NULL){
		
		if(!is_null($param)){
			$dados = $this->pdo->query2($query, $param);
		}else {
			$dados = $this->pdo->query2($query);
		}
		

		$this->rows = 	$dados->rowsAffected();	

		return $this->rows;

	}


	public function setPages($table, $additional_param  = NULL, $data = NULL){

		$this->pages_total = ceil($this->getTotalEntries($table, $additional_param, $data) / $this->entries_per_page);

	}

	public function setPagesSQL($query, $param = NULL){

		$this->pages_total = ceil($this->getTotalEntriesSQL($query,$param) / $this->entries_per_page);
	}

	public function getPagesTotal($table){

		return $this->pages_total;
	}




	public function retrieveData($sql, $extra = NULL){

		$this->currentpage  = $_GET[$this->qstring];


		$offset =  ($this->currentpage  - 1)  * $this->entries_per_page;

		if($offset < 0){
			$offset = 0;
		}else if( $offset > $this->rows){
			$offset = $this->this->pages_total;

		}

		$query = sprintf("%s LIMIT %d,%d ", $sql, $offset, $this->entries_per_page);

		if($extra != NULL){
			$data = $this->pdo->query2($query, $extra);
		}else {
			$data = $this->pdo->query($query);
		}
		


		return $data;


	}

	public function Pages(){

		$link = '<ul class="pagination">'.PHP_EOL;


		$qs =count($_GET);

		$character = ($qs >= 1) ? '&' : '?';

		for($i = 1; $i <= $this->pages_total; ++$i ){



			if(isset($_GET[$this->qstring])){

				$url = parse_url('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);


				$t = preg_replace('/'.$this->qstring.'=([0-9])/', $this->qstring.'='.$i, 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
				

				//print_r($t);

				$pagelink = $t;
				

			}else {
				$pagelink =  'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$character.$this->qstring.'='.$i;
			}
			//if($this->fla)
			if($this->currentpage == $i){
				$link .= sprintf('<li class="active"><a href="%s">%d</a></li>', $pagelink, $i);

			}else {
				$link .= sprintf('
					
					<li><a href="%s">%d</a></li>
					
					'

					, $pagelink, $i);
			}
			//$this->entries_per_page *= $_GET[$this->qstring];

		}

		$link .= '</ul>';


		print $link;
	}


	public function lastPage(){


		$last = $this->pages_total;
		$pagelink =  $_SERVER['PHP_SELF'].'?'.$this->qstring.'='.$last;


		printf('<a href="%s"> Ultima Pagina </a>', $pagelink);
	}


	public function firstPage(){

		$pagelink =  $_SERVER['PHP_SELF'].'?'.$this->qstring.'=1';
		printf('<a href="%s"> Primeira Pagina </a>', $pagelink);
	}




}



?>


