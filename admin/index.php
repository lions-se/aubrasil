<?
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
/*
TELA DE ADMIN NAO MEXER
 */

include_once("adminconf.inc.php");
include_once("../config.php");

session_start();
ob_start();


Flight::path(ADMIN_PATH.DIR.'app_class'.DIR);


Flight::set('flight.views.path', 'layouts');


Flight::register('Pagina', 'Pagination', array($db, 9,'pages'), function($pag){

});

Flight::register('db', 'DatabasePDO', array($config['hostname'],  $config['username'], $config['passwd'], 3306, DatabasePDO::DRIVER_MYSQL, $config['db'],  true ), function($db){
    });


//Flight::register('dblite', 'DatabasePDO', array(NULL,NULL,NULL,NULL, DatabasePDO::DRIVER_SQLITE3, SQLITE_PATH.DIR.'d.db', true ), function($db){
//   });


Flight::register('loginSession', 'AdminSession', array(), function($d){

//});
});

Flight::register('buscaCep','CepWebService', array(), function($cep){

});

Flight::register('Pagina', 'Pagination', array($db, 5), function($pag){

});


Flight::set('db', $db);
Flight::set('loginSession', Flight::loginSession());

Flight::set('pagseg_token', TOKEN);
Flight::set('pagseg_email', EMAIL_PAGSEGURO);
//Flight::set('dblite', $db_lite);

/**
 * LOGIN TEMPORARIO
 */


if(!isset($_SESSION['loginCredentials'])){
	$_SESSION['loginCredentials'] = Flight::get('loginCredentials');
	Flight::set('loginCredentials', $_SESSION['loginCredentials']);
}

//var_dump($_SESSION['loginCredentials'] );
//Flight::set('loginCredentials', )


Flight::render('header.php', array(), 'header_data');
Flight::render('top.php', array(), 'top_data');


Flight::route('GET /marketing', array('Marketing','listMarketing'));

Flight::route('GET /marketing/editar/@id:[0-9]+', array('Marketing','editaMarketing'));
Flight::route('POST /marketing/editar/@id:[0-9]+', array('Marketing','doEditaMarketing'));


Flight::route('GET /marketing/apagar/@id:[0-9]+', array('Marketing','apagaMarketing'));
Flight::route('POST /marketing/apagar/@id:[0-9]+', array('Marketing','doApagaMarketing'));


Flight::route('GET /marketing/adicionar', array('Marketing','cadastraMarketing'));
Flight::route('POST /marketing/adicionar', array('Marketing','processCadastraMarketing'));



Flight::route('GET /(home)', array('Home','index'));

/**
 * FRANQUIAS
 */
Flight::route('GET /franquias', array('Franquias','index'));

Flight::route('GET /franquias/mostrar/@franquia_id', array('Franquias','showFranquias'));
Flight::route('POST /franquias/mostrar/@franquia_id', array('Franquias','atualizaFranquias'));


Flight::route('GET /franquias/deletar/@franquia_id', array('Franquias','deleteFranquia'));
Flight::route('POST /franquias/deletar/@franquia_id', array('Franquias','deleteFranquia'));

Flight::route('GET /franquias/cadastrar', array('Franquias','cadastraFranquia'));
Flight::route('POST /franquias/cadastrar', array('Franquias','cadastraProcessForm'));


Flight::route('GET /minha_franquia', array('Franquias','minhaFranquia'));
Flight::route('POST /minha_franquia', array('Franquias','atualizaMinhaFranquia'));

Flight::route('POST /franquias/do_cadastra', array('Franquias','cadastraProcessForm'));
Flight::route('POST /franquias/do_delete/@id', array('Franquias','deleteProcessForm'));




/**
 * PRODUTOS
 */
Flight::route('GET /produtos', array('Produtos','index'));
Flight::route('GET /produtos/editar/@id:[0-9]+', array('Produtos','pagEditar'));


Flight::route('GET /produtos/apagar/@id:[0-9]+', array('Produtos','pagApagar'));
Flight::route('POST /produtos/apagar/@id:[0-9]+', array('Produtos','processApaga'));

Flight::route('GET /produtos/adicionar', array('Produtos','pagAdicionar'));

Flight::route('POST /produtos/busca_subcategoria/@id:[0-9]+', array('Produtos','buscaSubcat'));
Flight::route('POST /produtos/do_edita', array('Produtos','processEdit'));

Flight::route('POST /produtos/do_cadastra', array('Produtos','processCadastra'));

/**
 * LOGIN
 * 
 */
Flight::route('GET /login', array('Login','index'));
Flight::route('POST /login/do_login', array('Login','doProcessLogin'));


Flight::route('GET /logout', array('Login','doLogout'));


/**
 * PRPRIETARIOS
 */
Flight::route('GET /proprietarios(/page/@pageid)', array('Proprietarios','index'));
Flight::route('GET /proprietarios/editar_proprietario/@proprietarioid', array('Proprietarios','editProprietario'));
Flight::route('GET /proprietarios/deleta_proprietario/@prop_id', array('Proprietarios','deletaProprietarios'));


Flight::route('GET /proprietarios/adicionar', array('Proprietarios','addProprietario'));
Flight::route('POST /proprietarios/adicionar', array('Proprietarios','processAddProprietario'));


Flight::route('POST /proprietarios/do_editProprietarios', array('Proprietarios','processEditProprietario'));
Flight::route('POST /proprietarios/do_updateCredentials', array('Proprietarios','processEditCredentials'));
Flight::route('POST /proprietarios/do_delete', array('Proprietarios','processDeleteProprietario'));


Flight::route('GET /proprietarios/add_adm/@prop_id', array('Proprietarios','addAdm'));
Flight::route('GET /proprietarios/remove_adm/@prop_id', array('Proprietarios','removeAdm'));



/**
 * VEICULOS
 * 
 */

Flight::route('GET /veiculos', array('Veiculos','index'));
Flight::route('GET /veiculos/adicionar', array('Veiculos','pagAddVeiculo'));
Flight::route('POST /veiculos/adicionar', array('Veiculos','processAddVeiculo'));

Flight::route('GET /veiculos/editar/@veiculonum', array('Veiculos','pagEditVeiculo'));
Flight::route('POST /veiculos/editar/@veiculonum', array('Veiculos','processEditVeiculo'));


Flight::route('GET /veiculos/apagar/@veiculonum', array('Veiculos','pagDeleteVeiculo'));

//Flight::route('GET /veiculos/apagar/@veiculonum', array('Veiculos','processDeleteVeiculo'));



/**
 * CATEGORIAS
 * 
 */
Flight::route('GET /categorias', array('Categorias','index'));

Flight::route('GET /categorias/editar/@id:[0-9]+', array('Categorias','editaCategoria'));
Flight::route('POST /categorias/editar/@id:[0-9]+', array('Categorias','processaEditaCategoria'));

Flight::route('GET /categorias/cadastrar', array('Categorias','cadastraCategoria'));
Flight::route('POST /categorias/cadastrar', array('Categorias','processaCadastroCategoria'));


Flight::route('GET /categorias/remover/@id:[0-9]+', array('Categorias','removeCategoria'));
Flight::route('POST /categorias/remover/@id:[0-9]+', array('Categorias','processaRemoveCategoria'));


/**
 * SUBCATEGORIAS
 */
//Flight::route('GET /subcategorias', array('SubCategoria','index'));
Flight::route('GET /subcategorias', array('Subcategoria', 'index'));

Flight::route('GET /subcategorias/cadastrar', array('Subcategoria', 'pagCadastraSubCategoria'));
Flight::route('POST /subcategorias/cadastrar', array('Subcategoria', 'processCadastraSubCategoria'));

Flight::route('GET /subcategorias/editar/@sub_categoria_id:[0-9]+', array('Subcategoria', 'editaSubCategoria'));
Flight::route('POST /subcategorias/editar/@sub_categoria_id:[0-9]+', array('Subcategoria', 'processaRemoveSubCategoria'));

Flight::route('GET /subcategorias/editar-categoria/@sub_categoria_id:[0-9]+', array('Subcategoria', 'attrCategoria'));
Flight::route('POST /subcategorias/editar-categoria/@sub_categoria_id:[0-9]+', array('Subcategoria', 'processAttrCategoria'));



/**
 * CONTEUDO ESTATICO
 */
Flight::route('GET /conteudo-estatico', array('Estatico', 'index'));

Flight::route('GET /conteudo-estatico/editar-franquia', array('Estatico', 'pagEditaFranquia'));
Flight::route('POST /conteudo-estatico/editar-franquia', array('Estatico', 'processEditaFranquia'));

Flight::route('GET /conteudo-estatico/editar-empresa', array('Estatico', 'pagEditaEmpresa'));
Flight::route('POST /conteudo-estatico/editar-empresa', array('Estatico', 'processEditaEmpresa'));


Flight::route('GET /conteudo-estatico/editar-treinamentos', array('Estatico', 'pagEditaTreina'));
Flight::route('POST /conteudo-estatico/editar-treinamentos', array('Estatico', 'processEditaTreina'));


Flight::route('GET /conteudo-estatico/editar-servicos', array('Estatico', 'pagEditaServicos'));
Flight::route('POST /conteudo-estatico/editar-servicos', array('Estatico', 'processEditaServicos'));



Flight::route('GET /conteudo-estatico/editar-sua-franquia', array('Estatico', 'pagEditaSuaFranquia'));
Flight::route('POST /conteudo-estatico/editar-sua-franquia', array('Estatico', 'processEditaSuaFranquia'));


Flight::route('GET /conteudo-estatico/editar-termos', array('Estatico', 'pagEditaTermos'));
Flight::route('POST /conteudo-estatico/editar-termos', array('Estatico', 'processEditaTermos'));


//Flight::route('GET /conteudo-estatico', array('Estatico', 'index'));



/**
 * EDITAR DADOS
 */

Flight::route('GET /editar_dados', array('Usuario','index'));
Flight::route('POST /editar_dados/update', array('Usuario','processForm'));


/**nt
 * CLIENTES
 */
Flight::route('GET /clientes', array('Clientes','index'));


Flight::route('GET /clientes/editar/@id:[0-9]+', array('Clientes','pagEditarClientes'));
Flight::route('POST /clientes/editar/@id:[0-9]+', array('Clientes','editCliente'));



Flight::route('GET /clientes/apagar/@id:[0-9]+', array('Clientes','pagApagarClientes'));
Flight::route('GET /clientes/ver_pedidos/@id:[0-9]+', array('Clientes', 'mostraPedidos'));
Flight::route('GET /clientes/ver_pedidos/@id:[0-9]+/nota_virtual/@ordem:[0-9]+', array('Clientes', 'verProdutos'));

Flight::route('GET /troca_pedido/@clienteid:[0-9]+/@pedidoid:[0-9]+/@statusid:[0-9]+', array('Clientes', 'trocaPedidoStatus'));

//utils 

Flight::route('GET|POST /busca_usuario/@mix', array('Utils','BuscaProprietario'));

/**
 * CLASSIFICADOS
 */

Flight::route('GET /classificados/', array('Classificado','index'));

Flight::route('GET /classificados/adicionar', array('Classificado','adicionar'));
Flight::route('POST /classificados/adicionar', array('Classificado','doAdicionar'));

Flight::route('GET /classificados/editar/@id:[0-9]+', array('Classificado','editar'));
Flight::route('POST /classificados/editar/@id:[0-9]+', array('Classificado','doEditar'));

Flight::route('GET /classificados/apagar/@id:[0-9]+', array('Classificado','apagar'));
Flight::route('POST /classificados/apagar/@id:[0-9]+', array('Classificado','doApagar'));


/**
 * PEDIDOS
 */
Flight::route('GET /pedidos', array('Pedidos','Listar'));

Flight::route('POST /classificados/apagar/@id:[0-9]+', array('Classificado','doApagar'));



Flight::route('GET|POST /busca_cep(/@mix)', function($cep){	

	if($_SERVER['REQUEST_METHOD'] == "POST" ){

		$tcep = $_POST['cep'];

	}else {

		$tcep = $cep;
	}

	Utils::buscaCep($tcep);

});


Flight::route('GET|POST /busca_modelo_veiculo(/@marca_id:[0-9]+/@modelo_id:[0-9]+)', function(){
	//var_dump(func_num_args());
	//Utils::buscaCep2($modelo);
	if(func_num_args() == 3) {

		$marca_id = func_get_arg(0);
		$modelo_id = func_get_arg(1);
		Utils::optionModeloVeiculo($marca_id,$modelo_id);
		
	}else {
		Flight::notFound();
	}


	

});


Flight::route('GET|POST /busca_marca(/@mix)', function($marca_id){



	Utils::listarModelos($_POST['marca']);

});


Flight::route('GET|POST /download/@id:[0-9](-@nome)', function($id){

	Utils::DownloadFile($id);

});


Flight::route('GET|POST /retorno/@transacao', function($id){



header('Content-Type: text/html; charset=ISO-8859-1');



	
	// POST recebido, indica que é a requisição do NPI.
	$npi = new PagSeguroNpi();
	$result = $npi->notificationPost();
	
	$transacaoID = isset($_GET['TransacaoID']) ? $_GET['TransacaoID'] : '';
	
	
	var_dump($result);


});



//Flight::route('GET /marketing', array('Marketing','listMarketing'), 'conteudo');


Flight::render('footer.php', array(), 'footer_data');
Flight::start();
ob_flush();
?>