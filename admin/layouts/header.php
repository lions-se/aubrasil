<?
ini_set('default_charset', 'UTF-8');
header('Content-Type: text/html; charset=utf-8');
// Adds X-Frame-Options to HTTP header, so that page cannot be shown in an iframe.
header('X-Frame-Options: DENY');

// Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
header('X-Frame-Options: SAMEORIGIN');

header("X-Content-Security-Policy: allow 'self'");

// Adds the Content-Security-Policy to the HTTP header.
// JavaScript will be restricted to the same domain as the page itself, but
// is allowed inside the HTML page (no separate *.js file required).
header("X-Content-Security-Policy: allow 'self'; options inline-script");
?>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo ADM_TITULO.'-'.ADM_EMPRESA; ?></title>


		<!-- Windows/IE font smoothing -->
<meta http-equiv="cleartype" content="on">
<!-- Use latest IE randering angine and switch on chrome Frame if available -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">



<link href="/css/custom-theme/jquery-ui-1.10.0.custom.css" rel="stylesheet">
<link href="/css/admin.css" rel="stylesheet"  />
<link href="/css/chosen.min.css" rel="stylesheet" />
<link href="/css/summernote/summernote.css" rel="stylesheet" />
<link href="/css/summernote/summernote-bs3.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/font-awesome.min.css" rel="stylesheet">



<script src="/js/jquery-1.10.js"></script>
<script src="/js/jquery-delaytime.js"></script>
<script src="/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-filestyle.min.js"></script>




</head>
<body>

