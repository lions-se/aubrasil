<?



?>

<div class="container">
<h1> Listar Categorias</h1>
<div class="bt_espaco">
    <a href="<?php echo rootURL().'/categorias/cadastrar'; ?>" class="btn btn-primary btn-lg">
      <span class="glyphicon glyphicon-plus"></span> Adicionar Novo Cadastro
    </a>
</div>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Categoria</th>
			<th>Ações</th>
		</tr>
	</thead>
	<tbody>
		<tbody>

			<? foreach ($q->querydata as $key => $value) { ?>
			<tr>
				<td><?php echo $value['CAT_NOME']; ?></td>
				
				
				<td>
					
					
					<!-- Split button -->
<div class="btn-group">
  <button type="button" class="btn btn-primary">Ações</button>
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a  href="<?php echo rootURL().'/categorias/editar/'.$value['CATEGORIA_ID'] ?>">Editar</a></li>
    <li><a href="<?php echo rootURL().'/categorias/remover/'.$value['CATEGORIA_ID'] ?>">Remover</a></li>
  </ul>
</div>
				</td>
			</tr>
			<? } ?>


		</tbody>
	</tbody>
</table>

			<?php echo $pag->Pages(); ?>
</div>