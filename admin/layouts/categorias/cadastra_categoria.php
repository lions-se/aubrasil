<?php

 //include_once("do_cadastraCategoria.php");
 ?>

     <? if(isset($_SESSION['flash'])){

          flash();
          kill_alert();

    } ?>


<div class="container edituser-container">
	<div class="container">

		<h1>Cadastrar Categoria<small> Produtos</small></h1>


        <div class="row">
     		<form class="form-horizontal" id="formEditFranquia"  method="post" enctype="multipart/form-data" role="form">
            
           <button type="button" class="btn btn-default" name="btAdd" id="id_btAdd" > <i class="glyphicon glyphicon-plus"> </i> Adicionar Mais</a></button>
      		
        <? if(isset($_SESSION['qtdRegistros'])) { 

              for($i = 0; $i < $_SESSION['qtdRegistros']; $i++){
        ?>	

  			<div id="formCopy">
          <? if(isset($_SESSION['qtdRegistros'])){ ?>
            <legend>&nbsp;</legend>
          <? } ?>
  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Nome da Categoria</label>
    				<div class="col-sm-10">
      					<input type="text" class="form-control campotexto" name="nome[]" value="<?php echo formValue($dados['nome'][$i]); ?>"  id="cpNome" placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Nome Interno</label>
    				<div class="col-sm-10">
      				<input type="text" name="nomeinterno[]" value="<?php echo formValue($dados['nomeinterno'][$i]); ?>" class="form-control campotexto" row="4" col="3" />
    				</div>
  				</div>
      </div>
      <? }

      }else { ?>

  <div id="formCopy">
          <? if(isset($_SESSION['qtdRegistros'])){ ?>
            <legend>&nbsp;</legend>
          <? } ?>
          <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Nome da Categoria</label>
            <div class="col-sm-10">
                <input type="text" class="form-control campotexto" name="nome[]" value="<?php echo formValue($dados['nome'][$i]); ?>"  id="cpNome" placeholder="Nome">
            </div>
          </div>

          <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Nome Interno</label>
            <div class="col-sm-10">
              <input type="text" name="nomeinterno[]" value="<?php echo formValue($dados['nomeinterno'][$i]); ?>" class="form-control campotexto" row="4" col="3" />
            </div>
          </div>
      </div>
      <? } 
      
      ?>






				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" id="btAtiva" name="SendForm" class="btn btn-primary btn-lg">Cadastrar</button>
					</div>
				</div>	






	</div>	
</div>

<script>

  $(function(){

    $("#id_btAdd").on('click', function(){

      var cp = $("#formCopy").html();

      var separador = <? echo isset($_SESSION['qtdRegistros']) ? '\'\'' : '\'<legend>&nbsp</legend>\'' ?>

      $("#formCopy").append(separador + cp);

    });

  });


  
</script>