<?php
/*
	ob_start();
	session_start();
	require_once("../config.php");
	include("adminconf.inc.php");

	include("header.php");
	include("top.php");
	//include("main.php");
	
	


$query = "SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.EMAIL_SECUNDARIO,
proprietario.EMAIL,
proprietario.CPF,
proprietario.ESTADO_CIVIL,
proprietario.RG,
proprietario.DATA_REGISTRO,
proprietario.TELCOMERCIAL,
proprietario.TELRESIDENCIAL,
proprietario.TELCELULAR,
proprietario.SEXO,
proprietario.DATA_NASCIMENTO,
proprietario.CEP,
proprietario.COMPLEMENTO,
proprietario.ENDERECO,
proprietario.NUMERO,
proprietario.BAIRRO,
proprietario.CIDADE,
proprietario.ESTADO,
proprietario.PAIS,
proprietario.CODIGOEMAIL,
proprietario.CONFIRMA_CADASTRO,
permissao.PERMISSAO_ID,
permissao.ROLE
FROM
proprietario
INNER JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID

WHERE
proprietario.PROPRIETARIO_ID = :id";
	
	
	$q = $db->query2($query, array(':id' => $_SESSION['usuarioid']));

*/
$nome = $q->querydata[0]['NOME'];
$sobrenome = $q->querydata[0]['SOBRENOME'];
$cpf = $q->querydata[0]['CPF'];
$rg = $q->querydata[0]['RG'];
$cep = $q->querydata[0]['CEPFRANQUIA'];
$dob = $q->querydata[0]['DATA_NASCIMENTO'];
$end = $q->querydata[0]['ENDERECO'];
$cidade = $q->querydata[0]['CIDADE'];
$bairro = $q->querydata[0]['BAIRRO'];
$uf = $q->querydata[0]['ESTADO'];



?>
<div class="container">

      <?
      if(isset($_SESSION['flash'])){
        echo flash();
        kill_alert();
      }

     ?>

  <div class="container">

   <?   
          if(isset($perm) && $perm != 1 && $troca === true){
            include_once("layouts/franquias/editaFranquia.php");
          }
    ?>

		<h1>Editar Dados Cadastrais</h1>
        
        <div class="row">
      <form class="form-horizontal" id="formEdit" action="<? echo rootURL().'/editar_dados/update' ?>"  method="post" enctype="application/x-www-form-urlencoded" role="form">
  <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Nome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="nome" value="<?php echo formValue($nome); ?>"  id="cpNome" placeholder="Nome">
    </div>
  </div>
  <div class="form-group">
    <label for="cpSobrenome" class="col-sm-2 control-label">Sobrenome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="sobrenome" value="<?php echo formValue($sobrenome); ?>" id="cpSobrenome" placeholder="Seu Sobrenome">
    </div>
  </div>
  
   <div class="form-group">
    <label for="cpCPF" class="col-sm-2 control-label">CPF</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="cpf" value="<?php echo formValue($cpf); ?>" id="cpCPF" placeholder="Seu CPF">
    </div>
  </div>
  
  
   <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">RG</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="rg" value="<?php echo formValue($rg); ?>" id="cpRg" placeholder="Numero do RG">
    </div>
  </div>
  
  
   <div class="form-group">
    <label for="cpDOB" class="col-sm-2 control-label">Data de Nascimento</label>
    <div class="col-sm-10">
		<div class="campodocs">
			<input class="span2   campotexto" readonly date-calendar="true" id="nascimento"  name="dob" size="16" value="<?php echo formValueDate($dob); ?>"   type="text">
		</div>
        
    </div>
  	</div>
    
    
    <?  Flight::render('estatico/buscar_cep', array('cep' => $cep, 'endereco' => $end, 'cidade' => $cidade, 'bairro' => $bairro, 'estado' => $uf )); ?>
  
 

    </div>
  </div>
  
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div id="avisoSucesso" class="alert alert-success alert-dismissible aviso">Enviado Com Sucesso</div>
			<div id="avisoFalha" class="alert alert-danger alert-dismissible aviso">Falha no Envio</div>
		</div>
	</div>
    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btSendFormEdita" name="SendForm" class="btn btn-default">Atualizar</button>
    </div>
  </div>
</form>
</div>
        
	</div>
</div>



<script>
	$(function(){


		
    $("#btBuscaCep").on('click',function(e){
  
    
      
    $(document).ajaxStart(function() {
            $("#buscaCepLoading").show(0.5);
        }).ajaxStop(function() {
            $("#buscaCepLoading").hide(0,5);
        });
      
    var cep = $("#cpCEP").val();
    var cep_end = "<?php echo rootURL().'/busca_cep/' ?>" + cep; 
      
    $.ajax({
        url: cep_end,
        type: "get",
    dataType:"json",
        
    
    
    
        success: function(dados){
            

      
        $("#cpEnd").val(dados.logradouro);
        $("#cpBairro").val(dados.bairro);
        $("#cpCidade").val(dados.cidade);
        $("#status").html(dados);
  
      
        },
        error:function(){
            //alert("failure");
            //$("#result").html('There is error while submit');
        }
    }); // fimajax
  
    });
	
	        
	});

		
			
</script>

<?php
	include("footer.php");
	ob_end_flush();
?>