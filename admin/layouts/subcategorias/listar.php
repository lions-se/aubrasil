<?



?>

<div class="container">
<h1> Listar Subcategorias</h1>
<div class="bt_espaco">
    <a href="<?php echo rootURL().'/subcategorias/cadastrar'; ?>" class="btn btn-primary btn-lg">
      <span class="glyphicon glyphicon-plus"></span> Adicionar Novo Cadastro
    </a>
</div>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Subcategoria</th>
			<th>Atribuido a Categoria:</th>
			<th>Ações</th>
		</tr> 
	</thead>
	<tbody>
		<tbody>

			<? foreach ($q->querydata as $key => $value) { ?>
			<tr>
				<td><?php echo $value['SUBCAT_NOME']; ?></td>
				<td><a href="<?php echo rootURL().'/subcategorias/editar-categoria/'.$value['SUBCATEGORIA_ID'] ?>" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-remove"></span> <?php echo $value['CAT_NOME']; ?></a> </td>
				
				<td>
					
					
					<!-- Split button -->
<div class="btn-group">
  <button type="button" class="btn btn-primary">Ações</button>
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a  href="<?php echo rootURL().'/subcategorias/editar/'.$value['SUBCATEGORIA_ID'] ?>">Editar</a></li>
    <li><a href="<?php echo rootURL().'/subcategorias/remover/'.$value['SUBCATEGORIA_ID'] ?>">Remover</a></li>
  </ul>
</div>
				</td>
			</tr>
			<? } ?>


		</tbody>
	</tbody>
</table>

			<?php echo $pag->Pages(); ?>
</div>