<div class="container">
      <form class="form-horizontal" id="formEdit"  method="post" enctype="application/x-www-form-urlencoded" role="form">
		
		<legend>Atribuir Categoria a Subcategoria</legend>

       <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">Categoria</label>
    <div class="col-sm-10">
    	<select  name="categoria" class="form-control  campotexto">
        	<? 
        		$d = "";
        		for($i = 0; $i < count($dados); ++$i){

        			$id = $dados[$i]['CATEGORIA_ID'];
        			$nomes = $dados[$i]['NOME'];
        			$nomeinterno =  $dados[$i]['NOMEINTERNO'];


        			if($id == $atualSubcategoria){
        				$d .= sprintf('<option selected value="%s">%s</option>', $id, $nomes);
        			}else {
        				$d .= sprintf('<option value="%s">%s</option>', $id, $nomes);
        			}
        		}
        	?>

        	<?php echo $d; ?>
        </select>

    </div>
  </div>
  
 
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btSendFormEdita" name="SendForm" class="btn btn-default">Atualizar</button>
    </div>
  </div>
</form>
</div>