<?php

include("do_setAdmin.php");


$checkstr = "SELECT COUNT(proprietario.PROPRIETARIO_ID) AS CONTAGEM FROM proprietario
WHERE proprietario.PROPRIETARIO_ID =  :id";


$check = $db->query2($checkstr,
								array(":id"=> $_GET['do_adm'])

	);






$query = "SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.EMAIL,
proprietario.CPF,
proprietario.RG,

proprietario.CONFIRMA_CADASTRO
FROM
proprietario
WHERE proprietario.PROPRIETARIO_ID = :id";


$q = $db->query2($query, array(':id'=> $_GET['do_adm'] ));


$user_exists = intval($check->querydata[0]['CONTAGEM']);

if($user_exists == 0){

	alert("Houve uma falha", "Usuario Não Existe",ALERTA_FALHA);
	header("location: index.php?opcao=listar-usuarios");

}


$nome = $q->querydata[0]['NOME']; 
$sobrenome = $q->querydata[0]['SOBRENOME']; 
$rg = $q->querydata[0]['RG']; 
$cpf = $q->querydata[0]['CPF']; 

?>	

<div class="container edituser-container">
	<div class="container fill">

		<h4>Retirar Permissao de Administrador do Usuario: <?php echo $nome; ?></h4>
        
        <div class="row">
      <form class="form-horizontal" id="formEdit"  method="post" enctype="application/x-www-form-urlencoded" role="form">
  <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Nome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto"    readonly  name="nome" value="<?php echo formValue($nome); ?>"  id="cpNome" placeholder="Nome">
    </div>
  </div>
  <div class="form-group">
    <label for="cpSobrenome" class="col-sm-2 control-label">Sobrenome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto"   readonly  name="sobrenome" value="<?php echo formValue($sobrenome); ?>" id="cpSobrenome" placeholder="Seu Sobrenome">
    </div>
  </div>

     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">RG</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto"   readonly name="rg" value="<?php echo formValue($rg); ?>" id="cpRg" placeholder="Numero do RG">
    </div>
  </div>
  
   <div class="form-group">
    <label for="cpCPF" class="col-sm-2 control-label">CPF</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" readonly name="cpf"value="<?php echo formValue($cpf); ?>" id="cpCPF" placeholder="Seu CPF">
    </div>
  </div>
  


    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btSendForm" name="SendForm" class="btn btn-primary">Atribuir  Permissão de Administrador</button>
    </div>
  </div>
</form>
</div>
        
	</div>
</div>