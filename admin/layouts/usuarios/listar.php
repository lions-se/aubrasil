<?php 
  
  $where = "";
  
  if($_SERVER['REQUEST_METHOD'] == "POST"){
    
    //print_r($_POST);
    $campo = "";
    if( !empty($_POST['cpBuscar']) && !empty($_POST['cpSelect']) ){
      switch($_POST['cpSelect']){
        
        case 'Nome':
          $campo = 'NOME';
          break;
        
        case 'CPF':
          $campo = "CPF";
          break;
        case 'Documento RG':
          $campo = "RG";
          break;

        case 'E-Mail':
          $campo = "EMAIL";
          break;

        default:
          $campo = 'NOME';
          break;
      }
      
      $busca = $_POST['cpBuscar'];
      $_SESSION['whereProprietario'] = array('campo' => $campo, 'busca' => $busca);   //sprintf(" WHERE %s LIKE '%s%%'", $campo, $busca);
      header("location: index.php?opcao=listar-proprietarios");

      
    }
    
  }


$dados = isset($_SESSION['whereProprietario']) ? $_SESSION['whereProprietario'] : NULL;
//unset($_SESSION['whereProprietario']);

//print_r($_SESSION);
if(isset($_SESSION['whereProprietario'])){

  $where = sprintf(" WHERE %s LIKE '%s%%'", $dados['campo'] ,$dados['busca']);

}else {

  $where = NULL;
}



  $query = "SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.EMAIL,
proprietario.CPF,
proprietario.RG,
proprietario.DATA_REGISTRO,
proprietario.DATA_NASCIMENTO,
proprietario.CEP,
proprietario.ENDERECO,
proprietario.BAIRRO,
proprietario.CIDADE,
proprietario.ESTADO,
proprietario.PAIS,
proprietario.CODIGOEMAIL,
proprietario.CONFIRMA_CADASTRO
FROM
proprietario
WHERE proprietario.PERMISSAO_ID = 1
";
  
  $pag = new Pagination($db,5);
  $pag->setPages('proprietario','WHERE proprietario.PERMISSAO_ID = :id', array(':id'=> 1));
 
  $q = $pag->retrieveData($query);
 

  //$q = $db->query($query, NULL, NULL, DatabasePDO::FETCH_OBJECT);
  unset($_SESSION['whereProprietario']);



?>



<div class="container">
  <?
  if(isset($_SESSION['flash'])){

    $v = $_SESSION['flash'];
    echo $v;
    kill_alert();

  }
  ?>
<h1>Lista de Super-Usuarios<small> São usuarios com permissao de administrador</small></h1>
<p class="text-muted">Filtros:</p>

<div class="container form-busca">
<form class="form-inline" id="formgeral"  method="post">
<fieldset>



<!-- Search input-->
<div class="control-group">
  <label class="control-label" for="cpBuscar">Buscar</label>
  <div class="controls">
    <input id="cpBuscar" name="cpBuscar" type="text" placeholder="Busque aqui " class="input-xlarge search-query" value="<?php  echo $_SESSION['whereProprietario']['busca']; ?>">
    
  </div>
</div>

<!-- Select Basic -->
<div class="control-group">
  <label class="control-label" for="cpSelect">Busque Por</label>
  <div class="controls">
    <select id="cpSelect" name="cpSelect" class="input-xlarge">
      <option>Nome</option>
      <option>CPF</option>
      <option>RG</option>
      <option>EMAIL</option>


    </select>
  </div>
</div>

<!-- Button -->
<div class="control-group">
  <label class="control-label" for="btSend"></label>
  <div class="controls">
    <button id="singlebutton" name="btSend" class="btn btn-info">Buscar</button>
  </div>
</div>

<input type="hidden" name="checkSent" value="formSent" />
</fieldset>



</div>

<div class="bt_espaco">
    <a href="?opcao=listar-usuarios&editar=true" class="btn btn-primary btn-lg">
      <span class="glyphicon glyphicon-plus"></span> Adicionar Novo Usuario
    </a>
</div>


<div class="row grid-margin">
  
     
    <div class="col-xs-offset col-xs-1">
      <p class="text-center">ID</p>
    </div>
    
    <div class="col-xs-offset col-xs-2">
      <p class="text-center">Nome</p>
    </div>
    
    <div class="col-xs-offset col-xs-1">
      <p class="text-center">CPF</p>
    </div>
    
    <div class="col-xs-offset col-xs-1">
      <p class="text-center">RG</p>
    </div>
    
    <div class="col-xs-offset col-xs-6">
      <p class="text-center">Ações:</p>
    </div>
    
</div>  
<?php 
  //print_r($q->querydata[0]);
    //for($i = 0; $i < $q->rowsAffected();$i++){
    foreach ($q->querydata as $key => $value) {
      # code...
      $id           = $value['PROPRIETARIO_ID'];
      $nome         =  $value['NOME'];
      $CPF          =  $value['CPF'];
      $RG           =  $value['RG'];
      $email        =  $value['EMAIL'];
      $perm         =  $value['PERMISSAO_ID'];
  

      $protect = ($id == 1) ? 'readonly' : '';
      $mute = ($id == 1)  ? 'text-muted' : '';
      $texto = ($id == 1) ? 'Não  Permitido' : 'Retirar Permissão de Administrador'

  
?>
<div class="row grid-margin">


    
    <div class="col-xs-offset-xs-offset  col-xs-1">
    <p class="<?php echo $mute; ?> text-center"><?php echo $id; ?></p>
    </div>

        <div class="col-xs-offset-xs-offset  col-xs-2">
      <p class="<?php echo $mute; ?>  text-center"><?php echo $nome; ?></p>
    </div>
    
    <div class="col-xs-offset  col-xs-1">
      <p class="<?php echo $mute; ?>  text-center"><?php echo $CPF; ?></p>
    </div>
    
    <div class="col-xs-offset  col-xs-1">
      <p class="<?php echo $mute; ?>  text-center"><?php echo $RG; ?> </p>
    </div>
    
    <div class="col-xs-offset  col-xs-6">
      <div class="row-fluid center-block">


            <div class="col-xs-offset  col-xs-2">
              
              <?
                $btperm = ($perm == 1) ? 'btn-danger' : 'btn-primary';
              ?>

              <? if($id != 1){ ?>
              <a  href="?opcao=listar-usuarios&setadm=<?php echo $id; ?>" class="<?php echo $protect; ?> btn <?php echo $btperm;  ?>"
                  <span class="glyphicon glyphicon-pencil"></span> Retirar Permissão de Administrador
              </a>
            

            <? }else {

                printf('<p class="text center text-muted">Não Permitido</p>');

            }  ?>
            </div>
            
            <div class="col-xs-offset  col-xs-2">
              &nbsp;
            </div>
            

            <div class="col-xs-offset  col-xs-2">
              &nbsp;
            </div>
            
            <div class="col-xs-offset  col-xs-6">
              &nbsp;
            </div>
        </div>
    </div>

</div>

<? } ?>
</form> 

<?php $pag->Pages(); ?>

</div>

    
</div>



</div>

<script>
  



function apagar(id){
  $(function(){

    $.ajax({

      url: '<?php echo urlbase().'/interno/franquias/deleteFranquia.php'; ?>',
      type: "POST",
      dataType: "json",
      data: {"userid": id},

      success: function(data){
         //var d = $.parseJSON(data);
          console.log(data);
          //$("#apagar-modal").modal("show");

      },
      error: function(xchr, obj, error){

          console.error(error + ' : ' + xchr);

      }

    });

  });


}



$(function(){


    $("#buscaProprietario").on('click',function(){


    $.ajax({

      url: '<?php echo  urlbase().'/interno/franquias/buscaProprietario.php'; ?>',
      dataType: "json",
      type: 'POST',
      data: $("#cpProprietario").serialize(),

      success: function(data){


          $("input[name=propnome]").val(data[0][0]);
          $("input[name=propsobrenome]").val(data[0][1]);
          $("input[name=proprg]").val(data[0][2]);
          $("input[name=propid]").val(data[0][3]);
          //console.log(data);


      }

    });


  });


  $(function (){
    $("[data-toggle=popover]").popover({

      html: true,
      animation: true

    });    
  });   

      $("#formgeral button").each(function(){

        var campo = $(this);



    });

});

    
    
    
  
</script>