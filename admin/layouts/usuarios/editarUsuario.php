<div class="container edituser-container">


  <?php
    if(isset($_SESSION['flash'])){

        echo flash();
        kill_alert();

    }

  ?>


  <?php 
  
  $where = "";
  
  if($_SERVER['REQUEST_METHOD'] == "POST"){
    
    //print_r($_POST);
    $campo = "";
    if( !empty($_POST['cpBuscar']) && !empty($_POST['cpSelect']) ){
      switch($_POST['cpSelect']){
        
        case 'Nome':
          $campo = 'NOME';
          break;
        
        case 'CPF':
          $campo = "CPF";
          break;
        case 'Documento RG':
          $campo = "RG";
          break;

        case 'E-Mail':
          $campo = "EMAIL";
          break;

        default:
          $campo = 'NOME';
          break;
      }
      
      $busca = $_POST['cpBuscar'];
      $_SESSION['whereProprietario'] = array('campo' => $campo, 'busca' => $busca);   //sprintf(" WHERE %s LIKE '%s%%'", $campo, $busca);
      header("location: index.php?opcao=listar-proprietarios");

      
    }
    
  }


$dados = isset($_SESSION['whereProprietario']) ? $_SESSION['whereProprietario'] : NULL;
//unset($_SESSION['whereProprietario']);

//print_r($_SESSION);
if(isset($_SESSION['whereProprietario'])){

  $where = sprintf(" WHERE %s LIKE '%s%%'", $dados['campo'] ,$dados['busca']);

}else {

  $where = NULL;
}



  $query = "SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.EMAIL,
proprietario.CPF,
proprietario.RG,
proprietario.DATA_REGISTRO,
proprietario.DATA_NASCIMENTO,
proprietario.CEP,
proprietario.ENDERECO,
proprietario.BAIRRO,
proprietario.CIDADE,
proprietario.ESTADO,
proprietario.PAIS,
proprietario.CODIGOEMAIL,
proprietario.CONFIRMA_CADASTRO
FROM
proprietario
WHERE proprietario.PERMISSAO_ID != 1";
  
  $pag = new Pagination($db,5);
  $pag->setPages('proprietario','WHERE proprietario.PERMISSAO_ID != :id', array(':id'=> 1));
 
  $q = $pag->retrieveData($query);
 

  //$q = $db->query($query, NULL, NULL, DatabasePDO::FETCH_OBJECT);
  unset($_SESSION['whereProprietario']);



?>



<div class="container">
  <?
  if(isset($_SESSION['flash'])){

    $v = $_SESSION['flash'];
    echo $v;
    kill_alert();

  }


  include("do_editUser.php");

  ?>
<h1>Listar Proprietarios</h1>
<p class="text-muted">Filtros:</p>

<div class="container form-busca">
<form class="form-inline" id="formgeral"  method="post">
<fieldset>



<!-- Search input-->
<div class="control-group">
  <label class="control-label" for="cpBuscar">Buscar</label>
  <div class="controls">
    <input id="cpBuscar" name="cpBuscar" type="text" placeholder="Busque aqui " class="input-xlarge search-query" value="<?php  echo $_SESSION['whereProprietario']['busca']; ?>">
    
  </div>
</div>

<!-- Select Basic -->
<div class="control-group">
  <label class="control-label" for="cpSelect">Busque Por</label>
  <div class="controls">
    <select id="cpSelect" name="cpSelect" class="input-xlarge">
      <option>Nome</option>
      <option>CPF</option>
      <option>RG</option>
      <option>EMAIL</option>


    </select>
  </div>
</div>

<!-- Button -->
<div class="control-group">
  <label class="control-label" for="btSend"></label>
  <div class="controls">
    <button id="singlebutton" name="btSend" class="btn btn-info">Buscar</button>
  </div>
</div>

<input type="hidden" name="checkSent" value="formSent" />
</fieldset>



</div>



<div class="row grid-margin">
  
     
    <div class="col-xs-offset col-xs-1">
      <p class="text-center">ID</p>
    </div>
    
    <div class="col-xs-offset col-xs-2">
      <p class="text-center">Nome</p>
    </div>
    
    <div class="col-xs-offset col-xs-1">
      <p class="text-center">CPF</p>
    </div>
    
    <div class="col-xs-offset col-xs-1">
      <p class="text-center">RG</p>
    </div>
    
    <div class="col-xs-offset col-xs-6">
      <p class="text-center">&nbsp;</p>
    </div>
    
</div>  
<?php 
  //print_r($q->querydata[0]);
    //for($i = 0; $i < $q->rowsAffected();$i++){
    foreach ($q->querydata as $key => $value) {
      # code...
      $id           = $value['PROPRIETARIO_ID'];
      $nome         =  $value['NOME'];
      $CPF          =  $value['CPF'];
      $RG           =  $value['RG'];
      $email        =  $value['EMAIL'];
  


  
?>
<div class="row grid-margin">


    
    <div class="col-xs-offset-xs-offset  col-xs-1">
    <p class="text-muted text-center"><?php echo $id; ?></p>
    </div>

        <div class="col-xs-offset-xs-offset  col-xs-2">
      <p class="text-muted text-center"><?php echo $nome; ?></p>
    </div>
    
    <div class="col-xs-offset  col-xs-1">
      <p class="text-muted text-center"><?php echo $CPF; ?></p>
    </div>
    
    <div class="col-xs-offset  col-xs-1">
      <p class="text-muted"><?php echo $RG; ?> </p>
    </div>
    
    <div class="col-xs-offset  col-xs-6">
      <div class="row-fluid center-block">


            <div class="col-xs-offset  col-xs-2">
              
              <a href="?opcao=listar-usuarios&do_adm=<?php echo $id; ?>" class="btn btn-primary">
                  <span class="glyphicon glyphicon-pencil"></span> Tornar Administrador
              </a>
            </div>
            
            
            <div class="col-xs-offset  col-xs-2">
              &nbsp;
            </div>
            

            <div class="col-xs-offset  col-xs-2">
			&nbsp;
            </div>
            
            <div class="col-xs-offset  col-xs-6">
              &nbsp;
            </div>
        </div>
    </div>

</div>

<? } ?>
</form> 

<?php $pag->Pages(); ?>

</div>

    
</div>



</div>

	<div class="container">

		    <h2>Area do Administrador</h2>
    <form class="form-horizontal" id="formEditFranquia"  method="post" enctype="application/x-www-form-urlencoded" role="form">
      <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Trocar Proprietario: </label>
  <button type="button" id="buscaProprietario" class=" pull-left btn  btn-primary btn-sm">
    <span class="glyphicon glyphicon-search"></span> Buscar Proprietario</button>   
    <div class="col-sm-8">
      <input type="text" class="form-control campodata" name="proprietario"   id="cpProprietario" placeholder="RG do Proprietario">
    </div>
  </div>
      
       <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Id:</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control campodata" name="propid" value="" id="cpNome"  >
    </div>
  </div>

      <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Nome Proprietario:</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control campotexto" name="propnome" value=""  id="cpNome"  >
    </div>
  </div>

      <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Sobrenome:</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control campotexto" name="propsobrenome" value=""  id="cpNome"  >
    </div>
  </div>

        <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">RG</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control campotexto" name="proprg" value=""  id="cpNome" >
    </div>
  </div>

  <input type="hidden" name="changeProp" value="1">

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btAtiva" name="SendForm" class="btn btn-success btn-sm">Tornar Administrador</button>
    </div>
  </div>
  


    

</form>

</div>

</div>


<script type="text/javascript">
	
	$(function(){

		  $("#buscaProprietario").on('click',function(){


    $.ajax({

      url: '<?php echo  urlbase().'/interno/franquias/buscaProprietario.php'; ?>',
      dataType: "json",
      type: 'POST',
      data: $("#cpProprietario").serialize(),

      success: function(data){


          $("input[name=propnome]").val(data[0][0]);
          $("input[name=propsobrenome]").val(data[0][1]);
          $("input[name=proprg]").val(data[0][2]);
          $("input[name=propid]").val(data[0][3]);
          //console.log(data);


      }

    });


  });

	});

</script>