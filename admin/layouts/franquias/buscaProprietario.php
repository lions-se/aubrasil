<?php
//header('Content-Type: application/text');
include("../../adminconf.inc.php");
include(ROOT_PATH.'/'.'config.php');

if ( $_SERVER['REQUEST_METHOD'] != "POST"){

	exit(0);
}


$where = '';

if(is_numeric(substr($_POST['proprietario'], 0,1))){

	$where = "WHERE proprietario.RG LIKE '{$_POST['proprietario']}%' AND proprietario.PERMISSAO_ID != 1";

}else {

	$where = "WHERE proprietario.NOME LIKE '{$_POST['proprietario']}%' AND proprietario.PERMISSAO_ID != 1";

}


if(!empty($_POST['proprietario'])){
	$query  = "
		SELECT
			permissao.NOME AS PERM_NOME,
			permissao.ROLE,
			proprietario.NOME,
			proprietario.SOBRENOME,
			proprietario.PERMISSAO_ID,
			proprietario.RG,
			proprietario.PROPRIETARIO_ID
		FROM
			permissao
			INNER JOIN proprietario ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID ".$where;


$q = $db->query($query, NULL,NULL, DatabasePDO::FETCH_OBJECT);
	
$nomes = array();
foreach ($q->querydata as $key => $value) {
	$tmp[0] = $value['NOME'];
	$tmp[1] = $value['SOBRENOME'];
	$tmp[2] =  $value['RG'];
	$tmp[3] = $value['PROPRIETARIO_ID'];

	array_push($nomes, $tmp);
}	

print json_encode($nomes);

}

?>