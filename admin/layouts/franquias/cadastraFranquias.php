<?



?>


<div class="container edituser-container">
	<div class="container">

		<h1>Cadastrar Franquia</h1>

        <div class="row">
      <form class="form-horizontal"  id="formEditFranquia"  method="post" enctype="application/x-www-form-urlencoded" role="form">
     
      <input type="hidden" name="userid" value="<?php echo $franq_id; ?>" > 
     <fieldset>
  <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Nome Franquia</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="nome" value="<?php echo formValue($franqnome); ?>"  id="cpNome" placeholder="Nome">
    </div>
  </div>
  <div class="form-group">
    <label for="cpSlogan" class="col-sm-2 control-label">Slogan</label>
    <div class="col-sm-10">
      <textarea id="cpSlogan" name="slogan" rows="4" cols="1" class="form-control"><?php echo formValue($slogan); ?></textarea>
    </div>
  </div>
   <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Telefone</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="telefone" value="<?php echo formValue($telefone); ?>"  id="cpNome" placeholder="Telefone">
    </div>
  </div>
     <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">E-Mail</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="email" value="<?php echo formValue($franqnome); ?>"  id="cpNome" placeholder="Telefone">
    </div>
  </div>

       <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Endereço SMTP</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="endereco_smtp" value="<?php echo formValue($franqnome); ?>"  id="cpNome" placeholder="Telefone">
    </div>
  </div>
       <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Login SMTP</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="login_smtp" value="<?php echo formValue($franqnome); ?>"  id="cpNome" placeholder="Telefone">
    </div>
  </div>

     <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Senha SMTP</label>
    <div class="col-sm-10">
      <input type="senha_smtp" class="form-control campotexto" name="senha_smtp" value="<?php echo formValue($franqnome); ?>"  id="cpNome" placeholder="Telefone">
    </div>
  </div>
  
      <? 
      Flight::render('estatico/buscar_cep.php');
      Flight::render('estatico/buscar_proprietario') 
      ?>

   
  
   
    
 
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div id="avisoSucesso" class="alert alert-success alert-dismissible aviso">Enviado Com Sucesso

                <button type="button" class="close" data-dismiss="alert">
                  <i class="glyphicon glyphicon-remove"></i>
                </button>
      </div>
			<div id="avisoFalha" class="alert alert-danger alert-dismissible aviso">Falha no Envio, Altere algum campo e envie novamente


      </div>
		</div>
	</div>

    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btSendForm" name="SendForm" class="btn btn-default">Atualizar</button>
    </div>
  </div>

    </fieldset>

     
  



</form>
</div>
        
	</div>
</div>

<script>
	
$(function(){


         $('input[name=telefone]').mask('(00) 0000-0000',
   {onKeyPress: function(phone, e, currentField, options){
    var new_sp_phone = phone.match(/^(\(11\) 9(5[0-9]|6[0-9]|7[01234569]|8[0-9]|9[0-9])[0-9]{1})/g);
    new_sp_phone ? $(currentField).mask('(00) 00000-0000', options) : $(currentField).mask('(00) 0000-0000', options)
  }
});

      $("input[name=cep]").mask("00000-000");


      $("input[name=proprietario]").mask("AA.000.000.A");
 



 

	

});
</script>
