<?
/*
if( empty($_GET['deletar'])){
	header("HTTP/1.1 403 Unauthorized" );
	exit(0);	
}


if($_SERVER['REQUEST_METHOD'] == "POST"){
	if(isset($_GET['do_delete'])){
		$q =$db->delete('franquia', 'FRANQUIA_ID ='.$_GET['do_delete']);
		header("location: ?opcao=listar-franquias");
	
	}else {

	header("HTTP/1.1 403 Unauthorized" );
	exit(0);	
	}
}


$q = $db->query("SELECT
proprietario.PROPRIETARIO_ID,
proprietario.FRANQUIA_ID,
franquia.FRANQUIA_ID,
franquia.PROPRIETARIO_ID,
franquia.NOMEFRANQUIA,
franquia.SLOGAN,
franquia.TELFRANQUIA,
permissao.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME
FROM
franquia
INNER JOIN proprietario ON proprietario.FRANQUIA_ID = franquia.FRANQUIA_ID AND franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
INNER JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID

WHERE proprietario.FRANQUIA_ID = ".$_GET['deletar']);

*/

$franq_id = $q->querydata[0]['FRANQUIA_ID'];
$adm_id = $q->querydata[0]['PERMISSAO_ID'];
$nomefranquia = $q->querydata[0]['NOMEFRANQUIA'];
$slogan = $q->querydata[0]['SLOGAN'];
$tel = $q->querydata[0]['TELFRANQUIA'];
$nome = $q->querydata[0]['NOME'];
$sobrenome = $q->querydata[0]['SOBRENOME'];


?>


<div class="container edituser-container">
	<div class="container">

		<h1>Tem Certeza que Deseja Deletar: aaa</h1>
        
        <div class="row">
      <form class="form-horizontal" id="formEditFranquia" action="<?php echo rootURL().'/franquias/do_delete/'.$franq_id; ?>"  method="post" enctype="application/x-www-form-urlencoded" role="form">
     
      <input type="hidden" name="userid" value="<?php echo $franq_id; ?>" > 
     <fieldset>
  <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Nome Franquia</label>
    <div class="col-sm-10">
      <input   disabled type="text" class="form-control campotexto" name="nome" value="<?php echo formValue($nomefranquia); ?>"  id="cpNome" placeholder="Nome">
    </div>
  </div>
  <div class="form-group">
    <label for="cpSlogan" class="col-sm-2 control-label">Slogan</label>
    <div class="col-sm-10">
      <textarea  disabled id="cpSlogan" name="slogan" rows="4" cols="1" class="form-control"><?php echo formValue($slogan); ?></textarea>
    </div>
  </div>


    <div class="form-group">
    <label for="cpSlogan" class="col-sm-2 control-label">Pertence ao Proprietario:</label>
    <div class="col-sm-10">
           <input   disabled type="text" class="form-control campotexto" name="nome" value="<?php echo formValue(sprintf("%s %s", $nome, $sobrenome)); ?>"  id="cpNome" placeholder="Nome">


    </div>
  </div>



   
       <div class="form-group">
    <label for="cpTel" class="col-sm-2 control-label">Telefone:</label>
    <div class="col-sm-10">
      <input disabled type="text" class="form-control  campotexto" name="telefone" value="<?php echo formValue($tel); ?>" id="cpTel" placeholder="Telefone">
    </div>
  </div>
  
    
 
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div id="avisoSucesso" class="alert alert-success alert-dismissible aviso">Enviado Com Sucesso

                <button type="button" class="close" data-dismiss="alert">
                  <i class="glyphicon glyphicon-remove"></i>
                </button>
      </div>
			<div id="avisoFalha" class="alert alert-danger alert-dismissible aviso">Falha no Envio, Altere algum campo e envie novamente


      </div>
		</div>
	</div>

    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">

    	<div class="row">	
    		<div class="col col-sm-2">
      					<button type="submit" id="btDelete" name="SendForm" class="btn btn-danger">Apagar</button>
    		</div>
    		<div class="col col-sm-2">
      					<button type="button" id="Cancela" name="SendForm" class="btn btn-success">Voltar</button>
    			
    		</div>

    	</div>
    </div>
  </div>

  

    </fieldset>
</form>
    
</div>
        
	</div>
</div>
