<?php



/*
$q = $db->query("

 SELECT
produto.PRODUTO_ID,
produto.FRANQUIA_ID,
produto.NOME as PRODUTO_NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PRECO,
produto.DESCONTOPRODUTO,
produto.DESCONTOCLIENTE,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
rel_prod_subcategoria.PRODUTO_ID,
rel_prod_subcategoria.SUBCATEGORIA_ID,
subcategoria.SUBCATEGORIA_ID,
subcategoria.CATEGORIA_ID,
subcategoria.NOME as SUBCATEGORIA_NOME,
categoria.CATEGORIA_ID,
categoria.NOME as CATEGORIA_NOME
FROM
produto
INNER JOIN rel_prod_subcategoria ON rel_prod_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
INNER JOIN subcategoria ON rel_prod_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
INNER JOIN categoria ON subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
WHERE produto.PRODUTO_ID = ".$_GET['editar']);
*/

  $prod_id = $q->querydata[0]['PRODUTO_ID'];
  $prod_nome = $q->querydata[0]['PRODUTO_NOME'];
  $descricao = $q->querydata[0]['DESCRICAO'];

  $subcat_id = $q->querydata[0]['SUBCATEGORIA_ID'];
  $cat_id = $q->querydata[0]['CATEGORIA_ID'];
  
  $subcat_nome = $q->querydata[0]['SUBCATEGORIA_NOME'];
  $cat_nome = $q->querydata[0]['CATEGORIA_NOME'];


  $preco =  number_format($q->querydata[0]['PRECO'],2,'.','');
  $descprod = number_format($q->querydata[0]['DESCONTOPRODUTO'],2,'.','');
  $desccliente = number_format($q->querydata[0]['DESCONTOCLIENTE'],2,'.','');
  $descfranq = number_format($q->querydata[0]['DESCONTOFRANQUEADO'],2,'.','');

  $largura = number_format($q->querydata[0]['LARGURA'],2,'.','');
  $altura = number_format($q->querydata[0]['ALTURA'],2,'.','');
  $comprimento = number_format($q->querydata[0]['COMPRIMENTO'],2,'.','');
  $peso = number_format($q->querydata[0]['PESO'],2,'.','');
 
 

 ?>

<div class="container edituser-container">
	<div class="container">

		<h1>Editar<small>  Editar Produto</small></h1>

        <div class="row">
     		<form class="form-horizontal" action="<?php echo rootURL().'/produtos/do_edita';  ?>" id="formEditFranquia"  method="post" enctype="multipart/form-data" role="form">
     
      			<input type="hidden" name="userid" value="<?php echo $prod_id; ?>" > 
     			<fieldset>
  				
  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Nome Do Produto</label>
    				<div class="col-sm-10">
      					<input type="text" class="form-control campotexto" name="nome" value="<?php echo formValue($prod_nome); ?>"  id="cpNome" placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Descrição</label>
    				<div class="col-sm-10">
      					<textarea name="descricao" class="form-control campotexto" row="4" col="3">  <?php echo formValue($descricao); ?>   </textarea>
    				</div>
  				</div>


  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 1:</label>
    				<div class="col-sm-10">
      					<input type="file"  data-classButton="btn btn-primary"  class="filestyle" name="imagem1" value="<?php echo formValue($franqnome); ?>" placeholder="Nome">
    				</div>
  				</div>


  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 2:</label>
    				<div class="col-sm-10">
      					<input type="file" class="form-control campotexto" name="imagem2" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
    				</div>
  				</div>


  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 3:</label>
    				<div class="col-sm-10">
      					<input type="file" class="form-control campotexto" name="imagem3" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 4:</label>
    				<div class="col-sm-10">
      					<input type="file" class="form-control campotexto" name="imagem4" value="<?php echo formValue($franqnome); ?>"  placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 5:</label>
    				<div class="col-sm-10">	<input type="file" class="form-control campotexto" name="imagem5" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 6</label>
    				<div class="col-sm-10">
      					<input type="file" class="form-control campotexto" name="imagem6" value="<?php echo formValue($franqnome); ?>"  placeholder="Nome">
    				</div>
  				</div>


  				  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Preço:</label>
    				<div class="col-sm-10">
      					<input type="text" data-type="preco" class="form-control input-lg campopreco" name="preco" id="cpPreco" value="<?php echo formValue($preco); ?>"  placeholder="Nome">
    				</div>
  				</div>



  				  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Desconto do Produto:</label>
    				<div class="col-sm-10">
      					<input type="text" data-type="preco"   class="form-control input-lg campopreco" name="descontoProduto" value="<?php echo formValue($descprod); ?>"  id="cpDescProduto" placeholder="Nome">
    				</div>
  				</div>


  				  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Desconto para Clientes:</label>
    				<div class="col-sm-10">
      					<input type="text"  data-type="preco"  class="form-control input-lg campopreco" name="descontoCliente" value="<?php echo formValue($desccliente); ?>"  id="cpDescCliente" placeholder="Nome">
    				</div>
  				</div>



  							<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Desconto para Franqueados:</label>
    				<div class="col-sm-10">
      					<input type="text" data-type="preco" class="form-control input-lg campopreco" name="descontoFranqueado" value="<?php echo formValue($descfranq); ?>"  id="cpDescFranqueado" placeholder="Nome">
    				</div>
  				</div>

  <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Altura:</label>
            <div class="col-sm-10">
                <input type="text" data-type="medida" class="form-control input-lg campopreco" name="altura" id="cpPreco" value="<?php echo formValue($altura); ?>"  placeholder="Nome">
            </div>
          </div>


            <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Largura:</label>
            <div class="col-sm-10">
                <input type="text" data-type="medida" class="form-control input-lg campopreco" name="largura" id="cpPreco" value="<?php echo formValue($largura); ?>"  placeholder="Nome">
            </div>
          </div>


            <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Comprimento:</label>
            <div class="col-sm-10">
                <input type="text" data-type="medida" class="form-control input-lg campopreco" name="comprimento" id="cpPreco" value="<?php echo formValue($comprimento); ?>"  placeholder="Nome">
            </div>
          </div>


            <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Peso</label>
            <div class="col-sm-10">
                <input type="text" data-type="medida" class="form-control input-lg campopreco" name="peso" id="cpPreco" value="<?php echo formValue($peso); ?>"  placeholder="Nome">
            </div>
          </div>




  										<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Categoria:</label>
    				<div class="col-sm-10">
      					<select id="cpCategoria" class="form-control campodata" name="categoria">
                <option value="-1">Selecione  Sua Categoria</option>
      					<?
      						  
                    echo categoria($q->querydata[0]['CATEGORIA_ID']);

      					?>
      				
      					</select>
    				</div>
  				</div>

  					<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Subcategoria:</label>
    				<div class="col-sm-10">
      					<select id="cpSubcategoria" class="form-control campodata" name="subcategoria">
                  
                  
                  <?
                      //echo Utils::buscaSubcategoriasArray($cat_id);
                      echo subcategoria($produto_id);
                  ?>

      					</select>
    				</div>
  				</div>


				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" id="btAtiva" name="SendForm" class="btn btn-primary btn-lg">Editar <? echo $prod_nome; ?> </button>
					</div>
				</div>	






	</div>	
</div>

<script type="text/javascript">
	
    function convert(num){

      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    }



		$(function(){

   var opt = {

          reverse : true,

         onKeyPress: function(valor, event, current, options) {
              
            var val = current.val();
            
            current.val(val + ' cm');
            //current.append(" cm");
            //alert('CEP Completed!:' + cep);
          }

       }

        $("input[name=altura]").mask("###.##0.00", opt );
        $("input[name=largura]").mask("###.##0.00", opt );
        $("input[name=comprimento]").mask("###.##0.00", opt );
        $("input[name=peso]").mask("###.###", {reverse: true});

        $("input[data-type=medida]").popover({

        title: 'Aviso',
        content: "Use ponto ao  invés de vírgulas para casas decimais, Pois se trata de uma medida",
        delay: 300,
        trigger: 'focus'

    }).blur(function(){

      $("input[data-type=preco]").popover('hide');

    });


        $("input[data-type=preco]").popover({

        title: 'Aviso',
        content: "Use ponto ao  invés de vírgulas para casas decimais, não colocar R$ ou algum outro tipo de caracter.  Use somente Numeros e Pontos",
        delay: 300,
        trigger: 'focus'

    }).blur(function(){

      $("input[data-type=preco]").popover('hide');

    });





       


           


      $("#cpDescProduto").on('blur',function(){


        var preco = $("#cpPreco").val()

        $("#cpMostraDescProd").empty();


        var descProd = convert((preco - $(this).val()));

        if( descProd  > 0  ){
          

         $('<p id="cpMostraDescProd" class="text-muted">Desconto de Produto: R$'+ descProd +'</p>').insertAfter($(this));
          //console.log(descProd);
        }else {

          console.log(0);
        }

      })




      $("#cpDescCliente").on('blur',function(){




        var preco = $("#cpPreco").val()


        $("#cpMostraDescCliente").empty()

        var descProd = (preco - $(this).val());

        if( descProd  > 0  ){
             $('<p id="cpMostraDescCliente" class="text-muted">Desconto para Clientes: R$'+ descProd +'</p>').insertAfter($(this));
        }else {

          console.log(0);
        }

      })




			$("#cpCategoria").on('change',function(){


        $("#cpSubcategoria").empty();




				$.ajax({
					url: "<?php echo rootURL().'/produtos/busca_subcategoria/'?>" +  $("#cpCategoria").val() ,
					dataType: 'json',
					type: "POST",
					

					success: function(data){


            var htmlcontent;


            for(var i = 0; i < data.length; i++){
              if(data[i] == null){
                htmlcontent += $.trim('<option value="-1" >Sem Priduto</option>');
              }else {
                htmlcontent +=  $.trim('<option value="'+ data[i].id +'" >'+ data[i].nome + '</option>');
              }
            }

            $("#cpSubcategoria").html(htmlcontent);

            /*
            var index;
            var htmlcontent = ' <option value="<?php echo $subcat_id ?>" selected><?php echo $subcat_nome ?></option>';
          
            
            for(index = 0; index < data.dados.length; index++){

              if(data.dados[index].NOME != '' && data.dados[index].NOME != undefined){
                //$("#cpSubcategoria").append();
                htmlcontent +=  $.trim('<option value="'+ data.dados[index].SUBCATEGORIA_ID +'" >'+ data.dados[index].NOME + '</option>');
              }
            }



            $("#cpSubcategoria").html(htmlcontent);
           
						*/
          

          
          console.log(data);
           
					},
					error: function(xchr,obj,error){             

            console.error(xchr + ' ' + error);

					}

				});


			});

		});


</script>