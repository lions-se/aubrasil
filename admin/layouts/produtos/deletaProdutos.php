<?php
/*
  require("do_apaga.php");


$q = $db->query("

 SELECT
produto.PRODUTO_ID,
produto.FRANQUIA_ID,
produto.NOME as PRODUTO_NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PRECO,
produto.DESCONTOPRODUTO,
produto.DESCONTOCLIENTE,
produto.DESCONTOFRANQUEADO,
rel_prod_subcategoria.PRODUTO_ID,
rel_prod_subcategoria.SUBCATEGORIA_ID,
subcategoria.SUBCATEGORIA_ID,
subcategoria.CATEGORIA_ID,
subcategoria.NOME as SUBCATEGORIA_NOME,
categoria.CATEGORIA_ID,
categoria.NOME as CATEGORIA_NOME
FROM
produto
INNER JOIN rel_prod_subcategoria ON rel_prod_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
INNER JOIN subcategoria ON rel_prod_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
INNER JOIN categoria ON subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
WHERE produto.PRODUTO_ID = ".$_GET['deletar']);
*/

  $prod_id = $q->querydata[0]['PRODUTO_ID'];
  $prod_nome = $q->querydata[0]['PRODUTO_NOME'];
  $descricao = $q->querydata[0]['DESCRICAO'];

  $subcat_id = $q->querydata[0]['SUBCATEGORIA_ID'];
  $cat_id = $q->querydata[0]['CATEGORIA_ID'];
  
  $subcat_nome = $q->querydata[0]['SUBCATEGORIA_NOME'];
  $cat_nome = $q->querydata[0]['CATEGORIA_NOME'];


  $preco =  number_format($q->querydata[0]['PRECO'],2,'.','');
  $descprod = number_format($q->querydata[0]['DESCONTOPRODUTO'],2,'.','');
  $desccliente = number_format($q->querydata[0]['DESCONTOCLIENTE'],2,'.','');
  $descfranq = number_format($q->querydata[0]['DESCONTOFRANQUEADO'],2,'.','');



 ?>

<div class="container edituser-container">
	<div class="container">

		<h1>Apagar<small> Certeza que Deseja Apagar o Produto: <?php echo $prod_nome; ?>?</small></h1>


        <div class="row">
     		<form class="form-horizontal" id="formEditFranquia"    method="post" enctype="multipart/form-data" role="form">
     
      			<input type="hidden" disabled name="userid" value="<?php echo $prod_id; ?>" > 
     			<fieldset>
  				
  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Nome Do Produto</label>
    				<div class="col-sm-10">
      					<input type="text" disabled   class="form-control campotexto" name="nome" value="<?php echo formValue($prod_nome); ?>"  id="cpNome" placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Descrição</label>
    				<div class="col-sm-10">
      					<textarea  disabled name="descricao" class="form-control campotexto" row="4" col="3">  <?php echo formValue($descricao); ?>   </textarea>
    				</div>
  				</div>


  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 1:</label>
    				<div class="col-sm-10">
      					<input type="file" disabled  data-classButton="btn btn-primary"  class="filestyle" name="imagem1" value="<?php echo formValue($franqnome); ?>" placeholder="Nome">
    				</div>
  				</div>


  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 2:</label>
    				<div class="col-sm-10">
      					<input type="file" disabled class="form-control campotexto" name="imagem2" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
    				</div>
  				</div>


  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 3:</label>
    				<div class="col-sm-10">
      					<input type="file" disabled class="form-control campotexto" name="imagem3" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 4:</label>
    				<div class="col-sm-10">
      					<input type="file" disabled class="form-control campotexto" name="imagem4" value="<?php echo formValue($franqnome); ?>"  placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 5:</label>
    				<div class="col-sm-10">	<input type="file" disabled class="form-control campotexto" name="imagem5" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 6</label>
    				<div class="col-sm-10">
      					<input type="file" disabled class="form-control campotexto" name="imagem6" value="<?php echo formValue($franqnome); ?>"  placeholder="Nome">
    				</div>
  				</div>


  				  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Preço:</label>
    				<div class="col-sm-10">
      					<input type="text" disabled disabled data-type="preco" class="form-control input-lg campopreco" name="preco" id="cpPreco" value="<?php echo formValue($preco); ?>"  placeholder="Nome">
    				</div>
  				</div>



  				  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Desconto do Produto:</label>
    				<div class="col-sm-10">
      					<input type="text" disabled data-type="preco"   class="form-control input-lg campopreco" name="descontoProduto" value="<?php echo formValue($descprod); ?>"  id="cpDescProduto" placeholder="Nome">
    				</div>
  				</div>


  				  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Desconto para Clientes:</label>
    				<div class="col-sm-10">
      					<input type="text" disabled  data-type="preco"  class="form-control input-lg campopreco" name="descontoCliente" value="<?php echo formValue($desccliente); ?>"  id="cpDescCliente" placeholder="Nome">
    				</div>
  				</div>



  							<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Desconto para Franqueados:</label>
    				<div class="col-sm-10">
      					<input type="text" disabled data-type="preco" class="form-control input-lg campopreco" name="descontoFranqueado" value="<?php echo formValue($descfranq); ?>"  id="cpDescFranqueado" placeholder="Nome">
    				</div>
  				</div>


  									
    				</div>
  				</div>


				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" id="btAtiva" name="SendForm" class="btn btn-danger btn-lg">Apagar</button>
					</div>
				</div>	






	</div>	
</div>

<script type="text/javascript">
	
    function convert(num){

      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    }

		$("[data-type=preco]").on('focus', function(e){

			var id = $(this);

			

			$(id).popover({

				title: 'Aviso',
				content: "Use ponto ao  invés de vírgulas para casas decimais, não colocar R$ ou algum outro tipo de caracter.  Use somente Numeros e Pontos",
				delay: 300,
				trigger: 'focus'
			});

		}).blur(function(){
			var id = $(this);

			$(id).popover('hide');

		});



		$(function(){


              $.ajax({
          url: '<?php echo  urlbase().'/interno/produtos/buscaCategorias.php'; ?>',
          dataType: 'json',
          type: "POST",
          data: $("#cpCategoria").serialize(),

          success: function(data){

            
            var index;
            var htmlcontent = ' <option value="<?php echo $subcat_id ?>" selected>Selecionado: <?php echo $subcat_nome ?></option>';
          
            
            for(index = 0; index < data.dados.length; index++){

              if(data.dados[index].NOME != '' && data.dados[index].NOME != undefined){
                //$("#cpSubcategoria").append();
                htmlcontent +=  $.trim('<option value="'+ data.dados[index].SUBCATEGORIA_ID +'" >'+ data.dados[index].NOME + '</option>');
              }
            }



            $("#cpSubcategoria").html(htmlcontent);
           // console.log(htmlcontent);
            
           
          },
          error: function(xchr,obj,error){             

            console.error(xchr + ' ' + error);

          }

        });



      $("#cpDescProduto").on('blur',function(){


        var preco = $("#cpPreco").val()

        $("#cpMostraDescProd").empty();


        var descProd = convert((preco - $(this).val()));

        if( descProd  > 0  ){
          

         $('<p id="cpMostraDescProd" class="text-muted">Desconto de Produto: R$'+ descProd +'</p>').insertAfter($(this));
          //console.log(descProd);
        }else {

          console.log(0);
        }

      })




      $("#cpDescCliente").on('blur',function(){




        var preco = $("#cpPreco").val()


        $("#cpMostraDescCliente").empty()

        var descProd = (preco - $(this).val());

        if( descProd  > 0  ){
             $('<p id="cpMostraDescCliente" class="text-muted">Desconto para Clientes: R$'+ descProd +'</p>').insertAfter($(this));
        }else {

          console.log(0);
        }

      })




			$("#cpCategoria").on('change',function(){


        $("#cpSubcategoria").empty();




				$.ajax({
					url: '<?php echo  urlbase().'/interno/produtos/buscaCategorias.php'; ?>',
					dataType: 'json',
					type: "POST",
					data: $("#cpCategoria").serialize(),

					success: function(data){

            
            var index;
            var htmlcontent = ' <option value="<?php echo $subcat_id ?>" selected><?php echo $subcat_nome ?></option>';
          
            
            for(index = 0; index < data.dados.length; index++){

              if(data.dados[index].NOME != '' && data.dados[index].NOME != undefined){
                //$("#cpSubcategoria").append();
                htmlcontent +=  $.trim('<option value="'+ data.dados[index].SUBCATEGORIA_ID +'" >'+ data.dados[index].NOME + '</option>');
              }
            }



            $("#cpSubcategoria").html(htmlcontent);
           // console.log(htmlcontent);
						
           
					},
					error: function(xchr,obj,error){             

            console.error(xchr + ' ' + error);

					}

				});

			});

		});


</script>