<?php

 // require("do_cadastro.php");
 ?>

<div class="container edituser-container">
	<div class="container">

		<h1>Cadastrar Produto<small>  Adicionar Produtos</small></h1>


        <div class="row">
     		<form class="form-horizontal" id="formEditFranquia" action="<?php echo rootURL().'/produtos/do_cadastra' ?>"  method="post" enctype="multipart/form-data" role="form">
     
      			<input type="hidden" name="userid" value="<?php echo $franq_id; ?>" > 
     			<fieldset>
  				
  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Nome Do Produto</label>
    				<div class="col-sm-10">
      					<input type="text" class="form-control campotexto" name="nome" value="<?php echo formValue($franqnome); ?>"  id="cpNome" placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Descrição</label>
    				<div class="col-sm-10">
      					<textarea name="descricao" class="form-control campotexto" row="4" col="3"></textarea>
    				</div>
  				</div>


  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 1:</label>
    				<div class="col-sm-10">
      					<input type="file"  data-classButton="btn btn-primary"  class="filestyle" name="imagem1" value="<?php echo formValue($franqnome); ?>" placeholder="Nome">
    				</div>
  				</div>


  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 2:</label>
    				<div class="col-sm-10">
      					<input type="file" class="form-control campotexto" name="imagem2" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
    				</div>
  				</div>


  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 3:</label>
    				<div class="col-sm-10">
      					<input type="file" class="form-control campotexto" name="imagem3" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 4:</label>
    				<div class="col-sm-10">
      					<input type="file" class="form-control campotexto" name="imagem4" value="<?php echo formValue($franqnome); ?>"  placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 5:</label>
    				<div class="col-sm-10">	<input type="file" class="form-control campotexto" name="imagem5" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
    				</div>
  				</div>

  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Imagem 6</label>
    				<div class="col-sm-10">
      					<input type="file" class="form-control campotexto" name="imagem6" value="<?php echo formValue($franqnome); ?>"  placeholder="Nome">
    				</div>
  				</div>


  				  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Preço:</label>
    				<div class="col-sm-10">
      					<input type="text" data-type="preco" class="form-control input-lg campopreco" name="preco" id="cpPreco" value="<?php echo formValue($franqnome); ?>"  placeholder="Preço">
    				</div>
  				</div>



  				  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Desconto do Produto:</label>
    				<div class="col-sm-10">
      					<input type="text" data-type="preco"   class="form-control input-lg campopreco" name="descontoProduto" value="<?php echo formValue($franqnome); ?>"  id="cpDescProduto" placeholder="Desconto Produto">
    				</div>
  				</div>


  				  				<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Desconto para Clientes:</label>
    				<div class="col-sm-10">
      					<input type="text"  data-type="preco"  class="form-control input-lg campopreco" name="descontoCliente" value="<?php echo formValue($franqnome); ?>"  id="cpDescCliente" placeholder="Desconto Cliente">
    				</div>
  				</div>



  							<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Desconto para Franqueados:</label>
    				<div class="col-sm-10">
      					<input type="text" data-type="preco" class="form-control input-lg campopreco" name="descontoFranqueado" value="<?php echo formValue($franqnome); ?>"  id="cpDescFranqueado" placeholder="Valor Desconto para Franqueados">
    				</div>
  				</div>

                      <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Altura:</label>
            <div class="col-sm-10">
                <input type="text" data-type="medida" class="form-control input-lg campopreco" name="altura" id="cpPreco" value="<?php echo formValue($altura); ?>"  placeholder="Altura">
            </div>
          </div>


            <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Largura:</label>
            <div class="col-sm-10">
                <input type="text" data-type="medida" class="form-control input-lg campopreco" name="largura" id="cpPreco" value="<?php echo formValue($largura); ?>"  placeholder="Largura">
            </div>
          </div>


            <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Comprimento:</label>
            <div class="col-sm-10">
                <input type="text" data-type="medida" class="form-control input-lg campopreco" name="comprimento" id="cpPreco" value="<?php echo formValue($comprimento); ?>"  placeholder="Comprimento">
            </div>
          </div>


            <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Peso</label>
            <div class="col-sm-10">
                <input type="text" data-type="medida" class="form-control input-lg campopreco" name="peso" id="cpPreco" value="<?php echo formValue($peso); ?>"  placeholder="Nome">
            </div>
          </div>



  										<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Categoria:</label>
    				<div class="col-sm-10">
      					<select id="cpCategoria" class="form-control campodata" name="categoria">
      					<?
      						//$cat = $db->query("SELECT CATEGORIA_ID,NOME FROM categoria");

      						foreach($cat->querydata as $key => $value){

      							$id = $value['CATEGORIA_ID'];
      							$categoria = $value['NOME'];
      					?>

      						<option value="<?php echo $id ?>"><? echo $categoria ?></option>

      					<?
      						
      						}
      					 
      					?>
      				
      					</select>
    				</div>
  				</div>


  					<div class="form-group ">
    				<label for="cpNome" class="col-sm-2 control-label">Subcategoria:</label>
    				<div class="col-sm-10">
      					<select id="cpSubcategoria" class="form-control campodata" name="subcategoria">
      					</select>
    				</div>
  				</div>


				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" id="btAtiva" name="SendForm" class="btn btn-primary btn-lg">Cadastrar</button>
					</div>
				</div>	






	</div>	
</div>

<script type="text/javascript">
	
    function convert(num){

      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    }



		



		$(function(){


       var opt = {

          reverse : true,

         onKeyPress: function(valor, event, current, options) {
              
            var val = current.val();
            
            current.val(val + ' cm');
            //current.append(" cm");
            //alert('CEP Completed!:' + cep);
          }

       }

        $("input[name=altura]").mask("###.##0.00", opt );
        $("input[name=largura]").mask("###.##0.00", opt );
        $("input[name=comprimento]").mask("###.##0.00", opt );
        $("input[name=peso]").mask("###.##0.00");

        $("input[data-type=medida]").popover({

        title: 'Aviso',
        content: "Use ponto ao  invés de vírgulas para casas decimais, Pois se trata de uma medida",
        delay: 300,
        trigger: 'focus'

    }).blur(function(){

      $("input[data-type=preco]").popover('hide');

    });


        $("input[data-type=preco]").popover({

        title: 'Aviso',
        content: "Use ponto ao  invés de vírgulas para casas decimais, não colocar R$ ou algum outro tipo de caracter.  Use somente Numeros e Pontos",
        delay: 300,
        trigger: 'focus'

    }).blur(function(){

      $("input[data-type=preco]").popover('hide');

    });




      $("#cpDescProduto").on('blur',function(){


        var preco = $("#cpPreco").val()

        $("#cpMostraDescProd").empty();


        var descProd = convert((preco - $(this).val()));

        if( descProd  > 0  ){
          

         $('<p id="cpMostraDescProd" class="text-muted">Desconto de Produto: R$'+ descProd +'</p>').insertAfter($(this));
          //console.log(descProd);
        }else {

          console.log(0);
        }

      })




      $("#cpDescCliente").on('blur',function(){




        var preco = $("#cpPreco").val()


        $("#cpMostraDescCliente").empty()

        var descProd = (preco - $(this).val());

        if( descProd  > 0  ){
             $('<p id="cpMostraDescCliente" class="text-muted">Desconto para Clientes: R$'+ descProd +'</p>').insertAfter($(this));
        }else {

          console.log(0);
        }

      })



      $("#cpCategoria").on('change',function(){


        $("#cpSubcategoria").empty();




        $.ajax({
          url: "<?php echo rootURL().'/produtos/busca_subcategoria/'?>" +  $("#cpCategoria").val() ,
          dataType: 'json',
          type: "POST",
          

          success: function(data){


            var htmlcontent;


            for(var i = 0; i < data.length; i++){
            
              htmlcontent +=  $.trim('<option value="'+ data[i].id +'" >'+ data[i].nome + '</option>');

            }

            $("#cpSubcategoria").html(htmlcontent);

            /*
            var index;
            var htmlcontent = ' <option value="<?php echo $subcat_id ?>" selected><?php echo $subcat_nome ?></option>';
          
            
            for(index = 0; index < data.dados.length; index++){

              if(data.dados[index].NOME != '' && data.dados[index].NOME != undefined){
                //$("#cpSubcategoria").append();
                htmlcontent +=  $.trim('<option value="'+ data.dados[index].SUBCATEGORIA_ID +'" >'+ data.dados[index].NOME + '</option>');
              }
            }



            $("#cpSubcategoria").html(htmlcontent);
           
            */
          

          
          console.log(data);
           
          },
          error: function(xchr,obj,error){             

            console.error(xchr + ' ' + error);

          }

        });


      });

		});


</script>