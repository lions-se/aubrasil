<div class="container">
	<h1> Ver Pedido: </h1>
	<form action="" class="form-horizontal">
		<legend>Pedido #<?php echo sprintf("%08d",$data->querydata[0]['ORDEM_ID']); ?></legend>
			
			<table  class="table table-striped table-bordered">
				<thead>
					<tr>
						<th width="50">Produto:</th>
						<th width="160">Produto:</th>
						<th width="40">Quantidade:</th>
						<th width="150">Valor Unitário:</th>
						<th>Subtotal</th>
					</tr>
				</thead>
				<tbody>
					
					<? foreach ($data->querydata as $key => $value) { ?>
					<tr>
						<td><img src="/uploads/thumbs/<?php echo $value['IMAGEM1']; ?>"></td>
						<td><? echo $value['PRODUTO_NOME']; ?></td>
						<td><? echo $value['QUANTIDADE']; ?></td>
						<td><? printf("R$ %.2f x %d",$value['PRECO'], $value['QUANTIDADE']); ?></td>

						
						<? if($value['REFERENCIA'] == 1){ ?>
							<? $subtotal = (($value['PRECO'] - $value['DESCONTOPRODUTO']) - $value['DESCONTOCLIENTE']) * $value['QUANTIDADE'] ; ?>
							<td><? printf("R$ %.2f", $subtotal); ?> </td>
						<? }else{
							$subtotal = ($value['PRECO'] - $value['DESCONTOPRODUTO']) *  $value['QUANTIDADE'];
						 ?>
							<td><? printf("R$ %.2f", $subtotal); ?> </td>
						<? } ?>
					</tr>
					<? 

						$total += $subtotal;

					?>
					<? } ?>

					<tr>
						<td></td>
						<td></td>
						<td colspan="5"><? printf("Total: R$ %.2f", $total); ?></td>
					</tr>

				</tbody>

			</table>
		
			<button onClick="javascript:  window.history.back();" type="button" class="btn btn-primary">Voltar</button>

	</form>

</div>