<div class="container">




	
	<? if(empty($data->querydata)){ ?>
		
		<div class="row">
			<div class="col col-md-12">
						<div class="msg_sem_pedidos">
							<h1>Não há pedidos cadastrados</h1>
						</div>
			</div>
		</div>

	<? }else{ ?>

	<h1>Visualizar Pedidos</h1>
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				<th>Pedido:</th>
				<th>Visualização:</th>
				<th>Status</th>
				<th>Ações</th>
				
			</tr>
		</thead>
		<tbody>

			<? foreach($data->querydata as $key => $value){ 

					$ordem_id = $value['ORDEM_ID'];


				?>
			<tr>
				<td>Pedido #<? printf("%08d", $value['ORDEM_ID']);  ?></td>
				<td><a href="<?php echo rootURL()."/clientes/ver_pedidos/".$clienteid.'/nota_virtual/'.$value['ORDEM_ID']; ?>" class="btn btn-lg btn-primary">Ver Pedido</a> </td>
				<td><?php echo $value['STATUS'] ?></td>


				<td>
					
						<div class="btn-group">
  <button type="button" class="btn btn-primary">Status da Transação</button>
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    	<? foreach (getPedidoStatus() as $key => $val) { ?>
    	
    	<li><a  href="<?php echo rootURL().'/troca_pedido/'.$clienteid.'/'.$ordem_id.'/'.$val['pedido_status_id'] ?>"><?php echo $val['status']; ?></a></li>		
    
     <?	} ?>
  </ul>
</div>


				</td>
			</tr>
			<? } ?>
		</tbody>
	</table>
	<? } ?>

	<? $pag->Pages(); ?>

	<a href="<?php echo  rootURL().'/clientes/' ?>" class="btn btn-default">Voltar</a>
</div>
