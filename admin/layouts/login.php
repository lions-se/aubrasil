 <?
    #require_once("do_login.php");
 ?>

  <div class="container">

    <div class="span7 center">



    <?
        if(isset($_SESSION['flash'])){

            $t =  flash();
            echo $t;
            kill_alert();
        }
    ?>

        
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h5 class="text-center">
                       Login</h5>
                    <form class="form form-signup" action="<? echo rootURL().'/login/do_login' ?>"  method="POST" role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                            <input type="text" name="login" class="form-control" placeholder="E-Mail" />
                        </div>
                    </div>
               
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                            <input type="password" name="passwd" class="form-control" placeholder="Senha" />
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-login  btn-block">
                  <span class="glyphicon glyphicon-user"></span> LOGIN
                </button> 

                  </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>

