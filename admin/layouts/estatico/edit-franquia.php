<div class="container-fluid">
      <form class="form-horizontal" id="formEdit"  method="post" enctype="application/x-www-form-urlencoded" role="form">
		
		<legend>Editar <?php echo $nome; ?></legend>
		
				<div class="campo_edittexto">
    				<textarea name="texto"   id="editor"><?php echo $dados; ?></textarea>
    			</div>
    			</div>
	
 	<legend>Atualizar Texto</legend>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    	<a href="<?php echo rootURL().'/conteudo-estatico'; ?>" class="btn btn-default btn-sm">Voltar</a>
      <button type="submit" id="btSendFormEdita" name="SendForm" class="btn btn-primary btn-sm">Atualizar Texto  </button>
    </div>
  </div>
</form>
</div>

<script type="text/javascript">
	
	$(function(){
		$('#editor').summernote({
			  height: 300,
			  width: 20
		});

		$("#btSendFormEdita").on('click', function(){

			$.ajax({
				url: '<?php echo Flight::request()->url; ?>',
				dataType: 'text',
				type: 'POST',
				data: {'texto' : $("#editor").code()},

				success: function(data){

						console.log(data)

				}
			});

		});

	});

</script>