<div id="validaCep" class="form-group">
    <label for="cpCep" class="col-sm-2 control-label">CEP</label>
    <div class="col-sm-10">
      <div class="campodocs">
        <input class="span2  campocep" name="cep" id="campoCep" value="<?php echo $cep; ?>"   size="16" type="text">
        <button type="button" id="btBuscaCep" class="btn btn-primary btn-sm">Buscar CEP</button>
        <span id="buscaCepLoading" class="add-on pull-right"><img src="img/fbloader.gif" /> Buscando CEP</span>
        </div>
    </div>
  </div>
  
     <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Endereço</label>
    <div class="col-sm-10">
      <input type="text" name="endereco" class="form-control campotexto" value="<?php echo formValue($endereco); ?>" id="cpEnd" placeholder="Endereço">
    </div>
  </div>
  
       <div class="form-group ">
    <label for="cpBairro" class="col-sm-2 control-label">Bairro</label>
    <div class="col-sm-10">
      <input type="text" name="bairro" class="form-control campotexto" id="cpBairro" value="<?php echo formValue($bairro); ?>" placeholder="Bairro">
    </div>
  </div>
       <div class="form-group">
    <label for="cpCidade" class="col-sm-2 control-label">Cidade</label>
    <div class="col-sm-10">
      <input type="text" name="cidade" class="form-control  campotexto"   value="<?php echo formValue($cidade); ?>" id="cpCidade" placeholder="Cidade">
    </div>
  </div>

   <div class="form-group">
      <label for="cpPais" class="col-sm-2 control-label">Estado</label>
    <div class="col-sm-10">
      <select id="cpPais" name="estado" class="form-control  campotexto">
          <?php echo estadosBrasileiros($estado); ?>
        </select>
  </div>
  </div>

  <script>
  
$(function(){


          $('input[name=telefone]').mask('(00) 0000-0000',
   {onKeyPress: function(phone, e, currentField, options){
    var new_sp_phone = phone.match(/^(\(11\) 9(5[0-9]|6[0-9]|7[01234569]|8[0-9]|9[0-9])[0-9]{1})/g);
    new_sp_phone ? $(currentField).mask('(00) 00000-0000', options) : $(currentField).mask('(00) 0000-0000', options)
  }
});
      $("input[name=cep]").mask("00000-000");


    $("#btBuscaCep").click(function(){  

      var cep = $("input[name=cep]").val();


      $.post("<?php echo rootURL().'/busca_cep/' ?>", {'cep' : cep }, function (data){

        //console.log(data);
        $("#cpEnd").val(data.tipo_logradouro +' '+  data.logradouro);
        $("#cpCidade").val(data.cidade);
        $("#cpBairro").val(data.bairro);

      });


    });

  

});
</script>