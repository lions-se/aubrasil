<?

$rg = $q['RG'];
$proprietario_id = $q['PROPRIETARIO_ID'];
$proprietario_nome =$q['NOME'] ;
$proprietario_sobrenome =$q['SOBRENOME'] ;
$marca_id = $q['MARCA_ID'];
$modelo_id = $q['MODELO_ID'];

$placa = $q['PLACA'];
$chassi = $q['CHASSI'];
$renavam = $q['RENAVAM'];
$cor = $q['COR'];
$procedencia = $q['PROCEDENCIA'];
$zerokm = $q['ZEROKM'];


?>

      <h2>Editar Veiculos</h2>
    <form class="form-horizontal" id="formEditFranquia"  method="post" enctype="application/x-www-form-urlencoded" role="form">
      <div class="form-group ">
   
      <?php Flight::render('estatico/buscar_proprietario.php', array('rg' => $rg, 'id' => $proprietario_id, 'nome' => $proprietario_nome, 'sobrenome' => $proprietario_sobrenome) ); ?>

  <input type="hidden" name="changeProp" value="1">


        <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label chosen-select-no-results">Marca do Veiculo</label>
    <div class="col-sm-10">
      <select id="cpMarca" class="form-control campocep" name="marca">
        <option selected>Nenhum</option>
       <?  mostraMarca($marca_id); ?>
      </select>
    </div>
  </div>

          <div class="form-group ">
    <label for="cpModelo" class="col-sm-2 control-label">Modelo do Veiculo</label>
    <div class="col-sm-10">
      <select id="cpModelo" class="form-control  campocep" name="modelo">
        <?php  echo Utils::optionModeloVeiculo($marca_id,$modelo_id) ?>
      </select>
    </div>
  </div>

        <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Placa</label>
    <div class="col-sm-10">
      <input type="text"  class="form-control campocep" name="placa" value="<?php echo $placa; ?>"   id="cpPlaca"  >
    </div>
  </div>

          <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Chassi</label>
    <div class="col-sm-10">
      <input type="text"  class="form-control campocep" name="chassi" value="<?php echo $chassi; ?>"   id="cpNome"  >
    </div>
  </div>

            <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">RENAVAM</label>
    <div class="col-sm-10">
      <input type="text"  class="form-control campocep" name="renavam" value="<?php echo $renavam; ?>"  id="cpNome"  >
    </div>
  </div>


            <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Capacidade</label>
    <div class="col-sm-10">
        <select name=" capacidade"class="form-control campocep" class="form-control" >
          <option value="0"  selected>Quantidade de Lugares</option>
         <? for($i = 1; $i <= 12; ++$i ){
          
          $texto = $i == 1 ? "Lugar" : "Lugares";

          if($i == $q['CAPACIDADE']){
            echo printf('<option selected value="%d" >%d %s</option>', $i, $i, $texto);
          }else{
            echo printf('<option value="%d" >%d %s</option>', $i, $i, $texto);
          }
         } ?>

        </select>
    </div>
  </div>


          <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label chosen-select-no-results">Zero Km:</label>
    <div class="col-sm-10">
      <select id="cpMarca" name="zerokm">
          <?php 
              $opc = array(0 => 'Não', 1 => 'Sim');
              $opc_it = new ArrayIterator($opc);

              while($opc_it->valid()){

                  if($zerokm == $opc_it->key()){
                      echo sprintf('<option selected value="%s">%s</option>',$opc_it->key(), $opc_it->current());
                    }else {
                      echo sprintf('<option value="%s">%s</option>',$opc_it->key(), $opc_it->current());

                    }


                  $opc_it->next();
              }

           ?>
      </select>

    </div>
  </div>


            <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Cor</label>
    <div class="col-sm-10">
      <input type="text"  class="form-control campocep" name="cor" value="<?php echo $cor; ?>"  id="cpNome"  >
    </div>
  </div>


            <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Procedencia</label>
    <div class="col-sm-10">
            <select id="cpMarca"  class="form-control campocep" name="procedencia">
              <?
                  $proc_opcao = array(1 => 'Nacional', 2 => 'Importado');
                  $proc = new ArrayIterator($proc_opcao);

                  while($proc->valid()){

                    if($procedencia == $proc->key()){
                      echo sprintf('<option selected value="%s">%s</option>',$proc->key(), $proc->current());
                    }else {
                      echo sprintf('<option value="%s">%s</option>',$proc->key(), $proc->current());

                    }
                    $proc->next();
                  }

              ?>
      </select>
    </div>
  </div>


  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btAtiva" name="SendForm" class="btn btn-success btn-sm">Editar</button>
    </div>
  </div>
  


    

</form>


<script type="text/javascript">



  
  $(function(){


    var opt = {

      onKeyPress: function(letra, event, current, option){


          current.val(letra.toUpperCase());
          //console.log(current);

      }

    }

      $("input[name=chassi]").mask("SSS SSSSS SS SSSSSS", opt);

       $("input[name=renavam]").mask("AAAAAAAAAAA", opt);

      $("input[name=placa]").mask("SSS-0000", opt);

        var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }


    $("cpMarca").chosen(config);


    $("#cpMarca").on('change',function(){

      $.ajax({

        url: "<?php echo  rootURL().'/busca_marca' ?>",
        dataType: 'json',
        type: 'POST',
        data: $("#cpMarca").serialize(),


        success: function(data){

           $("#cpModelo").empty()
           console.log(data);
           for(var i = 0; i < data.length; i++){
              var carro = data[i]

              var tag = '<option value="'+ carro.ID +'">'+ carro.NOME +'</option>'


              $("#cpModelo").append(tag);
           }

        },

        error: function(x,o,e){

            console.log(x + ' ' + e);

        }

      });

    });



      $("#buscaProprietario").on('click',function(){


    $.ajax({

      url: '<?php echo  urlbase().'/interno/franquias/buscaProprietario.php'; ?>',
      dataType: "json",
      type: 'POST',
      data: $("#cpProprietario").serialize(),

      success: function(data){


          $("input[name=propnome]").val(data[0][0]);
          $("input[name=propsobrenome]").val(data[0][1]);
          $("input[name=proprg]").val(data[0][2]);
          $("input[name=propid]").val(data[0][3]);
          //console.log(data);


      }

    });


  });


    <? if(isset($_GET['adduser'])){ ?>
      do_ajax(<?php echo $adduser; ?>);
    <? } ?>


  });

</script>