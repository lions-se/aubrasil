<?php 
/*
  
  $where = "";
  
  if($_SERVER['REQUEST_METHOD'] == "POST"){
    
    //print_r($_POST);
    $campo = "";
    if( !empty($_POST['cpBuscar']) && !empty($_POST['cpSelect']) ){
      switch($_POST['cpSelect']){
        
        case 'Nome':
          $campo = 'NOME';
          break;
        
        case 'CPF':
          $campo = "CPF";
          break;
        case 'Documento RG':
          $campo = "RG";
          break;

        case 'E-Mail':
          $campo = "EMAIL";
          break;

        default:
          $campo = 'NOME';
          break;
      }
      
      $busca = $_POST['cpBuscar'];
      $_SESSION['whereProprietario'] = array('campo' => $campo, 'busca' => $busca);   //sprintf(" WHERE %s LIKE '%s%%'", $campo, $busca);
      header("location: index.php?opcao=listar-proprietarios");

      
    }
    
  }


$dados = isset($_SESSION['whereProprietario']) ? $_SESSION['whereProprietario'] : NULL;
//unset($_SESSION['whereProprietario']);

//print_r($_SESSION);
if(isset($_SESSION['whereProprietario'])){

  $where = sprintf(" WHERE %s LIKE '%s%%' AND PERMISSAO_ID != 1 ", $dados['campo'] ,$dados['busca']);

}else {

  $where = NULL;
}



  $query = "SELECT
proprietario.PROPRIETARIO_ID AS PROP_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.RG,
proprietario.CPF,
veiculo_proprietario.VEICULO_ID,
veiculo_proprietario.PROPRIETARIO_ID AS VEICULO_PROP_ID,
veiculo.MODELO_ID,
modelo.MODELO,
marca.NOMEMARCA
FROM
proprietario
RIGHT JOIN veiculo_proprietario ON veiculo_proprietario.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
INNER JOIN veiculo ON veiculo_proprietario.VEICULO_ID = veiculo.VEICULO_ID
INNER JOIN modelo ON veiculo.MODELO_ID = modelo.MODELO_ID
INNER JOIN marca ON modelo.MARCA_ID = marca.MARCA_ID





".$where;
  
  $pag = new Pagination($db,5);
  $pag->setPages('proprietario');
 
  $q = $pag->retrieveData($query);
 

  //$q = $db->query($query, NULL, NULL, DatabasePDO::FETCH_OBJECT);
  unset($_SESSION['whereProprietario']);

*/

?>



<div class="container">

<h1>Listar Veiculos</h1>
<p class="text-muted">Filtros:</p>

<div class="container form-busca">
<form class="form-inline" id="formgeral"  method="post">
<fieldset>



<!-- Search input-->
<div class="control-group">
  <label class="control-label" for="cpBuscar">Buscar</label>
  <div class="controls">
    <input id="cpBuscar" name="cpBuscar" type="text" placeholder="Busque aqui " class="input-xlarge search-query" value="<?php  echo $_SESSION['whereProprietario']['busca']; ?>">
    
  </div>
</div>

<!-- Select Basic -->
<div class="control-group">
  <label class="control-label" for="cpSelect">Busque Por</label>
  <div class="controls">
    <select id="cpSelect" name="cpSelect" class="input-xlarge">
      <option>Nome</option>
      <option>CPF</option>
      <option>RG</option>
      <option>EMAIL</option>


    </select>
  </div>
</div>

<!-- Button -->
<div class="control-group">
  <label class="control-label" for="btSend"></label>
  <div class="controls">
    <button id="singlebutton" name="btSend" class="btn btn-info">Buscar</button>
  </div>
</div>

<input type="hidden" name="checkSent" value="formSent" />
</fieldset>



</div>

<div class="bt_espaco">
    <a href="<?php echo rootURL().'/veiculos/adicionar' ?>" class="btn btn-primary btn-lg">
      <span class="glyphicon glyphicon-plus"></span> Adicionar novo veículo
    </a>
</div>


<table class="table table-striped table-condensed">
    
      <thead>
        <tr>
          <th width="150"><p class="text-center">Proprietario</p></th>
          <th width="80"><p class="text-center">Modelo</p></th>
          <th width="150"><p class="text-center">Marca</p></th>
          <th width="90"><p class="text-center">Placa</p></th>
          <th width="150"><p class="text-center">Ações</p></th>
        </tr>
      </thead>
      <tbody>

        <?php 
  //print_r($q->querydata[0]);
    //for($i = 0; $i < $q->rowsAffected();$i++){
    foreach ($q->querydata as $key => $value) {
      # code...
      $proprietario   =  sprintf("%s %s",$value['NOME'], $value['SOBRENOME']);
      $modelo         =  $value['MODELO'];
      $marca          =  $value['NOMEMARCA'];
      $placa          =  $value['PLACA'];
      $email          =  $value['EMAIL'];
      $id             =  $value['VEICULO_ID'];
  


  
?>
        <tr>
          <td><p class="text-muted text-center"><?php echo $proprietario; ?></p></td>
          <td><p class="text-muted text-center"><?php echo $modelo; ?></p></td>
          <td><p class="text-muted text-center"><?php echo $marca; ?></p></td>
          <td><p class="text-muted text-center" ><?php echo $placa; ?> </p></td>
          <td><p class="text-muted text-center"><div class="btn-group">
  <button type="button" class="btn btn-primary">Ações</button>
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a  href="<?php echo rootURL().'/veiculos/editar/'.$id; ?>">Editar</a></li>
    <li><a  href="<?php echo rootURL().'/veiculos/apagar/'.$id; ?>">Apagar</a></li>

  </ul>
</div>
    </p></td>
        </tr>
      </tbody>

      <? } ?>

</table>
<?php $pag->Pages(); ?>


</div>



</div>

<script>
  



function apagar(id){
  $(function(){

    $.ajax({

      url: '<?php echo urlbase().'/interno/franquias/deleteFranquia.php'; ?>',
      type: "POST",
      dataType: "json",
      data: {"userid": id},

      success: function(data){
         //var d = $.parseJSON(data);
          console.log(data);
          //$("#apagar-modal").modal("show");

      },
      error: function(xchr, obj, error){

          console.error(error + ' : ' + xchr);

      }

    });

  });


}



$(function(){

  $(function (){
    $("[data-toggle=popover]").popover({

      html: true,
      animation: true

    });    
  });   

      $("#formgeral button").each(function(){

        var campo = $(this);



    });

});

    
    
    
  
</script>