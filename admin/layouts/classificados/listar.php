<?php

/*
	session_start();
	$start = microtime(true);
	require_once("../config.php");
	include("adminconf.inc.php");
	session_start();
	include("header.php");
	include("top.php");
	//include("main.php");


$query = "SELECT
marketing.MARKETING_ID,
marketing.NOME,
marketing.DESCRICAO,
marketing.PATH
FROM
marketing";
	
	$p = new Pagination($db,3);
	$p->setPages('marketing');



	$q = $p->retrieveData($query);

*/

?>

<div class="container main-container">

	
			<? if($_SESSION['loginCredentials']->permissao_id == 1){ ?>
    			<a href="<?php echo rootURL().'/classificados/adicionar'; ?>" class="btn btn-primary btn-lg">
      			<span class="glyphicon glyphicon-plus"></span> Adicionar Novo Anuncio</a>
      		<? } ?>


		<h1> Classificados<small> Adicione aqui o seu anuncio </small> </h1>


		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th >Nome</th>
					<th >Descrição</th>
					<th >Imagem</th>


					<? if( Utils::getPermission() == 1 ){ ?>
					<th>Ações</th>
					<? } ?>
				</tr>
			</thead>
			<tbody>
				<tr>
					<?php  for($i =0; $i < $q->rowsAffected(); $i++){


						$path_full = realpath('uploads/classificados');
						$path_relative = 'uploads/classificados';
						$id =  $q->querydata[$i]['CLASSIFICADO_ID'];
						$nome = $q->querydata[$i]['NOME'];
						$texto = $q->querydata[$i]['TEXTO'];
						$imagem = $q->querydata[$i]['IMAGEM'];
						$miniatura = file_exists($path_full.DIR.'thumbs'.DIR.$path) ? $path_relative.'/'.$path : 'imagens/sem_foto.jpg';

	?>
					<td><?php echo $nome; ?></td>
					<td><a class="btn btn-xs btn-primary" role="button" data-container="body" data-content="<?php echo $texto; ?>" title="Descrição" data-toggle="popover"  data-original-title="A Title"> Ver Descrição</a></td>
					<td><img src="<?php echo  $imagem; ?>" /></td>

					
					<? if( Utils::getPermission() == 1 ){ ?>
					<td><div class="btn-group">
  <button type="button" class="btn btn-primary">Ações</button>
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a  href="<?php echo rootURL().'/classificados/editar/'.$id ?>">Editar</a></li>
    <li><a  href="<?php echo rootURL().'/classificados/apagar/'.$id ?>">Apagar</a></li>
</div></td>
<? } ?>
				</tr>

					<?
	}
	?>

			</tbody>
		</table>


	

	




	<?
		define('TIME_GERA', sprintf('<p class="text-muted text-center ">Esta página foi gerada em: %.4f segundos</p>', microtime(true) - $start));

	?>


	<?php echo $p->Pages(); ?>


</div>


<script type="text/javascript">
	
	  $(function (){
    $("[data-toggle=popover]").popover({

      html: true,
      animation: true

    });    
  });  


</script>



