<?php
	

	if($_SERVER['REQUEST_METHOD'] == "POST"){


		$limpacpf = array('-','.');
		$limparg = array('.','-');

		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$cpf  = str_replace($limpacpf, '', $_POST['cpf']) ;
		$rg   = str_replace($limpacpf, '', $_POST['rg']) ;
		$dob = $_POST['dob'];
		$cep = $_POST['cep'];
		$end = $_POST['endereco'];
		$bairro = $_POST['bairro'];
		$cidade = $_POST['cidade'];
		$pais = $_POST['pais'];
		$estado = $_POST['estado'];

		$sexo = $_POST['sexo'];
		$estadocivil = $_POST['estadocivil'];
		$complemento = $_POST['complemento'];
		$emailsecundario   = $_POST['emailsecundario'];


		$telcomercial = $_POST['telcomercial'];
		$telresidencial = $_POST['telcontato'];
		$telcelular = $_POST['telcelular'];


		if(empty($rg) || empty($cpf)){

				print('<div class="alert alert-danger"><strong>Erro:</strong> Documentos como CPF e RG Sao Campos Obrigatorios a Preencher</div>');

		}else{

			$db->update('proprietario', array(

										 	'EMAIL_SECUNDARIO' => $emailsecundario,
										  	'COMPLEMENTO' => $complemento,
										  	'TELCOMERCIAL' => $telcomercial,
										  	'TELRESIDENCIAL' => $telresidencial,
										  	'TELCELULAR' => $telcelular,
										  	'ESTADO_CIVIL' => $estadocivil,
											'NOME' => $nome,
										  	'SOBRENOME' => $sobrenome,
										  	'CPF' => $cpf,
										  	'RG'  => $rg,
										  	'DATA_NASCIMENTO' => date_convert($dob),
										  	'CEP' => $cep,
										  	'ENDERECO' => $end,
										  	'BAIRRO' => $bairro,
										  	'CIDADE' => $cidade,
										  	'ESTADO' => $estado,
										  	'SEXO' => $sexo,
										  	'ESTADO_CIVIL' => $estadocivil,
										  	'COMPLEMENTO' => $complemento,
										  	'PAIS' => $pais), 'proprietario.PROPRIETARIO_ID = '.$_GET['editar']);

			alert("Sucesso"," Os Dados do Usuario: ".$nome." foram atualizados com sucesso");
			header("location: index.php?opcao=listar-proprietarios");


		}

	}


?>