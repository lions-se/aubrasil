	<?

		//include("do_cadastraProprietario.php");

$franqnome = $dados['nome'];
$sobrenome = $dados['sobrenome'];
$rg = $dados['telefone'];
$cpf = $dados['cpf'];
$telresidencial = $dados['telcontato'];
$telcomercial  = $dados['telcomercial'];
$telcelular = $dados['telcelular'];
$dob = $dados['dob'];

	?>

	<div class="container edituser-container">
	<div class="container fill">

		<h1>Cadastrar um Novo Usuario</h1>
        
        <div class="row">
      <form class="form-horizontal"  id="formEdit"  method="post" enctype="application/x-www-form-urlencoded" role="form">
  <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Nome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="nome" value="<?php echo formValue($nome); ?>"  id="cpNome" placeholder="Nome">
    </div>
  </div>
  <div class="form-group">
    <label for="cpSobrenome" class="col-sm-2 control-label">Sobrenome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="sobrenome" value="<?php echo formValue($sobrenome); ?>" id="cpSobrenome" placeholder="Seu Sobrenome">
    </div>
  </div>

     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">RG</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="rg" value="<?php echo formValue($rg); ?>" id="cpRg" placeholder="Numero do RG">
    </div>
  </div>
  
   <div class="form-group">
    <label for="cpCPF" class="col-sm-2 control-label">CPF</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="cpf" value="<?php echo formValue($cpf); ?>" id="cpCPF" placeholder="Seu CPF">
    </div>
  </div>
  
  
     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">Telefone de Contato:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotel" name="telcontato" value="<?php echo formValue($telresidencial); ?>" id="cpRg" placeholder="Telefone de Contato">
    </div>
  </div>


   <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">Telefone Comercial</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotel" name="telcomercial" value="<?php echo formValue($telcomercial); ?>" id="cpRg" placeholder="Telefone Comercial">
    </div>
  </div>

     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">Telefone Celular</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotel" name="telcelular" value="<?php echo formValue($telcelular); ?>" id="cpRg" placeholder="Telefone Celular">
    </div>
  </div>



  
  
   <div class="form-group">
    <label for="cpDOB" class="col-sm-2 control-label">Data de Nascimento</label>
    <div class="col-sm-10">
		<div class="campodocs" id="cpEditDate" data-date="<?php echo formValueDate($dob); ?>" data-date-format="dd-mm-yyyy">
				<input class="span2  campotexto" date-calendar="true" name="dob" id="cpDob" size="16" value="<?php echo formValueDate($dob); ?>" readonly type="text">
				<span class="add-on pull-right"><i class="glyphicon glyphicon-list-alt"></i></span>
			  </div>
        
    </div>
  	</div>



                <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">Sexo:</label>
    <div class="col-sm-10">
      <select id="cpPais" name="sexo" class="form-control  campotexto">
          <option value="M" selected>Masculino</option>
          <option value="F">Feminino</option>
        </select>

    </div>
  </div>

                <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">Estado Civil</label>
    <div class="col-sm-10">
      <select id="cpPais" name="estadocivil" class="form-control  campotexto">
          <option value="1" selected>Solteiro (a)</option>
          <option value="2">Casado (a)</option>
          <option value="3">Divorciado (a)</option>
          <option value="4">Viuvo(a)</option>
        </select>

    </div>
  </div>
  
    
    
       <? 
      Flight::render('estatico/buscar_cep.php');
      ?>
  
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<div id="avisoSucesso" class="alert alert-success alert-dismissible aviso">Enviado Com Sucesso</div>
			<div id="avisoFalha" class="alert alert-danger alert-dismissible aviso">Falha no Envio</div>
		</div>
	</div>

	 <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">E-Mail (login):</label>
    <div class="col-sm-10">
      <input type="text" name="login" value="<?php echo formValue($login); ?>" class="form-control campotexto" id="cpEnd" placeholder="E-Mail">
    </div>
  </div>

	  <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Senha:</label>
    <div class="col-sm-10">
      <input type="password" name="senha" class="form-control campotexto" id="cpEnd" placeholder="Senha">
    </div>
  </div>


   <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Repetir a Senha:</label>
    <div class="col-sm-10">
      <input type="password" name="repete_senha" class="form-control campotexto"  id="cpEnd" placeholder="Repetir Senha">
    </div>
  </div>

               <div class="form-group ">
    <label for="cpBairro" class="col-sm-2 control-label">E-Mail Secundario (Opcional)</label>
    <div class="col-sm-10">
      <input type="text" name="emailsecundario" class="form-control campotexto" id="cpBairro" value="<?php echo formValue($emailsecundario); ?>" placeholder="E-Mail Secundario">
    </div>
  </div>


    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btSendForm" name="SendForm" class="btn btn-primary">Cadastrar</button>
    </div>
  </div>
</form>
</div>
        
	</div>
</div>



<script>
	$(function(){
		
		
		 $("input[name=telcontato]").mask("(00) 0000-0000");
     $("input[name=telcomercial]").mask("(00) 0000-0000");


     $("input[name=rg]").mask("00.000.000-A");
     $("input[name=cpf]").mask("000.000.000-00");


     var celulares = {onKeyPress:function(cel){

      var masks = ['(00) 0000-0000', '(00) 90000-0000'];

        var maskcel = cel.length > 9 ? masks[1] : masks[0];

        $('input[name=telcelular]').mask(maskcel,this) ;
        console.log(cel.length);

     }};

     $('input[name=telcelular]').mask('(00) 00000-0000', celulares);

		//event.preventDefault();




        $("cpDob").datepicker({
      dateFormat: 'dd-mm-yy',
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
      nextText: 'Próximo',
      prevText: 'Anterior',
      changeMonth: true,
      changeYear: true,
      yearRange: '1910:2014'

    });
	        
	});

		
			
</script>

