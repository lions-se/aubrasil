<?php
/*
if(isset($_GET['apagar'])){

	if($_SERVER['REQUEST_METHOD'] == "POST"){

		include("do_apagaProprietarios.php");
	}
	
	$q = $db->query("
		SELECT
			proprietario.PROPRIETARIO_ID,
			proprietario.PERMISSAO_ID,
			proprietario.NOME,
			proprietario.SOBRENOME,
			proprietario.EMAIL,
			proprietario.CPF,
			proprietario.RG,
			proprietario.DATA_REGISTRO,
			proprietario.DATA_NASCIMENTO,
			proprietario.CEP,
			proprietario.ENDERECO,
			proprietario.BAIRRO,
			proprietario.CIDADE,
			proprietario.ESTADO,
			proprietario.PAIS,
			proprietario.CODIGOEMAIL,
			proprietario.CONFIRMA_CADASTRO,
			permissao.PERMISSAO_ID,
			permissao.NOME as PERM_NOME
		FROM
			proprietario
		INNER JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID


		WHERE 
			proprietario.PROPRIETARIO_ID  = ".$_GET['apagar']);

}

*/
$id = $q->querydata[0]['PROPRIETARIO_ID'];
$nome = $q->querydata[0]['NOME'];
$sobrenome = $q->querydata[0]['SOBRENOME'];
$cpf = $q->querydata[0]['CPF'];
$rg = $q->querydata[0]['RG'];
$cep = $q->querydata[0]['CEPFRANQUIA'];
$dob = $q->querydata[0]['DATA_NASCIMENTO'];
$end = $q->querydata[0]['ENDERECO'];
$cidade = $q->querydata[0]['CIDADE'];
$bairro = $q->querydata[0]['BAIRRO'];
$uf = $q->querydata[0]['ESTADO'];
$login =  $q->querydata[0]['EMAIL'];

?>




	<div class="container edituser-container">
	<div class="container fill">

		<h1>Apagar <small>Certeza que Deseja Apagar o Proprietario: <?php echo $nome;  ?></small> </h1>
        
        <div class="row">
      <form class="form-horizontal" id="formEdit" action="<?php echo rootURL().'/proprietarios/do_delete' ?>"  method="post" enctype="application/x-www-form-urlencoded" role="form">
  
      <input type="text" name="userid" value="<?php echo $id;  ?>">

  <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label" >Nome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="nome" disabled value="<?php echo formValue($nome); ?>"  id="cpNome" placeholder="Nome">
    </div>
  </div>
  <div class="form-group">
    <label for="cpSobrenome" class="col-sm-2 control-label">Sobrenome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="sobrenome"         disabled value="<?php echo formValue($sobrenome); ?>" id="cpSobrenome" placeholder="Seu Sobrenome">
    </div>
  </div>
  
   <div class="form-group">
    <label for="cpCPF" class="col-sm-2 control-label">CPF</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="cpf"         disabled value="<?php echo formValue($cpf); ?>" id="cpCPF" placeholder="Seu CPF">
    </div>
  </div>
  
  
   <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">RG</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="rg"        disabled value="<?php echo formValue($rg); ?>" id="cpRg" placeholder="Numero do RG">
    </div>
  </div>
  
  
   <div class="form-group">
    <label for="cpDOB" class="col-sm-2 control-label">Data de Nascimento</label>
    <div class="col-sm-10">
		<div class="campodocs" id="cpEditDateProprietarios" data-date="<?php echo formValueDate($dob); ?>" data-date-format="dd-mm-yyyy">
				<input type="text" class="span2  campotexto"       disabled name="dob" size="16" value="<?php echo formValueDate($dob); ?>"  type="text">
				<span class="add-on pull-right"><i class="glyphicon glyphicon-list-alt"></i></span>
			  </div>
        
    </div>
  	</div>
    
    
    <div id="validaCep" class="form-group">
    <label for="cpCep" class="col-sm-2 control-label">CEP</label>
    <div class="col-sm-10">
     	<div class="campodocs">
				<input class="span2  campocep" name="cep"      disabled id="cpCEP" value="<?php echo formValue($cep); ?>"  size="16" type="text">

			  </div>
    </div>
  </div>
  
     <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Endereço</label>
    <div class="col-sm-10">
      <input type="text" name="endereco"     disabled class="form-control campotexto" value="<?php echo formValue($end); ?>" id="cpEnd" placeholder="Endereço">
    </div>
  </div>
  
       <div class="form-group ">
    <label for="cpBairro" class="col-sm-2 control-label">Bairro</label>
    <div class="col-sm-10">
      <input type="text" name="bairro" disabled class="form-control campotexto" id="cpBairro" value="<?php echo formValue($bairro); ?>" placeholder="Bairro">
    </div>
  </div>
       <div class="form-group">
    <label for="cpCidade" class="col-sm-2 control-label">Cidade</label>
    <div class="col-sm-10">
      <input type="text" name="cidade"    disabled class="form-control  campotexto"   value="<?php echo formValue($cidade); ?>" id="cpCidade" placeholder="Cidade">
    </div>
  </div>

    <div class="form-group">
      <label for="cpPais" class="col-sm-2 control-label">Estado</label>
    <div class="col-sm-10">
      <select id="cpPais" name="estado"  disabled  class="form-control  campotexto">
          <?php echo estadosBrasileiros(); ?>
        </select>
  </div>
  </div>

  
        <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">País</label>
    <div class="col-sm-10">
    	<select id="cpPais" name="pais" disabled class="form-control  campotexto">
        	<?php echo showPaises(); ?>
        </select>

    </div>
  </div>
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btSendForm" name="SendForm" class="btn btn-danger">Apagar</button>
    </div>
  </div>
</form>
</div>





        
	</div>
</div>
