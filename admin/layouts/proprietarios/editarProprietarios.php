<?php

/*
$id = $_GET['editar'];


if( $_SERVER['REQUEST_METHOD'] == "POST"){

	if(isset($_POST['sendtype']) == 'edit-account'){

		include("do_editUser.php");

	}else{

		include("do_editarProprietario.php");
	}
}

	
	
	$q = $db->query("
SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.EMAIL,
proprietario.CPF,
proprietario.RG,
proprietario.DATA_REGISTRO,
proprietario.DATA_NASCIMENTO,
proprietario.CEP,
proprietario.ENDERECO,
proprietario.BAIRRO,
proprietario.CIDADE,
proprietario.ESTADO,
proprietario.PAIS,
proprietario.CODIGOEMAIL,
proprietario.CONFIRMA_CADASTRO,
permissao.PERMISSAO_ID,
permissao.NOME AS PERM_NOME,
proprietario.EMAIL_SECUNDARIO,
proprietario.ESTADO_CIVIL,
proprietario.TELCOMERCIAL,
proprietario.TELCELULAR,
proprietario.TELRESIDENCIAL,
proprietario.SEXO,
proprietario.COMPLEMENTO,
proprietario.NUMERO
FROM
proprietario
INNER JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID


WHERE
proprietario.PROPRIETARIO_ID = ".$id);

*/
$prop_id  =  $q->querydata[0]['PROPRIETARIO_ID'];
$nome = $q->querydata[0]['NOME'];
$sobrenome = $q->querydata[0]['SOBRENOME'];
$cpf = $q->querydata[0]['CPF'];
$rg = $q->querydata[0]['RG'];
$cep = $q->querydata[0]['CEPFRANQUIA'];
$dob = $q->querydata[0]['DATA_NASCIMENTO'];
$end = $q->querydata[0]['ENDERECO'];
$cidade = $q->querydata[0]['CIDADE'];
$bairro = $q->querydata[0]['BAIRRO'];
$uf = $q->querydata[0]['ESTADO'];
$login =  $q->querydata[0]['LOGIN'];

$telcomercial =   $q->querydata[0]['TELCOMERCIAL'];
$telresidencial =  $q->querydata[0]['TELRESIDENCIAL'];
$telcelular =  $q->querydata[0]['TELCELULAR'];
$emailsecundario = $q->querydata[0]['EMAIL_SECUNDARIO'];
$complemento = $q->querydata[0]['COMPLEMENTO'];
$sexo =  $q->querydata[0]['SEXO'];


$_SESSION['pagAnterior'] = Flight::request()->url;
  
?>	<div class="container edituser-container">
	<div class="container fill">

		<h1>Editar Dados Cadastrais</h1>



        
        <div class="span7">
      <form class="form-horizontal" action="<?php echo  rootURL().'/proprietarios/do_editProprietarios' ?>" id="formEdit"  method="post" enctype="application/x-www-form-urlencoded" role="form">
  
        <input type="hidden" name="userid" value="<?php echo $prop_id; ?>">

  <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Nome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="nome" value="<?php echo formValue($nome); ?>"  id="cpNome" placeholder="Nome">
    </div>
  </div>
  <div class="form-group">
    <label for="cpSobrenome" class="col-sm-2 control-label">Sobrenome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="sobrenome" value="<?php echo formValue($sobrenome); ?>" id="cpSobrenome" placeholder="Seu Sobrenome">
    </div>
  </div>

     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">RG</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="rg" value="<?php echo formValue($rg); ?>" id="cpRg" placeholder="Numero do RG">
    </div>
  </div>
  
  
   <div class="form-group">
    <label for="cpCPF" class="col-sm-2 control-label">CPF</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="cpf" value="<?php echo formValue($cpf); ?>" id="cpCPF" placeholder="Seu CPF">
    </div>
  </div>

     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">Telefone de Contato:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="telcontato" value="<?php echo formValue($telresidencial); ?>" id="cpRg" placeholder="Telefone de Contato">
    </div>
  </div>


   <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">Telefone Comercial</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="telcomercial" value="<?php echo formValue($telcomercial); ?>" id="cpRg" placeholder="Telefone Comercial">
    </div>
  </div>

     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">Telefone Celular</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="telcelular" value="<?php echo formValue($telcelular); ?>" id="cpRg" placeholder="Telefone Celular">
    </div>
  </div>
  
  

  
   <div class="form-group">
    <label for="cpDOB" class="col-sm-2 control-label">Data de Nascimento</label>
    <div class="col-sm-10">
		<div class="campodocs" id="cpEditDateProprietarios" data-date="<?php echo formValueDate($dob); ?>" data-date-format="dd-mm-yyyy">
				<input type="text" class="span2   campotexto" date-calendar="true" name="dob" size="16" value="<?php echo formValueDate($dob); ?>"  type="text">
				<span class="add-on pull-right"><i class="glyphicon glyphicon-list-alt"></i></span>
			  </div>
        
    </div>
  	</div>


                <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">Sexo:</label>
    <div class="col-sm-10">
      <select id="cpPais" name="sexo" class="form-control  campotexto">
          <? echo sexo($sexo); ?>
        </select>

    </div>
  </div>

                <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">Estado Civil</label>
    <div class="col-sm-10">
      <select id="cpPais" name="estadocivil" class="form-control  campotexto">
         <? echo estadoCivil($estadocivil); ?>
        </select>

    </div>
  </div>
  
    
    
    <div id="validaCep" class="form-group">
    <label for="cpCep" class="col-sm-2 control-label">CEP</label>
    <div class="col-sm-10">
     	<div class="campodocs">
				<input class="span2  campocep" name="cep" id="cpCEP" value="<?php echo formValue($cep); ?>"  size="16" type="text">
                <a id="btBuscaCep" class="btn btn-primary btn-sm">Buscar CEP</a>
				<span id="buscaCepLoading" class="add-on pull-right"><img src="img/fbloader.gif" /> Buscando CEP</span>
			  </div>
    </div>
  </div>
  
     <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Endereço</label>
    <div class="col-sm-10">
      <input type="text" name="endereco" class="form-control campotexto" value="<?php echo formValue($end); ?>" id="cpEnd" placeholder="Endereço">
    </div>
  </div>
  
       <div class="form-group ">
    <label for="cpBairro" class="col-sm-2 control-label">Bairro</label>
    <div class="col-sm-10">
      <input type="text" name="bairro" class="form-control campotexto" id="cpBairro" value="<?php echo formValue($bairro); ?>" placeholder="Bairro">
    </div>
  </div>
       <div class="form-group">
    <label for="cpCidade" class="col-sm-2 control-label">Cidade</label>
    <div class="col-sm-10">
      <input type="text" name="cidade" class="form-control  campotexto"   value="<?php echo formValue($cidade); ?>" id="cpCidade" placeholder="Cidade">
    </div>
  </div>

           <div class="form-group ">
    <label for="cpBairro" class="col-sm-2 control-label">Complemento</label>
    <div class="col-sm-10">
      <input type="text" name="complemento" class="form-control campotel" id="cpBairro" value="<?php echo formValue($complemento); ?>" placeholder="Complemento">
    </div>
  </div>


             <div class="form-group ">
    <label for="cpBairro" class="col-sm-2 control-label">E-Mail Secundario (Opcional)</label>
    <div class="col-sm-10">
      <input type="text" name="emailsecundario" class="form-control campotexto" id="cpBairro" value="<?php echo formValue($emailsecundario); ?>" placeholder="E-Mail Secundario">
    </div>
  </div>


    <div class="form-group">
      <label for="cpPais" class="col-sm-2 control-label">Estado</label>
    <div class="col-sm-10">
      <select id="cpPais" name="estado" class="form-control  campotexto">
          <?php echo estadosBrasileiros(); ?>
        </select>
  </div>
  </div>

  
        <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">País</label>
    <div class="col-sm-10">
    	<select id="cpPais" name="pais" class="form-control  campotexto">
        	<?php echo showPaises(); ?>
        </select>

    </div>
  </div>
  
    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btSendForm" name="SendForm" class="btn btn-default">Atualizar</button>
    </div>
  </div>
</form>
</div>




<form class="form-horizontal" id="formCredenciais" action="<?php echo rootURL().'/proprietarios/do_updateCredentials' ?>"  method="post" enctype="application/x-www-form-urlencoded">


	<input type="hidden" value="edit-account" name="sendtype"/>
  <input type="hidden" name="userid" value="<?php echo $prop_id; ?>">

	<legend class="campotexto">Alterar Credenciais</legend>
	  <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Login:</label>
    <div class="col-sm-10">
      <input type="text" name="endereco" disabled class="form-control campotexto" value="<?php echo formValue($login); ?>" id="cpEnd" placeholder="Endereço">
    </div>
  </div>



    <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Senha:</label>
    <div class="col-sm-10">
      <input type="password" name="senha" class="form-control campotexto" id="cpEnd" placeholder="Senha">
    </div>
  </div>


   <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Repetir a Senha:</label>
    <div class="col-sm-10">
      <input type="password" name="repete_senha" class="form-control campotexto"  id="cpEnd" placeholder="Repetir Senha">
    </div>
  </div>


  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
     	<button class="btn btn-default" type="submit" >Atualizar Credenciais </button>
    </div>
  </div>


</form>



        
	</div>
</div>







<script>
		




	$(function(){
		
     $("input[name=telcontato]").mask("(00) 0000-0000");
     $("input[name=telcomercial]").mask("(00) 0000-0000");


     $("input[name=rg]").mask("00.000.000-A");
     $("input[name=cpf]").mask("000.000.000-00");


     var celulares = {onKeyPress:function(cel){

      var masks = ['(00) 0000-0000', '(00) 00000-0000'];

        var maskcel = cel.length > 10 ? masks[0] : masks[1];

        $('input[name=telcelular]').mask(maskcel,this)  

     }};

     $('input[name=telcelular]').mask('(00) 0000-0000', celulares);

		
    $("cpEditDateProprietarios").datepicker({

      onSelect: function(d,t){

        console.log(d);
        //$("cpEditDateProprietarios").html(d);

      }

    });


		$("#btBuscaCep").on('click',function(e){
	
		
			
		$(document).ajaxStart(function() {
            $("#buscaCepLoading").show(0.5);
        }).ajaxStop(function() {
            $("#buscaCepLoading").hide(0,5);
        });
			
		var cep = $("#cpCEP").val();
		var cep_end = "<?php echo rootURL().'/busca_cep/' ?>" + cep; 
			
		$.ajax({
        url: cep_end,
        type: "get",
		dataType:"json",
        
		
		
		
        success: function(dados){
            

			
				$("#cpEnd").val(dados.logradouro);
				$("#cpBairro").val(dados.bairro);
				$("#cpCidade").val(dados.localidade);
				$("#status").html(dados);
	
			
        },
        error:function(){
            //alert("failure");
            //$("#result").html('There is error while submit');
        }
    }); // fimajax
	
		});
});
			
</script>

