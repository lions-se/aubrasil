<?php

date_default_timezone_set('America/Sao_Paulo');

if($_SERVER['REQUEST_METHOD'] == "POST"){

		$limpacpf = array('-','.');
		$limparg = array('.','-');

		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$cpf  = str_replace($limpacpf, '', $_POST['cpf']) ;
		$rg   = str_replace($limpacpf, '', $_POST['rg']) ;
		$dob = $_POST['dob'];
		$cep = $_POST['cep'];
		$end = $_POST['endereco'];
		$bairro = $_POST['bairro'];
		$cidade = $_POST['cidade'];
		$pais = $_POST['pais'];
		$estado = $_POST['estado'];

		$sexo = $_POST['sexo'];
		$estadocivil = !empty($_POST['estadocivil']) ? $_POST['estadocivil'] : 'M'   ;
		$complemento = !empty($_POST['complemento']) ? $_POST['complemento'] : 'casa'; 
		$emailsecundario   = !empty($_POST['emailsecundario']) ? $_POST['emailsecundario'] : 'noemail@email.com';


		$login = $_POST['login'];
		$senha = $_POST['senha'];
		$repete = $_POST['repete_senha'];


		$telcomercial = $_POST['telcomercial'];
		$telresidencial = $_POST['telcontato'];
		$telcelular = $_POST['telcelular'];

		$hash_sn = BCrypt::hash($senha,8);
		$hash_rep = BCrypt::hash($repete,8); 



		$registro = date("Y-m-d H:i:s");



			$checkRG = $db-$db->query2("SELECT EXISTS(SELECT 1 FROM proprietario WHERE RG = :rg) as EXISTE", array(':rg'=>$rg));
			$checkEmail= $db->query2("SELECT EXISTS(SELECT 1 FROM proprietario WHERE EMAIL = :email) as EXISTE", array(':email'=>$login));
			
			
			
			
			
			
			if(!checkPost($estadocivil)){

			alert("Erro:", "Preencha corretamente seu estado Civil");
			flash();
			}else {
				
				$data['EMAIL_SECUNDARIO'] = $emailsecundario;
			
			}



			if($checkRG->querydata[0]['EXISTE'] == 1){

				alert('Houve um Problema','RG Ja Existente na base de dados', ALERTA_ERRO);
				flash();
				header("location: index.php?opcao=listar-proprietarios&cadastrar=true");


			}else if(empty($rg)){
				alert('Houve um Problema','Campo de RG Vazio', ALERTA_ERRO);
				flash();
				header("location: index.php?opcao=listar-proprietarios&cadastrar=true");
			}


			




				if($checkEmail->querydata[0]['EXISTE']  == 1){

					alert('Houve um Problema','Email de Login Ja Existente na base de dados');
					flash();
					header("location: index.php?opcao=listar-proprietarios&cadastrar=true");

				}else if(empty($login)){

					alert('Houve um Problema','Campo E-Mail Vazio', ALERTA_ERRO);
					flash();
					header("location: index.php?opcao=listar-proprietarios&cadastrar=true");
				}


		if(!checkPost($login) || !checkPost($senha) || !checkPost($repete) ){

			


			alert("Erro: ", "Login, Senha e Repetir Senha são campos Obrigatorios");
			flash();

		}else if(!BCrypt::check($senha, $hash_rep)){

				alert("Erro: ", "O Campo de repetir senha não coincide com a senha digitada");
				flash();
			
		}else if(!matchEmail($login)){

			    alert("Erro: ", "Insira um E-mail valido");
			    flash();


		}else if(!checkPost($cpf) || !checkPost($rg)){

			alert("Erro: ", "Obrigatorio Preencher campo  CPF e RG");
			flash();


		}else if(!checkPost($nome) || ! checkPost($sobrenome)){


			alert("Erro: ", "Prencha Nome e Sobrenome");
			flash();


		}else if(!checkPost($cep)){

			alert("Erro:", "Por Favor o Preencha o CEP");
			flash();

		}else if(!checkPost($telresidencial)){
				alert("Houve um Problema", "Adicione um telefone para contato");
		}else if(!checkPost($telresidencial)){
				$telcomercial = ' ' ;
		}else if(!checkPost($telcomercial)){
				$telcomercial = ' ';
		}else {






			$data = array('NOME' => $nome,
										  'SEXO' => $sexo,
										  'TELCOMERCIAL' => $telcomercial,
										  'TELRESIDENCIAL' => $telresidencial,
										  'TELCELULAR' => $telcelular,
										  'ESTADO_CIVIL' => $estadocivil,
										  'PERMISSAO_ID' => 2,
										  'SOBRENOME' => $sobrenome,
										  'CPF' => $cpf,
										  'RG'  => $rg,
										  'DATA_NASCIMENTO' => date_convert($dob),
										  'CEP' => $cep,
										  'ENDERECO' => $end,
										  'CIDADE' => $cidade,
										  'ESTADO' => $estado,
										  'EMAIL' => $login,
										   'DATA_REGISTRO' => $registro,
										   'PAIS' => $pais,
										  'PASSWD' => $hash_sn );


			if(checkPost($complemento)){
				
				$data['COMPLEMENTO'] = $complemento;	
			}


			if(checkPost($bairro)){
				
				$data['BAIRRO'] = $bairro;
			}

			
			$db->insert('proprietario',$data);

		

			alert("Sucesso: ","Foi Criado um proprietario Com Sucesso" );
			header("location: ?opcao=listar-proprietarios");
			
		}
		


	

	//alert("Sucesso", "Foi Cadastrado com Sucesso!");
	//header("location: ?opcao=listar-proprietarios");



}

?>