	<?

		//include("do_cadastraProprietario.php");

	?>

	<div class="container edituser-container">
	<div class="container fill">

		<h1>Cadastrar um Novo Usuario</h1>
        
        <div class="row">
      <form class="form-horizontal"  id="formEdit"  method="post" enctype="multipart/form-data" role="form">
  <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Nome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="nome" value="<?php echo formValue($nome); ?>"  id="cpNome" placeholder="Nome">
    </div>
  </div>

    <div class="form-group">
    <label for="cpSlogan" class="col-sm-2 control-label">Descriçao</label>
    <div class="col-sm-10">
      <textarea id="cpSlogan" name="descricao" rows="4" cols="1" class="form-control"><?php echo formValue($slogan); ?></textarea>
    </div>
  </div>

  <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Miniatura do Arquivo:</label>
            <div class="col-sm-10">
                <input type="file"  data-classButton="btn btn-primary"  class="filestyle" name="miniatura" value="<?php echo formValue($franqnome); ?>" placeholder="Nome">
            </div>
          </div>


          <div class="form-group ">
            <label for="cpNome" class="col-sm-2 control-label">Arquivo (Limite de Upload de 20MB)</label>
            <div class="col-sm-10">
                <input type="file" class="form-control campotexto" name="arquivo" value="<?php echo formValue($franqnome); ?>"   placeholder="Nome">
            </div>
          </div>


  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btSendForm" name="SendForm" class="btn btn-primary">Cadastrar</button>
    </div>
  </div>
</form>
</div>
        
	</div>
</div>



<script>
	$(function(){
		
		
		 $("input[name=telcontato]").mask("(00) 0000-0000");
     $("input[name=telcomercial]").mask("(00) 0000-0000");


     $("input[name=rg]").mask("00.000.000-A");
     $("input[name=cpf]").mask("000.000.000-00");


     var celulares = {onKeyPress:function(cel){

      var masks = ['(00) 0000-0000', '(00) 90000-0000'];

        var maskcel = cel.length > 9 ? masks[1] : masks[0];

        $('input[name=telcelular]').mask(maskcel,this) ;
        console.log(cel.length);

     }};

     $('input[name=telcelular]').mask('(00) 00000-0000', celulares);

		//event.preventDefault();
			
		$("#btBuscaCep").on('click',function(e){
	
		
			
		$(document).ajaxStart(function() {
            $("#buscaCepLoading").show(0.5);
        }).ajaxStop(function() {
            $("#buscaCepLoading").hide(0,5);
        });
			
		var cpf = $("#cpCEP").val();
		var cep_end = "cep.php"; 
			
		$.ajax({
        url: cep_end,
        type: "get",
		dataType:"json",
        data:  {'cep':cpf},
		
		
		
        success: function(dados){
            
			
			
				$("#cpEnd").val(dados.logradouro);
				$("#cpBairro").val(dados.bairro);
				$("#cpCidade").val(dados.localidade);
				$("#status").html(dados);
	
			
        },
        error:function(){
            //alert("failure");
            //$("#result").html('There is error while submit');
        }
    }); // fimajax
	
		});




        $("cpDob").datepicker({
      dateFormat: 'dd-mm-yy',
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
      nextText: 'Próximo',
      prevText: 'Anterior',
      changeMonth: true,
      changeYear: true,
      yearRange: '1910:2014'

    });
	        
	});

		
			
</script>

