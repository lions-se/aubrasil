<nav class="navbar navbar-inverse navbar-fixed-top topo-admin" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<? echo rootURL(); ?>"><?php echo ADM_EMPRESA; ?></a>
  </div>

  


  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      
       <? if(Utils::getPermission() > 0 ){ ?>
      <li><a href="<?php echo rootURL(); ?>/marketing">Marketing</a></li>
      <? } ?>
  
      <? if(Utils::getPermission() == 1){ ?>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Franquias<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<? echo rootURL().'/franquias/cadastrar' ?>">Criar uma  Nova Franquia</a></li>
          <li><a href="<? echo rootURL().'/franquias' ?>">Gerenciar Franquias</a></li>

        </ul>
      </li>
   


      
       <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produtos<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo rootURL().'/produtos/adicionar' ?>">Adicionar Produtos</a></li>
          <li><a href="<?php echo rootURL().'/produtos' ?>">Gerenciar Produtos</a></li>
        </ul>
      </li>




       <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Proprietarios<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo rootURL().'/proprietarios/adicionar' ?>">Adicionar Proprietarios</a></li>
          <li><a href="<?php echo rootURL().'/proprietarios' ?>">Gerenciar Proprietarios</a></li>
        </ul>
      </li>




       <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Veículos<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo rootURL().'/veiculos/adicionar'; ?>">Cadastrar Veiculos</a></li>
          <li><a href="<?php echo rootURL().'/veiculos'; ?>">Listar Veículos Cadastrados</a></li>
        </ul>
      </li>

 <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Outros<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo rootURL().'/conteudo-estatico'; ?>">Editar Conteudo Estatico</a></li>
          <li class="divider"></li>
          <li><a href="<?php echo rootURL().'/categorias'; ?>">Listar Categorias</a></li>
          <li><a href="<?php echo rootURL().'/subcategorias'; ?>">Listar Subcategorias</a></li>
          <li class="divider"></li>
          <li><a href="<?php echo rootURL().'/classificados'; ?>">Listar Classificados</a></li>
          <li><a href="<?php echo rootURL().'/classificados/adicionar'; ?>">Adicionar Classificado</a></li>

 

        

        </ul>
      </li>



       <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuários Administrativos<b class="caret"></b></a>
        <ul class="dropdown-menu">
          <!--<li><a href="index.php?opcao=listar-usuarios&editar=true">Adicionar Usuario</a></li>-->
          <!--<li><a href="index.php?opcao=listar-usuarios">Listar Usuarios</a></li>-->
          <!--<li class="divider"></li>-->

          <li><a href="<?php echo rootURL().'/clientes'; ?>">Listar Clientes</a></li>
          <li><a href="<?php echo rootURL().'/pedidos'; ?>">Listar Pedidos Feitos</a></li>
          

        </ul>
      </li>
      
      <? } ?>



    </ul>

    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><? echo isset($_SESSION['loginCredentials']) ? ($_SESSION['loginCredentials']->nome) : 'Usuario Desconhecido';   ?><b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo rootURL().'/editar_dados' ?>">Editar Dados</a></li>
          <li><a href="#">Planos Franqueado</a></li>
          <li class="divider"></li>
          <li><a href="<?php echo rootURL().'/logout'; ?>">Sair do Painel</a></li>
        </ul>
      </li>
    </ul>
  </div><!-- /.navbar-collapse -->
</nav>
