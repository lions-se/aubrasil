<div class="container">

    <h1> O Que Deseja Fazer?..</h1>
    <div class="row">
	   

       <div class="col-xs-2">
            <?php if(Utils::getPermission() == 1 ){ ?>
            <a href="<?php echo genLinks(array('Franquias')); ?>" class="btn btn-primary btn-lg">
        	   Franquias
            </a>
             <h5 class="text-left text-muted">Adicionar e Remover Franquias</h5>
            <? }else { ?>
            <a href="<?php echo genLinks(array('minha_franquia')); ?>" class="btn btn-primary btn-lg">
                Minha Franquia
            </a>
            <h5 class="text-left text-muted">Visualizar  e Editar Dados da Minha Franquia</h5>
            <? } ?>
            
        </div>


                <div class="col-xs-4">
            <a href="<?php echo genLinks(array('marketing')); ?>" class="btn btn-primary btn-lg">
                Materiais para Marketing
            </a>
        <h5 class="text-left text-muted">Adicione / Remova  Materiais Para Marketing da Franquia</h5>
        </div>
    </div>
    	

        

         <?php if(Utils::getPermission() == 1 ){ ?>
        <div class="col-xs-2">
            <a href="<?php echo genLinks(array('proprietarios')); ?>" class="btn btn-primary btn-lg">
        	   Proprietários
            </a>
            <h5 class="text-left text-muted">Proprietários Cadastrados</h5>
        </div>
    	



        <div class="col-xs-2">
            <a href="<?php echo genLinks(array('produtos')); ?>" class="btn btn-primary btn-lg">
                Produtos
            </a>
            <h5 class="text-left text-muted">Adicione Remova e Produtos para toda a franquia</h5>
        </div>
    
    
    



    <div class="row">

           <div class="col-xs-2">
            <a href="<?php echo genLinks(array('editar_dados')); ?>" class="btn btn-primary btn-lg">
            Ver Meu Perfil
        </a>
                <h5 class="text-left text-muted">Editar Perfil do Franqueado / Usuario</h5>
    </div>


          <div class="col-xs-2">
            <a href="<?php echo genLinks(array('veiculos')); ?>" class="btn btn-primary btn-lg">
              Veículos
            </a>
            <h5 class="text-left text-muted">Cadastro de Veículos</h5>
        </div>
        <? } ?>

     <div class="col-xs-10">
        &nbsp;
    </div>


</div>