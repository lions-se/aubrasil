<?php
	ob_start();
	session_start();
	require_once("../config.php");
	include("adminconf.inc.php");

	include("header.php");
	include("top.php");


	if($_SESSION['role'] != 1){

		alert("Houve Um Problema!", "Voce nao term permissão de acesso");
		header("location: index.php");
	}


	$users = "SELECT
proprietario.PROPRIETARIO_ID,
franquia.FRANQUIA_ID,
permissao.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.EMAIL
FROM
proprietario
LEFT JOIN franquia ON franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
LEFT JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID
WHERE franquia.FRANQUIA_ID IS NULL
";

$cont = $db->query2($users);




?>



  <div class="container fill">

  	<div class="container main-container">

      <?
      if(isset($_SESSION['flash'])){
        echo flash();
        kill_alert();
      }

     ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
                Please Wait...</div>
            <div class="alert alert-success" style="display:none;">
                <span class="glyphicon glyphicon-ok"></span> Drag table row and cange Order</div>
            <table class="table" class="campotexto">
                <thead>
                    <tr>
                        <th>
                            Nome
                        </th>
                        <th>
                           Sobrenome
                        </th>
                        <th>
                           E-mail:
                        </th>
                    </tr>
                </thead>
                <tbody>

                	<? foreach($cont->querydata as $data){


                		$id = $data['PROPRIETARIO_ID'];
                		$nome = $data['NOME'];
                		$sobrenome = $data['SOBRENOME'];
                		$email = $data['EMAIL'];




                	 ?>

                    <tr class="active">

               			<?php  if($id != 1){ ?>
                        <td>
                          <a href="index.php?opcao=listar-proprietarios&editar=<?php echo $id; ?>">  <?php echo $nome; ?></a>
                          &nbsp;&nbsp;&nbsp;<a href="index.php?opcao=listar-franquias&cadastrar=true&adduser=<?php echo $id; ?>">Corrigir Franquia</a>
                        </td>
                        <td>
                            <?php echo $sobrenome;  ?>
                        </td>
                        <td>
                           <?php echo $email;  ?>
                        </td>
                    </tr>

                    <? 

                    	}

                } ?>
                 
                   
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
	
	(function ($) {
    $.fn.extend({
        tableAddCounter: function (options) {

            // set up default options 
            var defaults = {
                title: '#',
                start: 1,
                id: false,
                cssClass: false
            };

            // Overwrite default options with user provided
            var options = $.extend({}, defaults, options);

            return $(this).each(function () {
                // Make sure this is a table tag
                if ($(this).is('table')) {

                    // Add column title unless set to 'false'
                    if (!options.title) options.title = '';
                    $('th:first-child, thead td:first-child', this).each(function () {
                        var tagName = $(this).prop('tagName');
                        $(this).before('<' + tagName + ' rowspan="' + $('thead tr').length + '" class="' + options.cssClass + '" id="' + options.id + '">' + options.title + '</' + tagName + '>');
                    });

                    // Add counter starting counter from 'start'
                    $('tbody td:first-child', this).each(function (i) {
                        $(this).before('<td>' + (options.start + i) + '</td>');
                    });

                }
            });
        }
    });
})(jQuery);


</script>
<?php
	include("footer.php");
	ob_end_flush();
?>