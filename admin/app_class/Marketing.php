<?

class Marketing {

	public static function listMarketing(){

		

		//var_dump($_SESSION['loginCredentials']);

			
			

		$db = Flight::get('db');
		$pag = Flight::Pagina();

		$query = "SELECT
marketing.MARKETING_ID,
marketing.NOME,
marketing.DESCRICAO,
marketing.PATH
FROM
marketing";
		$pag->setPages('marketing');
		$query = $pag->retrieveData($query);

		//echo "TESTE";
		Flight::render('marketing/marketing.php',array('q' => $query,'p' => $pag),'conteudo');
		Flight::render('main.php');
	}


	public static function cadastraMarketing(){

			if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(rootURL().'/login');
		}


		Flight::render('marketing/adicionar.php', array(),'conteudo');
		Flight::render('main');	
	}

	public static function processCadastraMarketing(){

			if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

		 
		$upload = new Upload(realpath('uploads/arquivos') , array( 
			'ext_allowed' => array('jpg','png','jpeg', 'psd', 'cdr', 'xpi', 'bmp', 'mp3'),
			'max_size' => "500K"	), $_FILES);


		$images = $upload->doUploadResize("miniatura");
		$arquivo = $upload->doUpload('arquivo');


		$db->insert('marketing', array('NOME' => $_POST['nome'],
								       'DESCRICAO' => $_POST['descricao'],
								       'PATH' => $arquivo,
								       'MINIATURA' => $images));


		alert('Sucesso', sprintf("Novo Arquivo: %s  Adicionado com Sucesso!", $_POST['nome'] ) );
		Flight::redirect(rootURL().'/marketing');


	}


	public static function editaMarketing($id){

		$db = Flight::get('db');

		$query = "SELECT
marketing.MARKETING_ID,
marketing.NOME,
marketing.DESCRICAO,
marketing.MINIATURA,
marketing.PATH
FROM
marketing
WHERE marketing.MARKETING_ID = :id";
	
	$q = $db->query2($query, array(':id' => $id));

	Flight::render('marketing/editar.php', array('dados' => $q->querydata[0]), 'conteudo');
	Flight::render('main');


	}


	public static function doEditaMarketing($id){

		$db  = Flight::get('db');

		$db->update('marketing', array('NOME' => $_POST['nome'],
										'DESCRICAO' => $_POST['descricao']), 'MARKETING_ID = '.$id);

		alert('sucesso', "Foi Editado Com Sucesso!");
		Flight::redirect(rootURL().'/marketing');	

	}


	public static function apagaMarketing($id){

				$db = Flight::get('db');

		$query = "SELECT
marketing.MARKETING_ID,
marketing.NOME,
marketing.DESCRICAO,
marketing.MINIATURA,
marketing.PATH
FROM
marketing
WHERE marketing.MARKETING_ID = :id";
	
	$q = $db->query2($query, array(':id' => $id));

	Flight::render('marketing/apagar.php', array('dados' => $q->querydata[0]), 'conteudo');
	Flight::render('main');



	}

	public static function doApagaMarketing($id){

			$db  = Flight::get('db');


		$query = "SELECT
marketing.MARKETING_ID,
marketing.NOME,
marketing.DESCRICAO,
marketing.MINIATURA,
marketing.PATH
FROM
marketing
WHERE marketing.MARKETING_ID = :id";

		
		$q  = $db->query2($query, array(':id' => $id ));


		$db->delete('marketing', 'MARKETING_ID = '.$id);

		alert('sucesso', "Foi Apagado Com Sucesso!");
		Flight::redirect(rootURL().'/marketing');	

	}

}

?>