<?php

class Estatico {
	public static function index(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		Flight::render('estatico/index.php', array(), 'conteudo');
		Flight::render('main');

	}


	public static function pagEditaFranquia(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

		$query = "SELECT conteudo_id, conteudo_afranquia FROM conteudo_estatico WHERE conteudo_id = 1";

		$q = $db->query2($query);


		Flight::render('estatico/edit-franquia.php', array('dados' => $q->querydata[0]['conteudo_afranquia']), 'conteudo');
		Flight::render('main');

	}

	public static function processEditaFranquia(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');


		$db->update('conteudo_estatico', array('CONTEUDO_AFRANQUIA' => $_POST['texto']), 'conteudo_id =  1');

		alert('Sucesso', 'Franquia Foi Editado Com Sucesso');
		Flight::redirect(Flight::request()->url);

		//$db->update2('conteudo_estatico', array('CONTEUDO_AFRANQUIA' => $_POST['texto']), 'conteudo_id = :id', array(':id' => 1));
	
		//alert('Sucesso', 'Franquia Foi Editado Com Sucesso');
		//Flight::redirect(Flight::request()->url);

		//return Flight::json(array('sucesso' => true));

	}


	public static function pagEditaEmpresa(){
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');

		$query = "SELECT conteudo_id, conteudo_aempresa FROM conteudo_estatico WHERE conteudo_id = 1";

		$q = $db->query2($query);


		Flight::render('estatico/edit-franquia.php', array('dados' => $q->querydata[0]['conteudo_aempresa'], 'nome' => 'A Empresa'), 'conteudo');
		Flight::render('main');


	}

	public static function processEditaEmpresa(){
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}
		$db = Flight::get('db');



		//$db->update2('conteudo_estatico', array('CONTEUDO_AEMPRESA' => $_POST['texto']), 'conteudo_id = :id', array(':id' => 1));
		$db->update('conteudo_estatico', array('CONTEUDO_AEMPRESA' => $_POST['texto']), 'conteudo_id =  1');

		alert('Sucesso', 'Franquia Foi Editado Com Sucesso');
		Flight::redirect(Flight::request()->url);



		//var_dump($_POST['texto']);
		//$db->update('conteudo_estatico', array('CONTEUDO_AEMPRESA' => $_POST['texto']), 'conteudo_id =  1');

		//return Flight::json(array('sucesso' => true));

	}


	public static function pagEditaTreina(){
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');

		$query = "SELECT conteudo_id, conteudo_treina FROM conteudo_estatico WHERE conteudo_id = 1";

		$q = $db->query2($query);


		Flight::render('estatico/edit-franquia.php', array('dados' => $q->querydata[0]['conteudo_treina'], 'nome' => 'Treinamentos'), 'conteudo');
		Flight::render('main');


	}

	public static function processEditaTreina(){
		
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');


		$db->update('conteudo_estatico', array('CONTEUDO_TREINA' => $_POST['texto']), 'conteudo_id =  1');

		alert('Sucesso', 'Franquia Foi Editado Com Sucesso');
		Flight::redirect(Flight::request()->url);

	}



	public static function pagEditaServicos(){
		
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

		$query = "SELECT conteudo_id, conteudo_servicos FROM conteudo_estatico WHERE conteudo_id = 1";

		$q = $db->query2($query);


		Flight::render('estatico/edit-franquia.php', array('dados' => $q->querydata[0]['conteudo_servicos'], 'nome' => 'Serviços'), 'conteudo');
		Flight::render('main');


	}

	public static function processEditaServicos(){
		
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');


		$db->update('conteudo_estatico', array('CONTEUDO_SERVICOS' => $_POST['texto']), 'conteudo_id =  1');

		alert('Sucesso', 'Franquia Foi Editado Com Sucesso');
		Flight::redirect(Flight::request()->url);

	}


	public static function pagEditaSuaFranquia(){
		
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

		$query = "SELECT conteudo_id, conteudo_suafranquia FROM conteudo_estatico WHERE conteudo_id = 1";

		$q = $db->query2($query);


		Flight::render('estatico/edit-franquia.php', array('dados' => $q->querydata[0]['conteudo_suafranquia'], 'nome' => 'Sua Franquia'), 'conteudo');
		Flight::render('main');


	}

	public static function processEditaSuaFranquia(){
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');


		$db->update('conteudo_estatico', array('CONTEUDO_SUAFRANQUIA' => $_POST['texto']), 'conteudo_id =  1');

		alert('Sucesso', 'Sua Franquia Foi Editado Com Sucesso');
		Flight::redirect(Flight::request()->url);
	}


	public static function pagEditaTermos(){
		
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

		$query = "SELECT conteudo_id, CONTEUDO_TERMOS FROM conteudo_estatico WHERE conteudo_id = 1";

		$q = $db->query2($query);


		Flight::render('estatico/edit-franquia.php', array('dados' => $q->querydata[0]['CONTEUDO_TERMOS'], 'nome' => 'Termos'), 'conteudo');
		Flight::render('main');


	}

	public static function processEditaTermos(){

		if(!Utils::checkUser(true)){
			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');


		$db->update('conteudo_estatico', array('CONTEUDO_TERMOS' => $_POST['texto']), 'conteudo_id =  1');

		alert('Sucesso', 'Termos de Serviços a Foi Editado Com Sucesso');
		Flight::redirect(Flight::request()->url);

	}

}

?>