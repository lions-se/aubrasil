<?


class Subcategoria {



	public static function index(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		//$db = Flight::get('db');
		$pag = Flight::Pagina();
		$query = "SELECT
subcategoria.NOME AS SUBCAT_NOME,
categoria.NOME AS CAT_NOME,
subcategoria.NOMEINTERNO,
subcategoria.SUBCATEGORIA_ID
FROM
categoria_subcategoria
LEFT JOIN categoria ON categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
LEFT JOIN subcategoria ON categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID";
	$pag->setPages('subcategoria');
	$q = $pag->retrieveData($query);

	Flight::render('subcategorias/listar.php', array('q' => $q, 'pag' => $pag), 'conteudo');
	Flight::render('main.php');

	}


	public static function attrCategoria($subcategoria_id){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');
		$query = "SELECT
categoria.CATEGORIA_ID,
categoria.NOME,
categoria.NOMEINTERNO,
categoria.URL
FROM
categoria
";

		$q = $db->query2($query);



		Flight::render('subcategorias/edit_categoria.php', array('dados' => $q->querydata, 'atualSubcategoria' => $subcategoria_id  ), 'conteudo');
		Flight::render('main');
	}


	public static function processAttrCategoria($subcat_id){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');

		$db->update2('categoria_subcategoria', array('CATEGORIA_ID' => $_POST['categoria']), 'SUBCATEGORIA_ID = :sub_id', array(':sub_id' => $subcat_id) );
		alert('Sucesso', sprintf("Subcategoria Cadastrada com sucesso"));
		Flight::redirect(sprintf("%s/subcategorias/", rootURL()));


	}


	public static function pagCadastraSubCategoria(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}



		$db = Flight::get('db');
		$query = "SELECT
categoria.CATEGORIA_ID,
categoria.NOME,
categoria.NOMEINTERNO,
categoria.URL
FROM
categoria
";

		$q = $db->query2($query);

		Flight::render('subcategorias/cadastro.php', array('dados' => $q->querydata), 'conteudo');
		Flight::render('main');


	}







	public static function processCadastraSubCategoria(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');

		$lastid = $db->insert('subcategoria', array('NOME' => $_POST['nome'], 'NOMEINTERNO' => $_POST['nomeinterno']));
		$db->insert('categoria_subcategoria', array('CATEGORIA_ID' => $_POST['categoria'], 'SUBCATEGORIA_ID' => $lastid));
		alert('Sucesso', sprintf("Subcategoria Cadastrada com sucesso"));
		Flight::redirect(sprintf("%s/subcategorias/", rootURL()));

	}


	public static function processaRemoveSubCategoria(){

	}

	public static function removeSubCategoria(){



	}


	public static function editaSubcategoria($sub_id){

			$db = Flight::get('db');

		$query = "SELECT
subcategoria.NOME AS SUBCAT_NOME,
subcategoria.NOMEINTERNO,
subcategoria.SUBCATEGORIA_ID,
categoria.CATEGORIA_ID,
categoria.NOME as CAT_NOME
FROM
categoria_subcategoria
LEFT JOIN categoria ON categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
LEFT JOIN subcategoria ON categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE subcategoria.SUBCATEGORIA_ID= :id";

		$q = $db->query2($query, array(':id' => $sub_id ));


		$cat_query = "SELECT
categoria.CATEGORIA_ID,
categoria.NOME,
categoria.NOMEINTERNO
FROM
categoria";
		
		$categorias = $db->query2($cat_query);

		Flight::render('subcategorias/edit_subcategoria.php', array('dados' => $q->querydata, 'categoria' =>$categorias ), 'conteudo');
		Flight::render('main');
	} 


}

?>