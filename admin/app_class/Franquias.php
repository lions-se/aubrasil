<? 



class Franquias {

	public static function index(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$pag = Flight::Pagina();

		$q_string = "SELECT
franquia.PROPRIETARIO_ID,
proprietario.PROPRIETARIO_ID,
permissao.PERMISSAO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.LOGIN,
proprietario.CPF,
proprietario.RG,
franquia.NOMEFRANQUIA,
franquia.SLOGAN,
franquia.TELFRANQUIA,
franquia.FRANQUIA_ID
FROM
franquia
INNER JOIN proprietario ON franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
INNER JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID";

	$q = $pag->retrieveData($q_string);

	Flight::render('franquias/listar.php', array('q' => $q, 'pag' => $pag),'conteudo');
	Flight::render('main');

}


public static function showFranquias($franquia_id)
{

	if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
	}



	$db = Flight::get('db');

	$query = "SELECT
franquia.FRANQUIA_ID,
franquia.PROPRIETARIO_ID,
franquia.NOMEFRANQUIA,
franquia.SLOGAN,
franquia.TELFRANQUIA,
franquia.CEPFRANQUIA,
franquia.ENDERECO,
franquia.CIDADE,
franquia.BAIRRO,
franquia.ESTADO,
franquia.ENDERECO_SMTP,
franquia.LOGIN_SMTP,
franquia.EMAIL,
franquia.SENHA_SMTP,
proprietario.PROPRIETARIO_ID,
proprietario.NOME,
proprietario.SOBRENOME
FROM
franquia
INNER JOIN proprietario ON franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
WHERE franquia.FRANQUIA_ID = :id";


$q = $db->query2($query,array('id' => $franquia_id));
	
	
	Flight::render('franquias/editaFranquia.php', array('q' => $q), 'conteudo');
	Flight::render('main');



	}



	public static function cadastraFranquia(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		Flight::render('franquias/cadastraFranquias.php', array(),'conteudo');
		Flight::render('main');
	}


	public static function cadastraProcessForm(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$franqnome = $_POST['nome'];
		$franqslogan = $_POST['slogan'];
		$franqtel = $_POST['telefone'];
		$cep = str_replace(array('.','-'),'', $_POST['cep']);
		
		$db = Flight::get('db');

		if(empty($franqnome)){

			alert("Houve um Problema", "Necessita Inserir um nome para franquia", ALERTA_ERRO);
			//header("location: index.php?opcao=listar-franquias&cadastrar=true");
			Flight::redirect(rootURL().'/franquias');
		}


		if(empty($franqslogan)){

			alert("Houve um Problema", "Necessita Inserir um slogan para franquia", ALERTA_ERRO);
			//header("location: index.php?opcao=listar-franquias&cadastrar=true");
			Flight::redirect(rootURL().'/franquias');
		}

		if(empty($cep)){

			alert("Houve um Problema", "Necessita Inserir um CEP para franquia", ALERTA_ERRO);
			//header("location: index.php?opcao=listar-franquias&cadastrar=true");
			Flight::redirect(rootURL().'/franquias');
		}



		$data = array(
							'PROPRIETARIO_ID' => intval($_POST['propid']),
							'NOMEFRANQUIA'=> $_POST['nome'],
						  	'SLOGAN'=> $_POST['slogan'],
						    'TELFRANQUIA' => $_POST['telefone'],
						    'CEPFRANQUIA' => $cep,
						    'ENDERECO' => $_POST['endereco'],
						    'BAIRRO' => $_POST['bairro'],
						    'CIDADE' => $_POST['cidade'],
						    'ESTADO' => $_POST['estado'],
						    'EMAIL' =>$_POST['email'],
						    'ENDERECO_SMTP' => $_POST['endereco_smtp'] ,
							'LOGIN_SMTP'=> $_POST['login_smtp'],
							'SENHA_SMTP' => $_POST['senha_smtp'],

						   
						   );

		


		var_dump($data);

		$db->insert('franquia', $data);
		alert('Sucesso', "Franquia Cadastrada com Sucesso");
		Flight::redirect(rootURL().'/franquias');

	}


	public static function deleteFranquia($franquia_id){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}



		$db = Flight::get('db');


		$query = "SELECT
franquia.NOMEFRANQUIA,
franquia.SLOGAN,
franquia.TELFRANQUIA,
franquia.FRANQUIA_ID,
franquia.CEPFRANQUIA,
franquia.ENDERECO,
franquia.BAIRRO,
franquia.CIDADE,
franquia.ESTADO,
franquia.PROPRIETARIO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.RG,
proprietario.CPF,
permissao.PERMISSAO_ID
FROM
franquia
LEFT JOIN proprietario ON franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
LEFT JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID
WHERE franquia.FRANQUIA_ID = :franquia_id
GROUP BY
franquia.PROPRIETARIO_ID";




	$q = $db->query2($query, array(':franquia_id' => $franquia_id));


	Flight::render('franquias/deleteFranquia.php', array('q'  => $q, 'conteudo'));
	Flight::render('main');


			
	}


	public static function deleteProcessForm($franquiaid){

		
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$id = $_POST['userid'];

		$db = Flight::get('db');

		$db->update('franquia', array('PROPRIETARIO_ID' => NULL ), 'FRANQUIA_ID = '.$id);

		$db->delete('franquia','FRANQUIA_ID = '.$franquiaid);
		alert('Sucesso', 'Franquia Deletada com Sucesso');
		Flight::redirect(rootURL().'/franquias');

	}

	public static function minhaFranquia(){



			$db = Flight::get('db');

	$query = "SELECT
franquia.FRANQUIA_ID,
franquia.PROPRIETARIO_ID,
franquia.NOMEFRANQUIA,
franquia.SLOGAN,
franquia.TELFRANQUIA,
franquia.CEPFRANQUIA,
franquia.ENDERECO,
franquia.CIDADE,
franquia.BAIRRO,
franquia.ESTADO,
franquia.ENDERECO_SMTP,
franquia.LOGIN_SMTP,
franquia.EMAIL,
franquia.SENHA_SMTP,
proprietario.PROPRIETARIO_ID,
proprietario.NOME,
proprietario.SOBRENOME
FROM
franquia
LEFT JOIN proprietario ON franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
WHERE franquia.PROPRIETARIO_ID = :id
";


	$q = $db->query2($query,array(':id' => $_SESSION['loginCredentials']->user_id));
	
	if($q->rowsAffected() == 1){
	Flight::render('franquias/editaFranquia.php', array('q' => $q), 'conteudo');
	Flight::render('main');
	}else {

		alert('Aviso',"O Sr. Ainda Não possui nenhuma franquia", ALERTA_AVISO);
		Flight::redirect(rootURL().'/');

	}



	
	}


	public static function atualizaFranquias($franquia_num){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

		foreach ($_POST as $key => $value) {
			if(!empty($key) && !empty($value)){

				$cont[$key] = $value;

			}
		}

		if(!validateField($_POST['login_smtp']) || !validateField($_POST['endereco_smtp'])){
			alert('Aviso', "Credenciais de SMTP Estão vazias. Impossibilitando  de envio de e-mail pro cliente ao cadastrar.  e não recebimento  de e-mails de contato <br/>
				Considere Preencher os  campos  SMTP para faciltar seu trabalho.
				", ALERTA_AVISO);
			Flight::redirect(sprintf("%s/franquias/mostrar/%d", rootURL(), $franquia_num));
			//Flight::render('franquias/editaFranquia', array('dados' => $_POST), 'conteudo');
			//Flight::render('main');
			return;
	
		}
		if(!validateField($_POST['nome']) || !validateField($_POST['slogan'])){

			alert('Erro', 'Preencha Corretamente os Formularios de Nome e Slogan', ALERTA_ERRO);
			Flight::redirect(sprintf("%s/franquias/mostrar/%d", rootURL(), $franquia_num));
		}

		if(!validateField($_POST['telefone'])){

			alert('Erro', 'Preencha Corretamente o telefone da Franquia (Não Pode Estar Vazio)', ALERTA_ERRO);
			Flight::redirect(sprintf("%s/franquias/mostrar/%d", rootURL(), $franquia_num));
		}

		if(!validateField($_POST['cep'])){

			alert('Erro', 'Preencha Corretamente CEP. pois com o cep que sera calculado frete dos produtos', ALERTA_ERRO);
			Flight::redirect(sprintf("%s/franquias/mostrar/%d", rootURL(), $franquia_num));
		}

		$q = $db->update('franquia', 
		array('NOMEFRANQUIA'=> $cont['nome'], 
			'SLOGAN' => $cont['slogan'], 
			'TELFRANQUIA'=> $cont['telefone'],
			'CEPFRANQUIA' => $cont['cep'],
			'ENDERECO' => $cont['endereco'],
			'BAIRRO' => $cont['bairro'],
			'CIDADE' => $cont['cidade'],
			'ESTADO' => $cont['estado'],
			'EMAIL' =>$_POST['email'],
			'ENDERECO_SMTP' => $_POST['endereco_smtp'] ,
			'LOGIN_SMTP'=> $_POST['login_smtp'],
			'SENHA_SMTP' => $_POST['senha_smtp']


			),


			'FRANQUIA_ID = '.$franquia_num
			);

			

			alert('Sucesso', 'Sua Franquia Foi Atualizada com sucesso');
			Flight::redirect(rootURL().'/franquias');

	}



	public static function atualizaMinhaFranquia(){



		$db = Flight::get('db');

		$franquia_num = $_POST['userid'];

		foreach ($_POST as $key => $value) {
			if(!empty($key) && !empty($value)){

				$cont[$key] = $value;

			}
		}

		if(!validateField($_POST['login_smtp']) || !validateField($_POST['endereco_smtp']) || !validateField($_POST['senha_smtp'])){
			alert('Aviso', "Credenciais de SMTP Estão vazias. Impossibilitando  de envio de e-mail pro cliente ao cadastrar.  e não recebimento  de e-mails de contato <br/>
				Considere Preencher os  campos  SMTP faciltar seu trabalho.
				", ALERTA_AVISO);
			Flight::redirect(sprintf("%s/minha_franquia", rootURL(), $franquia_num));
			//Flight::render('franquias/editaFranquia', array('dados' => $_POST), 'conteudo');
			//Flight::render('main');
		}




		if(!validateField($_POST['nome']) || !validateField($_POST['slogan'])){

			alert('Erro', 'Preencha Corretamente os Formularios de Nome e Slogan', ALERTA_ERRO);
			Flight::redirect(sprintf("%s/minha_franquia", rootURL(), $franquia_num));
		}

		if(!validateField($_POST['telefone'])){

			alert('Erro', 'Preencha Corretamente o telefone da Franquia (Não Pode Estar Vazio)', ALERTA_ERRO);
			Flight::redirect(sprintf("%s/minha_franquia", rootURL(), $franquia_num));
		}

		if(!validateField($_POST['cep'])){

			alert('Erro', 'Preencha Corretamente CEP. pois com o cep que sera calculado frete dos produtos', ALERTA_ERRO);
			Flight::redirect(sprintf("%s/minha_franquia", rootURL(), $franquia_num));
		}

		$q = $db->update('franquia', 
		array('NOMEFRANQUIA'=> $cont['nome'], 
			'SLOGAN' => $cont['slogan'], 
			'TELFRANQUIA'=> $cont['telefone'],
			'CEPFRANQUIA' => $cont['cep'],
			'ENDERECO' => $cont['endereco'],
			'BAIRRO' => $cont['bairro'],
			'CIDADE' => $cont['cidade'],
			'ESTADO' => $cont['estado'],
			'EMAIL' =>$_POST['email'],
			'ENDERECO_SMTP' => $_POST['endereco_smtp'] ,
			'LOGIN_SMTP'=> $_POST['login_smtp'],
			'SENHA_SMTP' => $_POST['senha_smtp']


			),

			'FRANQUIA_ID = '.$franquia_num);

			

			alert('Sucesso', 'Sua Franquia Foi Atualizada com sucesso');
			Flight::redirect(rootURL().'/minha_franquia');

	}




}

?>