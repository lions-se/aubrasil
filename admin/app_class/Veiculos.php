<?


class Veiculos {

	public static function index(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Problema', "Não Possui Permissão de acesso nesta area.", ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}



		$pag = Flight::Pagina();

		  $query = "SELECT
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.RG,
proprietario.CPF,
veiculo_proprietario.VEICULO_ID,
veiculo_proprietario.PROPRIETARIO_ID,
veiculo.MODELO_ID,
modelo.MODELO,
marca.NOMEMARCA
FROM
proprietario
INNER JOIN veiculo_proprietario ON veiculo_proprietario.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
INNER JOIN veiculo ON veiculo_proprietario.VEICULO_ID = veiculo.VEICULO_ID
INNER JOIN modelo ON veiculo.MODELO_ID = modelo.MODELO_ID
INNER JOIN marca ON modelo.MARCA_ID = marca.MARCA_ID
ORDER BY veiculo_proprietario.VEICULO_ID

";

		
		$pag->setPages('veiculo');
		$q = $pag->retrieveData($query);

		Flight::render('veiculos/listar.php', array('pag'=> $pag, 'q' => $q), 'conteudo');
		Flight::render('main');


	}

	public static function pagEditVeiculo($veiculoid){


		$db = Flight::db('db');


		$query = "SELECT
veiculo.MODELO_ID,
veiculo.COR,
veiculo.CHASSI,
veiculo.CAPACIDADE,
veiculo.PLACA,
veiculo.PROCEDENCIA,
veiculo.ZEROKM,
veiculo.RENAVAM,
proprietario.NOME,
proprietario.PROPRIETARIO_ID,
veiculo.VEICULO_ID,
proprietario.RG,
proprietario.SOBRENOME,
marca.MARCA_ID
FROM
veiculo
LEFT JOIN veiculo_proprietario ON veiculo_proprietario.VEICULO_ID = veiculo.VEICULO_ID
LEFT JOIN proprietario ON veiculo_proprietario.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
LEFT JOIN modelo ON veiculo.MODELO_ID = modelo.MODELO_ID
LEFT JOIN marca ON modelo.MARCA_ID = marca.MARCA_ID
WHERE veiculo.VEICULO_ID = :veiculo_id
ORDER BY 1,9";

	
	$q = $db->query($query, array(":veiculo_id" => $veiculoid));


		Flight::render('veiculos/edita_veiculo.php', array('q' => $q->querydata[0], 'funcao' => 'Buscar Proprietario'), 'conteudo');
		Flight::render('main');


	}



	public static function processEditVeiculo($veiculoid){
			if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

			$query = "SELECT
veiculo.MODELO_ID,
veiculo.COR,
veiculo.CHASSI,
veiculo.CAPACIDADE,
veiculo.PLACA,
veiculo.PROCEDENCIA,
veiculo.ZEROKM,
veiculo.RENAVAM,
proprietario.NOME,
proprietario.PROPRIETARIO_ID,
veiculo.VEICULO_ID,
proprietario.RG,
proprietario.SOBRENOME,
marca.MARCA_ID
FROM
veiculo
LEFT JOIN veiculo_proprietario ON veiculo_proprietario.VEICULO_ID = veiculo.VEICULO_ID
LEFT JOIN proprietario ON veiculo_proprietario.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
LEFT JOIN modelo ON veiculo.MODELO_ID = modelo.MODELO_ID
LEFT JOIN marca ON modelo.MARCA_ID = marca.MARCA_ID
WHERE veiculo.VEICULO_ID = :veiculo_id
ORDER BY 1,9";

	
		$q = $db->query($query, array(":veiculo_id" => $veiculoid));


		if(!validateField($_POST['placa']) || !validateField($_POST['renavam']) || !validateField($_POST['chassi'])){

			alert('Problema', "Não Deixe Essenciais Campos Vazios", ALERTA_ERRO);
			Flight::render('veiculos/edita_veiculo.php', array('q' => $q->querydata[0]), 'conteudo');	
			Flight::render('main');
		}else {



		$db->update('veiculo',  array('MODELO_ID' => $_POST['modelo'],
									  'CHASSI' => $_POST['chassi'],
									  'RENAVAM' => $_POST['renavam'],
									  'PLACA' => $_POST['placa'],
									  'CAPACIDADE' => $_POST['capacidade'],
									  'ZEROKM' => $_POST['zerokm'],
									  'COR' => $_POST['cor'],
									  'PROCEDENCIA' => $_POST['procedencia']), 
									  'veiculo.VEICULO_ID = '.$veiculoid);

		//$db->update('veiculo', )
		//
		$db->update('veiculo_proprietario', array('PROPRIETARIO_ID' => $q->querydata[0]['PROPRIETARIO_ID']),  'VEICULO_ID ='.$veiculoid);

		//alert('Sucesso', "Alterado Com Sucesso");
		//Flight::redirect(rootURL().'/veiculos');
		Flight::redirect(rootURL().'/veiculos');


	}


	}


	public static function pagDeleteVeiculo($veiculoid){
				if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

		$query = "SELECT
veiculo.MODELO_ID,
veiculo.COR,
veiculo.CHASSI,
veiculo.CAPACIDADE,
veiculo.PLACA,
veiculo.PROCEDENCIA,
veiculo.ZEROKM,
veiculo.RENAVAM,
proprietario.NOME,
proprietario.PROPRIETARIO_ID,
veiculo.VEICULO_ID,
proprietario.RG,
proprietario.SOBRENOME,
marca.MARCA_ID
FROM
veiculo
LEFT JOIN veiculo_proprietario ON veiculo_proprietario.VEICULO_ID = veiculo.VEICULO_ID
LEFT JOIN proprietario ON veiculo_proprietario.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
LEFT JOIN modelo ON veiculo.MODELO_ID = modelo.MODELO_ID
LEFT JOIN marca ON modelo.MARCA_ID = marca.MARCA_ID
WHERE veiculo.VEICULO_ID = :veiculo_id
ORDER BY 1,9";

	$q = $db->query2($query, array(':veiculo_id' => $veiculoid));



	$db->delete('veiculo', 'VEICULO_ID = '.$veiculoid);
	Flight::redirect(rootURL().'/veiculos');


	


	}


	public static  function pagAddVeiculo(){
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(rootURL().'/login');
		}

		//$db = Flight::get('db');

		Flight::render('veiculos/cadastrarVeiculo.php', array(), 'conteudo');
		Flight::render('main');

	}


	public static function processAddVeiculo(){




		if(!validateField($_POST['propid'])){
			alert('Problema', "Necessita Associar um Proprietario ao Veiculo", ALERTA_ERRO);
			Flight::render('veiculos/cadastrarVeiculo.php', array('dados' => $_POST), 'conteudo');
			Flight::render('main');
		}


		if(!validateField($_POST['chassi'])){
			alert('Problema', "Campo Chassi Obrigatorio", ALERTA_ERRO);
			Flight::render('veiculos/cadastrarVeiculo.php', array('dados' => $_POST), 'conteudo');
			Flight::render('main');
		}


		if(!validateField($_POST['renavam'])){
			alert('Problema', "Campo Renavam é Obrigatorio", ALERTA_ERRO);
			Flight::render('veiculos/cadastrarVeiculo.php', array('dados' => $_POST), 'conteudo');
			Flight::render('main');
		}


		if(!validateField($_POST['placa'])){
			alert('Problema', "Campo Placa é Obrigatorio", ALERTA_ERRO);
			Flight::render('veiculos/cadastrarVeiculo.php', array('dados' => $_POST), 'conteudo');
			Flight::render('main');
		}


		if(!validateField($_POST['renavam'])){
			alert('Problema', "Campo Renavam é Obrigatorio", ALERTA_ERRO);
			Flight::render('veiculos/cadastrarVeiculo.php', array('dados' => $_POST), 'conteudo');
			Flight::render('main');
		}

		if(!validateField($_POST['cor'])){
			alert('Problema', "Campo Cor é Obrigatorio", ALERTA_ERRO);
			Flight::render('veiculos/cadastrarVeiculo.php', array('dados' => $_POST), 'conteudo');
			Flight::render('main');
		}



		if(!validateField($_POST['modelo']) && $_POST['modelo'] != '0' ){ 
			alert('Problema', "Campo Modelo é Obrigatorio", ALERTA_ERRO);
			Flight::render('veiculos/cadastrarVeiculo.php', array('dados' => $_POST), 'conteudo');
			Flight::render('main');
		}else if(!validateField($_POST['marca']) && $_POST['marca'] != '0') {
			alert('Problema', "Campo Marca é Obrigatorio", ALERTA_ERRO);
			Flight::render('veiculos/cadastrarVeiculo.php', array('dados' => $_POST), 'conteudo');
			Flight::render('main');
		}


		$db = Flight::get('db');

		$last_id = $db->insert('veiculo', array('COR' => $_POST['cor'],
					 'CHASSI' => $_POST['chassi'],
					 'CAPACIDADE' => $_POST['capacidade'],
					 'PLACA' => $_POST['placa'],
					 'PROCEDENCIA' => $_POST['procedencia'],
					 'ZEROKM'  =>     $_POST['zerokm'],
					 'RENAVAM' =>     $_POST['renavam']));



		$db->insert('veiculo_proprietario', array('VEICULO_ID' => $last_id,
												  'PROPRIETARIO_ID' => $_POST['propid']));

		Flight::redirect(rootURL().'/veiculos');




	}
}

?>