<?

class Login {

	public static function index(){

		if($_SESSION['userIsLogged']){

			Flight::render('home/home.php', array(),'conteudo');
			Flight::render('main');

		}else {

			Flight::render('login.php', array(),'conteudo');
			Flight::render('main');

		}

	}

	public static function doProcessLogin(){

		$login  = $_POST['login'];
		$passwd = $_POST['passwd'];

		//$passwd_hash = password_hash($passwd, PASSWORD_BCRYPT, array('cost' => 10));

		$db = Flight::get('db');

		$query = "SELECT
proprietario.LOGIN,
proprietario.SENHA,
proprietario.PROPRIETARIO_ID,
permissao.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME
FROM
proprietario
INNER JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID
WHERE proprietario.LOGIN = :login";

		$q= $db->query2($query,array(':login' => $login));

		var_dump($passwd);
		$db_hash = $q->querydata[0]['SENHA'];


		if(empty($passwd) ||  empty($login)){

			alert('Houve um Problema', 'Preencha os Campos de Login e senha corretamente', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		if(!password_verify($passwd, $db_hash)){

			alert('Houve um Problema', 'Login ou Senha Incorretos', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		if(!isset($_SESSION['loginCredentials'])){

			$_SESSION['loginCredentials'] = Flight::loginSession();

			$_SESSION['loginCredentials']->nome = $q->querydata[0]['NOME'];
			$_SESSION['loginCredentials']->user_id = $q->querydata[0]['PROPRIETARIO_ID'];
			$_SESSION['loginCredentials']->permissao_id = $q->querydata[0]['PERMISSAO_ID'];
		}else {
			$_SESSION['loginCredentials']->nome = $q->querydata[0]['NOME'];
			$_SESSION['loginCredentials']->user_id = $q->querydata[0]['PROPRIETARIO_ID'];
			$_SESSION['loginCredentials']->permissao_id = $q->querydata[0]['PERMISSAO_ID'];
			$_SESSION['loginCredentials']->session_token =  session_id();
			
		}

		$_SESSION['userIsLogged'] = true;




		alert('Sucesso', 'Foi Feito Login Com Sucesso!');
		if(isset($_SESSION['redirBack'])){

			Flight::redirect($_SESSION['redirBack']);
		}else{
				Flight::redirect(rootURL());
		}
	

	}


	public static function doLogout(){
		if(isset($_SESSION['loginCredentials'])){

			unset($_SESSION['loginCredentials']);
			unset($_SESSION['userIsLogged']);
			$_SESSION['loginCredentials'] = Flight::loginSession();

			Flight::redirect(rootURL().'/login');
		}
	}

}

?>