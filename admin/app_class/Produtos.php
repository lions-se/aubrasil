<?


class Produtos {

	public static function index(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$pag = Flight::Pagina();
		$db  = Flight::get('db'); 

		$query = "SELECT
produto.PRODUTO_ID,
produto.NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PRECO,
produto.DESCONTOPRODUTO,
produto.DESCONTOCLIENTE,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
produto.PROMOCAO_ATIVA
FROM
produto
";
		

		$pag->setPages('produto');
		$q = $pag->retrieveData($query);





		Flight::render('produtos/index.php', array('q' => $q, 'pag' => $pag), 'conteudo');
		Flight::render('main');

	}


	public static function pagEditar($produto_id){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');

		$query  = "SELECT
produto.PRODUTO_ID,
produto.NOME as PRODUTO_NOME,
produto_categoria_subcategoria.SUBCATEGORIA_ID,
produto_categoria_subcategoria.CATEGORIA_ID,
categoria.NOME,
subcategoria.NOME as SUBCAT_NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PRECO,
produto.DESCONTOPRODUTO,
produto.DESCONTOCLIENTE,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
produto.PROMOCAO_ATIVA
FROM
produto
LEFT JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
INNER JOIN categoria ON produto_categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
INNER JOIN subcategoria ON produto_categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE produto.PRODUTO_ID = :produto_id ";


	$q = $db->query2($query, array(':produto_id' => $produto_id));


	//var_dump($q);
	//var_dump($all_categorias);
	//
	//
	//
	$subcategoria = Utils::SubcategoriaSelected($produto_id);

	Flight::render('produtos/editaProduto.php', array('produto_id' => $produto_id, 'q' => $q, 'db' => $db, 'dados_categoria' => $subcategoria), 'conteudo');
	Flight::render('main.php');

	}

	public static function pagApagar($produto_id){
		
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(rootURL().'/login');
		}


	$query  = "SELECT
produto.PRODUTO_ID,
produto.NOME as PRODUTO_NOME,
produto_categoria_subcategoria.SUBCATEGORIA_ID,
produto_categoria_subcategoria.CATEGORIA_ID,
categoria.NOME,
subcategoria.NOME as SUBCAT_NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PRECO,
produto.DESCONTOPRODUTO,
produto.DESCONTOCLIENTE,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
produto.PROMOCAO_ATIVA
FROM
produto
LEFT JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
INNER JOIN categoria ON produto_categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
INNER JOIN subcategoria ON produto_categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE produto.PRODUTO_ID = :produto_id ";

	$db = Flight::get('db');

	$q = $db->query2($query, array(':produto_id'=>$produto_id));


	Flight::render('produtos/deletaProdutos.php', array('q' => $q), 'conteudo');
	Flight::render('main');


	}

	public static function buscaSubcat($cat_id){

		return Utils::buscaSubcategorias($cat_id);

	}

	public static function processApaga($prod_id){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}
		include("../config.php");


	
		$db = new PDO("mysql:host=localhost;port=3306;dbname=".$config['db'], $config['username'], $config['passwd']);

		$userid = $_POST['userid'];



			
		$st = $db->prepare("DELETE FROM produto  WHERE PRODUTO_ID = :id");
		$st->bindParam

		echo $st->rowCount();
		//$db->delete2('produto', 'PRODUTO_ID = :id', array(':id' => $userid));
		
		//alert("Sucesso ", "O Produto foi apagado com  sucesso!");
		//Flight::redirect(rootURL().'/produtos');
		//header("location: ?opcao=listar-produtos");

		

	}

	public static function processEdit(){

		if(!Utils::checkUser()){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(rootURL().'/login');
		}


	$db = Flight::get('db');

	$upload = new Upload('../uploads' , NULL, $_FILES);
	$images = $upload->multipleUploadsResize(100,100);
	//var_dump($upload->path());

	$insert  = array();


	$peso = limpa_medidas($_POST['peso']);
	$altura = limpa_medidas($_POST['altura']);
	$largura = limpa_medidas($_POST['largura']);
	$comprimento = limpa_medidas($_POST['comprimento']);


	$insert = array("NOME" => $_POST['nome'],
					'DESCRICAO' => $_POST['descricao'],
					 'PRECO'  => $_POST['preco'],
					 'DESCONTOPRODUTO' =>$_POST['descontoProduto'],
					 'DESCONTOCLIENTE' => $_POST['descontoCliente'],
					 'DESCONTOFRANQUEADO' =>$_POST['descontoFranqueado'],
					 'PESO'               => $peso,
					 'ALTURA'             => $altura,
					 'LARGURA'            => $largura,
					 'COMPRIMENTO'        => $comprimento

					 );

	foreach ($images as $key => $value) {

		$insert[$key] = $value;
		# code...
	}

	


	$q = $db->update2('produto', $insert, 'produto.PRODUTO_ID = :id', array(':id'=>$_POST['userid']));


	$db->update2('produto_categoria_subcategoria', array('CATEGORIA_ID' => $_POST['categoria'], 'SUBCATEGORIA_ID' => $_POST['subcategoria']), 'PRODUTO_ID = :id', array(':id' => $_POST['userid']));

	//print_r($_FILES);
	alert("Sucesso", " Seu Cadastro foi Editado Com Sucesso");
	Flight::redirect(rootURL().'/produtos');


	}


	public static function processCadastra(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');

		}

		$db = Flight::get('db');

		$imagens = new Upload('../uploads' , NULL, $_FILES);
	echo $imagens->path();
	$nomes = $imagens->multipleUploadsResize(150,150);


	//if(isset($nomes['imagem1']) && isset($nomes['imagem2']) && isset($nomes['imagem3']) && isset($nomes['imagem4']) && isset($nomes['imagem5']) && isset($nomes['imagem6']) ){


	$peso = limpa_medidas($_POST['peso']);
	$altura = limpa_medidas($_POST['altura']);
	$largura = limpa_medidas($_POST['largura']);
	$comprimento = limpa_medidas($_POST['comprimento']);



	$data = array('NOME'=> $_POST['nome'], 
									 'DESCRICAO' => $_POST['descricao'],
									 'PRECO' =>$_POST['preco'],
									 'DESCONTOPRODUTO' => $_POST['descontoProduto'],
									 'DESCONTOCLIENTE' => $_POST['descontoCliente'],
									 'DESCONTOFRANQUEADO' => $_POST['descontoFranqueado'],
									 'PESO'               => $peso,
									 'ALTURA'             => $altura,
									 'LARGURA'            => $largura,
									 'COMPRIMENTO'        => $comprimento
									 );




	foreach($nomes as $key => $value){

		$nome = strtoupper($key);

		$data[$nome] = $value;


	}



	$lastid =   $db->insert('produto', $data);

	$db->insert('produto_categoria_subcategoria', array('PRODUTO_ID' => $lastid, 'CATEGORIA_ID' => $_POST['categoria'], 'SUBCATEGORIA_ID' => $_POST['subcategoria'] ));

	alert('Sucesso!', "Produto Cadastrado com Sucesso");
	Flight::redirect(rootURL().'/produtos');

	}

	public static function pagAdicionar(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');

		}

		$db = Flight::get('db');

		$cat = $db->query("SELECT CATEGORIA_ID,NOME FROM categoria");

		Flight::render('produtos/cadastraProduto.php', array('cat' => $cat), 'conteudo');
		Flight::render('main.php');

	}

}

?>