<?

class Clientes {
	public static function index(){

		
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');
		$pag = Flight::Pagina();

		$query = "SELECT
franquia.FRANQUIA_ID,
cliente.CLIENTE_ID,
cliente.NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG,
cliente.TELEFONE,
cliente.TELEFONECOMERCIAL
FROM
cliente
LEFT JOIN franquia ON cliente.FRANQUIA_ID = franquia.FRANQUIA_ID";
	$pag->setPages('cliente');
	$q  = $pag->retrieveData($query);

		Flight::render('clientes/index.php', array('q' => $q, 'pag' => $pag), 'conteudo');
		Flight::render('main');

	}

	public static function pagEditarClientes($id){


		$db = Flight::db("db");

		$query  = "SELECT * FROM
cliente
WHERE CLIENTE_ID = :id";

		$q = $db->query($query, array(':id' => $id));

		Flight::render('clientes/editar_cliente.php', array('user' => $q->querydata[0]), 'conteudo');
		Flight::render('main');


	}


	public static function pagApagarClientes($id){

		$db = Flight::db("db");

			$query  = "SELECT * FROM
cliente
WHERE CLIENTE_ID = :id";

		$q = $db->query($query, array(':id' => $id));

		Flight::render('clientes/apagar_cliente.php', array('user' => $q->querydata[0]), 'conteudo');
		Flight::render('main');

	}


	public static function mostraPedidos($clienteid){


	$query_pedidos = "SELECT
cliente.CLIENTE_ID,
ordem_pedido.DATA_PEDIDO,
ordem_pedido.DATA_ENTREGA,
pedido_status.STATUS,
ordem_pedido.ORDEM_ID,
cliente.NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG,
cliente.TELEFONE
FROM
cliente
LEFT JOIN rel_cliente_ordem_pedido ON rel_cliente_ordem_pedido.CLIENTE_ID = cliente.CLIENTE_ID
LEFT JOIN ordem_pedido ON rel_cliente_ordem_pedido.ORDEM_ID = ordem_pedido.ORDEM_ID
LEFT JOIN pedido_status ON ordem_pedido.PEDIDO_STATUS_ID = pedido_status.PEDIDO_STATUS_ID
WHERE cliente.CLIENTE_ID = :cliente_id AND  NOT ISNULL(ordem_pedido.ORDEM_ID)";



			$db = Flight::get('db');
			$pag = Flight::Pagina();

			$pag->setPages('ordem_pedido');
			#$pag->setPagesSQL($query_pedidos, array(':cliente_id' => $clienteid));

			$q = $pag->retrieveData($query_pedidos, array(':cliente_id' => $clienteid));
			//$q = $db->query2($query_pedidos, array(':cliente_id' => $clienteid));


	Flight::render('clientes/ver_pedidos.php', array('data' => $q, 'pag' => $pag,'clienteid' => $clienteid ), 'conteudo');
	Flight::render('main');


	}

	public static function verProdutos($clienteid, $ordemid){


			$db = Flight::get('db');

			$query = "SELECT
produto_ordem.PRODUTO_ORDEM_ID,
rel_produto_ordem_pedido.PRODUTO_ID,
rel_produto_ordem_pedido.PRODUTO_ORDEM_ID,
produto.NOME AS PRODUTO_NOME,
produto.IMAGEM1,
produto.PRECO,
produto.DESCONTOPRODUTO,
produto.DESCONTOCLIENTE,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
rel_cliente_ordem_pedido.CLIENTE_ID,
rel_cliente_ordem_pedido.ORDEM_ID,
produto_ordem.QUANTIDADE,
ordem_pedido.PEDIDO_STATUS_ID,
ordem_pedido.DATA_PEDIDO,
ordem_pedido.DATA_ENTREGA,
ordem_pedido.CODIGO_PAGSEGURO,
cliente.NOME as CLIENTE_NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG
FROM
ordem_pedido
LEFT JOIN produto_ordem ON produto_ordem.ORDEM_ID = ordem_pedido.ORDEM_ID
INNER JOIN rel_produto_ordem_pedido ON rel_produto_ordem_pedido.PRODUTO_ORDEM_ID = produto_ordem.PRODUTO_ORDEM_ID
INNER JOIN produto ON rel_produto_ordem_pedido.PRODUTO_ID = produto.PRODUTO_ID
INNER JOIN rel_cliente_ordem_pedido ON rel_cliente_ordem_pedido.ORDEM_ID = ordem_pedido.ORDEM_ID
INNER JOIN cliente ON rel_cliente_ordem_pedido.CLIENTE_ID = cliente.CLIENTE_ID
WHERE produto_ordem.ORDEM_ID = :ordem_id AND rel_cliente_ordem_pedido.CLIENTE_ID = :cliente_id

";
	


	$q = $db->query2($query,array(':ordem_id' => $ordemid, ':cliente_id' => $clienteid));

	if($q->rowsAffected() == 0){
		Flight::notFound();
	}

	Flight::set('fullpage', true);
	Flight::render('clientes/notavirtual.php', array('data' => $q, 'clienteid' => $clienteid  ), 'conteudo');
	Flight::render('main');
	}


	public static function trocaPedidoStatus($clienteid, $pedido_id,$status){

		$db = Flight::db();


		$dados = array();
		$dados['PEDIDO_STATUS_ID'] = intval($status);
		$db->update('ordem_pedido', $dados, "ORDEM_ID = ". $pedido_id);

		//$db->update2('ordem_pedido', $dados, 'ORDEM_ID = :pedido_id', array(':pedido_id' => $pedido_id ));
		Flight::redirect(rootURL().'/clientes/ver_pedidos/'.$clienteid);




	}


	public static function editCliente($clienteid){


		$db = Flight::db();


				$erros = array();
		$num_erros = 0;
		$str_erros = "";


		//$repete_senha = password_hash($_POST['repetesenha'], PASSWORD_BCRYPT, array('cost' => 10));


		if(empty($_POST['nome']) || empty($_POST['sobrenome'])){

			$erros[] = "nome e Sobrenome Não podem estar vazios";
			$num_erros += 1;
		}else if( empty($_POST['rg']) ){
			$erros[] = "Campo RG Não Pode Estar Vazio";
			$num_erros += 1;
		}else if( empty($_POST['telefone'])){
			$erros[] = "Deve Ter Pelo Menos um telefone para Contato";
			$num_erros += 1;
		}else if( $_POST['dob'] == '0-0-0'){
			$erros[] = "Coloque a Data de Nascimento Correta";
			$num_erros += 1;
		}else {

			
			$cleaner = array('(',')','-');

			$telefone = str_replace( $cleaner,'',$_POST['telefone']);
			$telefonecelular = str_replace( $cleaner,'',$_POST['telefonecelular']);
			$telefonecomercial = str_replace( $cleaner,'',$_POST['telefonecomercial']);


			$db->update('cliente',
				array('NOME' => $_POST['nome'],
					  'SOBRENOME' => $_POST['sobrenome'],
					  'RG' => $_POST['rg'],
					  'TELEFONE' => $telefone,
					  'TELEFONECELULAR' => $telefonecelular,
					  'TELEFONECOMERCIAL' => $telefonecomercial,
					  'SEXO' => $_POST['sexo'],
					  'ESTADOCIVIL' => $_POST['estadocivil'],
					  'PAIS' => $_POST['pais']

					 )
				,'cliente.CLIENTE_ID = '.$clienteid);
			
				//Util::UpdateCredentials($_SESSION['credentials']->id, array('nome' => $_POST['nome'], 'franquia_id' => $franquia_id));

				alert('Sucesso', 'Seus Dados Foram Editados Com Sucesso!');
				Flight::redirect(rootURL().'/clientes');	

		}

		if($num_erros != 0){

			foreach($erros as $key => $value){

				$str_erros .= $value;
			}

			
			alert('Problemas', $str_erros, ALERTA_ERRO);
			Flight::redirect(rootURL().'/clientes');
		}


	}

}

?>