<?


class Utils {

		public static function BuscaProprietarioNome($dados){



			$where = "WHERE proprietario.NOME LIKE :dados AND proprietario.PERMISSAO_ID != 1";



		$query = sprintf("SELECT
			permissao.NOME AS PERM_NOME,
			permissao.ROLE,
			proprietario.NOME,
			proprietario.SOBRENOME,
			proprietario.PERMISSAO_ID,
			proprietario.RG,
			proprietario.PROPRIETARIO_ID
		FROM
			permissao
			INNER JOIN proprietario ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID %s LIMIT 1", $where);

		$db = Flight::get('db');

	

		$q = $db->query2($query, array(':dados' => $dados.'%'));

		$data = array();

		$data['id'] = $q->querydata[0]['PROPRIETARIO_ID'];
		$data['nome'] = $q->querydata[0]['NOME'];
		$data['sobrenome'] = $q->querydata[0]['SOBRENOME'];
		$data['rg'] = $q->querydata[0]['RG'];



		return Flight::json($data);


	}

	public static function BuscaProprietario($dados){



		$where = "WHERE proprietario.RG LIKE :dados AND proprietario.PERMISSAO_ID != 1";


		$clean = str_replace(array('.','-',' '), '', $dados);

		$query = sprintf("SELECT
			permissao.NOME AS PERM_NOME,
			permissao.ROLE,
			proprietario.NOME,
			proprietario.SOBRENOME,
			proprietario.PERMISSAO_ID,
			proprietario.RG,
			proprietario.PROPRIETARIO_ID
		FROM
			permissao
			INNER JOIN proprietario ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID %s LIMIT 1", $where);

		$db = Flight::get('db');

	

		$q = $db->query2($query, array(':dados' => $clean.'%'));

		$data = array();

		$data['id'] = $q->querydata[0]['PROPRIETARIO_ID'];
		$data['nome'] = $q->querydata[0]['NOME'];
		$data['sobrenome'] = $q->querydata[0]['SOBRENOME'];
		$data['rg'] = $q->querydata[0]['RG'];



		return Flight::json($data);


	}
	/**
	 * Compara permissão na seção com banco de dados
	 * e assim sempre trazendo a versao banco de dados
	 * para evitar roubos de identidade
	 * @return int Id de Permissao
	 */
	public static function getPermission(){

		if(!isset($_SESSION['loginCredentials']->user_id) || empty( $_SESSION['loginCredentials']->user_id) ){
			return;
		}else {


		$userid = $_SESSION['loginCredentials']->user_id;
		$permid = $_SESSION['loginCredentials']->permissao_id;

		$db = Flight::get('db');

		$user_query = "SELECT
proprietario.PERMISSAO_ID,
proprietario.PROPRIETARIO_ID,
proprietario.NOME,
proprietario.LOGIN,
permissao.NOME
FROM
proprietario
LEFT JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID
WHERE proprietario.PROPRIETARIO_ID = :proprietario_id ";


		$user_db = $db->query2($user_query, array(':proprietario_id' => $userid));

		if($user_db->querydata[0]['PERMISSAO_ID'] == $permid) {
			return $permid;
		}

		return 0;
	}

	} 


	public static function checkUser($adm_only = false){

		if(!array_key_exists('loginCredentials', $_SESSION)){
			return false;
		}else if(empty($_SESSION['loginCredentials'])){
			return false;
		}else if( is_null($_SESSION['loginCredentials']->user_id)){
			return false;
		}

		$userid = $_SESSION['loginCredentials']->user_id;
		$permid = $_SESSION['loginCredentials']->permissao_id;


		$db = Flight::get('db');

		$strWhere = ($adm_only == true) ? ' AND proprietario.PERMISSAO_ID = 1' : '';



		$user_query = "SELECT
proprietario.PERMISSAO_ID,
proprietario.PROPRIETARIO_ID,
proprietario.NOME,
proprietario.LOGIN,
permissao.NOME
FROM
proprietario
LEFT JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID
WHERE proprietario.PROPRIETARIO_ID = :proprietario_id ".$strWhere;


		$user_db = $db->query2($user_query, array(':proprietario_id' => $userid));

		$q = $db->query2("SELECT EXISTS(SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID
FROM
proprietario
WHERE proprietario.PROPRIETARIO_ID = :id)  as EXISTE ", array(':id' => $userid));


		if(!$adm_only){

			return ($q->querydata[0]['EXISTE'] === 1) ? true : false;

		}else {

			if($user_db->querydata[0]['PERMISSAO_ID'] == 1){
				return true;
			}

		}



		
		return false;


	}


	public static function buscaSubcategorias($cat_id){


		$query = "SELECT
subcategoria.NOME as SUBCAT_NOME,
categoria.NOME as CAT_NOME,
categoria_subcategoria.SUBCATEGORIA_ID,
categoria_subcategoria.CATEGORIA_ID
FROM
categoria
LEFT JOIN categoria_subcategoria ON categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
LEFT JOIN subcategoria ON categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE categoria.CATEGORIA_ID = :categoria_id";
	$db = Flight::get('db');

	$q = $db->query2($query, array(':categoria_id'=>$cat_id));

	$data = array();

	foreach ($q->querydata as $key => $value) {
		$data[$key]['nome'] = $value['SUBCAT_NOME'];
		$data[$key]['id']   = $value['SUBCATEGORIA_ID'];
	}
	

	return Flight::json($data);

	}



	public static function SubcategoriaSelected($produto_id){

		$db = Flight::get('db');

		$query = "SELECT
produto.PRODUTO_ID,
categoria.NOME as CAT_NOME,
subcategoria.NOME as SUBCAT_NOME
FROM
produto
INNER JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
INNER JOIN categoria ON produto_categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
INNER JOIN subcategoria ON produto_categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE produto.PRODUTO_ID = :id

";

	$q = $db->query2($query, array(':id'=> $produto_id));


	$dados = new ArrayIterator($q->querydata);
	$saida = array();

	foreach ($dados as $key => $value) {
		
		$saida['categoria_nome'] = $value['CAT_NOME'];	
		$saida['subcategoria_nome'] = $value['SUBCAT_NOME'];		
		$saida['cat_id'] = $value['CATEGORIA_ID'];
		$saida['subcat_id'] = $value['SUBCATEGORIA_ID'];				
	}

	return $saida;

}


	public static  function buscaSubcategoriasArray($id){
		$q =  self::buscaSubcategorias($id);

		return json_decode($q);
	}

	public static function buscaCep($cep_raw){

		header('Content-Type: application/json');

		if(empty($cep_raw)){
			die("HUEEHUHUAHUEHHAUHHUEHUAUHA");
		}

	$pattern = "/^[0-9]{8,8}$/";
	$reg = preg_match($pattern,$cep,$matches);

	$remove = array('.','-',',');
		
	$cep = str_replace($remove, '', $cep_raw);

	if(!empty($reg) || $reg != NULL){
		$dados['erro'] = 0;	
	}else {
		$dados['erro'] = 1;
	}

	

	$cURL = curl_init('http://republicavirtual.com.br/web_cep.php?cep='.$cep.'&formato=json');
	curl_setopt($cURL, CURLOPT_RETURNTRANSFER,true);

	// Seguir qualquer redirecionamento que houver na URL
	curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, false);

	$resultado = curl_exec($cURL);

	// Pega o código de resposta HTTP
	$resposta = curl_getinfo($cURL, CURLINFO_HTTP_CODE);

	curl_close($cURL);

	if ($resposta != '404') {
		
		echo $resultado;
		
	} else {
		echo 'Site Esta Fora do Ar';
	}	

	} 


	public static function buscaCep2($cep_raw){

		$remove = array('-','.');

		$parseCep = str_replace($remove, '', $cep_raw);

		$busca = Flight::buscaCep();

		$novocep = ($_SERVER['REQUEST_METHOD'] == "POST") ? str_replace($remove, '',$_POST['cep'])  : str_replace($remove, '', $cep_raw);

		//var_dump($busca);
		$info = $busca->setCep($novocep)->setUser('ryonagana')->setPass('cb3252518')->find();


		$dados['logradouro'] = $info[0];
		$dados['bairro']     = $info[1];
		$dados['cidade']     = $info[2];
		$dados['estado']     = strtoupper($info[3]);

		print Flight::json($dados);


	}

	public static function DownloadFile($id){



		$db = Flight::get('db');


		$q = $db->query2("SELECT
marketing.PATH,
marketing.NOME,
marketing.DESCRICAO,
marketing.MARKETING_ID,
marketing.MINIATURA
FROM
marketing
WHERE MARKETING_ID = :id", array(':id' => $id));



		$ext  = extensao($filepath);
		$ctype = "";
		$download_rate = 85;

		switch($ext){

			case 'txt':
				$ctype = "plain/text";
				break;
			 case 'zip': 
			 	$ctype='application/zip';
			 	 break;

			 case 'jpg':
			 case 'jpeg':
			 	$ctype  = "image/jpeg";
			 	break;
			 default:
			 	$ctype='application/force-download';
			 	break;
		}	


		$uploads = ADMIN_PATH.DIR.'uploads'.DIR.'arquivos'.DIR.'arq'.DIR;
		$filepath = $uploads.$q->querydata[0]['PATH'];
		if(is_file($filepath) && file_exists($filepath)){

			
			header("Content-Type: ".$ctype);
			//header("Content-Type: plain/text");
			header('Content-Description: File Transfer');
    		header('Content-Disposition: attachment; filename='.basename($filepath));
    		header('Content-Transfer-Encoding: binary');
    		header('Expires: 0');
    		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    		header('Pragma: public');
		
			

    		$fp = fopen($filepath, "rb");


    			
    		flush();

    		
    		$first = true;

    		
    		#FIXME: remove BOM!!!!!!
   
    		while(!feof($fp)){
    			 set_time_limit(0);
    			// var_dump(fread($fp, 1024*8));
        		
        		$data =  fread($fp, round($download_rate * 1024));
        		$arq  .= $data;
        		unset($data);
        		
        		

        		flush();
        		ob_end_clean();
        		sleep(1);
 
    		}
    		

    		print $arq;

    		fclose($fp);

		}else {

			Flight::notFound();

		}




	}


	public static function listarModelos($marca_id){

		$db = Flight::get('db');

		$query =  "SELECT
marca.MARCA_ID,
modelo.MODELO
FROM
modelo
INNER JOIN marca ON modelo.MARCA_ID = marca.MARCA_ID
WHERE marca.MARCA_ID = :marca_id";
	
		$q = $db->query2($query, array(':marca_id'=>$marca_id));

		header('Content-Type: application/json');

		$out = array();

		foreach ($q->querydata as $key => $value) {
			$data['NOME'] = $value['MODELO'];
			$data['ID'] = $value['MARCA_ID'];

			$out[] = $data;
		}

		return Flight::json($out);

	}

	public static function optionModeloVeiculo($marca_id,$modelo_id){


		$db = Flight::get('db');

		$query =  "SELECT
modelo.MODELO_ID,
modelo.MARCA_ID,
modelo.MODELO,
marca.MARCA_ID
FROM
modelo
INNER JOIN marca ON modelo.MARCA_ID = marca.MARCA_ID
WHERE marca.MARCA_ID = :marca_id";

	$q = $db->query2($query, array(':marca_id' => $marca_id));


	$dados =  new ArrayIterator($q->querydata);

	$html = "";

	foreach ($dados as $key => $value) {
		
		
		if($value['MODELO_ID'] == $modelo_id){

			$html .= sprintf('<option selected value="%s">%s</option>', $value['MODELO_ID'], $value['MODELO']);

		}else {
			$html .= sprintf('<option value="%s">%s</option>', $value['MODELO_ID'], $value['MODELO']);
		}
		


		
	}

	return $html;

	}


	public static function getPagSeguroStatus($token, $email, $codigo){
			

			$credentials = new PagSeguroAccountCredentials(      
    $email,       
    $token);  
  

  
/*  
    Realizando uma consulta de transação a partir do código identificador  
    para obter o objeto PagSeguroTransaction 


*/  

    if(is_null($codigo) || empty($codigo) || !strpos($codigo,'-')){

    	return "Codigo Invalido";
    	exit;
    }

	$transaction = PagSeguroTransactionSearchService::searchByCode(  
   		$credentials,  
    	$codigo 
	); 

	$status = array(1 => 'Aguardando Pagamento',
					2 => 'Em análise',
					3 => 'Paga',
					4 => 'Disponivel',
					5 => 'Em Disputa',
					6 => 'Devolvida',
					7 => 'Cancelada');

	$status_id =  $transaction->getStatus()->getValue();
	
	return $status[$status_id];



 
	}




}

?>