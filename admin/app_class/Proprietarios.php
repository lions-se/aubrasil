<?

class Proprietarios {

	public static function index(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');
		$pag = Flight::Pagina();

		if($_SESSION['loginCredentials']->permissao_id == 1){
			$wherestr = '';
			$opt = NULL;
		}else {
			$wherestr = 'WHERE proprietario.PERMISSAO_ID != :id';
			$opt  = array(':id'=>1);
		}


		 $pag->setPages('proprietario', $wherestr, $opt);      

		$query = "SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.LOGIN,
proprietario.CPF,
proprietario.RG,
proprietario.DATA_REGISTRO,
proprietario.DATA_NASCIMENTO,
proprietario.CEPFRANQUIA,
proprietario.ENDERECO,
proprietario.BAIRRO,
proprietario.CIDADE,
proprietario.ESTADO,
proprietario.PAIS,
proprietario.CODIGOEMAIL

FROM
proprietario ". $wherestr;

	$q = $pag->retrieveData($query);

	Flight::render('proprietarios/index.php', array('q' => $q,'pag' => $pag), 'conteudo');
	Flight::render('main');


	}


	public static function addProprietario(){

			if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		Flight::render('proprietarios/cadastroProprietario', array('dados' => $_POST), 'conteudo');
		Flight::render('main');
	}



	public static function processAddProprietario(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');


		$limpacpf = array('-','.');
		$limparg = array('.','-');

		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$cpf  = str_replace($limpacpf, '', $_POST['cpf']) ;
		$rg   = str_replace($limpacpf, '', $_POST['rg']) ;
		$dob = $_POST['dob'];
		$cep = $_POST['cep'];
		$end = $_POST['endereco'];
		$bairro = $_POST['bairro'];
		$cidade = $_POST['cidade'];
		$pais = $_POST['pais'];
		$estado = $_POST['estado'];

		$sexo = $_POST['sexo'];
		$estadocivil = !empty($_POST['estadocivil']) ? $_POST['estadocivil'] : 'M'   ;
		$complemento = !empty($_POST['complemento']) ? $_POST['complemento'] : 'casa'; 
		$emailsecundario   = !empty($_POST['emailsecundario']) ? $_POST['emailsecundario'] : 'noemail@email.com';


		$login = $_POST['login'];
		$senha = $_POST['senha'];
		$repete = $_POST['repete_senha'];


		$telcomercial = $_POST['telcomercial'];
		$telresidencial = $_POST['telcontato'];
		$telcelular = $_POST['telcelular'];

		$hash_sn = BCrypt::hash($senha,8);
		$hash_rep = BCrypt::hash($repete,8); 
		$data = array();



		$registro = date("Y-m-d H:i:s");



			$checkRG = $db-$db->query2("SELECT EXISTS(SELECT 1 FROM proprietario WHERE RG = :rg) as EXISTE", array(':rg'=>$rg));
			$checkEmail= $db->query2("SELECT EXISTS(SELECT 1 FROM proprietario WHERE LOGIN = :email) as EXISTE", array(':email'=>$login));
			
			
			
			
			
			
			if(!checkPost($estadocivil)){

			alert("Erro:", "Preencha corretamente seu estado Civil");
			flash();
			}else {
				
				$data['EMAIL_SECUNDARIO'] = $emailsecundario;
			
			}



			if($checkRG->querydata[0]['EXISTE'] == 1){

				alert('Houve um Problema','RG Ja Existente na base de dados', ALERTA_ERRO);
				Flight::render('proprietarios/cadastroProprietario', array('dados', $_POST), 'conteudo');
				Flight::render('main');
				//header("location: index.php?opcao=listar-proprietarios&cadastrar=true");


			}else if(empty($rg)){
				alert('Houve um Problema','Campo de RG Vazio', ALERTA_ERRO);
				Flight::render('proprietarios/cadastroProprietario', array('dados', $_POST), 'conteudo');
				Flight::render('main');
			}


			




				if($checkEmail->querydata[0]['EXISTE']  == 1){

					alert('Houve um Problema','Email de Login Ja Existente na base de dados');
					flash();
				Flight::render('proprietarios/cadastroProprietario', array('dados', $_POST), 'conteudo');
				Flight::render('main');

				}else if(empty($login)){

					alert('Houve um Problema','Campo E-Mail Vazio', ALERTA_ERRO);
				Flight::render('proprietarios/cadastroProprietario', array('dados', $_POST), 'conteudo');
				Flight::render('main');
				}


		if(!checkPost($login) || !checkPost($senha) || !checkPost($repete) ){

			


			alert("Erro: ", "Login, Senha e Repetir Senha são campos Obrigatorios");
			flash();

		}else if(!BCrypt::check($senha, $hash_rep)){

				alert("Erro: ", "O Campo de repetir senha não coincide com a senha digitada");
				Flight::render('proprietarios/cadastroProprietario', array('dados', $_POST), 'conteudo');
				Flight::render('main');
			
		}else if(!matchEmail($login)){

			    alert("Erro: ", "Insira um E-mail valido");
				Flight::render('proprietarios/cadastroProprietario', array('dados', $_POST), 'conteudo');
				Flight::render('main');


		}else if(!checkPost($cpf) || !checkPost($rg)){

			alert("Erro: ", "Obrigatorio Preencher campo  CPF e RG");
				Flight::render('proprietarios/cadastroProprietario', array('dados', $_POST), 'conteudo');
				Flight::render('main');


		}else if(!checkPost($nome) || ! checkPost($sobrenome)){


			alert("Erro: ", "Prencha Nome e Sobrenome");
				Flight::render('proprietarios/cadastroProprietario', array('dados', $_POST), 'conteudo');
				Flight::render('main');


		}else if(!checkPost($cep)){

			alert("Erro:", "Por Favor o Preencha o CEP");
				Flight::render('proprietarios/cadastroProprietario', array('dados', $_POST), 'conteudo');
				Flight::render('main');

		}else if(!checkPost($telresidencial)){
				alert("Houve um Problema", "Adicione um telefone para contato");
		}else if(!checkPost($telresidencial)){
				$telcomercial = ' ' ;
		}else if(!checkPost($telcomercial)){
				$telcomercial = ' ';
		}


		if(is_null($pais) || empty($pais)){

			$pais = "Brasil";
		}



			$data = array('NOME' => $nome,
										  'SEXO' => $sexo,
										  'TELCOMERCIAL' => $telcomercial,
										  'TELRESIDENCIAL' => $telresidencial,
										  'TELCELULAR' => $telcelular,
										  'ESTADO_CIVIL' => $estadocivil,
										  'PERMISSAO_ID' => 2,
										  'SOBRENOME' => $sobrenome,
										  'CPF' => $cpf,
										  'RG'  => $rg,
										  'DATA_NASCIMENTO' => date_convert($dob),
										  'CEPFRANQUIA' => $cep,
										  'ENDERECO' => $end,
										  'CIDADE' => $cidade,
										  'ESTADO' => $estado,
										  'LOGIN' => $login,
										   'DATA_REGISTRO' => $registro,
										   'PAIS' => $pais,
										  'SENHA' => $hash_sn );


			if(checkPost($complemento)){
				
				$data['COMPLEMENTO'] = $complemento;	
			}


			if(checkPost($bairro)){
				
				$data['BAIRRO'] = $bairro;
			}


			$db->insert('proprietario',$data);
			//var_dump($data);
		

			alert("Sucesso: ","Foi Criado um proprietario Com Sucesso" );
			Flight::redirect(rootURL().'/proprietarios');
			//header("location: ?opcao=listar-proprietarios");


	
	}


	public static function editProprietario($prop_id){

			if(!Utils::checkUser(true)){

				$_SESSION['redirBack'] = Flight::request()->url;
				alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
				Flight::redirect(rootURL().'/login');
			}

			$db = Flight::get('db');

			$q = $db->query2("
SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.LOGIN,
proprietario.CPF,
proprietario.RG,
proprietario.DATA_REGISTRO,
proprietario.DATA_NASCIMENTO,
proprietario.CEPFRANQUIA,
proprietario.ENDERECO,
proprietario.BAIRRO,
proprietario.CIDADE,
proprietario.ESTADO,
proprietario.PAIS,
proprietario.CODIGOEMAIL,
permissao.PERMISSAO_ID,
permissao.NOME AS PERM_NOME,
proprietario.EMAIL_SECUNDARIO,
proprietario.ESTADO_CIVIL,
proprietario.TELCOMERCIAL,
proprietario.TELCELULAR,
proprietario.TELRESIDENCIAL,
proprietario.SEXO,
proprietario.COMPLEMENTO,
proprietario.NUMERO
FROM
proprietario
INNER JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID


WHERE
proprietario.PROPRIETARIO_ID = :proprietario_id", array(':proprietario_id' => $prop_id) );


	Flight::render('proprietarios/editarProprietarios.php', array('q' => $q, 'dados' => $_POST ), 'conteudo');
	Flight::render('main');
	

	}

		public static function processEditProprietario(){

			if(!Utils::checkUser(true)){

				$_SESSION['redirBack'] = Flight::request()->url;
				alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
				Flight::redirect(rootURL().'/login');
			}

			$db = Flight::get('db');

				$limpacpf = array('-','.');
		$limparg = array('.','-');

		$id = $_POST['userid'];
		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$cpf  = str_replace($limpacpf, '', $_POST['cpf']) ;
		$rg   = str_replace($limpacpf, '', $_POST['rg']) ;
		$dob = $_POST['dob'];
		$cep = $_POST['cep'];
		$end = $_POST['endereco'];
		$bairro = $_POST['bairro'];
		$cidade = $_POST['cidade'];
		$pais = $_POST['pais'];
		$estado = $_POST['estado'];

		$sexo = $_POST['sexo'];
		$estadocivil = $_POST['estadocivil'];
		$complemento = $_POST['complemento'];
		$emailsecundario   = $_POST['emailsecundario'];


		$telcomercial = $_POST['telcomercial'];
		$telresidencial = $_POST['telcontato'];
		$telcelular = $_POST['telcelular'];


		if(empty($rg) || empty($cpf)){

				print('<div class="alert alert-danger"><strong>Erro:</strong> Documentos como CPF e RG Sao Campos Obrigatorios a Preencher</div>');

		}else{

			$db->update('proprietario', array(

										 	'EMAIL_SECUNDARIO' => $emailsecundario,
										  	'COMPLEMENTO' => $complemento,
										  	'TELCOMERCIAL' => $telcomercial,
										  	'TELRESIDENCIAL' => $telresidencial,
										  	'TELCELULAR' => $telcelular,
										  	'ESTADO_CIVIL' => $estadocivil,
											'NOME' => $nome,
										  	'SOBRENOME' => $sobrenome,
										  	'CPF' => $cpf,
										  	'RG'  => $rg,
										  	'DATA_NASCIMENTO' => date_convert($dob),
										  	'CEPFRANQUIA' => $cep,
										  	'ENDERECO' => $end,
										  	'BAIRRO' => $bairro,
										  	'CIDADE' => $cidade,
										  	'ESTADO' => $estado,
										  	'SEXO' => $sexo,
										  	'ESTADO_CIVIL' => $estadocivil,
										  	'COMPLEMENTO' => $complemento,
										  	'PAIS' => $pais), 'PROPRIETARIO_ID = '.$id);

			alert("Sucesso"," Os Dados do Usuario: ".$nome." foram atualizados com sucesso");
			Flight::redirect(rootURL().'/proprietarios');
			//header("location: index.php?opcao=listar-proprietarios");
	}

}

	
	public static function processEditCredentials(){

			if(!Utils::checkUser(true)){

				$_SESSION['redirBack'] = Flight::request()->url;
				alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
				Flight::redirect(rootURL().'/login');
			}


			if(!Utils::checkUser()){

				$_SESSION['redirBack'] = Flight::request()->url;
				Flight::redirect(rootURL().'/login');
			}


			$db = Flight::get('db');

			if(!empty($_POST['senha']) && !empty($_POST['repete_senha'])){



			$senha = $_POST['senha'];
			$encsenha = password_hash($senha, PASSWORD_BCRYPT, array('cost' => 10));
		
			$repete = $_POST['repete_senha'];
			$id = $_POST['userid'];



			if(!password_verify($repete, $encsenha)){
				alert("Erro:", " As senhas devem coincidir");
				Flight::redirect($_SESSION['pagAnterior']);
					//header("loca
			}else {



				$db->update2('proprietario', array('SENHA' => $encsenha),

					'proprietario.PROPRIETARIO_ID = :id', array(':id' =>$id) );
					
					alert("Sucesso", " Sua Senha Foi Atualizada Com Sucesso");
					Flight::redirect($_SESSION['pagAnterior']);
					//header("location: ?opcao=listar-proprietarios");
				//print('<div class="alert alert-success"><strong>Sucesso:</strong>Sua Senha Foi Atualizada com Sucesso!</div>');
			}

		}else {

					alert("Erro:", " Houve um Erro Na Atualizacao das Senhas");
					//flash();
					Flight::redirect($_SESSION['pagAnterior']);
					//header("location: ?opcao=listar-proprietarios");

		}

	}


	public static function deletaProprietarios($prop_id){


			if(!Utils::checkUser(true)){

				$_SESSION['redirBack'] = Flight::request()->url;
				alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
				Flight::redirect(rootURL().'/login');
			}
		

		$db = Flight::get('db');


		$q = $db->query2("
		SELECT
			proprietario.PROPRIETARIO_ID,
			proprietario.PERMISSAO_ID,
			proprietario.NOME,
			proprietario.SOBRENOME,
			proprietario.CPF,
			proprietario.RG,
			proprietario.DATA_REGISTRO,
			proprietario.DATA_NASCIMENTO,
			proprietario.CEPFRANQUIA,
			proprietario.ENDERECO,
			proprietario.BAIRRO,
			proprietario.CIDADE,
			proprietario.ESTADO,
			proprietario.PAIS,
			proprietario.CODIGOEMAIL,
			permissao.PERMISSAO_ID,
			permissao.NOME as PERM_NOME
		FROM
			proprietario
		INNER JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID


		WHERE 
			proprietario.PROPRIETARIO_ID  = :id", array(':id'=> $prop_id));




	Flight::render('proprietarios/apagaProprietario.php', array('q' => $q), 'conteudo');
	Flight::render('main');
	}


	public static function processDeleteProprietario(){


			if(!Utils::checkUser(true)){

				$_SESSION['redirBack'] = Flight::request()->url;
				alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
				Flight::redirect(rootURL().'/login');
			}

			$db = Flight::get('db');

			$id = $_POST['userid'];


				$q = $db->query2(" SELECT
					  Count(*) as CONTAGEM
					  FROM
					  franquia
					  LEFT JOIN proprietario ON franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
					  WHERE franquia.PROPRIETARIO_ID = :id LIMIT 1", array(':id'=> $id));


	$registro = $q->querydata[0]['CONTAGEM'];

	if($registro == 1){

		$db->delete2("franquia", 'PROPRIETARIO_ID = :id', array(':id'=>$id));

	}
	
	$db->delete2('proprietario', 'proprietario.PROPRIETARIO_ID = :id', array(':id'=>$id ));
	alert("Sucesso", sprintf(" Proprietario  do id #%d Deletado com Sucesso!", $id));
	Flight::redirect(rootURL().'/proprietarios');
	//header("location: ?opcao=listar-proprietarios");


	}


	public static function addAdm($id){

					if(!Utils::checkUser(true)){

				$_SESSION['redirBack'] = Flight::request()->url;
				alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
				Flight::redirect(rootURL().'/login');
			}


			$db = Flight::get('db');

			$db->update('proprietario', array('PERMISSAO_ID' => 1), 'PROPRIETARIO_ID  = '.$id);

			alert('Sucesso', "Permissão de Acesso a Esse Usuario foi Trocada Para Administrador com Sucesso");
			Flight::redirect(rootURL().'/proprietarios');
		



	}

	public static function removeAdm($id){
		if(!Utils::checkUser(true)){

				$_SESSION['redirBack'] = Flight::request()->url;
				alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
				Flight::redirect(rootURL().'/login');
			}


			$db = Flight::get('db');

			$db->update('proprietario', array('PERMISSAO_ID' => 2), 'PROPRIETARIO_ID  = '.$id);

			alert('Sucesso', "Permissão de Acesso Administrador  para este Usuario Foi Removido ");
			Flight::redirect(rootURL().'/proprietarios');
		
		
	}



}




?>