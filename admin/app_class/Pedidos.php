<?php

class Pedidos {
	public static function Listar(){

		$db = Flight::get('db');
		$pag = new Pagination($db,5);
		$query = "
		SELECT
ordem_pedido.ORDEM_ID,
ordem_pedido.FRANQUIA_ID,
ordem_pedido.PEDIDO_STATUS_ID,
ordem_pedido.DATA_PEDIDO,
ordem_pedido.DATA_ENTREGA,
ordem_pedido.CODIGO_PAGSEGURO,
cliente.FRANQUIA_ID,
cliente.NOME,
cliente.SOBRENOME,
cliente.CLIENTE_ID
FROM
ordem_pedido
LEFT JOIN rel_cliente_ordem_pedido ON rel_cliente_ordem_pedido.ORDEM_ID = ordem_pedido.ORDEM_ID
LEFT JOIN cliente ON rel_cliente_ordem_pedido.CLIENTE_ID = cliente.CLIENTE_ID
";
	
	//var_dump($pag);
	$pag->setPagesSQL($query);
	$q = $pag->retrieveData($query);


	Flight::render("pedidos/listar.php", array('dados' => $q->querydata, 'pag' => $pag),'conteudo');
	Flight::render('main');


	}
}

?>