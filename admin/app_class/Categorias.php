<?


class Categorias {
	public static function index(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}


		//$db = Flight::get('db');
		$pag = Flight::Pagina();
		

		$query = "SELECT
categoria.CATEGORIA_ID,
categoria.NOME as CAT_NOME,
categoria.NOMEINTERNO,
categoria.URL
FROM
categoria
";

	$pag->setPages('categoria');
	$q = $pag->retrieveData($query);


	Flight::render('categorias/listar.php', array('q' => $q, 'pag' => $pag),'conteudo');
	Flight::render('main.php');


	}


	public static function editaCategoria($categoria_id){



		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}
		//$db = Flight::get('db');
		$pag = Flight::Pagina();
		

		$query = "SELECT
		categoria.CATEGORIA_ID,
		categoria.NOME as CAT_NOME,
		categoria.NOMEINTERNO,
		categoria.URL,
		subcategoria.NOME as SUBCAT_NOME
		FROM
		categoria
		LEFT JOIN categoria_subcategoria ON categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
		LEFT JOIN subcategoria ON categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
		WHERE  categoria.CATEGORIA_ID = :catid";

	$pag->setPages('categoria');
	$q = $pag->retrieveData($query, array(':catid' => $categoria_id ));


	Flight::render('categorias/edita_categoria.php', array('q' => $q),'conteudo');
	Flight::render('main.php');
	}

	public static function cadastraCategoria(){

		Flight::render('categorias/cadastra_categoria.php', array(), 'conteudo');
		Flight::render('main');


	}




	public static function removeCategoria($categoria_id){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

			$query = "SELECT
		categoria.CATEGORIA_ID,
		categoria.NOME as CAT_NOME,
		categoria.NOMEINTERNO,
		categoria.URL,
		subcategoria.NOME as SUBCAT_NOME
		FROM
		categoria
		LEFT JOIN categoria_subcategoria ON categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
		LEFT JOIN subcategoria ON categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
		WHERE  categoria.CATEGORIA_ID = :catid";

		$q = $db->query2($query, array(':catid' => $categoria_id));


		Flight::render('categorias/remove_categoria.php', array('q'=> $q), 'conteudo');
		Flight::render('main');

	}



	public static function processaCadastroCategoria(){

		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$erros = 0;
		$db = Flight::get('db');


		for($i = 0; $i < count($_POST['nome']); ++$i){

			$nome = $_POST['nome'][$i];
			$nomeinterno = $_POST['nomeinterno'][$i];

			if(!formValue($nome) &&  !formValue($nomeinterno)){

				$erros++;
			}

			if($erro == 0) {

				$db->insert('categoria', array('NOME' => $nome, 'NOMEINTERNO' => $nomeinterno));
			}else {

				$_SESSION['qtdRegistros'] = count($_POST['nome']);
				alert('Houve um Problema', "Preenchimento Invalido do Formulário", ALERTA_ERRO);
				Flight::render('categorias/listar.php', array('dados' => $_POST),'conteudo');
				Flight::render('main.php');

			}

		}



				alert('Sucesso', sprintf("Categoria Cadastrada com sucesso"));
				Flight::redirect(sprintf("%s/categorias/", rootURL()));
				//Flight::render('categorias/listar.php', array('dados' => $_POST),'conteudo');
				//Flight::render('main.php');

		
		
	}


	public static function processaRemoveCategoria($categoria_id){
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

		$db->delete2('categoria', 'CATEGORIA_ID = :id', array(':id' => $categoria_id));
		alert('Sucesso', sprintf("Categoria foi Deletada com sucesso"));
		Flight::redirect(sprintf("%s/categorias/", rootURL()));

		//Flight::render('categorias/listar.php', array(),'conteudo');
		//Flight::render('main.php');




	}

	public static function processaEditaCategoria($categoria_id){
		if(!Utils::checkUser(true)){

			$_SESSION['redirBack'] = Flight::request()->url;
			alert('Requisição Inválida', 'Você Não tem Permissão para acessar esta seção', ALERTA_ERRO);
			Flight::redirect(rootURL().'/login');
		}

		$db = Flight::get('db');

		var_dump($_POST);

		$db->update2('categoria', array('NOME' => $_POST['nome']), 'CATEGORIA_ID = :id', array(':id' => $categoria_id));

		alert('Sucesso', "Categoria Alterada comSucesso");
		Flight::redirect(rootURL().'/categorias');

	}




}

?>