<?

class Usuario {

	public static function index(){

		if(!Utils::checkUser()){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');

		$query = "SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.EMAIL_SECUNDARIO,
proprietario.CPF,
proprietario.ESTADO_CIVIL,
proprietario.RG,
proprietario.DATA_REGISTRO,
proprietario.TELCOMERCIAL,
proprietario.TELRESIDENCIAL,
proprietario.TELCELULAR,
proprietario.SEXO,
proprietario.DATA_NASCIMENTO,
proprietario.CEPFRANQUIA,
proprietario.COMPLEMENTO,
proprietario.ENDERECO,
proprietario.NUMERO,
proprietario.BAIRRO,
proprietario.CEPFRANQUIA,
proprietario.CIDADE,
proprietario.ESTADO,
proprietario.PAIS,
proprietario.LOGIN
FROM
proprietario
WHERE proprietario.PROPRIETARIO_ID = :id";

	$q = $db->query2($query, array(':id' => $_SESSION['loginCredentials']->user_id));

	Flight::render('editar_dados.php', array('q' => $q), 'conteudo');
	Flight::render('main');

	}

	public static function processForm(){



		if(!Utils::checkUser()){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(rootURL().'/login');
		}


		$db = Flight::get('db');

	
		$query = "SELECT
proprietario.PROPRIETARIO_ID,
proprietario.PERMISSAO_ID,
proprietario.NOME,
proprietario.SOBRENOME,
proprietario.EMAIL_SECUNDARIO,
proprietario.LOGIN,
proprietario.CPF,
proprietario.ESTADO_CIVIL,
proprietario.RG,
proprietario.DATA_REGISTRO,
proprietario.TELCOMERCIAL,
proprietario.TELRESIDENCIAL,
proprietario.TELCELULAR,
proprietario.SEXO,
proprietario.DATA_NASCIMENTO,
proprietario.CEPFRANQUIA,
proprietario.COMPLEMENTO,
proprietario.ENDERECO,
proprietario.NUMERO,
proprietario.BAIRRO,
proprietario.CIDADE,
proprietario.ESTADO,
proprietario.PAIS,
permissao.PERMISSAO_ID,
permissao.ROLE
FROM
proprietario
INNER JOIN permissao ON proprietario.PERMISSAO_ID = permissao.PERMISSAO_ID

WHERE
proprietario.PROPRIETARIO_ID = :id AND proprietario.PERMISSAO_ID = :perm_id";

		$q = $db->query2($query,

								array(':id' => $_SESSION['loginCredentials']->user_id, ':perm_id' => 1));

		$proprietario = $q->querydata[0]['PROPRIETARIO_ID'];



		
		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$cpf = $_POST['cpf'];
		$rg = $_POST['rg'];
		
		
		$update = $db->update2('proprietario', array('NOME'=>$nome,
													'SOBRENOME' => $sobrenome,
													'CPF' => $cpf,
													'RG' => $rg,
													'DATA_NASCIMENTO' => date_convert($_POST['dob']),
													'CEPFRANQUIA' => $_POST['cep'],
													'ENDERECO' => $_POST['endereco'],
													'BAIRRO' => $_POST['bairro'],
													'CIDADE' => $_POST['cidade'],
													'ESTADO' => $_POST['estado'],
													'PAIS'   => $_POST['pais']
													),											
													 sprintf("PROPRIETARIO_ID = :id"),

													 array(':id' => $_SESSION['loginCredentials']->user_id)
													

													);


		$cred = $_SESSION['loginCredentials'];
		$cred->nome = $nome;

		Flight::set('loginCredentials', $cred);


		alert("Sucesso! ", "Seus dados Foram Alterados");
		Flight::redirect(rootURL().'/editar_dados');
		//header('location: /');
		




	}
}


?>