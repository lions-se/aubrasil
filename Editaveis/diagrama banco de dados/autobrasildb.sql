drop table if exists CATEGORIA;

drop table if exists CLIENTE;

drop table if exists FRANQUIA;

drop table if exists MARCA;

drop table if exists MARKETING;

drop table if exists MODELO;

drop table if exists ORDEM_PEDIDO;

drop table if exists PEDIDO_STATUS;

drop table if exists PERMISSAO;

drop table if exists PRODUTO;

drop table if exists PROPRIETARIO;

drop table if exists REL_CLIENTE_ORDEM_PEDIDO;

drop table if exists REL_PRODUTO_ORDEM_PEDIDO;

drop table if exists REL_PROD_SUBCATEGORIA;

drop table if exists SUBCATEGORIA;

drop table if exists TIPO;

drop table if exists VEICULO;

drop table if exists VEICULO_PROPRIETARIO;

/*==============================================================*/
/* Table: CATEGORIA                                             */
/*==============================================================*/
create table CATEGORIA
(
   CATEGORIA_ID         int(11) not null auto_increment,
   NOME                 varchar(30),
   NOMEINTERNO          varchar(30) not null,
   URL                  varchar(30) not null,
   primary key (CATEGORIA_ID)
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE
(
   CLIENTE_ID           int(11) not null auto_increment,
   FRANQUIA_ID          int(11) not null,
   NOME                 varchar(30) not null,
   SOBRENOME            varchar(70) not null,
   CPF                  varchar(12) not null,
   RG                   varchar(12) not null,
   ESTADOCIVIL          smallint,
   SEXO                 char(1),
   DATA_NASCIMENTO      date,
   LOGIN                varchar(60),
   SENHA                varchar(64) not null,
   EMAILSECUNDARIO      varchar(60) not null,
   DESCRICAO            text,
   DATA_REGISTRO        datetime not null,
   CODIGO_CADASTRO      varchar(64),
   CONFIRMA_CADASTRO    bool,
   CODIGO_REFERENCIA    varchar(64),
   primary key (CLIENTE_ID)
);

/*==============================================================*/
/* Table: FRANQUIA                                              */
/*==============================================================*/
create table FRANQUIA
(
   FRANQUIA_ID          int(11) not null auto_increment,
   PROPRIETARIO_ID      int not null,
   NOMEFRANQUIA         varchar(60),
   SLOGAN               varchar(100),
   TELFRANQUIA          varchar(16) not null,
   CEPFRANQUIA          char(12),
   ENDERECO             char(255),
   BAIRRO               varchar(60),
   CIDADE               char(30),
   ESTADO               char(2),
   primary key (FRANQUIA_ID)
);

/*==============================================================*/
/* Table: MARCA                                                 */
/*==============================================================*/
create table MARCA
(
   MARCA_ID             int(11) not null auto_increment,
   TIPO_ID              int(11),
   NOMEMARCA            varchar(18),
   primary key (MARCA_ID)
);

/*==============================================================*/
/* Table: MARKETING                                             */
/*==============================================================*/
create table MARKETING
(
   MARKETING_ID         int(11) not null auto_increment,
   NOME                 varchar(30) not null,
   DESCRICAO            text,
   PATH                 varchar(255) not null,
   MINIATURA            varchar(127),
   primary key (MARKETING_ID)
);

/*==============================================================*/
/* Table: MODELO                                                */
/*==============================================================*/
create table MODELO
(
   MODELO_ID            int(11) not null auto_increment,
   MARCA_ID             int(11),
   MODELO               varchar(25),
   primary key (MODELO_ID)
);

/*==============================================================*/
/* Table: ORDEM_PEDIDO                                          */
/*==============================================================*/
create table ORDEM_PEDIDO
(
   ORDEM_ID             int(11) not null auto_increment,
   PEDIDO_STATUS_ID     int(11),
   DATA_PEDIDO          datetime,
   DATA_ENTREGA         date,
   primary key (ORDEM_ID)
);

/*==============================================================*/
/* Table: PEDIDO_STATUS                                         */
/*==============================================================*/
create table PEDIDO_STATUS
(
   PEDIDO_STATUS_ID     int(11) not null auto_increment,
   STATUS               smallint,
   primary key (PEDIDO_STATUS_ID)
);

/*==============================================================*/
/* Table: PERMISSAO                                             */
/*==============================================================*/
create table PERMISSAO
(
   PERMISSAO_ID         int not null auto_increment,
   NOME                 varchar(30),
   ROLE                 varchar(40),
   primary key (PERMISSAO_ID)
);

/*==============================================================*/
/* Table: PRODUTO                                               */
/*==============================================================*/
create table PRODUTO
(
   PRODUTO_ID           int(11) not null auto_increment,
   FRANQUIA_ID          int(11),
   NOME                 varchar(30) not null,
   DESCRICAO            text not null,
   IMAGEM1              varchar(70),
   IMAGEM2              varchar(70),
   IMAGEM3              varchar(70),
   IMAGEM4              varchar(70),
   IMAGEM5              varchar(70),
   IMAGEM6              varchar(70),
   PRECO                decimal(8,3),
   DESCONTOPRODUTO      decimal(8,3),
   DESCONTOCLIENTE      decimal(8,3),
   DESCONTOFRANQUEADO   decimal(8,3),
   LARGURA              float(6),
   ALTURA               float(6),
   COMPRIMENTO          float(6),
   PESO                 float(6),
   primary key (PRODUTO_ID)
);

/*==============================================================*/
/* Table: PROPRIETARIO                                          */
/*==============================================================*/
create table PROPRIETARIO
(
   PROPRIETARIO_ID      int not null auto_increment,
   PERMISSAO_ID         int not null,
   NOME                 varchar(30),
   SOBRENOME            varchar(70),
   EMAIL_SECUNDARIO     varchar(60),
   LOGIN                varchar(60) not null,
   SENHA                varchar(64) not null,
   CPF                  varchar(12) not null,
   ESTADO_CIVIL         smallint,
   RG                   varchar(12) not null,
   DATA_REGISTRO        datetime not null,
   TELCOMERCIAL         varchar(18),
   TELRESIDENCIAL       varchar(18),
   TELCELULAR           varchar(18),
   SEXO                 char(1),
   DATA_NASCIMENTO      date not null,
   CEPFRANQUIA          char(12),
   COMPLEMENTO          varchar(12),
   ENDERECO             char(255),
   NUMERO               smallint,
   BAIRRO               varchar(60),
   CIDADE               char(30),
   ESTADO               char(2),
   PAIS                 char(18),
   CODIGOEMAIL          varchar(64),
   CODIGO_CADASTRO      varchar(64),
   primary key (PROPRIETARIO_ID)
);

/*==============================================================*/
/* Table: REL_CLIENTE_ORDEM_PEDIDO                              */
/*==============================================================*/
create table REL_CLIENTE_ORDEM_PEDIDO
(
   CLIENTE_ID           int(11) not null,
   ORDEM_ID             int(11) not null,
   primary key (CLIENTE_ID, ORDEM_ID)
);

/*==============================================================*/
/* Table: REL_PRODUTO_ORDEM_PEDIDO                              */
/*==============================================================*/
create table REL_PRODUTO_ORDEM_PEDIDO
(
   PRODUTO_ID           int(11) not null,
   ORDEM_ID             int(11) not null,
   primary key (PRODUTO_ID, ORDEM_ID)
);

/*==============================================================*/
/* Table: REL_PROD_SUBCATEGORIA                                 */
/*==============================================================*/
create table REL_PROD_SUBCATEGORIA
(
   PRODUTO_ID           int(11) not null,
   SUBCATEGORIA_ID      int not null,
   primary key (PRODUTO_ID, SUBCATEGORIA_ID)
);

/*==============================================================*/
/* Table: SUBCATEGORIA                                          */
/*==============================================================*/
create table SUBCATEGORIA
(
   SUBCATEGORIA_ID      int not null auto_increment,
   CATEGORIA_ID         int(11),
   NOME                 varchar(30),
   NOMEINTERNO          varchar(30) not null,
   URL                  varchar(30) not null,
   primary key (SUBCATEGORIA_ID)
);

/*==============================================================*/
/* Table: TIPO                                                  */
/*==============================================================*/
create table TIPO
(
   TIPO_ID              int(11) not null auto_increment,
   TIPO                 varchar(10),
   primary key (TIPO_ID)
);

/*==============================================================*/
/* Table: VEICULO                                               */
/*==============================================================*/
create table VEICULO
(
   VEICULO_ID           int not null auto_increment,
   MODELO_ID            int(11),
   COR                  varchar(12),
   CHASSI               varchar(32),
   CAPACIDADE           smallint,
   PLACA                char(7),
   PROCEDENCIA          int,
   ZEROKM               bool,
   primary key (VEICULO_ID)
);

/*==============================================================*/
/* Table: VEICULO_PROPRIETARIO                                  */
/*==============================================================*/
create table VEICULO_PROPRIETARIO
(
   VEICULO_ID           int not null,
   PROPRIETARIO_ID      int not null,
   primary key (VEICULO_ID, PROPRIETARIO_ID)
);

alter table CLIENTE add constraint FK_CLIENTE_FRANQUIA foreign key (FRANQUIA_ID)
      references FRANQUIA (FRANQUIA_ID) on delete restrict on update restrict;

alter table FRANQUIA add constraint FK_PROPRIETARIO_FRANQUIA foreign key (PROPRIETARIO_ID)
      references PROPRIETARIO (PROPRIETARIO_ID) on delete restrict on update restrict;

alter table MARCA add constraint FK_MARCA_TIPO foreign key (TIPO_ID)
      references TIPO (TIPO_ID) on delete restrict on update restrict;

alter table MODELO add constraint FK_MARCA_MODELO foreign key (MARCA_ID)
      references MARCA (MARCA_ID) on delete restrict on update restrict;

alter table ORDEM_PEDIDO add constraint FK_REL_PEDIDOSTATUS_ORDEM_PEDIDO foreign key (PEDIDO_STATUS_ID)
      references PEDIDO_STATUS (PEDIDO_STATUS_ID) on delete restrict on update restrict;

alter table PRODUTO add constraint FK_FRANQUIA_PRODUTO foreign key (FRANQUIA_ID)
      references FRANQUIA (FRANQUIA_ID) on delete restrict on update restrict;

alter table PROPRIETARIO add constraint FK_PERMISSAO_PROPRIETARIO foreign key (PERMISSAO_ID)
      references PERMISSAO (PERMISSAO_ID) on delete restrict on update restrict;

alter table REL_CLIENTE_ORDEM_PEDIDO add constraint FK_REL_CLIENTE_ORDEM_PEDIDO foreign key (CLIENTE_ID)
      references CLIENTE (CLIENTE_ID) on delete restrict on update restrict;

alter table REL_CLIENTE_ORDEM_PEDIDO add constraint FK_REL_CLIENTE_ORDEM_PEDIDO2 foreign key (ORDEM_ID)
      references ORDEM_PEDIDO (ORDEM_ID) on delete restrict on update restrict;

alter table REL_PRODUTO_ORDEM_PEDIDO add constraint FK_REL_PRODUTO_ORDEM_PEDIDO foreign key (PRODUTO_ID)
      references PRODUTO (PRODUTO_ID) on delete restrict on update restrict;

alter table REL_PRODUTO_ORDEM_PEDIDO add constraint FK_REL_PRODUTO_ORDEM_PEDIDO2 foreign key (ORDEM_ID)
      references ORDEM_PEDIDO (ORDEM_ID) on delete restrict on update restrict;

alter table REL_PROD_SUBCATEGORIA add constraint FK_REL_PROD_SUBCATEGORIA foreign key (PRODUTO_ID)
      references PRODUTO (PRODUTO_ID) on delete restrict on update restrict;

alter table REL_PROD_SUBCATEGORIA add constraint FK_REL_PROD_SUBCATEGORIA2 foreign key (SUBCATEGORIA_ID)
      references SUBCATEGORIA (SUBCATEGORIA_ID) on delete restrict on update restrict;

alter table SUBCATEGORIA add constraint FK_REL_SUBCATEGORIA foreign key (CATEGORIA_ID)
      references CATEGORIA (CATEGORIA_ID) on delete restrict on update restrict;

alter table VEICULO add constraint FK_MODELO_VEICULO foreign key (MODELO_ID)
      references MODELO (MODELO_ID) on delete restrict on update restrict;

alter table VEICULO_PROPRIETARIO add constraint FK_VEICULO_PROPRIETARIO foreign key (VEICULO_ID)
      references VEICULO (VEICULO_ID) on delete restrict on update restrict;

alter table VEICULO_PROPRIETARIO add constraint FK_VEICULO_PROPRIETARIO2 foreign key (PROPRIETARIO_ID)
      references PROPRIETARIO (PROPRIETARIO_ID) on delete restrict on update restrict;
