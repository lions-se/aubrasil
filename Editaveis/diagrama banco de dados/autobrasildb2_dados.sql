/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50051
Source Host           : localhost:3306
Source Database       : autobrasildb2

Target Server Type    : MYSQL
Target Server Version : 50051
File Encoding         : 65001

Date: 2013-12-12 10:08:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categoria
-- ----------------------------
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `CATEGORIA_ID` int(11) NOT NULL auto_increment,
  `FRANQUIA_ID` int(11) default NULL,
  `NOME` varchar(30) default NULL,
  `NOMEINTERNO` varchar(30) NOT NULL,
  `URL` varchar(30) NOT NULL,
  PRIMARY KEY  (`CATEGORIA_ID`),
  KEY `FK_FRANQUIA_CATEGORIA` (`FRANQUIA_ID`),
  CONSTRAINT `FK_FRANQUIA_CATEGORIA` FOREIGN KEY (`FRANQUIA_ID`) REFERENCES `franquia` (`FRANQUIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categoria
-- ----------------------------
INSERT INTO `categoria` VALUES ('1', '1', 'Alarme', 'alarme', 'alarme');

-- ----------------------------
-- Table structure for franquia
-- ----------------------------
DROP TABLE IF EXISTS `franquia`;
CREATE TABLE `franquia` (
  `FRANQUIA_ID` int(11) NOT NULL auto_increment,
  `PROPRIETARIO_ID` int(11) NOT NULL,
  `NOMEFRANQUIA` varchar(60) default NULL,
  `SLOGAN` varchar(100) default NULL,
  `TELFRANQUIA` varchar(16) NOT NULL,
  `ENDERECO` char(255) default NULL,
  `FRANQUIA_ATIVA` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`FRANQUIA_ID`),
  KEY `FK_FK_PROPRIETARIO_FRANQUIA2` (`PROPRIETARIO_ID`),
  CONSTRAINT `FK_FK_PROPRIETARIO_FRANQUIA2` FOREIGN KEY (`PROPRIETARIO_ID`) REFERENCES `proprietario` (`PROPRIETARIO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of franquia
-- ----------------------------
INSERT INTO `franquia` VALUES ('1', '1', 'Minha Franquia teste', 'Teste De Slogan', '3633 7085', 'Rua Major Moreira', '0');

-- ----------------------------
-- Table structure for produto
-- ----------------------------
DROP TABLE IF EXISTS `produto`;
CREATE TABLE `produto` (
  `PRODUTO_ID` int(11) NOT NULL auto_increment,
  `CATEGORIA_ID` int(11) NOT NULL,
  `FRANQUIA_ID` int(11) default NULL,
  `NOME` varchar(30) NOT NULL,
  `DESCRICAO` text NOT NULL,
  `IMAGEM1` varchar(70) default NULL,
  `IMAGEM2` varchar(70) default NULL,
  `IMAGEM3` varchar(70) default NULL,
  `IMAGEM4` varchar(70) default NULL,
  `IMAGEM5` varchar(70) default NULL,
  `IMAGEM6` varchar(70) default NULL,
  PRIMARY KEY  (`PRODUTO_ID`),
  KEY `FK_FRANQUIA_PRODUTO` (`FRANQUIA_ID`),
  KEY `FK_PRODUTO_CATEGORIA` (`CATEGORIA_ID`),
  CONSTRAINT `FK_FRANQUIA_PRODUTO` FOREIGN KEY (`FRANQUIA_ID`) REFERENCES `franquia` (`FRANQUIA_ID`),
  CONSTRAINT `FK_PRODUTO_CATEGORIA` FOREIGN KEY (`CATEGORIA_ID`) REFERENCES `categoria` (`CATEGORIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of produto
-- ----------------------------
INSERT INTO `produto` VALUES ('1', '1', '1', 'Produto de Teste', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In id turpis enim. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam cursus iaculis sodales. Phasellus ut orci non nisi sagittis auctor. Nulla commodo libero eu interdum sollicitudin. Donec in sagittis mauris, ac semper risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat ipsum eget diam porttitor sagittis. In fermentum, augue vel vestibulum sollicitudin, ante. ', 'carro1.jpg', 'carro1.jpg', 'carro3.jpg', 'carro4.jpg', 'carro5.jpg', 'carro6.jpg');

-- ----------------------------
-- Table structure for proprietario
-- ----------------------------
DROP TABLE IF EXISTS `proprietario`;
CREATE TABLE `proprietario` (
  `PROPRIETARIO_ID` int(11) NOT NULL auto_increment,
  `FRANQUIA_ID` int(11) default NULL,
  `EMAIL` varchar(60) NOT NULL,
  `PASSWD` varchar(64) NOT NULL,
  `CPF` varchar(12) NOT NULL,
  `RG` varchar(12) NOT NULL,
  `DATA_REGISTRO` datetime NOT NULL,
  `DATA_NASCIMENTO` date NOT NULL,
  `CONFIRMA_CADASTRO` tinyint(1) default NULL,
  PRIMARY KEY  (`PROPRIETARIO_ID`),
  KEY `FK_FK_PROPRIETARIO_FRANQUIA` (`FRANQUIA_ID`),
  CONSTRAINT `FK_FK_PROPRIETARIO_FRANQUIA` FOREIGN KEY (`FRANQUIA_ID`) REFERENCES `franquia` (`FRANQUIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of proprietario
-- ----------------------------
INSERT INTO `proprietario` VALUES ('1', null, 'nicholasluis@gmail.com', 'admin', '34808074885', '410717211', '2013-12-10 17:47:57', '2013-12-10', '0');

-- ----------------------------
-- Table structure for subcategoria
-- ----------------------------
DROP TABLE IF EXISTS `subcategoria`;
CREATE TABLE `subcategoria` (
  `SUBCATEGORIA_ID` int(11) NOT NULL auto_increment,
  `CATEGORIA_ID` int(11) default NULL,
  `NOME` varchar(30) default NULL,
  `NOMEINTERNO` varchar(30) NOT NULL,
  `URL` varchar(30) NOT NULL,
  PRIMARY KEY  (`SUBCATEGORIA_ID`),
  KEY `FK_CATEGORIA_SUBCATEGORIA` (`CATEGORIA_ID`),
  CONSTRAINT `FK_CATEGORIA_SUBCATEGORIA` FOREIGN KEY (`CATEGORIA_ID`) REFERENCES `categoria` (`CATEGORIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of subcategoria
-- ----------------------------
INSERT INTO `subcategoria` VALUES ('1', '1', 'sdffd', 'fggf', 'fgfg');
