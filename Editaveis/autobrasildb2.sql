/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50535
Source Host           : localhost:3306
Source Database       : autobrasildb2

Target Server Type    : MYSQL
Target Server Version : 50535
File Encoding         : 65001

Date: 2014-02-27 09:50:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categoria
-- ----------------------------
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `CATEGORIA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOMEINTERNO` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`CATEGORIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of categoria
-- ----------------------------
INSERT INTO `categoria` VALUES ('1', 'Alarmes', 'alarmes', 'alarmes');
INSERT INTO `categoria` VALUES ('2', 'GPS', 'gps', 'gps');
INSERT INTO `categoria` VALUES ('3', 'Cabos e Fios', 'cabos-e-fios', 'cabos');
INSERT INTO `categoria` VALUES ('32', 'Carros', 'carros', '');
INSERT INTO `categoria` VALUES ('33', 'Teste 3', 'teste-3', '');

-- ----------------------------
-- Table structure for categoria_subcategoria
-- ----------------------------
DROP TABLE IF EXISTS `categoria_subcategoria`;
CREATE TABLE `categoria_subcategoria` (
  `SUBCATEGORIA_ID` int(11) NOT NULL,
  `CATEGORIA_ID` int(11) NOT NULL,
  PRIMARY KEY (`SUBCATEGORIA_ID`,`CATEGORIA_ID`),
  KEY `FK_CATEGORIA_SUBCATEGORIA2` (`CATEGORIA_ID`),
  CONSTRAINT `FK_CATEGORIA_SUBCATEGORIA` FOREIGN KEY (`SUBCATEGORIA_ID`) REFERENCES `subcategoria` (`SUBCATEGORIA_ID`),
  CONSTRAINT `FK_CATEGORIA_SUBCATEGORIA2` FOREIGN KEY (`CATEGORIA_ID`) REFERENCES `categoria` (`CATEGORIA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of categoria_subcategoria
-- ----------------------------
INSERT INTO `categoria_subcategoria` VALUES ('14', '1');
INSERT INTO `categoria_subcategoria` VALUES ('2', '2');
INSERT INTO `categoria_subcategoria` VALUES ('1', '3');
INSERT INTO `categoria_subcategoria` VALUES ('13', '3');
INSERT INTO `categoria_subcategoria` VALUES ('15', '33');

-- ----------------------------
-- Table structure for classificados
-- ----------------------------
DROP TABLE IF EXISTS `classificados`;
CREATE TABLE `classificados` (
  `CLASSIFICADO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGEM` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXTO` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`CLASSIFICADO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of classificados
-- ----------------------------
INSERT INTO `classificados` VALUES ('1', 'Tetse', 'dffg', 'Sfgfg');
INSERT INTO `classificados` VALUES ('2', 'Teste2', 'rr', 'rrtttrtyty');

-- ----------------------------
-- Table structure for cliente
-- ----------------------------
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente` (
  `CLIENTE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FRANQUIA_ID` int(11) NOT NULL,
  `NOME` varchar(30) NOT NULL,
  `SOBRENOME` varchar(70) NOT NULL,
  `CPF` varchar(12) NOT NULL,
  `RG` varchar(12) NOT NULL,
  `ESTADOCIVIL` smallint(6) DEFAULT NULL,
  `SEXO` char(1) DEFAULT NULL,
  `DATA_NASCIMENTO` date DEFAULT NULL,
  `LOGIN` varchar(60) DEFAULT NULL,
  `SENHA` varchar(64) NOT NULL,
  `EMAILSECUNDARIO` varchar(60) NOT NULL,
  `DESCRICAO` text,
  `PAIS` char(18) DEFAULT NULL,
  `DATA_REGISTRO` datetime NOT NULL,
  `CODIGO_CADASTRO` varchar(64) DEFAULT NULL,
  `CONFIRMA_CADASTRO` tinyint(1) DEFAULT NULL,
  `CODIGO_REFERENCIA` varchar(64) DEFAULT NULL,
  `TELEFONE` varchar(12) NOT NULL,
  `TELEFONECOMERCIAL` varchar(16) DEFAULT NULL,
  `TELEFONECELULAR` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`CLIENTE_ID`),
  KEY `FK_CLIENTE_FRANQUIA` (`FRANQUIA_ID`),
  CONSTRAINT `FK_CLIENTE_FRANQUIA` FOREIGN KEY (`FRANQUIA_ID`) REFERENCES `franquia` (`FRANQUIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cliente
-- ----------------------------
INSERT INTO `cliente` VALUES ('1', '3', 'Nicholasas111', 'Oliveira', '34808074885', '410717241', '1', 'M', '2014-02-21', 'nicholasluis@gmail.com', '$2y$10$NSdYKbYdJTT.G96/0pUVJ.RhhRY5g.iE6YFYGz.algB9sMJ.mAHsW', 'ryonagana@gmail.com', null, 'Brasil', '2014-01-28 16:49:09', 'OTY0MGE4ZWNiYWVhNmRhZDU0Mjk4ZTU0ZjZmMDc5ZjI', '1', 'MlcNOY', '55123646818', '55123646818', '55123646818');
INSERT INTO `cliente` VALUES ('2', '3', 'João', 'Souza', '34808074885', '410717241', '1', 'M', '0000-00-00', 'joao@embratel.com', '$2y$10$x67nkPoc8xnpzgijvBjD0uOplY/UBwPm5J3xl8FFpahSah0C2OkDO', 'nicholasluis@gmail.com', '', null, '0000-00-00 00:00:00', 'Mjk1NTQ3YmFkMzZjZDlhNzMxNmVkMDliNTE0NmRlZGM', '1', 'SABOAO', '55123646818', null, null);
INSERT INTO `cliente` VALUES ('3', '3', 'Nicholas', 'Oliveira', '3408074885', '41071724-1', '1', 'M', '0000-00-00', 'nicholasluis@gmail.com', '$2y$10$9bco78q1SZiLV064GwoGLeHFwgiVB7/XzLYANUyoKevmHnKyLhYBG', 'nicholasluis@gmail.com', null, null, '2014-02-13 15:12:27', 'N2ZmMDZlMGQzMWI2MDY2Y2FiYWZiMzdhNzY0ZjcwN2E', '1', '2YMDZz', '', null, null);
INSERT INTO `cliente` VALUES ('4', '3', 'Jose', 'Arimateia', '3408074885', '1236426818', '2', 'M', '0000-00-00', 'ryonagana@gmail.com', '$2y$10$26ADSMMMmtmT8e2PkhQuyOLo6kCrayv2sFfGj9JElmdcLVsP5pqd.', 'nicholasluis@gmail.com', null, null, '2014-02-13 15:14:56', 'ZDAyNTZiYTk0Njc3NjNhMGZkZGVmMjdjMGViZGM1ZWY', null, 'MG0ZZA', '', null, null);
INSERT INTO `cliente` VALUES ('5', '3', 'Carlos', 'Gama', '3408074885', '410717241', '1', 'M', '0000-00-00', 'ryonagana@gmail.com', '$2y$10$nPY7XG1JWKeoeSKAc8xRtuNxNYDcjFQKOmjsFIvrQgtDrjiFgzmlW', 'nicholasluis@gmail.com', null, null, '2014-02-13 15:46:17', 'ZDkwYjcwM2RmMjdkODE5NDFiN2VjMmM5MDQ0N2VkYWI', null, 'DdOWDj', '', null, null);
INSERT INTO `cliente` VALUES ('6', '3', 'Carlos', 'Nogueira', '3408074885', '410717241', '1', 'M', '0000-00-00', 'ryonagana@gmail.com', '$2y$10$Px6AcKJajMvanigEwKEKL.795allik5UJGS6.r8LBpxCwCSwtglIy', 'nicholasluis@gmail.com', null, null, '2014-02-13 15:47:55', 'ODdhZjBhYTM2OTY0YzFkOGNlZDE5YjcwODc2ZDQ2ZjU', null, 'TdjDOY', '', null, null);
INSERT INTO `cliente` VALUES ('7', '3', 'Franquia de Teste', 'Arimateia', '3408074885', '1236426818', '1', 'M', '0000-00-00', 'ryonagana@gmail.com', '$2y$10$7uHC8Fne1kgTwIkRF/UyQeACwaEYosviAtbZYRoNOp9pacBH9NOJW', 'nicholasluis@gmail.com', null, null, '2014-02-13 15:51:27', 'NmZhMzJlNWI3ZDA2M2RlOWJhZjM4MjgzZmU0Y2I4NjM', null, 'jzhz2I', '', null, null);
INSERT INTO `cliente` VALUES ('8', '3', 'Franquia de Teste', 'Arimateia', '3408074885', '1236426818', '1', 'M', '0000-00-00', 'ryonagana@gmail.com', '$2y$10$4lk1yFH49cBnDmdtjYlgWuYkQuwQMEawcEEPkRYnxc9mZCdLtgbXy', 'nicholasluis@gmail.com', null, null, '2014-02-13 15:55:15', 'NzdiMmU1MmMwZjIzMjI3NmQyNWE4YmRkZDBjNDBiYjI', '1', 'NRkjiz', '', null, null);

-- ----------------------------
-- Table structure for conteudo_estatico
-- ----------------------------
DROP TABLE IF EXISTS `conteudo_estatico`;
CREATE TABLE `conteudo_estatico` (
  `CONTEUDO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTEUDO_AFRANQUIA` text COLLATE utf8_unicode_ci,
  `CONTEUDO_AEMPRESA` text COLLATE utf8_unicode_ci,
  `CONTEUDO_TREINA` text COLLATE utf8_unicode_ci,
  `CONTEUDO_SERVICOS` text COLLATE utf8_unicode_ci,
  `CONTEUDO_SUAFRANQUIA` text COLLATE utf8_unicode_ci,
  `CONTEUDO_TERMOS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`CONTEUDO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of conteudo_estatico
-- ----------------------------
INSERT INTO `conteudo_estatico` VALUES ('1', '<h1></h1><h1><span style=\"color: rgb(0, 0, 0);\">Franquiasaaseb</span></h1><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; font-family: Arial, Helvetica, sans;\"><span style=\"color: rgb(0, 0, 0);\">Lorem ipsum dolor sit </span><span style=\"color: rgb(255, 0, 0);\">amet,</span><span style=\"color: rgb(0, 0, 0);\"> consectetur adipiscing elit. Donec dictum rutrum nibh vitae tempor. Fusce feugiat viverra nisl ac pretium. Pellentesque facilisis magna vitae est pulvinar fringilla. Nam sit amet laoreet ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante ante, tempus vitae augue eu, tincidunt rutrum risus. Duis dapibus, leo sit amet laoreet porttitor, nibh urna semper purus, fringilla semper augue mi eu mi. Praesent molestie erat odio, et aliquet diam egestas non. Quisque rhoncus commodo volutpat. Nulla facilisi. In euismod, tortor sed feugiat posuere, lorem dolor porttitor magna, vitae viverra magna enim in magna. Morbi dictum condimentum tempus. Pellentesque ac vestibulum ipsum.</span></p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed mattis porta felis, eu placerat turpis pellentesque vel. Cras a tempus mi. Mauris malesuada mollis massa, elementum auctor est hendrerit ac. Fusce id felis sed ipsum vehicula tempus. Sed dapibus augue posuere orci porta, quis sagittis libero auctor. Vivamus scelerisque massa neque, ornare mollis purus ornare eget. Nam scelerisque, leo non euismod lacinia, enim urna tristique dolor, dictum tristique justo nunc eu libero.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis feugiat ullamcorper massa eget ullamcorper. Morbi eleifend mi sit amet neque semper, eget dictum magna iaculis. Cras ultricies tempus nisl, placerat interdum tellus viverra non. Etiam quis ligula erat. Pellentesque molestie, nunc nec pulvinar interdum, metus ipsum dignissim leo, et scelerisque risus dolor id dui. Vestibulum sollicitudin nisi justo, facilisis euismod ipsum condimentum eu. In a convallis mauris.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Praesent et iaculis nisl. Nulla massa enim, pulvinar ut molestie ac, aliquet vitae neque. Vivamus sagittis felis id magna pulvinar, ac convallis massa scelerisque. Maecenas ut adipiscing massa. Pellentesque lobortis augue in magna facilisis, eget pellentesque lorem aliquam. Etiam ultricies, elit sit amet blandit egestas, sapien lectus porta nisi, sit amet sollicitudin diam arcu ut magna. Ut sapien nisl, adipiscing in suscipit a, ullamcorper et felis. Curabitur in dictum augue. Cras sagittis sagittis nisi, nec imperdiet augue elementum ac.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Integer ornare tempus ante ac tempus. Nullam blandit massa ante, vel cursus risus adipiscing in. Integer ipsum est, congue a tincidunt at, molestie eu diam. Sed a elit lobortis, auctor enim ut, dictum lorem. Donec dapibus non purus accumsan congue. Duis posuere consectetur turpis condimentum iaculis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris feugiat luctus posuere. Phasellus eu erat quam. Vivamus dictum sodales enim, blandit facilisis dolor tincidunt quis. Praesent et nibh id diam tincidunt laoreet. Vivamus lobortis gravida turpis vel blandit. Vestibulum eros ipsum, cursus et convallis id, consequat id urna.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis volutpat venenatis commodo. Nunc vitae justo nec augue eleifend porta. Phasellus molestie nibh sed facilisis laoreet. Donec sit amet odio quis lacus egestas dictum eu sed lacus. Donec ac tincidunt sapien. Morbi vulputate magna sagittis neque iaculis commodo. In accumsan mattis leo eu volutpat. Aenean dignissim, nunc et imperdiet molestie, ante nulla venenatis nisi, nec auctor urna ipsum vel magna. Donec massa leo, placerat nec est id, ultrices fringilla nunc. Integer id metus at neque vehicula hendrerit. Phasellus eget sollicitudin nisi. Ut adipiscing velit sapien, at convallis dui accumsan non. Vivamus vel pulvinar velit, malesuada fringilla lectus. Morbi in purus ut magna feugiat eleifend quis eget odio.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nunc justo tortor, cursus quis dictum ut, venenatis consequat felis. Integer varius ut magna a tristique. Integer dictum eget lectus sed aliquet. Aliquam nec dapibus metus. Pellentesque ultrices risus arcu, id egestas sem vestibulum non. Fusce pulvinar tortor ut risus porttitor, id fringilla sem congue. Integer eget nulla eleifend, fringilla magna a, imperdiet tortor. In vehicula lacinia auctor. Aenean egestas velit nec felis commodo tempor at a dolor. Vestibulum eget placerat nisi.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut mollis sed mauris non pellentesque. Pellentesque ut purus eu metus bibendum luctus ultricies at diam. Sed posuere malesuada mauris sed interdum. Fusce quis lectus at purus condimentum facilisis. Aenean accumsan massa id tellus gravida, et suscipit ante ornare. Integer ac bibendum dui. Pellentesque at quam nisi. Fusce venenatis est id venenatis rutrum.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla dictum nulla turpis, ac vestibulum metus volutpat ut. Vestibulum interdum laoreet est, quis sodales nibh iaculis eget. Quisque laoreet tortor id metus bibendum ullamcorper. Vestibulum molestie elementum lectus, quis posuere elit auctor ut. Etiam non leo ac mauris volutpat rutrum. Morbi suscipit, eros ut ornare pretium, sem ante rhoncus urna, lobortis bibendum magna dolor non lacus. Cras sodales risus nec orci posuere, a vulputate massa venenatis. Etiam interdum velit eros, ac pellentesque nisi tempor at. Duis eget dolor quam. Ut id velit vel diam sollicitudin congue. Etiam et pharetra ligula. Nulla ac semper mauris, a rutrum justo. Pellentesque eleifend tincidunt odio vel imperdiet. Etiam sit amet mollis arcu. In hac habitasse platea dictumst.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque dapibus, quam eget sodales eleifend, mi nisl egestas tortor, a molestie orci ante at libero. Proin rutrum sollicitudin eleifend. Ut vehicula, felis quis laoreet iaculis, dolor massa scelerisque orci, vel interdum tellus magna in lacus. Suspendisse facilisis ante ut felis gravida molestie. Nullam aliquet, velit et pellentesque posuere, sapien nisi commodo erat, sed dignissim turpis diam sit amet felis. Duis ac adipiscing nunc. Quisque fringilla fermentum auctor. Proin tempus diam non enim sagittis molestie. Aenean eu lorem euismod quam elementum viverra fringilla sed nibh. Vivamus eget tortor diam. Fusce posuere sem molestie rhoncus eleifend. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Aenean non nisi quis ligula elementum porttitor. Maecenas ac eleifend ligula. Integer nec mollis magna. Vivamus molestie fringilla libero non vestibulum. Proin interdum tellus quis ultrices accumsan. Nam pulvinar metus urna. In hac habitasse platea dictumst. Donec consequat iaculis lectus at tempor. Donec eget lectus odio. Integer laoreet non nulla sit amet luctus. Etiam semper tellus nibh, mollis accumsan massa congue id. Nunc ac aliquet orci, quis interdum massa.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Quisque eget turpis sit amet nisl sagittis iaculis. Mauris quis viverra lorem, quis faucibus leo. Fusce consectetur laoreet magna, et viverra dolor viverra id. Sed rhoncus massa a auctor adipiscing. Suspendisse porttitor et eros in sollicitudin. Nunc porta in nibh pulvinar ornare. Ut a congue massa. Vivamus nisl orci, volutpat eu libero quis, bibendum lacinia mi. Nunc accumsan ornare augue eget malesuada. Donec imperdiet ante tellus, in lacinia purus posuere at. In dictum egestas augue eget eleifend. Pellentesque vehicula nisl ac velit rhoncus, eget ullamcorper mauris dapibus.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce blandit sollicitudin congue. Morbi condimentum hendrerit dui, in semper lorem porttitor nec. Nunc vitae vulputate ligula, id mollis nisi. Morbi ante nibh, viverra porta nulla posuere, pellentesque dictum nisl. Cras quis quam orci. Morbi viverra, ligula eu pellentesque vehicula, sem justo placerat tellus, semper eleifend libero erat vel urna. In hac habitasse platea dictumst. Sed accumsan pulvinar lorem sit amet euismod. Ut quis orci sed erat ultrices consequat eu pellentesque diam. Morbi scelerisque convallis risus, eu tristique metus auctor sit amet. Nunc dolor velit, laoreet ac ipsum ac, volutpat dapibus enim. Proin vel accumsan metus, a ornare quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis posuere iaculis.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Etiam mattis sodales leo, a vestibulum nibh pretium sit amet. Nullam suscipit felis accumsan laoreet adipiscing. Morbi sed arcu suscipit, consequat nisi nec, ultricies mi. Proin adipiscing libero vitae felis commodo ullamcorper. Vivamus at congue sapien, at sagittis tortor. Phasellus at quam nec augue malesuada vulputate. Nam eleifend nunc magna, quis suscipit leo ultricies id. Pellentesque tristique massa non posuere convallis. Proin auctor semper varius. Maecenas commodo euismod leo, eget feugiat erat porta eget.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Maecenas aliquet, magna ac dignissim malesuada, lorem magna porttitor elit, a malesuada felis ante id velit. Donec sit amet lorem justo. Morbi nec fringilla felis, nec interdum dui. In adipiscing libero erat, ut vehicula nisi auctor nec. Nulla facilisi. Cras eget iaculis justo. Cras quis convallis purus. Nulla sit amet nisl nibh.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis tempus feugiat mollis. Sed tempus at velit nec hendrerit. Curabitur et adipiscing ipsum, nec volutpat odio. Proin auctor sollicitudin pharetra. Donec vestibulum, elit ut rutrum dapibus, odio nisl vestibulum justo, nec tincidunt ipsum enim eu velit. Nulla sit amet convallis risus. Cras ipsum nibh, cursus sed tristique vitae, auctor facilisis nunc. Maecenas pharetra interdum dolor semper aliquet. Donec nulla nisi, consectetur sed tortor at, varius faucibus diam. Maecenas condimentum tortor sem, faucibus fringilla quam sodales id. Cras eu faucibus tortor.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla luctus molestie mi sed sollicitudin. Sed condimentum ligula et interdum pharetra. Nulla fringilla massa velit, nec iaculis augue laoreet ut. Sed consectetur volutpat enim. Nulla facilisi. Integer iaculis leo urna. Nunc pulvinar tortor dictum nibh dictum pellentesque. Sed malesuada enim nunc, ut porttitor mauris mollis ut. Pellentesque feugiat quis est at tempus. Phasellus vel nibh ac nulla consequat viverra. Nulla porta vitae enim sit amet lacinia. Sed sed ullamcorper ipsum, a egestas mi. Cras ullamcorper interdum urna, ut auctor odio faucibus eget. Quisque tempus id ante in porttitor.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut hendrerit est sit amet mauris tempor tincidunt. Donec egestas arcu lorem, nec posuere urna accumsan eu. Nullam molestie elit a semper sagittis. Fusce sagittis quam nec lorem laoreet, vitae gravida risus ullamcorper. Ut sapien enim, vestibulum non elementum id, tempor ac dui. Vivamus fermentum pulvinar nisi, ut rhoncus ipsum lacinia at. Aenean eu sollicitudin libero, quis luctus dui. Sed eu aliquam odio. Fusce ante nunc, dignissim et sapien placerat, ultrices tempus mi. Integer quis tellus blandit sem ultricies cursus vel adipiscing quam. Donec vestibulum metus vel dui dictum dignissim. Maecenas tristique imperdiet urna in commodo. Etiam blandit faucibus urna, ac adipiscing est accumsan viverra. Cras eget arcu fermentum, egestas leo et, posuere diam. Nullam a venenatis odio.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed commodo, nunc vel iaculis tempus, ligula est viverra felis, non blandit augue urna in est. Vivamus ullamcorper fermentum turpis, vitae ultrices velit malesuada nec. Cras sit amet aliquam leo. Nulla dapibus iaculis justo et faucibus. Etiam ultrices ante vel velit venenatis, ac consequat risus tincidunt. In at cursus dui, sed consectetur leo. Praesent nec suscipit est. Duis pellentesque lobortis porta. Vestibulum blandit est vel tellus tincidunt, sed commodo quam ultrices. Aenean lacinia odio quis augue pulvinar, vitae tempus sapien fringilla.</p>', '<h1 style=\"color: rgb(51, 51, 51);\"><ul><li><span style=\"color: rgb(0, 0, 0); line-height: 1.1;\">Empresa</span><br></li></ul></h1><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; font-family: Arial, Helvetica, sans;\"><span style=\"color: rgb(0, 0, 0);\">Lorem ipsum dolor sit </span><span style=\"color: rgb(255, 0, 0);\">amet,</span><span style=\"color: rgb(0, 0, 0);\"> consectetur adipiscing elit. Donec dictum rutrum nibh vitae tempor. Fusce feugiat viverra nisl ac pretium. Pellentesque facilisis magna vitae est pulvinar fringilla. Nam sit amet laoreet ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante ante, tempus vitae augue eu, tincidunt rutrum risus. Duis dapibus, leo sit amet laoreet porttitor, nibh urna semper purus, fringilla semper augue mi eu mi. Praesent molestie erat odio, et aliquet diam egestas non. Quisque rhoncus commodo volutpat. Nulla facilisi. In euismod, tortor sed feugiat posuere, lorem dolor porttitor magna, vitae viverra magna enim in magna. Morbi dictum condimentum tempus. Pellentesque ac vestibulum ipsum.</span></p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed mattis porta felis, eu placerat turpis pellentesque vel. Cras a tempus mi. Mauris malesuada mollis massa, elementum auctor est hendrerit ac. Fusce id felis sed ipsum vehicula tempus. Sed dapibus augue posuere orci porta, quis sagittis libero auctor. Vivamus scelerisque massa neque, ornare mollis purus ornare eget. Nam scelerisque, leo non euismod lacinia, enim urna tristique dolor, dictum tristique justo nunc eu libero.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis feugiat ullamcorper massa eget ullamcorper. Morbi eleifend mi sit amet neque semper, eget dictum magna iaculis. Cras ultricies tempus nisl, placerat interdum tellus viverra non. Etiam quis ligula erat. Pellentesque molestie, nunc nec pulvinar interdum, metus ipsum dignissim leo, et scelerisque risus dolor id dui. Vestibulum sollicitudin nisi justo, facilisis euismod ipsum condimentum eu. In a convallis mauris.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Praesent et iaculis nisl. Nulla massa enim, pulvinar ut molestie ac, aliquet vitae neque. Vivamus sagittis felis id magna pulvinar, ac convallis massa scelerisque. Maecenas ut adipiscing massa. Pellentesque lobortis augue in magna facilisis, eget pellentesque lorem aliquam. Etiam ultricies, elit sit amet blandit egestas, sapien lectus porta nisi, sit amet sollicitudin diam arcu ut magna. Ut sapien nisl, adipiscing in suscipit a, ullamcorper et felis. Curabitur in dictum augue. Cras sagittis sagittis nisi, nec imperdiet augue elementum ac.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Integer ornare tempus ante ac tempus. Nullam blandit massa ante, vel cursus risus adipiscing in. Integer ipsum est, congue a tincidunt at, molestie eu diam. Sed a elit lobortis, auctor enim ut, dictum lorem. Donec dapibus non purus accumsan congue. Duis posuere consectetur turpis condimentum iaculis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris feugiat luctus posuere. Phasellus eu erat quam. Vivamus dictum sodales enim, blandit facilisis dolor tincidunt quis. Praesent et nibh id diam tincidunt laoreet. Vivamus lobortis gravida turpis vel blandit. Vestibulum eros ipsum, cursus et convallis id, consequat id urna.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis volutpat venenatis commodo. Nunc vitae justo nec augue eleifend porta. Phasellus molestie nibh sed facilisis laoreet. Donec sit amet odio quis lacus egestas dictum eu sed lacus. Donec ac tincidunt sapien. Morbi vulputate magna sagittis neque iaculis commodo. In accumsan mattis leo eu volutpat. Aenean dignissim, nunc et imperdiet molestie, ante nulla venenatis nisi, nec auctor urna ipsum vel magna. Donec massa leo, placerat nec est id, ultrices fringilla nunc. Integer id metus at neque vehicula hendrerit. Phasellus eget sollicitudin nisi. Ut adipiscing velit sapien, at convallis dui accumsan non. Vivamus vel pulvinar velit, malesuada fringilla lectus. Morbi in purus ut magna feugiat eleifend quis eget odio.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nunc justo tortor, cursus quis dictum ut, venenatis consequat felis. Integer varius ut magna a tristique. Integer dictum eget lectus sed aliquet. Aliquam nec dapibus metus. Pellentesque ultrices risus arcu, id egestas sem vestibulum non. Fusce pulvinar tortor ut risus porttitor, id fringilla sem congue. Integer eget nulla eleifend, fringilla magna a, imperdiet tortor. In vehicula lacinia auctor. Aenean egestas velit nec felis commodo tempor at a dolor. Vestibulum eget placerat nisi.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut mollis sed mauris non pellentesque. Pellentesque ut purus eu metus bibendum luctus ultricies at diam. Sed posuere malesuada mauris sed interdum. Fusce quis lectus at purus condimentum facilisis. Aenean accumsan massa id tellus gravida, et suscipit ante ornare. Integer ac bibendum dui. Pellentesque at quam nisi. Fusce venenatis est id venenatis rutrum.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla dictum nulla turpis, ac vestibulum metus volutpat ut. Vestibulum interdum laoreet est, quis sodales nibh iaculis eget. Quisque laoreet tortor id metus bibendum ullamcorper. Vestibulum molestie elementum lectus, quis posuere elit auctor ut. Etiam non leo ac mauris volutpat rutrum. Morbi suscipit, eros ut ornare pretium, sem ante rhoncus urna, lobortis bibendum magna dolor non lacus. Cras sodales risus nec orci posuere, a vulputate massa venenatis. Etiam interdum velit eros, ac pellentesque nisi tempor at. Duis eget dolor quam. Ut id velit vel diam sollicitudin congue. Etiam et pharetra ligula. Nulla ac semper mauris, a rutrum justo. Pellentesque eleifend tincidunt odio vel imperdiet. Etiam sit amet mollis arcu. In hac habitasse platea dictumst.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque dapibus, quam eget sodales eleifend, mi nisl egestas tortor, a molestie orci ante at libero. Proin rutrum sollicitudin eleifend. Ut vehicula, felis quis laoreet iaculis, dolor massa scelerisque orci, vel interdum tellus magna in lacus. Suspendisse facilisis ante ut felis gravida molestie. Nullam aliquet, velit et pellentesque posuere, sapien nisi commodo erat, sed dignissim turpis diam sit amet felis. Duis ac adipiscing nunc. Quisque fringilla fermentum auctor. Proin tempus diam non enim sagittis molestie. Aenean eu lorem euismod quam elementum viverra fringilla sed nibh. Vivamus eget tortor diam. Fusce posuere sem molestie rhoncus eleifend. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Aenean non nisi quis ligula elementum porttitor. Maecenas ac eleifend ligula. Integer nec mollis magna. Vivamus molestie fringilla libero non vestibulum. Proin interdum tellus quis ultrices accumsan. Nam pulvinar metus urna. In hac habitasse platea dictumst. Donec consequat iaculis lectus at tempor. Donec eget lectus odio. Integer laoreet non nulla sit amet luctus. Etiam semper tellus nibh, mollis accumsan massa congue id. Nunc ac aliquet orci, quis interdum massa.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Quisque eget turpis sit amet nisl sagittis iaculis. Mauris quis viverra lorem, quis faucibus leo. Fusce consectetur laoreet magna, et viverra dolor viverra id. Sed rhoncus massa a auctor adipiscing. Suspendisse porttitor et eros in sollicitudin. Nunc porta in nibh pulvinar ornare. Ut a congue massa. Vivamus nisl orci, volutpat eu libero quis, bibendum lacinia mi. Nunc accumsan ornare augue eget malesuada. Donec imperdiet ante tellus, in lacinia purus posuere at. In dictum egestas augue eget eleifend. Pellentesque vehicula nisl ac velit rhoncus, eget ullamcorper mauris dapibus.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce blandit sollicitudin congue. Morbi condimentum hendrerit dui, in semper lorem porttitor nec. Nunc vitae vulputate ligula, id mollis nisi. Morbi ante nibh, viverra porta nulla posuere, pellentesque dictum nisl. Cras quis quam orci. Morbi viverra, ligula eu pellentesque vehicula, sem justo placerat tellus, semper eleifend libero erat vel urna. In hac habitasse platea dictumst. Sed accumsan pulvinar lorem sit amet euismod. Ut quis orci sed erat ultrices consequat eu pellentesque diam. Morbi scelerisque convallis risus, eu tristique metus auctor sit amet. Nunc dolor velit, laoreet ac ipsum ac, volutpat dapibus enim. Proin vel accumsan metus, a ornare quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis posuere iaculis.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Etiam mattis sodales leo, a vestibulum nibh pretium sit amet. Nullam suscipit felis accumsan laoreet adipiscing. Morbi sed arcu suscipit, consequat nisi nec, ultricies mi. Proin adipiscing libero vitae felis commodo ullamcorper. Vivamus at congue sapien, at sagittis tortor. Phasellus at quam nec augue malesuada vulputate. Nam eleifend nunc magna, quis suscipit leo ultricies id. Pellentesque tristique massa non posuere convallis. Proin auctor semper varius. Maecenas commodo euismod leo, eget feugiat erat porta eget.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Maecenas aliquet, magna ac dignissim malesuada, lorem magna porttitor elit, a malesuada felis ante id velit. Donec sit amet lorem justo. Morbi nec fringilla felis, nec interdum dui. In adipiscing libero erat, ut vehicula nisi auctor nec. Nulla facilisi. Cras eget iaculis justo. Cras quis convallis purus. Nulla sit amet nisl nibh.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis tempus feugiat mollis. Sed tempus at velit nec hendrerit. Curabitur et adipiscing ipsum, nec volutpat odio. Proin auctor sollicitudin pharetra. Donec vestibulum, elit ut rutrum dapibus, odio nisl vestibulum justo, nec tincidunt ipsum enim eu velit. Nulla sit amet convallis risus. Cras ipsum nibh, cursus sed tristique vitae, auctor facilisis nunc. Maecenas pharetra interdum dolor semper aliquet. Donec nulla nisi, consectetur sed tortor at, varius faucibus diam. Maecenas condimentum tortor sem, faucibus fringilla quam sodales id. Cras eu faucibus tortor.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla luctus molestie mi sed sollicitudin. Sed condimentum ligula et interdum pharetra. Nulla fringilla massa velit, nec iaculis augue laoreet ut. Sed consectetur volutpat enim. Nulla facilisi. Integer iaculis leo urna. Nunc pulvinar tortor dictum nibh dictum pellentesque. Sed malesuada enim nunc, ut porttitor mauris mollis ut. Pellentesque feugiat quis est at tempus. Phasellus vel nibh ac nulla consequat viverra. Nulla porta vitae enim sit amet lacinia. Sed sed ullamcorper ipsum, a egestas mi. Cras ullamcorper interdum urna, ut auctor odio faucibus eget. Quisque tempus id ante in porttitor.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut hendrerit est sit amet mauris tempor tincidunt. Donec egestas arcu lorem, nec posuere urna accumsan eu. Nullam molestie elit a semper sagittis. Fusce sagittis quam nec lorem laoreet, vitae gravida risus ullamcorper. Ut sapien enim, vestibulum non elementum id, tempor ac dui. Vivamus fermentum pulvinar nisi, ut rhoncus ipsum lacinia at. Aenean eu sollicitudin libero, quis luctus dui. Sed eu aliquam odio. Fusce ante nunc, dignissim et sapien placerat, ultrices tempus mi. Integer quis tellus blandit sem ultricies cursus vel adipiscing quam. Donec vestibulum metus vel dui dictum dignissim. Maecenas tristique imperdiet urna in commodo. Etiam blandit faucibus urna, ac adipiscing est accumsan viverra. Cras eget arcu fermentum, egestas leo et, posuere diam. Nullam a venenatis odio.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed commodo, nunc vel iaculis tempus, ligula est viverra felis, non blandit augue urna in est. Vivamus ullamcorper fermentum turpis, vitae ultrices velit malesuada nec. Cras sit amet aliquam leo. Nulla dapibus iaculis justo et faucibus. Etiam ultrices ante vel velit venenatis, ac consequat risus tincidunt. In at cursus dui, sed consectetur leo. Praesent nec suscipit est. Duis pellentesque lobortis porta. Vestibulum blandit est vel tellus tincidunt, sed commodo quam ultrices. Aenean lacinia odio quis augue pulvinar, vitae tempus sapien fringilla.</p>', '<h1 style=\"color: rgb(51, 51, 51);\"></h1><h1><span style=\"color: rgb(0, 0, 0);\">Treinamentos</span></h1><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; font-family: Arial, Helvetica, sans;\"><span style=\"color: rgb(0, 0, 0);\">aaa</span></p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; font-family: Arial, Helvetica, sans;\"><span style=\"color: rgb(0, 0, 0);\">Lorem ipsum dolor sit </span><span style=\"color: rgb(255, 0, 0);\">amet,</span><span style=\"color: rgb(0, 0, 0);\"> consectetur adipiscing elit. Donec dictum rutrum nibh vitae tempor. Fusce feugiat viverra nisl ac pretium. Pellentesque facilisis magna vitae est pulvinar fringilla. Nam sit amet laoreet ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante ante, tempus vitae augue eu, tincidunt rutrum risus. Duis dapibus, leo sit amet laoreet porttitor, nibh urna semper purus, fringilla semper augue mi eu mi. Praesent molestie erat odio, et aliquet diam egestas non. Quisque rhoncus commodo volutpat. Nulla facilisi. In euismod, tortor sed feugiat posuere, lorem dolor porttitor magna, vitae viverra magna enim in magna. Morbi dictum condimentum tempus. Pellentesque ac vestibulum ipsum.</span></p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed mattis porta felis, eu placerat turpis pellentesque vel. Cras a tempus mi. Mauris malesuada mollis massa, elementum auctor est hendrerit ac. Fusce id felis sed ipsum vehicula tempus. Sed dapibus augue posuere orci porta, quis sagittis libero auctor. Vivamus scelerisque massa neque, ornare mollis purus ornare eget. Nam scelerisque, leo non euismod lacinia, enim urna tristique dolor, dictum tristique justo nunc eu libero.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis feugiat ullamcorper massa eget ullamcorper. Morbi eleifend mi sit amet neque semper, eget dictum magna iaculis. Cras ultricies tempus nisl, placerat interdum tellus viverra non. Etiam quis ligula erat. Pellentesque molestie, nunc nec pulvinar interdum, metus ipsum dignissim leo, et scelerisque risus dolor id dui. Vestibulum sollicitudin nisi justo, facilisis euismod ipsum condimentum eu. In a convallis mauris.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Praesent et iaculis nisl. Nulla massa enim, pulvinar ut molestie ac, aliquet vitae neque. Vivamus sagittis felis id magna pulvinar, ac convallis massa scelerisque. Maecenas ut adipiscing massa. Pellentesque lobortis augue in magna facilisis, eget pellentesque lorem aliquam. Etiam ultricies, elit sit amet blandit egestas, sapien lectus porta nisi, sit amet sollicitudin diam arcu ut magna. Ut sapien nisl, adipiscing in suscipit a, ullamcorper et felis. Curabitur in dictum augue. Cras sagittis sagittis nisi, nec imperdiet augue elementum ac.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Integer ornare tempus ante ac tempus. Nullam blandit massa ante, vel cursus risus adipiscing in. Integer ipsum est, congue a tincidunt at, molestie eu diam. Sed a elit lobortis, auctor enim ut, dictum lorem. Donec dapibus non purus accumsan congue. Duis posuere consectetur turpis condimentum iaculis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris feugiat luctus posuere. Phasellus eu erat quam. Vivamus dictum sodales enim, blandit facilisis dolor tincidunt quis. Praesent et nibh id diam tincidunt laoreet. Vivamus lobortis gravida turpis vel blandit. Vestibulum eros ipsum, cursus et convallis id, consequat id urna.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis volutpat venenatis commodo. Nunc vitae justo nec augue eleifend porta. Phasellus molestie nibh sed facilisis laoreet. Donec sit amet odio quis lacus egestas dictum eu sed lacus. Donec ac tincidunt sapien. Morbi vulputate magna sagittis neque iaculis commodo. In accumsan mattis leo eu volutpat. Aenean dignissim, nunc et imperdiet molestie, ante nulla venenatis nisi, nec auctor urna ipsum vel magna. Donec massa leo, placerat nec est id, ultrices fringilla nunc. Integer id metus at neque vehicula hendrerit. Phasellus eget sollicitudin nisi. Ut adipiscing velit sapien, at convallis dui accumsan non. Vivamus vel pulvinar velit, malesuada fringilla lectus. Morbi in purus ut magna feugiat eleifend quis eget odio.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nunc justo tortor, cursus quis dictum ut, venenatis consequat felis. Integer varius ut magna a tristique. Integer dictum eget lectus sed aliquet. Aliquam nec dapibus metus. Pellentesque ultrices risus arcu, id egestas sem vestibulum non. Fusce pulvinar tortor ut risus porttitor, id fringilla sem congue. Integer eget nulla eleifend, fringilla magna a, imperdiet tortor. In vehicula lacinia auctor. Aenean egestas velit nec felis commodo tempor at a dolor. Vestibulum eget placerat nisi.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut mollis sed mauris non pellentesque. Pellentesque ut purus eu metus bibendum luctus ultricies at diam. Sed posuere malesuada mauris sed interdum. Fusce quis lectus at purus condimentum facilisis. Aenean accumsan massa id tellus gravida, et suscipit ante ornare. Integer ac bibendum dui. Pellentesque at quam nisi. Fusce venenatis est id venenatis rutrum.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla dictum nulla turpis, ac vestibulum metus volutpat ut. Vestibulum interdum laoreet est, quis sodales nibh iaculis eget. Quisque laoreet tortor id metus bibendum ullamcorper. Vestibulum molestie elementum lectus, quis posuere elit auctor ut. Etiam non leo ac mauris volutpat rutrum. Morbi suscipit, eros ut ornare pretium, sem ante rhoncus urna, lobortis bibendum magna dolor non lacus. Cras sodales risus nec orci posuere, a vulputate massa venenatis. Etiam interdum velit eros, ac pellentesque nisi tempor at. Duis eget dolor quam. Ut id velit vel diam sollicitudin congue. Etiam et pharetra ligula. Nulla ac semper mauris, a rutrum justo. Pellentesque eleifend tincidunt odio vel imperdiet. Etiam sit amet mollis arcu. In hac habitasse platea dictumst.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque dapibus, quam eget sodales eleifend, mi nisl egestas tortor, a molestie orci ante at libero. Proin rutrum sollicitudin eleifend. Ut vehicula, felis quis laoreet iaculis, dolor massa scelerisque orci, vel interdum tellus magna in lacus. Suspendisse facilisis ante ut felis gravida molestie. Nullam aliquet, velit et pellentesque posuere, sapien nisi commodo erat, sed dignissim turpis diam sit amet felis. Duis ac adipiscing nunc. Quisque fringilla fermentum auctor. Proin tempus diam non enim sagittis molestie. Aenean eu lorem euismod quam elementum viverra fringilla sed nibh. Vivamus eget tortor diam. Fusce posuere sem molestie rhoncus eleifend. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Aenean non nisi quis ligula elementum porttitor. Maecenas ac eleifend ligula. Integer nec mollis magna. Vivamus molestie fringilla libero non vestibulum. Proin interdum tellus quis ultrices accumsan. Nam pulvinar metus urna. In hac habitasse platea dictumst. Donec consequat iaculis lectus at tempor. Donec eget lectus odio. Integer laoreet non nulla sit amet luctus. Etiam semper tellus nibh, mollis accumsan massa congue id. Nunc ac aliquet orci, quis interdum massa.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Quisque eget turpis sit amet nisl sagittis iaculis. Mauris quis viverra lorem, quis faucibus leo. Fusce consectetur laoreet magna, et viverra dolor viverra id. Sed rhoncus massa a auctor adipiscing. Suspendisse porttitor et eros in sollicitudin. Nunc porta in nibh pulvinar ornare. Ut a congue massa. Vivamus nisl orci, volutpat eu libero quis, bibendum lacinia mi. Nunc accumsan ornare augue eget malesuada. Donec imperdiet ante tellus, in lacinia purus posuere at. In dictum egestas augue eget eleifend. Pellentesque vehicula nisl ac velit rhoncus, eget ullamcorper mauris dapibus.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce blandit sollicitudin congue. Morbi condimentum hendrerit dui, in semper lorem porttitor nec. Nunc vitae vulputate ligula, id mollis nisi. Morbi ante nibh, viverra porta nulla posuere, pellentesque dictum nisl. Cras quis quam orci. Morbi viverra, ligula eu pellentesque vehicula, sem justo placerat tellus, semper eleifend libero erat vel urna. In hac habitasse platea dictumst. Sed accumsan pulvinar lorem sit amet euismod. Ut quis orci sed erat ultrices consequat eu pellentesque diam. Morbi scelerisque convallis risus, eu tristique metus auctor sit amet. Nunc dolor velit, laoreet ac ipsum ac, volutpat dapibus enim. Proin vel accumsan metus, a ornare quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis posuere iaculis.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Etiam mattis sodales leo, a vestibulum nibh pretium sit amet. Nullam suscipit felis accumsan laoreet adipiscing. Morbi sed arcu suscipit, consequat nisi nec, ultricies mi. Proin adipiscing libero vitae felis commodo ullamcorper. Vivamus at congue sapien, at sagittis tortor. Phasellus at quam nec augue malesuada vulputate. Nam eleifend nunc magna, quis suscipit leo ultricies id. Pellentesque tristique massa non posuere convallis. Proin auctor semper varius. Maecenas commodo euismod leo, eget feugiat erat porta eget.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Maecenas aliquet, magna ac dignissim malesuada, lorem magna porttitor elit, a malesuada felis ante id velit. Donec sit amet lorem justo. Morbi nec fringilla felis, nec interdum dui. In adipiscing libero erat, ut vehicula nisi auctor nec. Nulla facilisi. Cras eget iaculis justo. Cras quis convallis purus. Nulla sit amet nisl nibh.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis tempus feugiat mollis. Sed tempus at velit nec hendrerit. Curabitur et adipiscing ipsum, nec volutpat odio. Proin auctor sollicitudin pharetra. Donec vestibulum, elit ut rutrum dapibus, odio nisl vestibulum justo, nec tincidunt ipsum enim eu velit. Nulla sit amet convallis risus. Cras ipsum nibh, cursus sed tristique vitae, auctor facilisis nunc. Maecenas pharetra interdum dolor semper aliquet. Donec nulla nisi, consectetur sed tortor at, varius faucibus diam. Maecenas condimentum tortor sem, faucibus fringilla quam sodales id. Cras eu faucibus tortor.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla luctus molestie mi sed sollicitudin. Sed condimentum ligula et interdum pharetra. Nulla fringilla massa velit, nec iaculis augue laoreet ut. Sed consectetur volutpat enim. Nulla facilisi. Integer iaculis leo urna. Nunc pulvinar tortor dictum nibh dictum pellentesque. Sed malesuada enim nunc, ut porttitor mauris mollis ut. Pellentesque feugiat quis est at tempus. Phasellus vel nibh ac nulla consequat viverra. Nulla porta vitae enim sit amet lacinia. Sed sed ullamcorper ipsum, a egestas mi. Cras ullamcorper interdum urna, ut auctor odio faucibus eget. Quisque tempus id ante in porttitor.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut hendrerit est sit amet mauris tempor tincidunt. Donec egestas arcu lorem, nec posuere urna accumsan eu. Nullam molestie elit a semper sagittis. Fusce sagittis quam nec lorem laoreet, vitae gravida risus ullamcorper. Ut sapien enim, vestibulum non elementum id, tempor ac dui. Vivamus fermentum pulvinar nisi, ut rhoncus ipsum lacinia at. Aenean eu sollicitudin libero, quis luctus dui. Sed eu aliquam odio. Fusce ante nunc, dignissim et sapien placerat, ultrices tempus mi. Integer quis tellus blandit sem ultricies cursus vel adipiscing quam. Donec vestibulum metus vel dui dictum dignissim. Maecenas tristique imperdiet urna in commodo. Etiam blandit faucibus urna, ac adipiscing est accumsan viverra. Cras eget arcu fermentum, egestas leo et, posuere diam. Nullam a venenatis odio.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed commodo, nunc vel iaculis tempus, ligula est viverra felis, non blandit augue urna in est. Vivamus ullamcorper fermentum turpis, vitae ultrices velit malesuada nec. Cras sit amet aliquam leo. Nulla dapibus iaculis justo et faucibus. Etiam ultrices ante vel velit venenatis, ac consequat risus tincidunt. In at cursus dui, sed consectetur leo. Praesent nec suscipit est. Duis pellentesque lobortis porta. Vestibulum blandit est vel tellus tincidunt, sed commodo quam ultrices. Aenean lacinia odio quis augue pulvinar, vitae tempus sapien fringilla.</p>', '<h1 style=\"color: rgb(51, 51, 51);\"></h1><h1><span style=\"color: rgb(0, 0, 0);\">Serviços</span></h1><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; font-family: Arial, Helvetica, sans;\"><span style=\"color: rgb(0, 0, 0);\">sdfd</span></p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; font-family: Arial, Helvetica, sans;\"><span style=\"color: rgb(0, 0, 0);\">Lorem ipsum dolor sit </span><span style=\"color: rgb(255, 0, 0);\">amet,</span><span style=\"color: rgb(0, 0, 0);\"> consectetur adipiscing elit. Donec dictum rutrum nibh vitae tempor. Fusce feugiat viverra nisl ac pretium. Pellentesque facilisis magna vitae est pulvinar fringilla. Nam sit amet laoreet ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante ante, tempus vitae augue eu, tincidunt rutrum risus. Duis dapibus, leo sit amet laoreet porttitor, nibh urna semper purus, fringilla semper augue mi eu mi. Praesent molestie erat odio, et aliquet diam egestas non. Quisque rhoncus commodo volutpat. Nulla facilisi. In euismod, tortor sed feugiat posuere, lorem dolor porttitor magna, vitae viverra magna enim in magna. Morbi dictum condimentum tempus. Pellentesque ac vestibulum ipsum.</span></p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed mattis porta felis, eu placerat turpis pellentesque vel. Cras a tempus mi. Mauris malesuada mollis massa, elementum auctor est hendrerit ac. Fusce id felis sed ipsum vehicula tempus. Sed dapibus augue posuere orci porta, quis sagittis libero auctor. Vivamus scelerisque massa neque, ornare mollis purus ornare eget. Nam scelerisque, leo non euismod lacinia, enim urna tristique dolor, dictum tristique justo nunc eu libero.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis feugiat ullamcorper massa eget ullamcorper. Morbi eleifend mi sit amet neque semper, eget dictum magna iaculis. Cras ultricies tempus nisl, placerat interdum tellus viverra non. Etiam quis ligula erat. Pellentesque molestie, nunc nec pulvinar interdum, metus ipsum dignissim leo, et scelerisque risus dolor id dui. Vestibulum sollicitudin nisi justo, facilisis euismod ipsum condimentum eu. In a convallis mauris.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Praesent et iaculis nisl. Nulla massa enim, pulvinar ut molestie ac, aliquet vitae neque. Vivamus sagittis felis id magna pulvinar, ac convallis massa scelerisque. Maecenas ut adipiscing massa. Pellentesque lobortis augue in magna facilisis, eget pellentesque lorem aliquam. Etiam ultricies, elit sit amet blandit egestas, sapien lectus porta nisi, sit amet sollicitudin diam arcu ut magna. Ut sapien nisl, adipiscing in suscipit a, ullamcorper et felis. Curabitur in dictum augue. Cras sagittis sagittis nisi, nec imperdiet augue elementum ac.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Integer ornare tempus ante ac tempus. Nullam blandit massa ante, vel cursus risus adipiscing in. Integer ipsum est, congue a tincidunt at, molestie eu diam. Sed a elit lobortis, auctor enim ut, dictum lorem. Donec dapibus non purus accumsan congue. Duis posuere consectetur turpis condimentum iaculis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris feugiat luctus posuere. Phasellus eu erat quam. Vivamus dictum sodales enim, blandit facilisis dolor tincidunt quis. Praesent et nibh id diam tincidunt laoreet. Vivamus lobortis gravida turpis vel blandit. Vestibulum eros ipsum, cursus et convallis id, consequat id urna.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis volutpat venenatis commodo. Nunc vitae justo nec augue eleifend porta. Phasellus molestie nibh sed facilisis laoreet. Donec sit amet odio quis lacus egestas dictum eu sed lacus. Donec ac tincidunt sapien. Morbi vulputate magna sagittis neque iaculis commodo. In accumsan mattis leo eu volutpat. Aenean dignissim, nunc et imperdiet molestie, ante nulla venenatis nisi, nec auctor urna ipsum vel magna. Donec massa leo, placerat nec est id, ultrices fringilla nunc. Integer id metus at neque vehicula hendrerit. Phasellus eget sollicitudin nisi. Ut adipiscing velit sapien, at convallis dui accumsan non. Vivamus vel pulvinar velit, malesuada fringilla lectus. Morbi in purus ut magna feugiat eleifend quis eget odio.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nunc justo tortor, cursus quis dictum ut, venenatis consequat felis. Integer varius ut magna a tristique. Integer dictum eget lectus sed aliquet. Aliquam nec dapibus metus. Pellentesque ultrices risus arcu, id egestas sem vestibulum non. Fusce pulvinar tortor ut risus porttitor, id fringilla sem congue. Integer eget nulla eleifend, fringilla magna a, imperdiet tortor. In vehicula lacinia auctor. Aenean egestas velit nec felis commodo tempor at a dolor. Vestibulum eget placerat nisi.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut mollis sed mauris non pellentesque. Pellentesque ut purus eu metus bibendum luctus ultricies at diam. Sed posuere malesuada mauris sed interdum. Fusce quis lectus at purus condimentum facilisis. Aenean accumsan massa id tellus gravida, et suscipit ante ornare. Integer ac bibendum dui. Pellentesque at quam nisi. Fusce venenatis est id venenatis rutrum.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla dictum nulla turpis, ac vestibulum metus volutpat ut. Vestibulum interdum laoreet est, quis sodales nibh iaculis eget. Quisque laoreet tortor id metus bibendum ullamcorper. Vestibulum molestie elementum lectus, quis posuere elit auctor ut. Etiam non leo ac mauris volutpat rutrum. Morbi suscipit, eros ut ornare pretium, sem ante rhoncus urna, lobortis bibendum magna dolor non lacus. Cras sodales risus nec orci posuere, a vulputate massa venenatis. Etiam interdum velit eros, ac pellentesque nisi tempor at. Duis eget dolor quam. Ut id velit vel diam sollicitudin congue. Etiam et pharetra ligula. Nulla ac semper mauris, a rutrum justo. Pellentesque eleifend tincidunt odio vel imperdiet. Etiam sit amet mollis arcu. In hac habitasse platea dictumst.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque dapibus, quam eget sodales eleifend, mi nisl egestas tortor, a molestie orci ante at libero. Proin rutrum sollicitudin eleifend. Ut vehicula, felis quis laoreet iaculis, dolor massa scelerisque orci, vel interdum tellus magna in lacus. Suspendisse facilisis ante ut felis gravida molestie. Nullam aliquet, velit et pellentesque posuere, sapien nisi commodo erat, sed dignissim turpis diam sit amet felis. Duis ac adipiscing nunc. Quisque fringilla fermentum auctor. Proin tempus diam non enim sagittis molestie. Aenean eu lorem euismod quam elementum viverra fringilla sed nibh. Vivamus eget tortor diam. Fusce posuere sem molestie rhoncus eleifend. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Aenean non nisi quis ligula elementum porttitor. Maecenas ac eleifend ligula. Integer nec mollis magna. Vivamus molestie fringilla libero non vestibulum. Proin interdum tellus quis ultrices accumsan. Nam pulvinar metus urna. In hac habitasse platea dictumst. Donec consequat iaculis lectus at tempor. Donec eget lectus odio. Integer laoreet non nulla sit amet luctus. Etiam semper tellus nibh, mollis accumsan massa congue id. Nunc ac aliquet orci, quis interdum massa.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Quisque eget turpis sit amet nisl sagittis iaculis. Mauris quis viverra lorem, quis faucibus leo. Fusce consectetur laoreet magna, et viverra dolor viverra id. Sed rhoncus massa a auctor adipiscing. Suspendisse porttitor et eros in sollicitudin. Nunc porta in nibh pulvinar ornare. Ut a congue massa. Vivamus nisl orci, volutpat eu libero quis, bibendum lacinia mi. Nunc accumsan ornare augue eget malesuada. Donec imperdiet ante tellus, in lacinia purus posuere at. In dictum egestas augue eget eleifend. Pellentesque vehicula nisl ac velit rhoncus, eget ullamcorper mauris dapibus.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce blandit sollicitudin congue. Morbi condimentum hendrerit dui, in semper lorem porttitor nec. Nunc vitae vulputate ligula, id mollis nisi. Morbi ante nibh, viverra porta nulla posuere, pellentesque dictum nisl. Cras quis quam orci. Morbi viverra, ligula eu pellentesque vehicula, sem justo placerat tellus, semper eleifend libero erat vel urna. In hac habitasse platea dictumst. Sed accumsan pulvinar lorem sit amet euismod. Ut quis orci sed erat ultrices consequat eu pellentesque diam. Morbi scelerisque convallis risus, eu tristique metus auctor sit amet. Nunc dolor velit, laoreet ac ipsum ac, volutpat dapibus enim. Proin vel accumsan metus, a ornare quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis posuere iaculis.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Etiam mattis sodales leo, a vestibulum nibh pretium sit amet. Nullam suscipit felis accumsan laoreet adipiscing. Morbi sed arcu suscipit, consequat nisi nec, ultricies mi. Proin adipiscing libero vitae felis commodo ullamcorper. Vivamus at congue sapien, at sagittis tortor. Phasellus at quam nec augue malesuada vulputate. Nam eleifend nunc magna, quis suscipit leo ultricies id. Pellentesque tristique massa non posuere convallis. Proin auctor semper varius. Maecenas commodo euismod leo, eget feugiat erat porta eget.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Maecenas aliquet, magna ac dignissim malesuada, lorem magna porttitor elit, a malesuada felis ante id velit. Donec sit amet lorem justo. Morbi nec fringilla felis, nec interdum dui. In adipiscing libero erat, ut vehicula nisi auctor nec. Nulla facilisi. Cras eget iaculis justo. Cras quis convallis purus. Nulla sit amet nisl nibh.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis tempus feugiat mollis. Sed tempus at velit nec hendrerit. Curabitur et adipiscing ipsum, nec volutpat odio. Proin auctor sollicitudin pharetra. Donec vestibulum, elit ut rutrum dapibus, odio nisl vestibulum justo, nec tincidunt ipsum enim eu velit. Nulla sit amet convallis risus. Cras ipsum nibh, cursus sed tristique vitae, auctor facilisis nunc. Maecenas pharetra interdum dolor semper aliquet. Donec nulla nisi, consectetur sed tortor at, varius faucibus diam. Maecenas condimentum tortor sem, faucibus fringilla quam sodales id. Cras eu faucibus tortor.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla luctus molestie mi sed sollicitudin. Sed condimentum ligula et interdum pharetra. Nulla fringilla massa velit, nec iaculis augue laoreet ut. Sed consectetur volutpat enim. Nulla facilisi. Integer iaculis leo urna. Nunc pulvinar tortor dictum nibh dictum pellentesque. Sed malesuada enim nunc, ut porttitor mauris mollis ut. Pellentesque feugiat quis est at tempus. Phasellus vel nibh ac nulla consequat viverra. Nulla porta vitae enim sit amet lacinia. Sed sed ullamcorper ipsum, a egestas mi. Cras ullamcorper interdum urna, ut auctor odio faucibus eget. Quisque tempus id ante in porttitor.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut hendrerit est sit amet mauris tempor tincidunt. Donec egestas arcu lorem, nec posuere urna accumsan eu. Nullam molestie elit a semper sagittis. Fusce sagittis quam nec lorem laoreet, vitae gravida risus ullamcorper. Ut sapien enim, vestibulum non elementum id, tempor ac dui. Vivamus fermentum pulvinar nisi, ut rhoncus ipsum lacinia at. Aenean eu sollicitudin libero, quis luctus dui. Sed eu aliquam odio. Fusce ante nunc, dignissim et sapien placerat, ultrices tempus mi. Integer quis tellus blandit sem ultricies cursus vel adipiscing quam. Donec vestibulum metus vel dui dictum dignissim. Maecenas tristique imperdiet urna in commodo. Etiam blandit faucibus urna, ac adipiscing est accumsan viverra. Cras eget arcu fermentum, egestas leo et, posuere diam. Nullam a venenatis odio.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed commodo, nunc vel iaculis tempus, ligula est viverra felis, non blandit augue urna in est. Vivamus ullamcorper fermentum turpis, vitae ultrices velit malesuada nec. Cras sit amet aliquam leo. Nulla dapibus iaculis justo et faucibus. Etiam ultrices ante vel velit venenatis, ac consequat risus tincidunt. In at cursus dui, sed consectetur leo. Praesent nec suscipit est. Duis pellentesque lobortis porta. Vestibulum blandit est vel tellus tincidunt, sed commodo quam ultrices. Aenean lacinia odio quis augue pulvinar, vitae tempus sapien fringilla.</p>', '<h1 style=\"color: rgb(51, 51, 51);\"></h1><h1><span style=\"color: rgb(0, 0, 0);\">Sua Franquia</span></h1><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; font-family: Arial, Helvetica, sans;\"><span style=\"color: rgb(0, 0, 0);\">fdfdfddf</span></p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; font-family: Arial, Helvetica, sans;\"><span style=\"color: rgb(0, 0, 0);\">Lorem ipsum dolor sit </span><span style=\"color: rgb(255, 0, 0);\">amet,</span><span style=\"color: rgb(0, 0, 0);\"> consectetur adipiscing elit. Donec dictum rutrum nibh vitae tempor. Fusce feugiat viverra nisl ac pretium. Pellentesque facilisis magna vitae est pulvinar fringilla. Nam sit amet laoreet ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante ante, tempus vitae augue eu, tincidunt rutrum risus. Duis dapibus, leo sit amet laoreet porttitor, nibh urna semper purus, fringilla semper augue mi eu mi. Praesent molestie erat odio, et aliquet diam egestas non. Quisque rhoncus commodo volutpat. Nulla facilisi. In euismod, tortor sed feugiat posuere, lorem dolor porttitor magna, vitae viverra magna enim in magna. Morbi dictum condimentum tempus. Pellentesque ac vestibulum ipsum.</span></p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed mattis porta felis, eu placerat turpis pellentesque vel. Cras a tempus mi. Mauris malesuada mollis massa, elementum auctor est hendrerit ac. Fusce id felis sed ipsum vehicula tempus. Sed dapibus augue posuere orci porta, quis sagittis libero auctor. Vivamus scelerisque massa neque, ornare mollis purus ornare eget. Nam scelerisque, leo non euismod lacinia, enim urna tristique dolor, dictum tristique justo nunc eu libero.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis feugiat ullamcorper massa eget ullamcorper. Morbi eleifend mi sit amet neque semper, eget dictum magna iaculis. Cras ultricies tempus nisl, placerat interdum tellus viverra non. Etiam quis ligula erat. Pellentesque molestie, nunc nec pulvinar interdum, metus ipsum dignissim leo, et scelerisque risus dolor id dui. Vestibulum sollicitudin nisi justo, facilisis euismod ipsum condimentum eu. In a convallis mauris.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Praesent et iaculis nisl. Nulla massa enim, pulvinar ut molestie ac, aliquet vitae neque. Vivamus sagittis felis id magna pulvinar, ac convallis massa scelerisque. Maecenas ut adipiscing massa. Pellentesque lobortis augue in magna facilisis, eget pellentesque lorem aliquam. Etiam ultricies, elit sit amet blandit egestas, sapien lectus porta nisi, sit amet sollicitudin diam arcu ut magna. Ut sapien nisl, adipiscing in suscipit a, ullamcorper et felis. Curabitur in dictum augue. Cras sagittis sagittis nisi, nec imperdiet augue elementum ac.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Integer ornare tempus ante ac tempus. Nullam blandit massa ante, vel cursus risus adipiscing in. Integer ipsum est, congue a tincidunt at, molestie eu diam. Sed a elit lobortis, auctor enim ut, dictum lorem. Donec dapibus non purus accumsan congue. Duis posuere consectetur turpis condimentum iaculis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris feugiat luctus posuere. Phasellus eu erat quam. Vivamus dictum sodales enim, blandit facilisis dolor tincidunt quis. Praesent et nibh id diam tincidunt laoreet. Vivamus lobortis gravida turpis vel blandit. Vestibulum eros ipsum, cursus et convallis id, consequat id urna.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis volutpat venenatis commodo. Nunc vitae justo nec augue eleifend porta. Phasellus molestie nibh sed facilisis laoreet. Donec sit amet odio quis lacus egestas dictum eu sed lacus. Donec ac tincidunt sapien. Morbi vulputate magna sagittis neque iaculis commodo. In accumsan mattis leo eu volutpat. Aenean dignissim, nunc et imperdiet molestie, ante nulla venenatis nisi, nec auctor urna ipsum vel magna. Donec massa leo, placerat nec est id, ultrices fringilla nunc. Integer id metus at neque vehicula hendrerit. Phasellus eget sollicitudin nisi. Ut adipiscing velit sapien, at convallis dui accumsan non. Vivamus vel pulvinar velit, malesuada fringilla lectus. Morbi in purus ut magna feugiat eleifend quis eget odio.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nunc justo tortor, cursus quis dictum ut, venenatis consequat felis. Integer varius ut magna a tristique. Integer dictum eget lectus sed aliquet. Aliquam nec dapibus metus. Pellentesque ultrices risus arcu, id egestas sem vestibulum non. Fusce pulvinar tortor ut risus porttitor, id fringilla sem congue. Integer eget nulla eleifend, fringilla magna a, imperdiet tortor. In vehicula lacinia auctor. Aenean egestas velit nec felis commodo tempor at a dolor. Vestibulum eget placerat nisi.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut mollis sed mauris non pellentesque. Pellentesque ut purus eu metus bibendum luctus ultricies at diam. Sed posuere malesuada mauris sed interdum. Fusce quis lectus at purus condimentum facilisis. Aenean accumsan massa id tellus gravida, et suscipit ante ornare. Integer ac bibendum dui. Pellentesque at quam nisi. Fusce venenatis est id venenatis rutrum.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla dictum nulla turpis, ac vestibulum metus volutpat ut. Vestibulum interdum laoreet est, quis sodales nibh iaculis eget. Quisque laoreet tortor id metus bibendum ullamcorper. Vestibulum molestie elementum lectus, quis posuere elit auctor ut. Etiam non leo ac mauris volutpat rutrum. Morbi suscipit, eros ut ornare pretium, sem ante rhoncus urna, lobortis bibendum magna dolor non lacus. Cras sodales risus nec orci posuere, a vulputate massa venenatis. Etiam interdum velit eros, ac pellentesque nisi tempor at. Duis eget dolor quam. Ut id velit vel diam sollicitudin congue. Etiam et pharetra ligula. Nulla ac semper mauris, a rutrum justo. Pellentesque eleifend tincidunt odio vel imperdiet. Etiam sit amet mollis arcu. In hac habitasse platea dictumst.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque dapibus, quam eget sodales eleifend, mi nisl egestas tortor, a molestie orci ante at libero. Proin rutrum sollicitudin eleifend. Ut vehicula, felis quis laoreet iaculis, dolor massa scelerisque orci, vel interdum tellus magna in lacus. Suspendisse facilisis ante ut felis gravida molestie. Nullam aliquet, velit et pellentesque posuere, sapien nisi commodo erat, sed dignissim turpis diam sit amet felis. Duis ac adipiscing nunc. Quisque fringilla fermentum auctor. Proin tempus diam non enim sagittis molestie. Aenean eu lorem euismod quam elementum viverra fringilla sed nibh. Vivamus eget tortor diam. Fusce posuere sem molestie rhoncus eleifend. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Aenean non nisi quis ligula elementum porttitor. Maecenas ac eleifend ligula. Integer nec mollis magna. Vivamus molestie fringilla libero non vestibulum. Proin interdum tellus quis ultrices accumsan. Nam pulvinar metus urna. In hac habitasse platea dictumst. Donec consequat iaculis lectus at tempor. Donec eget lectus odio. Integer laoreet non nulla sit amet luctus. Etiam semper tellus nibh, mollis accumsan massa congue id. Nunc ac aliquet orci, quis interdum massa.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Quisque eget turpis sit amet nisl sagittis iaculis. Mauris quis viverra lorem, quis faucibus leo. Fusce consectetur laoreet magna, et viverra dolor viverra id. Sed rhoncus massa a auctor adipiscing. Suspendisse porttitor et eros in sollicitudin. Nunc porta in nibh pulvinar ornare. Ut a congue massa. Vivamus nisl orci, volutpat eu libero quis, bibendum lacinia mi. Nunc accumsan ornare augue eget malesuada. Donec imperdiet ante tellus, in lacinia purus posuere at. In dictum egestas augue eget eleifend. Pellentesque vehicula nisl ac velit rhoncus, eget ullamcorper mauris dapibus.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce blandit sollicitudin congue. Morbi condimentum hendrerit dui, in semper lorem porttitor nec. Nunc vitae vulputate ligula, id mollis nisi. Morbi ante nibh, viverra porta nulla posuere, pellentesque dictum nisl. Cras quis quam orci. Morbi viverra, ligula eu pellentesque vehicula, sem justo placerat tellus, semper eleifend libero erat vel urna. In hac habitasse platea dictumst. Sed accumsan pulvinar lorem sit amet euismod. Ut quis orci sed erat ultrices consequat eu pellentesque diam. Morbi scelerisque convallis risus, eu tristique metus auctor sit amet. Nunc dolor velit, laoreet ac ipsum ac, volutpat dapibus enim. Proin vel accumsan metus, a ornare quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis posuere iaculis.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Etiam mattis sodales leo, a vestibulum nibh pretium sit amet. Nullam suscipit felis accumsan laoreet adipiscing. Morbi sed arcu suscipit, consequat nisi nec, ultricies mi. Proin adipiscing libero vitae felis commodo ullamcorper. Vivamus at congue sapien, at sagittis tortor. Phasellus at quam nec augue malesuada vulputate. Nam eleifend nunc magna, quis suscipit leo ultricies id. Pellentesque tristique massa non posuere convallis. Proin auctor semper varius. Maecenas commodo euismod leo, eget feugiat erat porta eget.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Maecenas aliquet, magna ac dignissim malesuada, lorem magna porttitor elit, a malesuada felis ante id velit. Donec sit amet lorem justo. Morbi nec fringilla felis, nec interdum dui. In adipiscing libero erat, ut vehicula nisi auctor nec. Nulla facilisi. Cras eget iaculis justo. Cras quis convallis purus. Nulla sit amet nisl nibh.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Duis tempus feugiat mollis. Sed tempus at velit nec hendrerit. Curabitur et adipiscing ipsum, nec volutpat odio. Proin auctor sollicitudin pharetra. Donec vestibulum, elit ut rutrum dapibus, odio nisl vestibulum justo, nec tincidunt ipsum enim eu velit. Nulla sit amet convallis risus. Cras ipsum nibh, cursus sed tristique vitae, auctor facilisis nunc. Maecenas pharetra interdum dolor semper aliquet. Donec nulla nisi, consectetur sed tortor at, varius faucibus diam. Maecenas condimentum tortor sem, faucibus fringilla quam sodales id. Cras eu faucibus tortor.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nulla luctus molestie mi sed sollicitudin. Sed condimentum ligula et interdum pharetra. Nulla fringilla massa velit, nec iaculis augue laoreet ut. Sed consectetur volutpat enim. Nulla facilisi. Integer iaculis leo urna. Nunc pulvinar tortor dictum nibh dictum pellentesque. Sed malesuada enim nunc, ut porttitor mauris mollis ut. Pellentesque feugiat quis est at tempus. Phasellus vel nibh ac nulla consequat viverra. Nulla porta vitae enim sit amet lacinia. Sed sed ullamcorper ipsum, a egestas mi. Cras ullamcorper interdum urna, ut auctor odio faucibus eget. Quisque tempus id ante in porttitor.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Ut hendrerit est sit amet mauris tempor tincidunt. Donec egestas arcu lorem, nec posuere urna accumsan eu. Nullam molestie elit a semper sagittis. Fusce sagittis quam nec lorem laoreet, vitae gravida risus ullamcorper. Ut sapien enim, vestibulum non elementum id, tempor ac dui. Vivamus fermentum pulvinar nisi, ut rhoncus ipsum lacinia at. Aenean eu sollicitudin libero, quis luctus dui. Sed eu aliquam odio. Fusce ante nunc, dignissim et sapien placerat, ultrices tempus mi. Integer quis tellus blandit sem ultricies cursus vel adipiscing quam. Donec vestibulum metus vel dui dictum dignissim. Maecenas tristique imperdiet urna in commodo. Etiam blandit faucibus urna, ac adipiscing est accumsan viverra. Cras eget arcu fermentum, egestas leo et, posuere diam. Nullam a venenatis odio.</p><p style=\"margin-bottom: 14px; text-align: justify; font-size: 11px; line-height: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed commodo, nunc vel iaculis tempus, ligula est viverra felis, non blandit augue urna in est. Vivamus ullamcorper fermentum turpis, vitae ultrices velit malesuada nec. Cras sit amet aliquam leo. Nulla dapibus iaculis justo et faucibus. Etiam ultrices ante vel velit venenatis, ac consequat risus tincidunt. In at cursus dui, sed consectetur leo. Praesent nec suscipit est. Duis pellentesque lobortis porta. Vestibulum blandit est vel tellus tincidunt, sed commodo quam ultrices. Aenean lacinia odio quis augue pulvinar, vitae tempus sapien fringilla.</p>', '<h1>Termos e Serviços</h1><div>eererer</div><div><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at cursus justo. Integer id nisl ac eros suscipit cursus. Aliquam vel lacinia nibh. Vestibulum at vehicula dui. Fusce aliquet fermentum ligula, at aliquam risus iaculis sit amet. Quisque dapibus ligula id orci congue vulputate. In malesuada rhoncus metus, ac porttitor felis cursus a. Nam posuere nibh neque, ut venenatis ipsum ultrices in. Vestibulum suscipit gravida lobortis. Vivamus sed neque eros. Cras in fermentum ligula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur ac ipsum sed metus adipiscing iaculis. Praesent dictum, velit in venenatis auctor, lacus risus interdum dolor, id faucibus ligula magna ut eros. Curabitur leo ligula, posuere vitae urna consequat, gravida lacinia ipsum.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Donec quis justo eu est pretium sagittis et quis turpis. Quisque ornare mattis molestie. Curabitur blandit justo quis dignissim porttitor. Aliquam erat volutpat. Etiam tempor tristique libero, nec viverra nibh volutpat non. Duis sit amet ipsum vitae nisl congue tincidunt a ut neque. Sed accumsan id sem vulputate malesuada. Ut ac diam magna. Integer velit urna, rhoncus ut augue eleifend, tempus ultrices metus. Etiam ipsum turpis, bibendum et consectetur at, blandit sed mi. Suspendisse posuere enim nulla, non ullamcorper sapien luctus eget. Nulla lacus erat, tincidunt quis magna id, dignissim porttitor enim. Vestibulum in turpis sem.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Nunc a accumsan sem, pulvinar mattis velit. Quisque sapien nunc, mollis ac faucibus id, gravida eget augue. Mauris pretium iaculis dolor, ut posuere dolor vulputate nec. Donec dapibus sed dolor vel eleifend. Integer laoreet neque nec metus convallis bibendum. Donec porta mi eget quam dictum lacinia eu ac est. Morbi ac urna sit amet sem congue rutrum at laoreet mauris. Nunc gravida, eros ac bibendum congue, tellus turpis condimentum enim, ac sollicitudin lorem ante non libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse libero massa, commodo quis purus sed, vestibulum dapibus massa.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Sed tristique nisl massa, vitae consequat elit pulvinar id. Integer imperdiet laoreet lacinia. Aliquam nec justo non libero vestibulum interdum eu nec lorem. Sed iaculis sed massa ut molestie. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec eget lacus quis tortor sagittis vehicula. Sed odio justo, eleifend at placerat eget, tincidunt ut risus. In blandit erat vel ultrices ornare. Morbi dictum, tellus at ultrices tincidunt, diam purus volutpat nisi, id venenatis purus dolor eu sem. Ut vehicula leo eget dapibus aliquam. Nulla id dapibus erat.</p><p style=\"text-align: justify; font-size: 11px; line-height: 14px; margin-bottom: 14px; padding: 0px; color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans;\">Morbi aliquet metus commodo mollis auctor. Integer ut neque facilisis, consequat nulla a, consequat sapien. Nulla vel ullamcorper nisl. Integer mollis nibh vitae lacus tempus accumsan. Ut mollis magna erat, in iaculis risus lacinia a. Nulla vel accumsan est. Cras mollis metus vel euismod ullamcorper. Phasellus vitae turpis viverra, gravida enim ut, pharetra justo.</p></div>');

-- ----------------------------
-- Table structure for enderecos
-- ----------------------------
DROP TABLE IF EXISTS `enderecos`;
CREATE TABLE `enderecos` (
  `ENDERECO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CEP` char(12) DEFAULT NULL,
  `ENDERECO` char(255) NOT NULL,
  `BAIRRO` varchar(60) DEFAULT NULL,
  `NUMERO` smallint(6) DEFAULT NULL,
  `CIDADE` char(30) NOT NULL,
  `ESTADO` char(2) DEFAULT NULL,
  PRIMARY KEY (`ENDERECO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enderecos
-- ----------------------------
INSERT INTO `enderecos` VALUES ('1', '12422230', 'Rua Soldado Francisco Rodrigues', 'Residencial Campos Maia', '25', 'Pindamonhangaba', 'SP');
INSERT INTO `enderecos` VALUES ('2', '03123323', 'Rua Das Palmeiras', 'Residencial Campos Belos', '1337', 'Pindamonhangaba', 'SP');
INSERT INTO `enderecos` VALUES ('3', '12422230', 'Rua Soldado Francisco Rodrigues', 'Residencial Campos Maia', '120', 'Pindamonhangaba', 'SP');
INSERT INTO `enderecos` VALUES ('4', '24858500', 'BR-101 - lado par', 'Apolo II (Manilha)', '0', 'Itaboraí', 'AC');

-- ----------------------------
-- Table structure for franquia
-- ----------------------------
DROP TABLE IF EXISTS `franquia`;
CREATE TABLE `franquia` (
  `FRANQUIA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROPRIETARIO_ID` int(11) NOT NULL,
  `NOMEFRANQUIA` varchar(60) DEFAULT NULL,
  `SLOGAN` varchar(100) DEFAULT NULL,
  `TELFRANQUIA` varchar(16) NOT NULL,
  `CEPFRANQUIA` char(12) DEFAULT NULL,
  `ENDERECO` char(255) DEFAULT NULL,
  `BAIRRO` varchar(60) DEFAULT NULL,
  `CIDADE` char(30) DEFAULT NULL,
  `ESTADO` char(2) DEFAULT NULL,
  `EMAIL` varchar(75) DEFAULT NULL,
  `ENDERECO_SMTP` varchar(40) DEFAULT NULL,
  `LOGIN_SMTP` varchar(25) DEFAULT NULL,
  `SENHA_SMTP` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`FRANQUIA_ID`),
  KEY `FK_PROPRIETARIO_FRANQUIA` (`PROPRIETARIO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of franquia
-- ----------------------------
INSERT INTO `franquia` VALUES ('3', '0', 'Franquia Testea', 'Teste', '(12) 3642-6818', '12422-230', 'Rua Soldado Francisco Rodrigues', 'Residencial Campos Maia', 'Pindamonhangaba', 'SP', 'nicholasluis@gmail.com', 'mail.williamsites.com.br', 'teste@williamsites.com.br', 'cb3252');
INSERT INTO `franquia` VALUES ('4', '33', 'rtyty', 'ytytyt', '(43) 3445-4554', '12422-230', 'Rua Soldado Francisco Rodrigues', 'Residencial Campos Maia', 'Pindamonhangaba', 'SP', 'nicholasluis@gmail.com', 'teste.teste.com.br', 'login', 'senha');
INSERT INTO `franquia` VALUES ('6', '3', 'Teste de Franquia', 'ytytty', '(32) 3445-4545', '12422-230', 'Rua Soldado Francisco Rodrigues', 'Residencial Campos Maia', 'Pindamonhangaba', 'AC', '3344334', '3434433434', '343434', '343443');

-- ----------------------------
-- Table structure for marca
-- ----------------------------
DROP TABLE IF EXISTS `marca`;
CREATE TABLE `marca` (
  `MARCA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO_ID` int(11) DEFAULT NULL,
  `NOMEMARCA` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`MARCA_ID`),
  KEY `FK_MARCA_TIPO` (`TIPO_ID`),
  CONSTRAINT `FK_MARCA_TIPO` FOREIGN KEY (`TIPO_ID`) REFERENCES `tipo` (`TIPO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of marca
-- ----------------------------
INSERT INTO `marca` VALUES ('1', '1', 'CHEVROLET');
INSERT INTO `marca` VALUES ('2', '1', 'CITRO&Euml;N');
INSERT INTO `marca` VALUES ('3', '1', 'FIAT');
INSERT INTO `marca` VALUES ('4', '1', 'FORD');
INSERT INTO `marca` VALUES ('5', '1', 'HONDA');
INSERT INTO `marca` VALUES ('6', '1', 'MITSUBISHI');
INSERT INTO `marca` VALUES ('7', '1', 'PEUGEOT');
INSERT INTO `marca` VALUES ('8', '1', 'RENAULT');
INSERT INTO `marca` VALUES ('9', '1', 'TOYOTA');
INSERT INTO `marca` VALUES ('10', '1', 'VOLKSWAGEN');
INSERT INTO `marca` VALUES ('12', '1', 'ACURA');
INSERT INTO `marca` VALUES ('13', '1', 'ADAMO');
INSERT INTO `marca` VALUES ('14', '1', 'AGRALE');
INSERT INTO `marca` VALUES ('15', '1', 'ALFA ROMEO');
INSERT INTO `marca` VALUES ('16', '1', 'AMERICAR');
INSERT INTO `marca` VALUES ('17', '1', 'ASIA');
INSERT INTO `marca` VALUES ('18', '1', 'ASTON MARTIN');
INSERT INTO `marca` VALUES ('19', '1', 'AUDI');
INSERT INTO `marca` VALUES ('20', '1', 'AUSTIN');
INSERT INTO `marca` VALUES ('21', '1', 'BAJA');
INSERT INTO `marca` VALUES ('22', '1', 'BEACH');
INSERT INTO `marca` VALUES ('23', '1', 'BENTLEY');
INSERT INTO `marca` VALUES ('24', '1', 'BIANCO');
INSERT INTO `marca` VALUES ('25', '1', 'BMW');
INSERT INTO `marca` VALUES ('26', '1', 'BRM');
INSERT INTO `marca` VALUES ('27', '1', 'BUGWAY');
INSERT INTO `marca` VALUES ('28', '1', 'BUICK');
INSERT INTO `marca` VALUES ('29', '1', 'CADILLAC');
INSERT INTO `marca` VALUES ('30', '1', 'CBT');
INSERT INTO `marca` VALUES ('31', '1', 'CHAMONIX');
INSERT INTO `marca` VALUES ('32', '1', 'CHANA');
INSERT INTO `marca` VALUES ('33', '1', 'CHRYSLER');
INSERT INTO `marca` VALUES ('34', '1', 'CORD');
INSERT INTO `marca` VALUES ('35', '1', 'CROSS LANDER');
INSERT INTO `marca` VALUES ('36', '1', 'DAEWOO');
INSERT INTO `marca` VALUES ('37', '1', 'DAIHATSU');
INSERT INTO `marca` VALUES ('38', '1', 'DKW-VEMAG');
INSERT INTO `marca` VALUES ('39', '1', 'DODGE');
INSERT INTO `marca` VALUES ('40', '1', 'DUNNAS');
INSERT INTO `marca` VALUES ('41', '1', 'EFFA');
INSERT INTO `marca` VALUES ('42', '1', 'ENGESA');
INSERT INTO `marca` VALUES ('43', '1', 'ENVEMO');
INSERT INTO `marca` VALUES ('44', '1', 'FARUS');
INSERT INTO `marca` VALUES ('45', '1', 'FERRARI');
INSERT INTO `marca` VALUES ('46', '1', 'FNM');
INSERT INTO `marca` VALUES ('47', '1', 'FYBER');
INSERT INTO `marca` VALUES ('48', '1', 'GEO');
INSERT INTO `marca` VALUES ('49', '1', 'GMC');
INSERT INTO `marca` VALUES ('50', '1', 'GRANCAR VE&Iacute;');
INSERT INTO `marca` VALUES ('51', '1', 'GURGEL');
INSERT INTO `marca` VALUES ('52', '1', 'HUMMER');
INSERT INTO `marca` VALUES ('53', '1', 'HYUNDAI');
INSERT INTO `marca` VALUES ('54', '1', 'INFINITI');
INSERT INTO `marca` VALUES ('55', '1', 'INTERNATIONAL');
INSERT INTO `marca` VALUES ('56', '1', 'ISUZU');
INSERT INTO `marca` VALUES ('57', '1', 'IVECO');
INSERT INTO `marca` VALUES ('58', '1', 'JAGUAR');
INSERT INTO `marca` VALUES ('59', '1', 'JEEP');
INSERT INTO `marca` VALUES ('60', '1', 'JPX');
INSERT INTO `marca` VALUES ('61', '1', 'KIA');
INSERT INTO `marca` VALUES ('62', '1', 'LADA');
INSERT INTO `marca` VALUES ('63', '1', 'LAMBORGHINI');
INSERT INTO `marca` VALUES ('64', '1', 'LANCIA');
INSERT INTO `marca` VALUES ('65', '1', 'LAND ROVER');
INSERT INTO `marca` VALUES ('66', '1', 'LEXUS');
INSERT INTO `marca` VALUES ('67', '1', 'LINCOLN');
INSERT INTO `marca` VALUES ('68', '1', 'LOBINI');
INSERT INTO `marca` VALUES ('69', '1', 'LOTUS');
INSERT INTO `marca` VALUES ('70', '1', 'MARCOPOLO');
INSERT INTO `marca` VALUES ('71', '1', 'MARINA&acute;S');
INSERT INTO `marca` VALUES ('72', '1', 'MASERATI');
INSERT INTO `marca` VALUES ('73', '1', 'MATRA');
INSERT INTO `marca` VALUES ('74', '1', 'MAZDA');
INSERT INTO `marca` VALUES ('75', '1', 'MERCEDES-BENZ');
INSERT INTO `marca` VALUES ('76', '1', 'MERCURY');
INSERT INTO `marca` VALUES ('77', '1', 'MG');
INSERT INTO `marca` VALUES ('78', '1', 'MINI');
INSERT INTO `marca` VALUES ('79', '1', 'MIURA');
INSERT INTO `marca` VALUES ('80', '1', 'MORRIS');
INSERT INTO `marca` VALUES ('81', '1', 'MP LAFER');
INSERT INTO `marca` VALUES ('82', '1', 'NISSAN');
INSERT INTO `marca` VALUES ('83', '1', 'OLDSMOBILE');
INSERT INTO `marca` VALUES ('84', '1', 'OPEL');
INSERT INTO `marca` VALUES ('85', '1', 'PLYMOUTH');
INSERT INTO `marca` VALUES ('86', '1', 'PONTIAC');
INSERT INTO `marca` VALUES ('87', '1', 'PORSCHE');
INSERT INTO `marca` VALUES ('88', '1', 'PUMA');
INSERT INTO `marca` VALUES ('89', '1', 'ROLLS-ROYCE');
INSERT INTO `marca` VALUES ('90', '1', 'ROMI');
INSERT INTO `marca` VALUES ('91', '1', 'ROVER');
INSERT INTO `marca` VALUES ('92', '1', 'SAAB');
INSERT INTO `marca` VALUES ('93', '1', 'SANTA MATILDE');
INSERT INTO `marca` VALUES ('94', '1', 'SATURN');
INSERT INTO `marca` VALUES ('95', '1', 'SEAT');
INSERT INTO `marca` VALUES ('96', '1', 'SHELBY');
INSERT INTO `marca` VALUES ('97', '1', 'SHORT');
INSERT INTO `marca` VALUES ('98', '1', 'SIMCA');
INSERT INTO `marca` VALUES ('99', '1', 'SMART');
INSERT INTO `marca` VALUES ('100', '1', 'SSANGYONG');
INSERT INTO `marca` VALUES ('101', '1', 'STUDEBAKER');
INSERT INTO `marca` VALUES ('102', '1', 'SUBARU');
INSERT INTO `marca` VALUES ('103', '1', 'SUZUKI');
INSERT INTO `marca` VALUES ('104', '1', 'TROLLER');
INSERT INTO `marca` VALUES ('105', '1', 'UNIMOG');
INSERT INTO `marca` VALUES ('106', '1', 'VOLVO');
INSERT INTO `marca` VALUES ('107', '1', 'WALK');
INSERT INTO `marca` VALUES ('108', '1', 'WILLYS');
INSERT INTO `marca` VALUES ('109', '2', 'AGRALE');
INSERT INTO `marca` VALUES ('110', '2', 'BMW');
INSERT INTO `marca` VALUES ('111', '2', 'DUCATI');
INSERT INTO `marca` VALUES ('112', '2', 'HARLEY-DAVIDSON');
INSERT INTO `marca` VALUES ('113', '2', 'HONDA');
INSERT INTO `marca` VALUES ('114', '2', 'KASINSKI');
INSERT INTO `marca` VALUES ('115', '2', 'KAWASAKI');
INSERT INTO `marca` VALUES ('116', '2', 'SUNDOWN');
INSERT INTO `marca` VALUES ('117', '2', 'SUZUKI');
INSERT INTO `marca` VALUES ('118', '2', 'TRIUMPH');
INSERT INTO `marca` VALUES ('119', '2', 'YAMAHA');
INSERT INTO `marca` VALUES ('121', '2', 'AJS');
INSERT INTO `marca` VALUES ('122', '2', 'AMAZONAS');
INSERT INTO `marca` VALUES ('123', '2', 'AME AMAZONAS');
INSERT INTO `marca` VALUES ('124', '2', 'APRILIA');
INSERT INTO `marca` VALUES ('125', '2', 'BAJAJ');
INSERT INTO `marca` VALUES ('126', '2', 'BASHAN');
INSERT INTO `marca` VALUES ('127', '2', 'BENELLI');
INSERT INTO `marca` VALUES ('128', '2', 'BIMOTA');
INSERT INTO `marca` VALUES ('130', '2', 'BRANDY');
INSERT INTO `marca` VALUES ('131', '2', 'BRP');
INSERT INTO `marca` VALUES ('132', '2', 'BUELL');
INSERT INTO `marca` VALUES ('133', '2', 'BY CRISTO');
INSERT INTO `marca` VALUES ('134', '2', 'CAGIVA');
INSERT INTO `marca` VALUES ('135', '2', 'CALOI');
INSERT INTO `marca` VALUES ('136', '2', 'CHONGQING');
INSERT INTO `marca` VALUES ('137', '2', 'DAELIM');
INSERT INTO `marca` VALUES ('138', '2', 'DAFRA');
INSERT INTO `marca` VALUES ('139', '2', 'DAYUN');
INSERT INTO `marca` VALUES ('140', '2', 'DERBI');
INSERT INTO `marca` VALUES ('142', '2', 'EMME');
INSERT INTO `marca` VALUES ('143', '2', 'FOX');
INSERT INTO `marca` VALUES ('144', '2', 'FYM');
INSERT INTO `marca` VALUES ('145', '2', 'GARINNI');
INSERT INTO `marca` VALUES ('146', '2', 'GAS GAS');
INSERT INTO `marca` VALUES ('147', '2', 'GILERA');
INSERT INTO `marca` VALUES ('148', '2', 'GREEN');
INSERT INTO `marca` VALUES ('149', '2', 'GUZZI');
INSERT INTO `marca` VALUES ('150', '2', 'HAOBAO');
INSERT INTO `marca` VALUES ('152', '2', 'HARTFORD');
INSERT INTO `marca` VALUES ('154', '2', 'HUSABERG');
INSERT INTO `marca` VALUES ('155', '2', 'HUSQVARNA');
INSERT INTO `marca` VALUES ('156', '2', 'IROS');
INSERT INTO `marca` VALUES ('157', '2', 'JAWA');
INSERT INTO `marca` VALUES ('158', '2', 'JIAPENG VOLCANO');
INSERT INTO `marca` VALUES ('159', '2', 'JONNY');
INSERT INTO `marca` VALUES ('160', '2', 'KAHENA');
INSERT INTO `marca` VALUES ('163', '2', 'KIMCO');
INSERT INTO `marca` VALUES ('164', '2', 'KIN');
INSERT INTO `marca` VALUES ('165', '2', 'KTM');
INSERT INTO `marca` VALUES ('166', '2', 'L AQUILA');
INSERT INTO `marca` VALUES ('167', '2', 'LAMBRETTA');
INSERT INTO `marca` VALUES ('168', '2', 'LEOPARD');
INSERT INTO `marca` VALUES ('169', '2', 'LON-V');
INSERT INTO `marca` VALUES ('170', '2', 'MALAGUTI');
INSERT INTO `marca` VALUES ('171', '2', 'MBW');
INSERT INTO `marca` VALUES ('172', '2', 'MIZA');
INSERT INTO `marca` VALUES ('173', '2', 'MONTESA');
INSERT INTO `marca` VALUES ('174', '2', 'MOTO GUZZI');
INSERT INTO `marca` VALUES ('175', '2', 'MOTOR-Z');
INSERT INTO `marca` VALUES ('176', '2', 'MOTOVI');
INSERT INTO `marca` VALUES ('177', '2', 'MRX');
INSERT INTO `marca` VALUES ('178', '2', 'MV AGUSTA');
INSERT INTO `marca` VALUES ('179', '2', 'MVK');
INSERT INTO `marca` VALUES ('180', '2', 'ORCA');
INSERT INTO `marca` VALUES ('181', '2', 'PEGASSI');
INSERT INTO `marca` VALUES ('182', '2', 'PEUGEOT');
INSERT INTO `marca` VALUES ('183', '2', 'PIAGGIO');
INSERT INTO `marca` VALUES ('184', '2', 'POLARIS');
INSERT INTO `marca` VALUES ('185', '2', 'REGAL RAPTOR');
INSERT INTO `marca` VALUES ('186', '2', 'RIGUETE');
INSERT INTO `marca` VALUES ('187', '2', 'SACHS');
INSERT INTO `marca` VALUES ('188', '2', 'SANYANG');
INSERT INTO `marca` VALUES ('189', '2', 'SHINERAY');
INSERT INTO `marca` VALUES ('190', '2', 'SIAMOTO');
INSERT INTO `marca` VALUES ('193', '2', 'TRAXX');
INSERT INTO `marca` VALUES ('195', '2', 'VENTO');
INSERT INTO `marca` VALUES ('196', '2', 'WUYANG');
INSERT INTO `marca` VALUES ('198', '2', 'YINGANG');
INSERT INTO `marca` VALUES ('199', '2', 'ZEJHIANG');
INSERT INTO `marca` VALUES ('200', '2', 'ZHONGYU');

-- ----------------------------
-- Table structure for marketing
-- ----------------------------
DROP TABLE IF EXISTS `marketing`;
CREATE TABLE `marketing` (
  `MARKETING_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(30) NOT NULL,
  `DESCRICAO` text,
  `PATH` varchar(255) NOT NULL,
  `MINIATURA` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`MARKETING_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of marketing
-- ----------------------------
INSERT INTO `marketing` VALUES ('7', 'Franquia de Testesa', 'fhyudfggf', '33a0520f73e6d72709b9cccb692652b856d37742.psd', '578e43bbe1f3ba84218f3446addcae0292c78039.jpg');
INSERT INTO `marketing` VALUES ('8', 'Teste 3', 'rrt', '775d0416a9ebfb6a175e5ce41ee2d0cbf2d269cf.png', '1430862921b11879c027efff3a6dda3e479a0eeb.jpg');

-- ----------------------------
-- Table structure for modelo
-- ----------------------------
DROP TABLE IF EXISTS `modelo`;
CREATE TABLE `modelo` (
  `MODELO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MARCA_ID` int(11) DEFAULT NULL,
  `MODELO` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`MODELO_ID`),
  KEY `FK_MARCA_MODELO` (`MARCA_ID`),
  CONSTRAINT `FK_MARCA_MODELO` FOREIGN KEY (`MARCA_ID`) REFERENCES `marca` (`MARCA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2271 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of modelo
-- ----------------------------
INSERT INTO `modelo` VALUES ('1', '1', 'ZAFIRA');
INSERT INTO `modelo` VALUES ('2', '1', 'RAMONA');
INSERT INTO `modelo` VALUES ('3', '1', 'PRISMA');
INSERT INTO `modelo` VALUES ('4', '1', 'PICKUP');
INSERT INTO `modelo` VALUES ('5', '1', 'OPALA');
INSERT INTO `modelo` VALUES ('6', '1', 'OMEGA');
INSERT INTO `modelo` VALUES ('7', '1', 'MONZA');
INSERT INTO `modelo` VALUES ('8', '1', 'MONTANA');
INSERT INTO `modelo` VALUES ('9', '1', 'MERIVA');
INSERT INTO `modelo` VALUES ('10', '1', 'MARAJ');
INSERT INTO `modelo` VALUES ('11', '1', 'LUMINA');
INSERT INTO `modelo` VALUES ('12', '1', 'KADETT');
INSERT INTO `modelo` VALUES ('13', '1', 'IPANEMA');
INSERT INTO `modelo` VALUES ('14', '1', 'IMPALA');
INSERT INTO `modelo` VALUES ('15', '1', 'S10');
INSERT INTO `modelo` VALUES ('16', '1', 'S10 4X4');
INSERT INTO `modelo` VALUES ('17', '1', 'SATURN');
INSERT INTO `modelo` VALUES ('18', '1', 'VERANEIO');
INSERT INTO `modelo` VALUES ('19', '1', 'VECTRA');
INSERT INTO `modelo` VALUES ('20', '1', 'TRAFIC');
INSERT INTO `modelo` VALUES ('21', '1', 'TRACKER');
INSERT INTO `modelo` VALUES ('22', '1', 'TIGRA');
INSERT INTO `modelo` VALUES ('23', '1', 'SYCLONE');
INSERT INTO `modelo` VALUES ('24', '1', 'SUPREMA');
INSERT INTO `modelo` VALUES ('25', '1', 'SUBURBAN');
INSERT INTO `modelo` VALUES ('26', '1', 'SSR');
INSERT INTO `modelo` VALUES ('27', '1', 'SS');
INSERT INTO `modelo` VALUES ('28', '1', 'SPORT VAN');
INSERT INTO `modelo` VALUES ('29', '1', 'SPACE VAN');
INSERT INTO `modelo` VALUES ('30', '1', 'SILVERADO');
INSERT INTO `modelo` VALUES ('31', '1', 'GRAND BLAZER');
INSERT INTO `modelo` VALUES ('32', '1', 'FLEETLINE');
INSERT INTO `modelo` VALUES ('33', '1', 'D40');
INSERT INTO `modelo` VALUES ('34', '1', 'C20');
INSERT INTO `modelo` VALUES ('35', '1', 'C15');
INSERT INTO `modelo` VALUES ('36', '1', 'C14');
INSERT INTO `modelo` VALUES ('37', '1', 'C10');
INSERT INTO `modelo` VALUES ('38', '1', 'BUICK');
INSERT INTO `modelo` VALUES ('39', '1', 'BRASINCA');
INSERT INTO `modelo` VALUES ('40', '1', 'BONANZA');
INSERT INTO `modelo` VALUES ('41', '1', 'BLAZER');
INSERT INTO `modelo` VALUES ('42', '1', 'BEL AIR');
INSERT INTO `modelo` VALUES ('43', '1', 'ASTROVAN');
INSERT INTO `modelo` VALUES ('44', '1', 'ASTRA');
INSERT INTO `modelo` VALUES ('45', '1', 'A20');
INSERT INTO `modelo` VALUES ('46', '1', 'A10');
INSERT INTO `modelo` VALUES ('47', '1', '3100');
INSERT INTO `modelo` VALUES ('48', '1', 'CALIBRA');
INSERT INTO `modelo` VALUES ('49', '1', 'CAMARO');
INSERT INTO `modelo` VALUES ('50', '1', 'D20');
INSERT INTO `modelo` VALUES ('51', '1', 'D10');
INSERT INTO `modelo` VALUES ('52', '1', 'CORVETTE');
INSERT INTO `modelo` VALUES ('53', '1', 'CORSICA');
INSERT INTO `modelo` VALUES ('54', '1', 'CORSA');
INSERT INTO `modelo` VALUES ('55', '1', 'COLORADO');
INSERT INTO `modelo` VALUES ('56', '1', 'CLASSIC');
INSERT INTO `modelo` VALUES ('57', '1', 'CHEVY 500');
INSERT INTO `modelo` VALUES ('58', '1', 'CAPRICE');
INSERT INTO `modelo` VALUES ('59', '1', 'CARAVAN');
INSERT INTO `modelo` VALUES ('60', '1', 'CAVALIER');
INSERT INTO `modelo` VALUES ('61', '1', 'CELTA');
INSERT INTO `modelo` VALUES ('62', '1', 'CHEVELLE');
INSERT INTO `modelo` VALUES ('63', '1', 'CHEVETTE');
INSERT INTO `modelo` VALUES ('64', '2', 'EVASION');
INSERT INTO `modelo` VALUES ('65', '2', 'JUMPER');
INSERT INTO `modelo` VALUES ('66', '2', 'XANTIA');
INSERT INTO `modelo` VALUES ('67', '2', 'XM');
INSERT INTO `modelo` VALUES ('68', '2', 'XSARA');
INSERT INTO `modelo` VALUES ('69', '2', 'XSARA PICASSO');
INSERT INTO `modelo` VALUES ('70', '2', 'ZX');
INSERT INTO `modelo` VALUES ('71', '2', 'C8');
INSERT INTO `modelo` VALUES ('72', '2', 'C6');
INSERT INTO `modelo` VALUES ('73', '2', '3 CV');
INSERT INTO `modelo` VALUES ('74', '2', 'AX');
INSERT INTO `modelo` VALUES ('75', '2', 'BERLINGO');
INSERT INTO `modelo` VALUES ('76', '2', 'BX');
INSERT INTO `modelo` VALUES ('77', '2', 'C3');
INSERT INTO `modelo` VALUES ('78', '2', 'C4');
INSERT INTO `modelo` VALUES ('79', '2', 'C5');
INSERT INTO `modelo` VALUES ('80', '3', 'OGGI');
INSERT INTO `modelo` VALUES ('81', '3', 'PALIO');
INSERT INTO `modelo` VALUES ('82', '3', 'PANORAMA');
INSERT INTO `modelo` VALUES ('83', '3', 'PREMIO');
INSERT INTO `modelo` VALUES ('84', '3', 'PUNTO');
INSERT INTO `modelo` VALUES ('85', '3', 'SIENA');
INSERT INTO `modelo` VALUES ('86', '3', 'SPAZIO');
INSERT INTO `modelo` VALUES ('87', '3', 'STILO');
INSERT INTO `modelo` VALUES ('88', '3', 'STRADA');
INSERT INTO `modelo` VALUES ('89', '3', 'TEMPRA');
INSERT INTO `modelo` VALUES ('90', '3', 'TIPO');
INSERT INTO `modelo` VALUES ('91', '3', 'UNO');
INSERT INTO `modelo` VALUES ('92', '3', 'NOVO DUCATO');
INSERT INTO `modelo` VALUES ('93', '3', 'MAREA');
INSERT INTO `modelo` VALUES ('94', '3', 'IDEA');
INSERT INTO `modelo` VALUES ('95', '3', '124');
INSERT INTO `modelo` VALUES ('96', '3', '147');
INSERT INTO `modelo` VALUES ('97', '3', '600');
INSERT INTO `modelo` VALUES ('98', '3', 'BRAVA');
INSERT INTO `modelo` VALUES ('99', '3', 'CINQUECENTO');
INSERT INTO `modelo` VALUES ('100', '3', 'COUPÉ');
INSERT INTO `modelo` VALUES ('101', '3', 'DARDO');
INSERT INTO `modelo` VALUES ('102', '3', 'FIORINO');
INSERT INTO `modelo` VALUES ('103', '3', 'ELBA');
INSERT INTO `modelo` VALUES ('104', '3', 'DUNA');
INSERT INTO `modelo` VALUES ('105', '3', 'DUCATO');
INSERT INTO `modelo` VALUES ('106', '3', 'DOBLÒ');
INSERT INTO `modelo` VALUES ('107', '4', 'CONTOUR');
INSERT INTO `modelo` VALUES ('108', '4', 'CORCEL');
INSERT INTO `modelo` VALUES ('109', '4', 'CORCEL II');
INSERT INTO `modelo` VALUES ('110', '4', 'COURIER');
INSERT INTO `modelo` VALUES ('111', '4', 'CROWN VICTORIA');
INSERT INTO `modelo` VALUES ('112', '4', 'CUPÊ');
INSERT INTO `modelo` VALUES ('113', '4', 'DEL REY');
INSERT INTO `modelo` VALUES ('114', '4', 'ECOSPORT');
INSERT INTO `modelo` VALUES ('115', '4', 'ESCORT');
INSERT INTO `modelo` VALUES ('116', '4', 'EXPEDITION');
INSERT INTO `modelo` VALUES ('117', '4', 'EXPLORER');
INSERT INTO `modelo` VALUES ('118', '4', 'CONSUL');
INSERT INTO `modelo` VALUES ('119', '4', 'COBRA');
INSERT INTO `modelo` VALUES ('120', '4', 'CLUB WAGON');
INSERT INTO `modelo` VALUES ('121', '4', 'WINDSTAR');
INSERT INTO `modelo` VALUES ('122', '4', 'VICTORIA');
INSERT INTO `modelo` VALUES ('123', '4', 'VERSAILLES');
INSERT INTO `modelo` VALUES ('124', '4', 'VERONA');
INSERT INTO `modelo` VALUES ('125', '4', 'TUDOR');
INSERT INTO `modelo` VALUES ('126', '4', 'THUNDERBIRD');
INSERT INTO `modelo` VALUES ('127', '4', 'TAURUS');
INSERT INTO `modelo` VALUES ('128', '4', 'RURAL');
INSERT INTO `modelo` VALUES ('129', '4', 'AEROSTAR');
INSERT INTO `modelo` VALUES ('130', '4', 'ASPIRE');
INSERT INTO `modelo` VALUES ('131', '4', 'BELINA');
INSERT INTO `modelo` VALUES ('132', '4', 'EXPLORER XLT');
INSERT INTO `modelo` VALUES ('133', '4', 'F-1');
INSERT INTO `modelo` VALUES ('134', '4', 'JEEP');
INSERT INTO `modelo` VALUES ('135', '4', 'KA');
INSERT INTO `modelo` VALUES ('136', '4', 'LANDAU');
INSERT INTO `modelo` VALUES ('137', '4', 'MAVERICK');
INSERT INTO `modelo` VALUES ('138', '4', 'MUSTANG');
INSERT INTO `modelo` VALUES ('139', '4', 'ROYALE');
INSERT INTO `modelo` VALUES ('140', '4', 'RANGER');
INSERT INTO `modelo` VALUES ('141', '4', 'PROBE');
INSERT INTO `modelo` VALUES ('142', '4', 'PAMPA');
INSERT INTO `modelo` VALUES ('143', '4', 'MONDEO');
INSERT INTO `modelo` VALUES ('144', '4', 'MODELO T');
INSERT INTO `modelo` VALUES ('145', '4', 'IBIZA');
INSERT INTO `modelo` VALUES ('146', '4', 'GALAXIE');
INSERT INTO `modelo` VALUES ('147', '4', 'F-100');
INSERT INTO `modelo` VALUES ('148', '4', 'F-1000');
INSERT INTO `modelo` VALUES ('149', '4', 'F-150');
INSERT INTO `modelo` VALUES ('150', '4', 'F-250');
INSERT INTO `modelo` VALUES ('151', '4', 'F-350');
INSERT INTO `modelo` VALUES ('152', '4', 'F-75');
INSERT INTO `modelo` VALUES ('153', '4', 'F3 PICK UP');
INSERT INTO `modelo` VALUES ('154', '4', 'FIESTA');
INSERT INTO `modelo` VALUES ('155', '4', 'FOCUS');
INSERT INTO `modelo` VALUES ('156', '4', 'FURGLAINE');
INSERT INTO `modelo` VALUES ('157', '4', 'FUSION');
INSERT INTO `modelo` VALUES ('158', '4', 'MODELO A');
INSERT INTO `modelo` VALUES ('159', '5', 'PRELUDE');
INSERT INTO `modelo` VALUES ('160', '5', 'ACCORD');
INSERT INTO `modelo` VALUES ('161', '5', 'CIVIC');
INSERT INTO `modelo` VALUES ('162', '5', 'CRV');
INSERT INTO `modelo` VALUES ('163', '5', 'FIT');
INSERT INTO `modelo` VALUES ('164', '5', 'LEGEND');
INSERT INTO `modelo` VALUES ('165', '5', 'ODYSSEY');
INSERT INTO `modelo` VALUES ('166', '5', 'PASSPORT');
INSERT INTO `modelo` VALUES ('167', '6', 'SPACE WAGON');
INSERT INTO `modelo` VALUES ('168', '6', 'PAJERO');
INSERT INTO `modelo` VALUES ('169', '6', 'OUTLANDER');
INSERT INTO `modelo` VALUES ('170', '6', 'MONTERO');
INSERT INTO `modelo` VALUES ('171', '6', 'MIRAGE');
INSERT INTO `modelo` VALUES ('172', '6', '3000 GT');
INSERT INTO `modelo` VALUES ('173', '6', 'AIRTREK');
INSERT INTO `modelo` VALUES ('174', '6', 'COLT');
INSERT INTO `modelo` VALUES ('175', '6', 'DIAMANT');
INSERT INTO `modelo` VALUES ('176', '6', 'ECLIPSE');
INSERT INTO `modelo` VALUES ('177', '6', 'EXPO');
INSERT INTO `modelo` VALUES ('178', '6', 'GALANT');
INSERT INTO `modelo` VALUES ('179', '6', 'GRANDIS');
INSERT INTO `modelo` VALUES ('180', '6', 'LANCER');
INSERT INTO `modelo` VALUES ('181', '6', 'L300');
INSERT INTO `modelo` VALUES ('182', '6', 'L200');
INSERT INTO `modelo` VALUES ('183', '7', '309');
INSERT INTO `modelo` VALUES ('184', '7', 'GRD');
INSERT INTO `modelo` VALUES ('185', '7', 'PARTNER');
INSERT INTO `modelo` VALUES ('186', '7', '306');
INSERT INTO `modelo` VALUES ('187', '7', '206');
INSERT INTO `modelo` VALUES ('188', '7', '205');
INSERT INTO `modelo` VALUES ('189', '7', '106');
INSERT INTO `modelo` VALUES ('190', '7', '807');
INSERT INTO `modelo` VALUES ('191', '7', 'GD');
INSERT INTO `modelo` VALUES ('192', '7', 'BOXER');
INSERT INTO `modelo` VALUES ('193', '7', '320');
INSERT INTO `modelo` VALUES ('194', '7', '405');
INSERT INTO `modelo` VALUES ('195', '7', '406');
INSERT INTO `modelo` VALUES ('196', '7', '407');
INSERT INTO `modelo` VALUES ('197', '7', '504');
INSERT INTO `modelo` VALUES ('198', '7', '505');
INSERT INTO `modelo` VALUES ('199', '7', '605');
INSERT INTO `modelo` VALUES ('200', '7', '607');
INSERT INTO `modelo` VALUES ('201', '7', '806');
INSERT INTO `modelo` VALUES ('202', '7', '307');
INSERT INTO `modelo` VALUES ('203', '8', '807');
INSERT INTO `modelo` VALUES ('204', '8', 'BOXER');
INSERT INTO `modelo` VALUES ('205', '8', 'GD');
INSERT INTO `modelo` VALUES ('206', '8', 'GRD');
INSERT INTO `modelo` VALUES ('207', '8', 'PARTNER');
INSERT INTO `modelo` VALUES ('208', '8', '806');
INSERT INTO `modelo` VALUES ('209', '8', '607');
INSERT INTO `modelo` VALUES ('210', '8', '106');
INSERT INTO `modelo` VALUES ('211', '8', '205');
INSERT INTO `modelo` VALUES ('212', '8', '206');
INSERT INTO `modelo` VALUES ('213', '8', '306');
INSERT INTO `modelo` VALUES ('214', '8', '307');
INSERT INTO `modelo` VALUES ('215', '8', '309');
INSERT INTO `modelo` VALUES ('216', '8', '320');
INSERT INTO `modelo` VALUES ('217', '8', '405');
INSERT INTO `modelo` VALUES ('218', '8', '605');
INSERT INTO `modelo` VALUES ('219', '8', '505');
INSERT INTO `modelo` VALUES ('220', '8', '504');
INSERT INTO `modelo` VALUES ('221', '8', '407');
INSERT INTO `modelo` VALUES ('222', '8', '406');
INSERT INTO `modelo` VALUES ('223', '9', 'LEXUS');
INSERT INTO `modelo` VALUES ('224', '9', 'MR-2');
INSERT INTO `modelo` VALUES ('225', '9', 'PASEO');
INSERT INTO `modelo` VALUES ('226', '9', 'PREVIA');
INSERT INTO `modelo` VALUES ('227', '9', 'RAV4');
INSERT INTO `modelo` VALUES ('228', '9', 'SIENNA');
INSERT INTO `modelo` VALUES ('229', '9', 'SUPRA');
INSERT INTO `modelo` VALUES ('230', '9', 'T-100');
INSERT INTO `modelo` VALUES ('231', '9', 'TUNDRA');
INSERT INTO `modelo` VALUES ('232', '9', 'LAND CRUISER PRADO');
INSERT INTO `modelo` VALUES ('233', '9', 'LAND');
INSERT INTO `modelo` VALUES ('234', '9', 'HILUX SW4');
INSERT INTO `modelo` VALUES ('235', '9', '4RUNNER');
INSERT INTO `modelo` VALUES ('236', '9', 'BANDEIRANTE');
INSERT INTO `modelo` VALUES ('237', '9', 'BJ');
INSERT INTO `modelo` VALUES ('238', '9', 'CAMRY');
INSERT INTO `modelo` VALUES ('239', '9', 'CELICA');
INSERT INTO `modelo` VALUES ('240', '9', 'COROLLA');
INSERT INTO `modelo` VALUES ('241', '9', 'CORONA');
INSERT INTO `modelo` VALUES ('242', '9', 'FIELDER');
INSERT INTO `modelo` VALUES ('243', '9', 'HILUX');
INSERT INTO `modelo` VALUES ('244', '10', 'POLO SEDAN');
INSERT INTO `modelo` VALUES ('245', '10', 'POLO');
INSERT INTO `modelo` VALUES ('246', '10', 'POINTER');
INSERT INTO `modelo` VALUES ('247', '10', 'PASSAT');
INSERT INTO `modelo` VALUES ('248', '10', 'PARATI');
INSERT INTO `modelo` VALUES ('249', '10', 'PAG NICK');
INSERT INTO `modelo` VALUES ('250', '10', 'NEW BEETLE');
INSERT INTO `modelo` VALUES ('251', '10', 'LOGUS');
INSERT INTO `modelo` VALUES ('252', '10', 'QUANTUM');
INSERT INTO `modelo` VALUES ('253', '10', 'SANTANA');
INSERT INTO `modelo` VALUES ('254', '10', 'SAVEIRO');
INSERT INTO `modelo` VALUES ('255', '10', 'VOYAGE');
INSERT INTO `modelo` VALUES ('256', '10', 'VARIANT II');
INSERT INTO `modelo` VALUES ('257', '10', 'VARIANT');
INSERT INTO `modelo` VALUES ('258', '10', 'VAN');
INSERT INTO `modelo` VALUES ('259', '10', 'TOUAREG');
INSERT INTO `modelo` VALUES ('260', '10', 'TL');
INSERT INTO `modelo` VALUES ('261', '10', 'SPACEFOX');
INSERT INTO `modelo` VALUES ('262', '10', 'SP');
INSERT INTO `modelo` VALUES ('263', '10', 'KOMBI');
INSERT INTO `modelo` VALUES ('264', '10', 'KARMANN-GHIA');
INSERT INTO `modelo` VALUES ('265', '10', 'BUGGY');
INSERT INTO `modelo` VALUES ('266', '10', 'BRASILIA');
INSERT INTO `modelo` VALUES ('267', '10', 'BORA');
INSERT INTO `modelo` VALUES ('268', '10', 'BIANCO');
INSERT INTO `modelo` VALUES ('269', '10', 'APOLLO');
INSERT INTO `modelo` VALUES ('270', '10', '1600');
INSERT INTO `modelo` VALUES ('271', '10', '1300');
INSERT INTO `modelo` VALUES ('272', '10', '1200');
INSERT INTO `modelo` VALUES ('273', '10', 'BUGRE');
INSERT INTO `modelo` VALUES ('274', '10', 'CARAVELLE');
INSERT INTO `modelo` VALUES ('275', '10', 'CORRADO');
INSERT INTO `modelo` VALUES ('276', '10', 'JETTA III');
INSERT INTO `modelo` VALUES ('277', '10', 'GOLF');
INSERT INTO `modelo` VALUES ('278', '10', 'GOL');
INSERT INTO `modelo` VALUES ('279', '10', 'FUSCA');
INSERT INTO `modelo` VALUES ('280', '10', 'FOX');
INSERT INTO `modelo` VALUES ('281', '10', 'EUROVAN');
INSERT INTO `modelo` VALUES ('282', '10', 'CROSSFOX');
INSERT INTO `modelo` VALUES ('283', '10', 'COYOTE');
INSERT INTO `modelo` VALUES ('284', '12', 'NSX');
INSERT INTO `modelo` VALUES ('285', '12', 'LEGEND');
INSERT INTO `modelo` VALUES ('286', '12', 'INTEGRA');
INSERT INTO `modelo` VALUES ('287', '13', 'COUPÉ');
INSERT INTO `modelo` VALUES ('288', '14', 'FURGOVAN');
INSERT INTO `modelo` VALUES ('289', '14', 'MARRUÁ');
INSERT INTO `modelo` VALUES ('290', '15', '2300 SL');
INSERT INTO `modelo` VALUES ('291', '15', '2300 Ti');
INSERT INTO `modelo` VALUES ('292', '15', '2300 Ti4');
INSERT INTO `modelo` VALUES ('293', '15', 'ALFETTA');
INSERT INTO `modelo` VALUES ('294', '15', 'GIULIETTA');
INSERT INTO `modelo` VALUES ('295', '15', 'SPIDER');
INSERT INTO `modelo` VALUES ('296', '15', '2300 B');
INSERT INTO `modelo` VALUES ('297', '15', '1750');
INSERT INTO `modelo` VALUES ('298', '15', '145');
INSERT INTO `modelo` VALUES ('299', '15', '147');
INSERT INTO `modelo` VALUES ('300', '15', '155');
INSERT INTO `modelo` VALUES ('301', '15', '156');
INSERT INTO `modelo` VALUES ('302', '15', '164');
INSERT INTO `modelo` VALUES ('303', '15', '166');
INSERT INTO `modelo` VALUES ('304', '16', 'CLASSIC 427');
INSERT INTO `modelo` VALUES ('305', '16', 'CLASSIC XK120');
INSERT INTO `modelo` VALUES ('306', '17', 'TOWNER');
INSERT INTO `modelo` VALUES ('307', '17', 'TOPIC');
INSERT INTO `modelo` VALUES ('308', '17', 'ROCSTA');
INSERT INTO `modelo` VALUES ('309', '17', 'HI-TOPIC');
INSERT INTO `modelo` VALUES ('310', '17', 'GALLOPER');
INSERT INTO `modelo` VALUES ('311', '17', 'AM 825');
INSERT INTO `modelo` VALUES ('312', '18', 'DB7');
INSERT INTO `modelo` VALUES ('313', '18', 'V8 VANTAGE');
INSERT INTO `modelo` VALUES ('314', '19', 'RS2');
INSERT INTO `modelo` VALUES ('315', '19', 'RS4');
INSERT INTO `modelo` VALUES ('316', '19', 'RS6');
INSERT INTO `modelo` VALUES ('317', '19', 'S3');
INSERT INTO `modelo` VALUES ('318', '19', 'S4');
INSERT INTO `modelo` VALUES ('319', '19', 'S6');
INSERT INTO `modelo` VALUES ('320', '19', 'S8');
INSERT INTO `modelo` VALUES ('321', '19', 'TT');
INSERT INTO `modelo` VALUES ('322', '19', 'Q7');
INSERT INTO `modelo` VALUES ('323', '19', 'AVANT');
INSERT INTO `modelo` VALUES ('324', '19', '100');
INSERT INTO `modelo` VALUES ('325', '19', '80');
INSERT INTO `modelo` VALUES ('326', '19', 'A2');
INSERT INTO `modelo` VALUES ('327', '19', 'A3');
INSERT INTO `modelo` VALUES ('328', '19', 'A4');
INSERT INTO `modelo` VALUES ('329', '19', 'A6');
INSERT INTO `modelo` VALUES ('330', '19', 'A8');
INSERT INTO `modelo` VALUES ('331', '19', 'ALLROAD');
INSERT INTO `modelo` VALUES ('332', '20', 'A40');
INSERT INTO `modelo` VALUES ('333', '21', 'BUGGY');
INSERT INTO `modelo` VALUES ('334', '22', 'BABY TST');
INSERT INTO `modelo` VALUES ('335', '22', 'BABY');
INSERT INTO `modelo` VALUES ('336', '23', 'BROOKLANDS');
INSERT INTO `modelo` VALUES ('337', '23', 'CONTINENTAL');
INSERT INTO `modelo` VALUES ('338', '24', 'S');
INSERT INTO `modelo` VALUES ('339', '25', '650Ci');
INSERT INTO `modelo` VALUES ('340', '25', '650i');
INSERT INTO `modelo` VALUES ('341', '25', '730i');
INSERT INTO `modelo` VALUES ('342', '25', '730iA');
INSERT INTO `modelo` VALUES ('343', '25', '735i');
INSERT INTO `modelo` VALUES ('344', '25', '735IL E32');
INSERT INTO `modelo` VALUES ('345', '25', '740i');
INSERT INTO `modelo` VALUES ('346', '25', '650');
INSERT INTO `modelo` VALUES ('347', '25', '645CI');
INSERT INTO `modelo` VALUES ('348', '25', '635CSi');
INSERT INTO `modelo` VALUES ('349', '25', '550i');
INSERT INTO `modelo` VALUES ('350', '25', '545i');
INSERT INTO `modelo` VALUES ('351', '25', '540iTA');
INSERT INTO `modelo` VALUES ('352', '25', '540iA');
INSERT INTO `modelo` VALUES ('353', '25', '540i');
INSERT INTO `modelo` VALUES ('354', '25', '535i');
INSERT INTO `modelo` VALUES ('355', '25', '740iL');
INSERT INTO `modelo` VALUES ('356', '25', '745i');
INSERT INTO `modelo` VALUES ('357', '25', 'Z4');
INSERT INTO `modelo` VALUES ('358', '25', 'Z3');
INSERT INTO `modelo` VALUES ('359', '25', 'Z1');
INSERT INTO `modelo` VALUES ('360', '25', 'X5');
INSERT INTO `modelo` VALUES ('361', '25', 'X3');
INSERT INTO `modelo` VALUES ('362', '25', 'NEW CLASS');
INSERT INTO `modelo` VALUES ('363', '25', 'M6');
INSERT INTO `modelo` VALUES ('364', '25', 'M5');
INSERT INTO `modelo` VALUES ('365', '25', 'M3');
INSERT INTO `modelo` VALUES ('366', '25', 'ISETTA');
INSERT INTO `modelo` VALUES ('367', '25', '850CSi');
INSERT INTO `modelo` VALUES ('368', '25', '850Ci');
INSERT INTO `modelo` VALUES ('369', '25', '840Ci');
INSERT INTO `modelo` VALUES ('370', '25', '760Li');
INSERT INTO `modelo` VALUES ('371', '25', '750i');
INSERT INTO `modelo` VALUES ('372', '25', 'Z8');
INSERT INTO `modelo` VALUES ('373', '25', '530i');
INSERT INTO `modelo` VALUES ('374', '25', '120i');
INSERT INTO `modelo` VALUES ('375', '25', '323i');
INSERT INTO `modelo` VALUES ('376', '25', '323CiA');
INSERT INTO `modelo` VALUES ('377', '25', '323Ci');
INSERT INTO `modelo` VALUES ('378', '25', '320i');
INSERT INTO `modelo` VALUES ('379', '25', '318TiA');
INSERT INTO `modelo` VALUES ('380', '25', '318Ti');
INSERT INTO `modelo` VALUES ('381', '25', '318iSA');
INSERT INTO `modelo` VALUES ('382', '25', '318iS');
INSERT INTO `modelo` VALUES ('383', '25', '318i');
INSERT INTO `modelo` VALUES ('384', '25', '316i');
INSERT INTO `modelo` VALUES ('385', '25', '2002');
INSERT INTO `modelo` VALUES ('386', '25', '1802');
INSERT INTO `modelo` VALUES ('387', '25', '1602');
INSERT INTO `modelo` VALUES ('388', '25', '1600');
INSERT INTO `modelo` VALUES ('389', '25', '130i');
INSERT INTO `modelo` VALUES ('390', '25', '323iA');
INSERT INTO `modelo` VALUES ('391', '25', '323Ti');
INSERT INTO `modelo` VALUES ('392', '25', '528iL');
INSERT INTO `modelo` VALUES ('393', '25', '528iA');
INSERT INTO `modelo` VALUES ('394', '25', '528i');
INSERT INTO `modelo` VALUES ('395', '25', '525iA');
INSERT INTO `modelo` VALUES ('396', '25', '525i');
INSERT INTO `modelo` VALUES ('397', '25', '520i');
INSERT INTO `modelo` VALUES ('398', '25', '335i');
INSERT INTO `modelo` VALUES ('399', '25', '330iA');
INSERT INTO `modelo` VALUES ('400', '25', '330i');
INSERT INTO `modelo` VALUES ('401', '25', '325Ci');
INSERT INTO `modelo` VALUES ('402', '25', '325i');
INSERT INTO `modelo` VALUES ('403', '25', '325iA');
INSERT INTO `modelo` VALUES ('404', '25', '328i');
INSERT INTO `modelo` VALUES ('405', '25', '328iA');
INSERT INTO `modelo` VALUES ('406', '25', '330Ci');
INSERT INTO `modelo` VALUES ('407', '25', '330CiA');
INSERT INTO `modelo` VALUES ('408', '26', 'M-8');
INSERT INTO `modelo` VALUES ('409', '26', 'M-12');
INSERT INTO `modelo` VALUES ('410', '26', 'M-11');
INSERT INTO `modelo` VALUES ('411', '26', 'M-10 1/2');
INSERT INTO `modelo` VALUES ('412', '26', 'M-10');
INSERT INTO `modelo` VALUES ('413', '27', 'BUGGY');
INSERT INTO `modelo` VALUES ('414', '28', 'SPORTWAGON');
INSERT INTO `modelo` VALUES ('415', '28', 'ROADSTER');
INSERT INTO `modelo` VALUES ('416', '28', 'RIVIERA');
INSERT INTO `modelo` VALUES ('417', '28', 'REGAL');
INSERT INTO `modelo` VALUES ('418', '28', 'DINAFLOW');
INSERT INTO `modelo` VALUES ('419', '28', 'CENTURY');
INSERT INTO `modelo` VALUES ('420', '29', 'SRX');
INSERT INTO `modelo` VALUES ('421', '29', 'SEVILLE');
INSERT INTO `modelo` VALUES ('422', '29', 'LIMOUSINE');
INSERT INTO `modelo` VALUES ('423', '29', 'ESCALADE');
INSERT INTO `modelo` VALUES ('424', '29', 'ELDORADO');
INSERT INTO `modelo` VALUES ('425', '29', 'DE VILLE');
INSERT INTO `modelo` VALUES ('426', '29', 'BROUGHAM');
INSERT INTO `modelo` VALUES ('427', '30', 'JIPE JAVALI');
INSERT INTO `modelo` VALUES ('428', '31', 'SPEEDSTER');
INSERT INTO `modelo` VALUES ('429', '31', 'SPYDER');
INSERT INTO `modelo` VALUES ('430', '31', 'SUPER 90');
INSERT INTO `modelo` VALUES ('431', '32', 'UTILITY');
INSERT INTO `modelo` VALUES ('432', '32', 'FAMILY');
INSERT INTO `modelo` VALUES ('433', '32', 'CARGO X-CAB');
INSERT INTO `modelo` VALUES ('434', '32', 'CARGO');
INSERT INTO `modelo` VALUES ('435', '33', 'PACIFICA');
INSERT INTO `modelo` VALUES ('436', '33', 'PT CRUISER');
INSERT INTO `modelo` VALUES ('437', '33', 'SEBRING');
INSERT INTO `modelo` VALUES ('438', '33', 'STRATUS');
INSERT INTO `modelo` VALUES ('439', '33', 'TOWN & COUNTRY');
INSERT INTO `modelo` VALUES ('440', '33', 'VISION');
INSERT INTO `modelo` VALUES ('441', '33', 'VOYAGER');
INSERT INTO `modelo` VALUES ('442', '33', 'NEON');
INSERT INTO `modelo` VALUES ('443', '33', 'LE BARON');
INSERT INTO `modelo` VALUES ('444', '33', '300 C');
INSERT INTO `modelo` VALUES ('445', '33', '300 M');
INSERT INTO `modelo` VALUES ('446', '33', 'CARAVAN');
INSERT INTO `modelo` VALUES ('447', '33', 'CIRRUS');
INSERT INTO `modelo` VALUES ('448', '33', 'CONCORDE');
INSERT INTO `modelo` VALUES ('449', '33', 'DE SOTO');
INSERT INTO `modelo` VALUES ('450', '33', 'GRAND CARAVAN');
INSERT INTO `modelo` VALUES ('451', '34', 'RÉPLICA');
INSERT INTO `modelo` VALUES ('452', '35', 'CL');
INSERT INTO `modelo` VALUES ('453', '36', 'TICO');
INSERT INTO `modelo` VALUES ('454', '36', 'SUPER SALON');
INSERT INTO `modelo` VALUES ('455', '36', 'RACER');
INSERT INTO `modelo` VALUES ('456', '36', 'PRINCE');
INSERT INTO `modelo` VALUES ('457', '36', 'NUBIRA');
INSERT INTO `modelo` VALUES ('458', '36', 'LEGANZA');
INSERT INTO `modelo` VALUES ('459', '36', 'LANOS');
INSERT INTO `modelo` VALUES ('460', '36', 'ESPERO');
INSERT INTO `modelo` VALUES ('461', '37', 'TERIOS');
INSERT INTO `modelo` VALUES ('462', '37', 'APPLAUSE');
INSERT INTO `modelo` VALUES ('463', '37', 'CHARADE');
INSERT INTO `modelo` VALUES ('464', '37', 'CUORE');
INSERT INTO `modelo` VALUES ('465', '37', 'FEROZA');
INSERT INTO `modelo` VALUES ('466', '37', 'GRAN MOVE');
INSERT INTO `modelo` VALUES ('467', '37', 'MOVE VAN');
INSERT INTO `modelo` VALUES ('468', '38', 'VEMAGUET');
INSERT INTO `modelo` VALUES ('469', '38', 'FISSORE');
INSERT INTO `modelo` VALUES ('470', '38', 'CANDANGO');
INSERT INTO `modelo` VALUES ('471', '38', 'BELCAR');
INSERT INTO `modelo` VALUES ('472', '39', 'VIPER');
INSERT INTO `modelo` VALUES ('473', '39', 'STEALTH');
INSERT INTO `modelo` VALUES ('474', '39', 'RAM');
INSERT INTO `modelo` VALUES ('475', '39', 'POLARA');
INSERT INTO `modelo` VALUES ('476', '39', 'MAGNUM');
INSERT INTO `modelo` VALUES ('477', '39', 'LE BARON');
INSERT INTO `modelo` VALUES ('478', '39', 'INTREPID');
INSERT INTO `modelo` VALUES ('479', '39', 'DURANGO');
INSERT INTO `modelo` VALUES ('480', '39', 'DART');
INSERT INTO `modelo` VALUES ('481', '39', 'DAKOTA');
INSERT INTO `modelo` VALUES ('482', '39', 'CHARGER');
INSERT INTO `modelo` VALUES ('483', '39', 'AVENGER');
INSERT INTO `modelo` VALUES ('484', '40', 'BUGUE');
INSERT INTO `modelo` VALUES ('485', '41', 'FURGÃO');
INSERT INTO `modelo` VALUES ('486', '41', 'M100');
INSERT INTO `modelo` VALUES ('487', '41', 'MINIVAN');
INSERT INTO `modelo` VALUES ('488', '41', 'VAN PICKUP');
INSERT INTO `modelo` VALUES ('489', '42', 'ENGESA');
INSERT INTO `modelo` VALUES ('490', '43', 'SUPER 90');
INSERT INTO `modelo` VALUES ('491', '43', 'ENVEMO');
INSERT INTO `modelo` VALUES ('492', '43', 'CAMPER');
INSERT INTO `modelo` VALUES ('493', '45', 'F550');
INSERT INTO `modelo` VALUES ('494', '45', 'F512 M');
INSERT INTO `modelo` VALUES ('495', '45', 'F50');
INSERT INTO `modelo` VALUES ('496', '45', 'F456 M');
INSERT INTO `modelo` VALUES ('497', '45', 'F456');
INSERT INTO `modelo` VALUES ('498', '45', 'F430');
INSERT INTO `modelo` VALUES ('499', '45', 'F40');
INSERT INTO `modelo` VALUES ('500', '45', 'F360');
INSERT INTO `modelo` VALUES ('501', '45', 'F355 F1');
INSERT INTO `modelo` VALUES ('502', '45', 'F355');
INSERT INTO `modelo` VALUES ('503', '45', 'DINO 308');
INSERT INTO `modelo` VALUES ('504', '45', 'DINO 246 GT');
INSERT INTO `modelo` VALUES ('505', '45', '612');
INSERT INTO `modelo` VALUES ('506', '45', '348');
INSERT INTO `modelo` VALUES ('507', '45', 'F575M');
INSERT INTO `modelo` VALUES ('508', '46', '2000');
INSERT INTO `modelo` VALUES ('509', '48', 'METRO');
INSERT INTO `modelo` VALUES ('510', '48', 'STORM');
INSERT INTO `modelo` VALUES ('511', '49', 'YUKON');
INSERT INTO `modelo` VALUES ('512', '49', 'SS');
INSERT INTO `modelo` VALUES ('513', '49', 'SONOMA');
INSERT INTO `modelo` VALUES ('514', '49', 'SILVERADO');
INSERT INTO `modelo` VALUES ('515', '49', 'SIERRA');
INSERT INTO `modelo` VALUES ('516', '49', 'ENVOY');
INSERT INTO `modelo` VALUES ('517', '49', 'CHEYENNE');
INSERT INTO `modelo` VALUES ('518', '49', 'BLAZER');
INSERT INTO `modelo` VALUES ('519', '49', '3500 HD');
INSERT INTO `modelo` VALUES ('520', '50', 'FUTURA');
INSERT INTO `modelo` VALUES ('521', '51', 'XEF');
INSERT INTO `modelo` VALUES ('522', '51', 'XAVANTE');
INSERT INTO `modelo` VALUES ('523', '51', 'X12');
INSERT INTO `modelo` VALUES ('524', '51', 'SUPERMINI');
INSERT INTO `modelo` VALUES ('525', '51', 'G800');
INSERT INTO `modelo` VALUES ('526', '51', 'G-15');
INSERT INTO `modelo` VALUES ('527', '51', 'CARAJÁS');
INSERT INTO `modelo` VALUES ('528', '51', 'BR-800');
INSERT INTO `modelo` VALUES ('529', '52', 'H3');
INSERT INTO `modelo` VALUES ('530', '52', 'H2');
INSERT INTO `modelo` VALUES ('531', '52', 'AM');
INSERT INTO `modelo` VALUES ('532', '53', 'SANTA FÉ');
INSERT INTO `modelo` VALUES ('533', '53', 'SCOUPE');
INSERT INTO `modelo` VALUES ('534', '53', 'SONATA');
INSERT INTO `modelo` VALUES ('535', '53', 'TERRACAN');
INSERT INTO `modelo` VALUES ('536', '53', 'TIBURON');
INSERT INTO `modelo` VALUES ('537', '53', 'TRAJET');
INSERT INTO `modelo` VALUES ('538', '53', 'TUCSON');
INSERT INTO `modelo` VALUES ('539', '53', 'VERA CRUZ');
INSERT INTO `modelo` VALUES ('540', '53', 'PORTER');
INSERT INTO `modelo` VALUES ('541', '53', 'MATRIX');
INSERT INTO `modelo` VALUES ('542', '53', 'HR');
INSERT INTO `modelo` VALUES ('543', '53', 'ACCENT');
INSERT INTO `modelo` VALUES ('544', '53', 'ATOS');
INSERT INTO `modelo` VALUES ('545', '53', 'COUPÊ');
INSERT INTO `modelo` VALUES ('546', '53', 'ELANTRA');
INSERT INTO `modelo` VALUES ('547', '53', 'EXCEL');
INSERT INTO `modelo` VALUES ('548', '53', 'GALLOPER');
INSERT INTO `modelo` VALUES ('549', '53', 'H1 STAREX');
INSERT INTO `modelo` VALUES ('550', '53', 'H100');
INSERT INTO `modelo` VALUES ('551', '54', 'Q45');
INSERT INTO `modelo` VALUES ('552', '54', 'J30');
INSERT INTO `modelo` VALUES ('553', '54', 'G35');
INSERT INTO `modelo` VALUES ('554', '54', 'FX45');
INSERT INTO `modelo` VALUES ('555', '54', 'FX35');
INSERT INTO `modelo` VALUES ('556', '56', 'AMIGO');
INSERT INTO `modelo` VALUES ('557', '56', 'HOMBRE');
INSERT INTO `modelo` VALUES ('558', '56', 'IMPULSE');
INSERT INTO `modelo` VALUES ('559', '56', 'RODEO');
INSERT INTO `modelo` VALUES ('560', '56', 'TROOPER');
INSERT INTO `modelo` VALUES ('561', '57', 'DAILY 50.13');
INSERT INTO `modelo` VALUES ('562', '57', 'DAILY');
INSERT INTO `modelo` VALUES ('563', '57', '5912');
INSERT INTO `modelo` VALUES ('564', '58', 'XJ6');
INSERT INTO `modelo` VALUES ('565', '58', 'XJ8');
INSERT INTO `modelo` VALUES ('566', '58', 'XJR');
INSERT INTO `modelo` VALUES ('567', '58', 'XJS');
INSERT INTO `modelo` VALUES ('568', '58', 'XK');
INSERT INTO `modelo` VALUES ('569', '58', 'XK 120');
INSERT INTO `modelo` VALUES ('570', '58', 'XK8');
INSERT INTO `modelo` VALUES ('571', '58', 'XK8 AUTO');
INSERT INTO `modelo` VALUES ('572', '58', 'XKR');
INSERT INTO `modelo` VALUES ('573', '58', 'XJ12L');
INSERT INTO `modelo` VALUES ('574', '58', 'XJ12');
INSERT INTO `modelo` VALUES ('575', '58', 'DAIMLER');
INSERT INTO `modelo` VALUES ('576', '58', 'DAIMLER LONG');
INSERT INTO `modelo` VALUES ('577', '58', 'EXECUTIVE');
INSERT INTO `modelo` VALUES ('578', '58', 'LISTER');
INSERT INTO `modelo` VALUES ('579', '58', 'S-TYPE');
INSERT INTO `modelo` VALUES ('580', '58', 'SOVEREIGN LONG');
INSERT INTO `modelo` VALUES ('581', '58', 'X-TYPE');
INSERT INTO `modelo` VALUES ('582', '58', 'XJ');
INSERT INTO `modelo` VALUES ('583', '58', 'XJ-S');
INSERT INTO `modelo` VALUES ('584', '59', 'WRANGLER');
INSERT INTO `modelo` VALUES ('585', '59', 'LIBERTY');
INSERT INTO `modelo` VALUES ('586', '59', 'GRAND CHEROKEE');
INSERT INTO `modelo` VALUES ('587', '59', 'COMMANDER');
INSERT INTO `modelo` VALUES ('588', '59', 'CJ 5');
INSERT INTO `modelo` VALUES ('589', '59', 'CHEROKEE');
INSERT INTO `modelo` VALUES ('590', '60', 'MONTEZ');
INSERT INTO `modelo` VALUES ('591', '61', 'OPIRUS');
INSERT INTO `modelo` VALUES ('592', '61', 'PICANTO');
INSERT INTO `modelo` VALUES ('593', '61', 'SEPHIA');
INSERT INTO `modelo` VALUES ('594', '61', 'SHUMA');
INSERT INTO `modelo` VALUES ('595', '61', 'SORENTO');
INSERT INTO `modelo` VALUES ('596', '61', 'SPORTAGE');
INSERT INTO `modelo` VALUES ('597', '61', 'MAGENTIS');
INSERT INTO `modelo` VALUES ('598', '61', 'CLARUS');
INSERT INTO `modelo` VALUES ('599', '61', 'CERES');
INSERT INTO `modelo` VALUES ('600', '61', 'Cerato');
INSERT INTO `modelo` VALUES ('601', '61', 'CARNIVAL');
INSERT INTO `modelo` VALUES ('602', '61', 'CARENS');
INSERT INTO `modelo` VALUES ('603', '61', 'BONGO');
INSERT INTO `modelo` VALUES ('604', '61', 'BESTA');
INSERT INTO `modelo` VALUES ('605', '62', 'SAMARA');
INSERT INTO `modelo` VALUES ('606', '62', 'NIVA');
INSERT INTO `modelo` VALUES ('607', '62', 'LAIKA');
INSERT INTO `modelo` VALUES ('608', '63', 'DIABLO');
INSERT INTO `modelo` VALUES ('609', '63', 'GALLARDO');
INSERT INTO `modelo` VALUES ('610', '63', 'MURCIELAGO');
INSERT INTO `modelo` VALUES ('611', '64', 'DELTA');
INSERT INTO `modelo` VALUES ('612', '65', 'RANGE ROVER SPORT');
INSERT INTO `modelo` VALUES ('613', '65', 'RANGE ROVER');
INSERT INTO `modelo` VALUES ('614', '65', 'FREELANDER');
INSERT INTO `modelo` VALUES ('615', '65', 'DISCOVERY 3');
INSERT INTO `modelo` VALUES ('616', '65', 'DISCOVERY 2');
INSERT INTO `modelo` VALUES ('617', '65', 'DISCOVERY');
INSERT INTO `modelo` VALUES ('618', '65', 'DEFENDER');
INSERT INTO `modelo` VALUES ('619', '66', 'SC 430');
INSERT INTO `modelo` VALUES ('620', '66', 'SC 400');
INSERT INTO `modelo` VALUES ('621', '66', 'SC 300');
INSERT INTO `modelo` VALUES ('622', '66', 'RX 350');
INSERT INTO `modelo` VALUES ('623', '66', 'RX');
INSERT INTO `modelo` VALUES ('624', '66', 'LS 400');
INSERT INTO `modelo` VALUES ('625', '66', 'LS');
INSERT INTO `modelo` VALUES ('626', '66', 'GS');
INSERT INTO `modelo` VALUES ('627', '66', 'ES 350');
INSERT INTO `modelo` VALUES ('628', '66', 'ES');
INSERT INTO `modelo` VALUES ('629', '67', 'TOWN CAR');
INSERT INTO `modelo` VALUES ('630', '67', 'NAVIGATOR');
INSERT INTO `modelo` VALUES ('631', '67', 'MARK VIII');
INSERT INTO `modelo` VALUES ('632', '67', 'LS');
INSERT INTO `modelo` VALUES ('633', '67', 'CONTINENTAL');
INSERT INTO `modelo` VALUES ('634', '68', 'H1');
INSERT INTO `modelo` VALUES ('635', '69', 'ELAN');
INSERT INTO `modelo` VALUES ('636', '69', 'ELISE');
INSERT INTO `modelo` VALUES ('637', '69', 'ESPIRIT');
INSERT INTO `modelo` VALUES ('638', '69', 'EXIGE');
INSERT INTO `modelo` VALUES ('639', '70', 'VOLARE');
INSERT INTO `modelo` VALUES ('640', '70', 'FRATELLO');
INSERT INTO `modelo` VALUES ('641', '71', 'BEACH');
INSERT INTO `modelo` VALUES ('642', '71', 'SUPER');
INSERT INTO `modelo` VALUES ('643', '72', 'SPYDER');
INSERT INTO `modelo` VALUES ('644', '72', 'SHAMAL');
INSERT INTO `modelo` VALUES ('645', '72', 'QUATTROPORTE');
INSERT INTO `modelo` VALUES ('646', '72', 'GRANSPORT');
INSERT INTO `modelo` VALUES ('647', '72', 'GHIBLI');
INSERT INTO `modelo` VALUES ('648', '72', 'COUPÉ');
INSERT INTO `modelo` VALUES ('649', '72', '430');
INSERT INTO `modelo` VALUES ('650', '72', '4200');
INSERT INTO `modelo` VALUES ('651', '72', '3200');
INSERT INTO `modelo` VALUES ('652', '72', '222');
INSERT INTO `modelo` VALUES ('653', '73', 'PICK-UP');
INSERT INTO `modelo` VALUES ('654', '74', 'MX-5/MIATA');
INSERT INTO `modelo` VALUES ('655', '74', 'MX-6');
INSERT INTO `modelo` VALUES ('656', '74', 'NAVAJO');
INSERT INTO `modelo` VALUES ('657', '74', 'PROTEGÉ');
INSERT INTO `modelo` VALUES ('658', '74', 'RX-7');
INSERT INTO `modelo` VALUES ('659', '74', 'RX-8');
INSERT INTO `modelo` VALUES ('660', '74', 'XEDOS-9');
INSERT INTO `modelo` VALUES ('661', '74', 'MX-3');
INSERT INTO `modelo` VALUES ('662', '74', 'MX');
INSERT INTO `modelo` VALUES ('663', '74', '323');
INSERT INTO `modelo` VALUES ('664', '74', '626');
INSERT INTO `modelo` VALUES ('665', '74', '929');
INSERT INTO `modelo` VALUES ('666', '74', 'B-2200');
INSERT INTO `modelo` VALUES ('667', '74', 'B-2500');
INSERT INTO `modelo` VALUES ('668', '74', 'MILLENIA');
INSERT INTO `modelo` VALUES ('669', '74', 'MPV');
INSERT INTO `modelo` VALUES ('670', '75', 'E 500 EB4');
INSERT INTO `modelo` VALUES ('671', '75', 'E 500');
INSERT INTO `modelo` VALUES ('672', '75', 'E 430');
INSERT INTO `modelo` VALUES ('673', '75', 'E 420');
INSERT INTO `modelo` VALUES ('674', '75', 'E 350');
INSERT INTO `modelo` VALUES ('675', '75', 'E 320');
INSERT INTO `modelo` VALUES ('676', '75', 'E 240');
INSERT INTO `modelo` VALUES ('677', '75', 'E 55 AMG');
INSERT INTO `modelo` VALUES ('678', '75', 'E 55 K AMG');
INSERT INTO `modelo` VALUES ('679', '75', 'E 55 T AMG');
INSERT INTO `modelo` VALUES ('680', '75', 'E 65');
INSERT INTO `modelo` VALUES ('681', '75', 'G 350 TD');
INSERT INTO `modelo` VALUES ('682', '75', 'G 400');
INSERT INTO `modelo` VALUES ('683', '75', 'G 400 CDI SW 2850');
INSERT INTO `modelo` VALUES ('684', '75', 'G 500');
INSERT INTO `modelo` VALUES ('685', '75', 'G 500 SW 2850');
INSERT INTO `modelo` VALUES ('686', '75', 'E 220');
INSERT INTO `modelo` VALUES ('687', '75', 'E 200');
INSERT INTO `modelo` VALUES ('688', '75', 'CLASSE A');
INSERT INTO `modelo` VALUES ('689', '75', 'CLK');
INSERT INTO `modelo` VALUES ('690', '75', 'CLK 230');
INSERT INTO `modelo` VALUES ('691', '75', 'CLK 320');
INSERT INTO `modelo` VALUES ('692', '75', 'CLK 350');
INSERT INTO `modelo` VALUES ('693', '75', 'CLK 350 CA');
INSERT INTO `modelo` VALUES ('694', '75', 'CLK 430');
INSERT INTO `modelo` VALUES ('695', '75', 'CLK 500');
INSERT INTO `modelo` VALUES ('696', '75', 'CLK 500 CA');
INSERT INTO `modelo` VALUES ('697', '75', 'CLK 55 AMG');
INSERT INTO `modelo` VALUES ('698', '75', 'CLK 55 AMG CA');
INSERT INTO `modelo` VALUES ('699', '75', 'CLS 350');
INSERT INTO `modelo` VALUES ('700', '75', 'CLS 500');
INSERT INTO `modelo` VALUES ('701', '75', 'CLS 55 AMG');
INSERT INTO `modelo` VALUES ('702', '75', 'CLS 63 AMG');
INSERT INTO `modelo` VALUES ('703', '75', 'CL 65');
INSERT INTO `modelo` VALUES ('704', '75', 'G 55 AMG SW 2850');
INSERT INTO `modelo` VALUES ('705', '75', 'MICROÔNIBUS');
INSERT INTO `modelo` VALUES ('706', '75', 'S 600');
INSERT INTO `modelo` VALUES ('707', '75', 'SL 280');
INSERT INTO `modelo` VALUES ('708', '75', 'SL 320');
INSERT INTO `modelo` VALUES ('709', '75', 'SL 350');
INSERT INTO `modelo` VALUES ('710', '75', 'SL 500');
INSERT INTO `modelo` VALUES ('711', '75', 'SL 55 AMG');
INSERT INTO `modelo` VALUES ('712', '75', 'SL 55 K AMG');
INSERT INTO `modelo` VALUES ('713', '75', 'SL 600');
INSERT INTO `modelo` VALUES ('714', '75', 'SL 65 AMG');
INSERT INTO `modelo` VALUES ('715', '75', 'SLK 200');
INSERT INTO `modelo` VALUES ('716', '75', 'SLK 230');
INSERT INTO `modelo` VALUES ('717', '75', 'SLK 320');
INSERT INTO `modelo` VALUES ('718', '75', 'SLK 350');
INSERT INTO `modelo` VALUES ('719', '75', 'SLK 55 AMG');
INSERT INTO `modelo` VALUES ('720', '75', 'SMART');
INSERT INTO `modelo` VALUES ('721', '75', 'S 55');
INSERT INTO `modelo` VALUES ('722', '75', 'S 500 L');
INSERT INTO `modelo` VALUES ('723', '75', 'ML');
INSERT INTO `modelo` VALUES ('724', '75', 'ML 230');
INSERT INTO `modelo` VALUES ('725', '75', 'ML 320');
INSERT INTO `modelo` VALUES ('726', '75', 'ML 350');
INSERT INTO `modelo` VALUES ('727', '75', 'ML 430');
INSERT INTO `modelo` VALUES ('728', '75', 'ML 500');
INSERT INTO `modelo` VALUES ('729', '75', 'ML 55 AMG');
INSERT INTO `modelo` VALUES ('730', '75', 'R 500');
INSERT INTO `modelo` VALUES ('731', '75', 'R 500 L');
INSERT INTO `modelo` VALUES ('732', '75', 'R 63 AMG');
INSERT INTO `modelo` VALUES ('733', '75', 'R 63 L AMG');
INSERT INTO `modelo` VALUES ('734', '75', 'S 280');
INSERT INTO `modelo` VALUES ('735', '75', 'S 320');
INSERT INTO `modelo` VALUES ('736', '75', 'S 420');
INSERT INTO `modelo` VALUES ('737', '75', 'S 500');
INSERT INTO `modelo` VALUES ('738', '75', 'SPRINTER');
INSERT INTO `modelo` VALUES ('739', '75', '170 D');
INSERT INTO `modelo` VALUES ('740', '75', '280 CE');
INSERT INTO `modelo` VALUES ('741', '75', '280 E');
INSERT INTO `modelo` VALUES ('742', '75', '280 S');
INSERT INTO `modelo` VALUES ('743', '75', '280 SE');
INSERT INTO `modelo` VALUES ('744', '75', '280 SEL');
INSERT INTO `modelo` VALUES ('745', '75', '280 SL');
INSERT INTO `modelo` VALUES ('746', '75', '280 TE SW');
INSERT INTO `modelo` VALUES ('747', '75', '300 CE');
INSERT INTO `modelo` VALUES ('748', '75', '300 D');
INSERT INTO `modelo` VALUES ('749', '75', '300 E');
INSERT INTO `modelo` VALUES ('750', '75', '300 SE');
INSERT INTO `modelo` VALUES ('751', '75', '300 SEL');
INSERT INTO `modelo` VALUES ('752', '75', '300 SL');
INSERT INTO `modelo` VALUES ('753', '75', '300 TD');
INSERT INTO `modelo` VALUES ('754', '75', '300 TE');
INSERT INTO `modelo` VALUES ('755', '75', '280 C');
INSERT INTO `modelo` VALUES ('756', '75', '260 E');
INSERT INTO `modelo` VALUES ('757', '75', '180 D');
INSERT INTO `modelo` VALUES ('758', '75', '190 D');
INSERT INTO `modelo` VALUES ('759', '75', '190 E');
INSERT INTO `modelo` VALUES ('760', '75', '200 B');
INSERT INTO `modelo` VALUES ('761', '75', '200 D');
INSERT INTO `modelo` VALUES ('762', '75', '220 C');
INSERT INTO `modelo` VALUES ('763', '75', '220 D');
INSERT INTO `modelo` VALUES ('764', '75', '220 E');
INSERT INTO `modelo` VALUES ('765', '75', '220 S');
INSERT INTO `modelo` VALUES ('766', '75', '230 E');
INSERT INTO `modelo` VALUES ('767', '75', '230 S');
INSERT INTO `modelo` VALUES ('768', '75', '230.4');
INSERT INTO `modelo` VALUES ('769', '75', '230.6');
INSERT INTO `modelo` VALUES ('770', '75', '240 D');
INSERT INTO `modelo` VALUES ('771', '75', '250 CE');
INSERT INTO `modelo` VALUES ('772', '75', '313');
INSERT INTO `modelo` VALUES ('773', '75', '320 E');
INSERT INTO `modelo` VALUES ('774', '75', 'CL 600');
INSERT INTO `modelo` VALUES ('775', '75', 'C 180 K');
INSERT INTO `modelo` VALUES ('776', '75', 'C 200');
INSERT INTO `modelo` VALUES ('777', '75', 'C 200 K');
INSERT INTO `modelo` VALUES ('778', '75', 'C 220');
INSERT INTO `modelo` VALUES ('779', '75', 'C 230');
INSERT INTO `modelo` VALUES ('780', '75', 'C 240');
INSERT INTO `modelo` VALUES ('781', '75', 'C 280');
INSERT INTO `modelo` VALUES ('782', '75', 'C 32 AMG');
INSERT INTO `modelo` VALUES ('783', '75', 'C 320');
INSERT INTO `modelo` VALUES ('784', '75', 'C 350');
INSERT INTO `modelo` VALUES ('785', '75', 'C 36 AMG');
INSERT INTO `modelo` VALUES ('786', '75', 'C 43 AMG');
INSERT INTO `modelo` VALUES ('787', '75', 'C 55 AMG');
INSERT INTO `modelo` VALUES ('788', '75', 'C 55 TAMG');
INSERT INTO `modelo` VALUES ('789', '75', 'CL 500');
INSERT INTO `modelo` VALUES ('790', '75', 'C 180');
INSERT INTO `modelo` VALUES ('791', '75', 'B 200');
INSERT INTO `modelo` VALUES ('792', '75', '320 SL');
INSERT INTO `modelo` VALUES ('793', '75', '350 SE');
INSERT INTO `modelo` VALUES ('794', '75', '350 SEL');
INSERT INTO `modelo` VALUES ('795', '75', '350 SL');
INSERT INTO `modelo` VALUES ('796', '75', '450 SE');
INSERT INTO `modelo` VALUES ('797', '75', '450 SEL');
INSERT INTO `modelo` VALUES ('798', '75', '450 SL');
INSERT INTO `modelo` VALUES ('799', '75', '450 SLC');
INSERT INTO `modelo` VALUES ('800', '75', '500 E');
INSERT INTO `modelo` VALUES ('801', '75', '600 SL');
INSERT INTO `modelo` VALUES ('802', '75', '560 SEL');
INSERT INTO `modelo` VALUES ('803', '75', '560 SEC');
INSERT INTO `modelo` VALUES ('804', '75', '500 SL');
INSERT INTO `modelo` VALUES ('805', '75', '500 SEL');
INSERT INTO `modelo` VALUES ('806', '75', '500 SEC');
INSERT INTO `modelo` VALUES ('807', '75', '500 SE');
INSERT INTO `modelo` VALUES ('808', '76', 'VILLAGER');
INSERT INTO `modelo` VALUES ('809', '76', 'SABLE');
INSERT INTO `modelo` VALUES ('810', '76', 'MYSTIQUE');
INSERT INTO `modelo` VALUES ('811', '76', 'COUGAR');
INSERT INTO `modelo` VALUES ('812', '77', 'MGB');
INSERT INTO `modelo` VALUES ('813', '77', 'TC');
INSERT INTO `modelo` VALUES ('814', '78', 'MORRIS');
INSERT INTO `modelo` VALUES ('815', '78', 'COOPER');
INSERT INTO `modelo` VALUES ('816', '79', 'X8');
INSERT INTO `modelo` VALUES ('817', '79', 'TOP SPORT');
INSERT INTO `modelo` VALUES ('818', '79', 'TARGA');
INSERT INTO `modelo` VALUES ('819', '79', 'SPORT');
INSERT INTO `modelo` VALUES ('820', '79', 'SAGA');
INSERT INTO `modelo` VALUES ('821', '79', 'MTS');
INSERT INTO `modelo` VALUES ('822', '79', 'CABRIOLET');
INSERT INTO `modelo` VALUES ('823', '79', 'BG');
INSERT INTO `modelo` VALUES ('824', '80', 'MINI');
INSERT INTO `modelo` VALUES ('825', '81', 'CLASSICO');
INSERT INTO `modelo` VALUES ('826', '81', 'TI');
INSERT INTO `modelo` VALUES ('827', '82', 'NX');
INSERT INTO `modelo` VALUES ('828', '82', 'PATHFINDER');
INSERT INTO `modelo` VALUES ('829', '82', 'PICK-UP');
INSERT INTO `modelo` VALUES ('830', '82', 'PRIMERA');
INSERT INTO `modelo` VALUES ('831', '82', 'QUEST');
INSERT INTO `modelo` VALUES ('832', '82', 'SENTRA');
INSERT INTO `modelo` VALUES ('833', '82', 'STANZA');
INSERT INTO `modelo` VALUES ('834', '82', 'TERRANO');
INSERT INTO `modelo` VALUES ('835', '82', 'TIIDA');
INSERT INTO `modelo` VALUES ('836', '82', 'VAN');
INSERT INTO `modelo` VALUES ('837', '82', 'X-TRAIL');
INSERT INTO `modelo` VALUES ('838', '82', 'XTERRA');
INSERT INTO `modelo` VALUES ('839', '82', 'MURANO');
INSERT INTO `modelo` VALUES ('840', '82', 'MICRA');
INSERT INTO `modelo` VALUES ('841', '82', 'MAXIMA');
INSERT INTO `modelo` VALUES ('842', '82', '240SX');
INSERT INTO `modelo` VALUES ('843', '82', '300 ZX');
INSERT INTO `modelo` VALUES ('844', '82', '350Z');
INSERT INTO `modelo` VALUES ('845', '82', 'ALTIMA');
INSERT INTO `modelo` VALUES ('846', '82', 'ARMADA');
INSERT INTO `modelo` VALUES ('847', '82', 'AX');
INSERT INTO `modelo` VALUES ('848', '82', 'D');
INSERT INTO `modelo` VALUES ('849', '82', 'D21');
INSERT INTO `modelo` VALUES ('850', '82', 'FRONTIER');
INSERT INTO `modelo` VALUES ('851', '82', 'INFINITI');
INSERT INTO `modelo` VALUES ('852', '82', 'J30');
INSERT INTO `modelo` VALUES ('853', '82', 'KING CAB');
INSERT INTO `modelo` VALUES ('854', '83', 'F-85 CUTLASS');
INSERT INTO `modelo` VALUES ('855', '83', 'ACHIEVA');
INSERT INTO `modelo` VALUES ('856', '84', 'RECORD');
INSERT INTO `modelo` VALUES ('857', '85', 'GRAND VOYAGER');
INSERT INTO `modelo` VALUES ('858', '86', 'TRANS SPORT');
INSERT INTO `modelo` VALUES ('859', '86', 'SOLSTICE');
INSERT INTO `modelo` VALUES ('860', '86', 'GTO');
INSERT INTO `modelo` VALUES ('861', '86', 'FORMULA 402');
INSERT INTO `modelo` VALUES ('862', '86', 'FORMULA 401');
INSERT INTO `modelo` VALUES ('863', '86', 'FORMULA 400');
INSERT INTO `modelo` VALUES ('864', '86', 'FIREBIRD');
INSERT INTO `modelo` VALUES ('865', '86', 'FIERO');
INSERT INTO `modelo` VALUES ('866', '87', '968');
INSERT INTO `modelo` VALUES ('867', '87', 'BOXSTER');
INSERT INTO `modelo` VALUES ('868', '87', 'BOXSTER S');
INSERT INTO `modelo` VALUES ('869', '87', 'CARRERA');
INSERT INTO `modelo` VALUES ('870', '87', 'CAYENNE');
INSERT INTO `modelo` VALUES ('871', '87', 'CAYMAN');
INSERT INTO `modelo` VALUES ('872', '87', 'CAYMAN S');
INSERT INTO `modelo` VALUES ('873', '87', 'KREMER');
INSERT INTO `modelo` VALUES ('874', '87', 'SPYDER 500');
INSERT INTO `modelo` VALUES ('875', '87', 'SPYDER 550');
INSERT INTO `modelo` VALUES ('876', '87', '944');
INSERT INTO `modelo` VALUES ('877', '87', '928');
INSERT INTO `modelo` VALUES ('878', '87', '911T');
INSERT INTO `modelo` VALUES ('879', '87', '911');
INSERT INTO `modelo` VALUES ('880', '87', '914');
INSERT INTO `modelo` VALUES ('881', '88', 'GTC');
INSERT INTO `modelo` VALUES ('882', '88', 'P-018S');
INSERT INTO `modelo` VALUES ('883', '88', 'GTS');
INSERT INTO `modelo` VALUES ('884', '88', 'GTE');
INSERT INTO `modelo` VALUES ('885', '88', 'GTB/S2');
INSERT INTO `modelo` VALUES ('886', '88', 'GTB');
INSERT INTO `modelo` VALUES ('887', '88', 'GT');
INSERT INTO `modelo` VALUES ('888', '88', 'AMV');
INSERT INTO `modelo` VALUES ('889', '89', 'SILVER SPUR');
INSERT INTO `modelo` VALUES ('890', '89', 'SILVER SHADOW');
INSERT INTO `modelo` VALUES ('891', '90', 'ROMI-ISETTA');
INSERT INTO `modelo` VALUES ('892', '91', 'Streetwise');
INSERT INTO `modelo` VALUES ('893', '91', 'série 800');
INSERT INTO `modelo` VALUES ('894', '91', 'série 600');
INSERT INTO `modelo` VALUES ('895', '91', 'série 400');
INSERT INTO `modelo` VALUES ('896', '91', 'série 200');
INSERT INTO `modelo` VALUES ('897', '91', 'série 100');
INSERT INTO `modelo` VALUES ('898', '91', '75');
INSERT INTO `modelo` VALUES ('899', '91', '45');
INSERT INTO `modelo` VALUES ('900', '91', '25');
INSERT INTO `modelo` VALUES ('901', '92', '9000');
INSERT INTO `modelo` VALUES ('902', '92', '900');
INSERT INTO `modelo` VALUES ('903', '92', '9-5');
INSERT INTO `modelo` VALUES ('904', '92', '9-3');
INSERT INTO `modelo` VALUES ('905', '93', 'SM 4.1');
INSERT INTO `modelo` VALUES ('906', '94', 'SL-2');
INSERT INTO `modelo` VALUES ('907', '95', 'Toledo');
INSERT INTO `modelo` VALUES ('908', '95', 'Terra');
INSERT INTO `modelo` VALUES ('909', '95', 'Marbella');
INSERT INTO `modelo` VALUES ('910', '95', 'Málaga');
INSERT INTO `modelo` VALUES ('911', '95', 'Leon');
INSERT INTO `modelo` VALUES ('912', '95', 'Inca');
INSERT INTO `modelo` VALUES ('913', '95', 'Ibiza');
INSERT INTO `modelo` VALUES ('914', '95', 'Cordoba');
INSERT INTO `modelo` VALUES ('915', '95', 'Arosa');
INSERT INTO `modelo` VALUES ('916', '95', 'Altea');
INSERT INTO `modelo` VALUES ('917', '95', 'Alhambra');
INSERT INTO `modelo` VALUES ('918', '96', 'MUSTANG');
INSERT INTO `modelo` VALUES ('919', '96', 'COBRA');
INSERT INTO `modelo` VALUES ('920', '97', 'TURBO EX');
INSERT INTO `modelo` VALUES ('921', '98', 'CHAMBORD');
INSERT INTO `modelo` VALUES ('922', '99', 'Roadster Coupé');
INSERT INTO `modelo` VALUES ('923', '99', 'Roadster');
INSERT INTO `modelo` VALUES ('924', '99', 'Fortwo');
INSERT INTO `modelo` VALUES ('925', '99', 'Forfour');
INSERT INTO `modelo` VALUES ('926', '99', 'Crossblade');
INSERT INTO `modelo` VALUES ('927', '100', 'REXTON II');
INSERT INTO `modelo` VALUES ('928', '100', 'REXTON');
INSERT INTO `modelo` VALUES ('929', '100', 'MUSSO');
INSERT INTO `modelo` VALUES ('930', '100', 'KYRON');
INSERT INTO `modelo` VALUES ('931', '100', 'KORANDO');
INSERT INTO `modelo` VALUES ('932', '100', 'ISTANA');
INSERT INTO `modelo` VALUES ('933', '100', 'CHAIRMAN');
INSERT INTO `modelo` VALUES ('934', '100', 'ACTYON SPORTS');
INSERT INTO `modelo` VALUES ('935', '100', 'ACTYON');
INSERT INTO `modelo` VALUES ('936', '101', 'STUDEBAKER PICK-UP');
INSERT INTO `modelo` VALUES ('937', '102', 'VIVIO');
INSERT INTO `modelo` VALUES ('938', '102', 'TRIBECA');
INSERT INTO `modelo` VALUES ('939', '102', 'SVX');
INSERT INTO `modelo` VALUES ('940', '102', 'OUTBACK');
INSERT INTO `modelo` VALUES ('941', '102', 'LEGACY');
INSERT INTO `modelo` VALUES ('942', '102', 'IMPREZA');
INSERT INTO `modelo` VALUES ('943', '102', 'FORESTER');
INSERT INTO `modelo` VALUES ('944', '103', 'Wagon R+');
INSERT INTO `modelo` VALUES ('945', '103', 'Vitara');
INSERT INTO `modelo` VALUES ('946', '103', 'Swift');
INSERT INTO `modelo` VALUES ('947', '103', 'Santana');
INSERT INTO `modelo` VALUES ('948', '103', 'Samurai');
INSERT INTO `modelo` VALUES ('949', '103', 'Liana');
INSERT INTO `modelo` VALUES ('950', '103', 'Jimny');
INSERT INTO `modelo` VALUES ('951', '103', 'Ignis');
INSERT INTO `modelo` VALUES ('952', '103', 'Grand Vitara');
INSERT INTO `modelo` VALUES ('953', '103', 'Baleno');
INSERT INTO `modelo` VALUES ('954', '103', 'Alto');
INSERT INTO `modelo` VALUES ('955', '104', 'T4');
INSERT INTO `modelo` VALUES ('956', '104', 'RF');
INSERT INTO `modelo` VALUES ('957', '104', 'PANTANAL');
INSERT INTO `modelo` VALUES ('958', '105', 'UNIMOG');
INSERT INTO `modelo` VALUES ('959', '106', 'S60');
INSERT INTO `modelo` VALUES ('960', '106', 'S70');
INSERT INTO `modelo` VALUES ('961', '106', 'S80');
INSERT INTO `modelo` VALUES ('962', '106', 'V40');
INSERT INTO `modelo` VALUES ('963', '106', 'V50');
INSERT INTO `modelo` VALUES ('964', '106', 'V70');
INSERT INTO `modelo` VALUES ('965', '106', 'XC70');
INSERT INTO `modelo` VALUES ('966', '106', 'XC90');
INSERT INTO `modelo` VALUES ('967', '106', 'S40');
INSERT INTO `modelo` VALUES ('968', '106', 'C70');
INSERT INTO `modelo` VALUES ('969', '106', '440');
INSERT INTO `modelo` VALUES ('970', '106', '444');
INSERT INTO `modelo` VALUES ('971', '106', '460');
INSERT INTO `modelo` VALUES ('972', '106', '850');
INSERT INTO `modelo` VALUES ('973', '106', '855');
INSERT INTO `modelo` VALUES ('974', '106', '940');
INSERT INTO `modelo` VALUES ('975', '106', '960');
INSERT INTO `modelo` VALUES ('976', '106', 'C30');
INSERT INTO `modelo` VALUES ('977', '107', 'WALKSPORT');
INSERT INTO `modelo` VALUES ('978', '108', 'CJ3A');
INSERT INTO `modelo` VALUES ('979', '108', 'AERO-WILLYS');
INSERT INTO `modelo` VALUES ('980', '108', 'JEEP');
INSERT INTO `modelo` VALUES ('981', '108', 'RURAL');
INSERT INTO `modelo` VALUES ('982', '113', '1200 GOLD WING');
INSERT INTO `modelo` VALUES ('983', '113', '125');
INSERT INTO `modelo` VALUES ('984', '113', '1500 GOLD WING');
INSERT INTO `modelo` VALUES ('985', '113', '750 MAGNA');
INSERT INTO `modelo` VALUES ('986', '113', 'BIZ 125 +');
INSERT INTO `modelo` VALUES ('987', '113', 'BIZ 125 ES');
INSERT INTO `modelo` VALUES ('988', '113', 'BIZ 125 EX');
INSERT INTO `modelo` VALUES ('989', '113', 'BIZ 125 KS');
INSERT INTO `modelo` VALUES ('990', '113', 'C 100 BIZ');
INSERT INTO `modelo` VALUES ('991', '113', 'C 100 BIZ ES');
INSERT INTO `modelo` VALUES ('992', '113', 'C 100 BIZ+');
INSERT INTO `modelo` VALUES ('993', '113', 'C 100 DREAM');
INSERT INTO `modelo` VALUES ('994', '113', 'CB 1000 - BIG ONE');
INSERT INTO `modelo` VALUES ('995', '113', 'CB 1000R');
INSERT INTO `modelo` VALUES ('996', '113', 'CB 1300 SUPER FOUR');
INSERT INTO `modelo` VALUES ('997', '113', 'CB 300R');
INSERT INTO `modelo` VALUES ('998', '113', 'CB 350K');
INSERT INTO `modelo` VALUES ('999', '113', 'CB 360');
INSERT INTO `modelo` VALUES ('1000', '113', 'CB 400');
INSERT INTO `modelo` VALUES ('1001', '113', 'CB 400 FOUR');
INSERT INTO `modelo` VALUES ('1002', '113', 'CB 400 II');
INSERT INTO `modelo` VALUES ('1003', '113', 'CB 450');
INSERT INTO `modelo` VALUES ('1004', '113', 'CB 450 DX');
INSERT INTO `modelo` VALUES ('1005', '113', 'CB 50');
INSERT INTO `modelo` VALUES ('1006', '113', 'CB 500');
INSERT INTO `modelo` VALUES ('1007', '113', 'CB 500 FOUR');
INSERT INTO `modelo` VALUES ('1008', '113', 'CB 550 FOUR');
INSERT INTO `modelo` VALUES ('1009', '113', 'CB 600F HORNET');
INSERT INTO `modelo` VALUES ('1010', '113', 'CB 750 FOUR');
INSERT INTO `modelo` VALUES ('1011', '113', 'CB 750 KZ');
INSERT INTO `modelo` VALUES ('1012', '113', 'CB 900F BOLD&acute;OR');
INSERT INTO `modelo` VALUES ('1013', '113', 'CB 900F HORNET');
INSERT INTO `modelo` VALUES ('1014', '113', 'CBF 125');
INSERT INTO `modelo` VALUES ('1015', '113', 'CBR 1000F');
INSERT INTO `modelo` VALUES ('1016', '113', 'CBR 1000RR FIREBLADE');
INSERT INTO `modelo` VALUES ('1017', '113', 'CBR 1000RR REPSOL');
INSERT INTO `modelo` VALUES ('1018', '113', 'CBR 1100XX SUPER BLACKBIR');
INSERT INTO `modelo` VALUES ('1019', '113', 'CBR 125R');
INSERT INTO `modelo` VALUES ('1020', '113', 'CBR 450');
INSERT INTO `modelo` VALUES ('1021', '113', 'CBR 450 SR');
INSERT INTO `modelo` VALUES ('1022', '113', 'CBR 600F');
INSERT INTO `modelo` VALUES ('1023', '113', 'CBR 600RR');
INSERT INTO `modelo` VALUES ('1024', '113', 'CBR 900RR');
INSERT INTO `modelo` VALUES ('1025', '113', 'CBR 900RR FIREBLADE');
INSERT INTO `modelo` VALUES ('1026', '113', 'CBR 929RR');
INSERT INTO `modelo` VALUES ('1027', '113', 'CBR 954RR FIREBLADE');
INSERT INTO `modelo` VALUES ('1028', '113', 'CBX 1050');
INSERT INTO `modelo` VALUES ('1029', '113', 'CBX 150 AERO');
INSERT INTO `modelo` VALUES ('1030', '113', 'CBX 200 STRADA');
INSERT INTO `modelo` VALUES ('1031', '113', 'CBX 250 TWISTER');
INSERT INTO `modelo` VALUES ('1032', '113', 'CBX 750 FOUR');
INSERT INTO `modelo` VALUES ('1033', '113', 'CBX 750 FOUR INDY');
INSERT INTO `modelo` VALUES ('1034', '113', 'CBX 750R');
INSERT INTO `modelo` VALUES ('1035', '113', 'CG 125');
INSERT INTO `modelo` VALUES ('1036', '113', 'CG 125 CARGO');
INSERT INTO `modelo` VALUES ('1037', '113', 'CG 125 CARGO ES');
INSERT INTO `modelo` VALUES ('1038', '113', 'CG 125 CARGO KS');
INSERT INTO `modelo` VALUES ('1039', '113', 'CG 125 ES FAN');
INSERT INTO `modelo` VALUES ('1040', '113', 'CG 125 FAN');
INSERT INTO `modelo` VALUES ('1041', '113', 'CG 125 KS FAN');
INSERT INTO `modelo` VALUES ('1042', '113', 'CG 125 ML');
INSERT INTO `modelo` VALUES ('1043', '113', 'CG 125 TITAN');
INSERT INTO `modelo` VALUES ('1044', '113', 'CG 125 TITAN ES');
INSERT INTO `modelo` VALUES ('1045', '113', 'CG 125 TITAN KS');
INSERT INTO `modelo` VALUES ('1046', '113', 'CG 125 TITAN KSE');
INSERT INTO `modelo` VALUES ('1047', '113', 'CG 125 TODAY');
INSERT INTO `modelo` VALUES ('1048', '113', 'CG 150 FAN ESDI');
INSERT INTO `modelo` VALUES ('1049', '113', 'CG 150 FAN ESI');
INSERT INTO `modelo` VALUES ('1050', '113', 'CG 150 JOB');
INSERT INTO `modelo` VALUES ('1051', '113', 'CG 150 SPORT');
INSERT INTO `modelo` VALUES ('1052', '113', 'CG 150 TITAN EDI&Ccedil;&');
INSERT INTO `modelo` VALUES ('1053', '113', 'CG 150 TITAN ES');
INSERT INTO `modelo` VALUES ('1054', '113', 'CG 150 TITAN ESD');
INSERT INTO `modelo` VALUES ('1055', '113', 'CG 150 TITAN KS');
INSERT INTO `modelo` VALUES ('1056', '113', 'CG 150 TITAN MIX ES');
INSERT INTO `modelo` VALUES ('1057', '113', 'CG 150 TITAN MIX ESD');
INSERT INTO `modelo` VALUES ('1058', '113', 'CG 150 TITAN MIX EX');
INSERT INTO `modelo` VALUES ('1059', '113', 'CG 150 TITAN MIX KS');
INSERT INTO `modelo` VALUES ('1060', '113', 'CH 125R SPACY');
INSERT INTO `modelo` VALUES ('1061', '113', 'CR 125');
INSERT INTO `modelo` VALUES ('1062', '113', 'CR 250');
INSERT INTO `modelo` VALUES ('1063', '113', 'CR 80');
INSERT INTO `modelo` VALUES ('1064', '113', 'CRF 150R');
INSERT INTO `modelo` VALUES ('1065', '113', 'CRF 230F');
INSERT INTO `modelo` VALUES ('1066', '113', 'CRF 250R');
INSERT INTO `modelo` VALUES ('1067', '113', 'CRF 250X');
INSERT INTO `modelo` VALUES ('1068', '113', 'CRF 450R');
INSERT INTO `modelo` VALUES ('1069', '113', 'CRF 450X');
INSERT INTO `modelo` VALUES ('1070', '113', 'CRF 50F');
INSERT INTO `modelo` VALUES ('1071', '113', 'GL 1000 GOLD WING');
INSERT INTO `modelo` VALUES ('1072', '113', 'GL 1500CF VALKYRIE INTERS');
INSERT INTO `modelo` VALUES ('1073', '113', 'GL 1800 GOLD WING');
INSERT INTO `modelo` VALUES ('1074', '113', 'HELIX 250');
INSERT INTO `modelo` VALUES ('1075', '113', 'INTERCEPTOR V-FOUR');
INSERT INTO `modelo` VALUES ('1076', '113', 'LEAD 110');
INSERT INTO `modelo` VALUES ('1077', '113', 'LEAD 110 ES');
INSERT INTO `modelo` VALUES ('1078', '113', 'NX 150');
INSERT INTO `modelo` VALUES ('1079', '113', 'NX 200');
INSERT INTO `modelo` VALUES ('1080', '113', 'NX 350 SAHARA');
INSERT INTO `modelo` VALUES ('1081', '113', 'NX 650 DOMINATOR');
INSERT INTO `modelo` VALUES ('1082', '113', 'NX4 FALCON');
INSERT INTO `modelo` VALUES ('1083', '113', 'NXR 125 BROS ES');
INSERT INTO `modelo` VALUES ('1084', '113', 'NXR 125 BROS KS');
INSERT INTO `modelo` VALUES ('1085', '113', 'NXR 150 BROS ES');
INSERT INTO `modelo` VALUES ('1086', '113', 'NXR 150 BROS ESD');
INSERT INTO `modelo` VALUES ('1087', '113', 'NXR 150 BROS KS');
INSERT INTO `modelo` VALUES ('1088', '113', 'NXR 150 BROS MIX ES');
INSERT INTO `modelo` VALUES ('1089', '113', 'NXR 150 BROS MIX ESD');
INSERT INTO `modelo` VALUES ('1090', '113', 'NXR 150 BROS MIX KS');
INSERT INTO `modelo` VALUES ('1091', '113', 'POP 100');
INSERT INTO `modelo` VALUES ('1092', '113', 'SHADOW 1100 AC');
INSERT INTO `modelo` VALUES ('1093', '113', 'SHADOW 1100 OURO');
INSERT INTO `modelo` VALUES ('1094', '113', 'SHADOW 1100 VT');
INSERT INTO `modelo` VALUES ('1095', '113', 'SHADOW 600C VT');
INSERT INTO `modelo` VALUES ('1096', '113', 'SHADOW 750');
INSERT INTO `modelo` VALUES ('1097', '113', 'SHADOW 750 AM');
INSERT INTO `modelo` VALUES ('1098', '113', 'ST 1100 PAN EUROPEAN');
INSERT INTO `modelo` VALUES ('1099', '113', 'SUPER HAWK 1000');
INSERT INTO `modelo` VALUES ('1100', '113', 'TRX 350 FOURTRAX 4X2');
INSERT INTO `modelo` VALUES ('1101', '113', 'TRX 350 FOURTRAX 4X4');
INSERT INTO `modelo` VALUES ('1102', '113', 'TRX 350 FOURTRAX FM 4X4');
INSERT INTO `modelo` VALUES ('1103', '113', 'TRX 420 FOURTRAX 4X2');
INSERT INTO `modelo` VALUES ('1104', '113', 'TRX 420 FOURTRAX 4X4');
INSERT INTO `modelo` VALUES ('1105', '113', 'VALKYRIE 1500');
INSERT INTO `modelo` VALUES ('1106', '113', 'VALKYRIE 1800');
INSERT INTO `modelo` VALUES ('1107', '113', 'VF750F INTERCEPTOR');
INSERT INTO `modelo` VALUES ('1108', '113', 'VFR 1200F');
INSERT INTO `modelo` VALUES ('1109', '113', 'VT 1300 CR');
INSERT INTO `modelo` VALUES ('1110', '113', 'VT 1300 CS');
INSERT INTO `modelo` VALUES ('1111', '113', 'VT 1300 CX');
INSERT INTO `modelo` VALUES ('1112', '113', 'VTR 250');
INSERT INTO `modelo` VALUES ('1113', '113', 'VTX 1800C');
INSERT INTO `modelo` VALUES ('1114', '113', 'XL 1000V VARADERO');
INSERT INTO `modelo` VALUES ('1115', '113', 'XL 125 DUTY');
INSERT INTO `modelo` VALUES ('1116', '113', 'XL 125 VAREDERO');
INSERT INTO `modelo` VALUES ('1117', '113', 'XL 125S');
INSERT INTO `modelo` VALUES ('1118', '113', 'XL 250R');
INSERT INTO `modelo` VALUES ('1119', '113', 'XL 600V TRANSALP');
INSERT INTO `modelo` VALUES ('1120', '113', 'XL 700V TRANSALP');
INSERT INTO `modelo` VALUES ('1121', '113', 'XLR 125');
INSERT INTO `modelo` VALUES ('1122', '113', 'XLR 125 ES');
INSERT INTO `modelo` VALUES ('1123', '113', 'XLX 250R');
INSERT INTO `modelo` VALUES ('1124', '113', 'XLX 350R');
INSERT INTO `modelo` VALUES ('1126', '113', 'XR 250 TORNADO');
INSERT INTO `modelo` VALUES ('1127', '113', 'XR 250R');
INSERT INTO `modelo` VALUES ('1128', '113', 'XR 400R');
INSERT INTO `modelo` VALUES ('1129', '113', 'XR 650');
INSERT INTO `modelo` VALUES ('1130', '113', 'XR 650L');
INSERT INTO `modelo` VALUES ('1131', '113', 'XRE 300');
INSERT INTO `modelo` VALUES ('1132', '113', 'XRV 750 AFRICA TWIN');
INSERT INTO `modelo` VALUES ('1153', '113', 'XR 200R');
INSERT INTO `modelo` VALUES ('1154', '110', 'F 650');
INSERT INTO `modelo` VALUES ('1155', '110', 'F 650 CS');
INSERT INTO `modelo` VALUES ('1156', '110', 'F 650 GS');
INSERT INTO `modelo` VALUES ('1157', '110', 'F 650 GS DAKAR');
INSERT INTO `modelo` VALUES ('1158', '110', 'F 800 GS');
INSERT INTO `modelo` VALUES ('1159', '110', 'F 800 R');
INSERT INTO `modelo` VALUES ('1160', '110', 'F 800 S');
INSERT INTO `modelo` VALUES ('1161', '110', 'G 450 X');
INSERT INTO `modelo` VALUES ('1162', '110', 'G 650 GS');
INSERT INTO `modelo` VALUES ('1163', '110', 'G 650 XCHALLENGE');
INSERT INTO `modelo` VALUES ('1164', '110', 'G 650 XCOUNTRY');
INSERT INTO `modelo` VALUES ('1165', '110', 'G 650 XMOTO');
INSERT INTO `modelo` VALUES ('1166', '110', 'K 100');
INSERT INTO `modelo` VALUES ('1167', '110', 'K 100 RS');
INSERT INTO `modelo` VALUES ('1168', '110', 'K 1100 LT');
INSERT INTO `modelo` VALUES ('1169', '110', 'K 1100 RS');
INSERT INTO `modelo` VALUES ('1170', '110', 'K 1200 GT');
INSERT INTO `modelo` VALUES ('1171', '110', 'K 1200 LT');
INSERT INTO `modelo` VALUES ('1172', '110', 'K 1200 R');
INSERT INTO `modelo` VALUES ('1173', '110', 'K 1200 RS');
INSERT INTO `modelo` VALUES ('1174', '110', 'K 1200 S');
INSERT INTO `modelo` VALUES ('1175', '110', 'K 1300 GT');
INSERT INTO `modelo` VALUES ('1176', '110', 'K 1300 R');
INSERT INTO `modelo` VALUES ('1177', '110', 'K 1300 S');
INSERT INTO `modelo` VALUES ('1178', '110', 'K 1600 GT');
INSERT INTO `modelo` VALUES ('1179', '110', 'K 1600 GTL');
INSERT INTO `modelo` VALUES ('1180', '110', 'K 75');
INSERT INTO `modelo` VALUES ('1181', '110', 'R 100 RS');
INSERT INTO `modelo` VALUES ('1182', '110', 'R 1100 GS');
INSERT INTO `modelo` VALUES ('1183', '110', 'R 1100 GS 75');
INSERT INTO `modelo` VALUES ('1184', '110', 'R 1100 R');
INSERT INTO `modelo` VALUES ('1185', '110', 'R 1100 RS');
INSERT INTO `modelo` VALUES ('1186', '110', 'R 1100 RT');
INSERT INTO `modelo` VALUES ('1187', '110', 'R 1100 S');
INSERT INTO `modelo` VALUES ('1188', '110', 'R 1150 GS');
INSERT INTO `modelo` VALUES ('1189', '110', 'R 1150 GS ADVENTURE');
INSERT INTO `modelo` VALUES ('1190', '110', 'R 1150 R');
INSERT INTO `modelo` VALUES ('1191', '110', 'R 1150 R ROCKSTER');
INSERT INTO `modelo` VALUES ('1192', '110', 'R 1150 RS');
INSERT INTO `modelo` VALUES ('1193', '110', 'R 1150 RT');
INSERT INTO `modelo` VALUES ('1194', '110', 'R 1200 C');
INSERT INTO `modelo` VALUES ('1195', '110', 'R 1200 C AVANTGARD');
INSERT INTO `modelo` VALUES ('1196', '110', 'R 1200 C CLASSIC');
INSERT INTO `modelo` VALUES ('1197', '110', 'R 1200 C INDEPENDENT');
INSERT INTO `modelo` VALUES ('1198', '110', 'R 1200 CL');
INSERT INTO `modelo` VALUES ('1199', '110', 'R 1200 GS');
INSERT INTO `modelo` VALUES ('1200', '110', 'R 1200 GS ADVENTURE');
INSERT INTO `modelo` VALUES ('1201', '110', 'R 1200 GS HP2');
INSERT INTO `modelo` VALUES ('1202', '110', 'R 1200 RT');
INSERT INTO `modelo` VALUES ('1203', '110', 'R90S');
INSERT INTO `modelo` VALUES ('1204', '110', 'S 1000 RR');
INSERT INTO `modelo` VALUES ('1205', '111', '749');
INSERT INTO `modelo` VALUES ('1206', '111', '848');
INSERT INTO `modelo` VALUES ('1207', '111', '916');
INSERT INTO `modelo` VALUES ('1208', '111', '996');
INSERT INTO `modelo` VALUES ('1209', '111', '996 S');
INSERT INTO `modelo` VALUES ('1210', '111', '998');
INSERT INTO `modelo` VALUES ('1211', '111', '999');
INSERT INTO `modelo` VALUES ('1212', '111', '999 R XEROX');
INSERT INTO `modelo` VALUES ('1213', '111', 'D16 RR');
INSERT INTO `modelo` VALUES ('1214', '111', 'HIPERMOTARD 1100 EVO');
INSERT INTO `modelo` VALUES ('1215', '111', 'HYPERMOTARD 1100');
INSERT INTO `modelo` VALUES ('1216', '111', 'HYPERMOTARD 796');
INSERT INTO `modelo` VALUES ('1217', '111', 'MONSTER 1100');
INSERT INTO `modelo` VALUES ('1218', '111', 'MONSTER 1100S');
INSERT INTO `modelo` VALUES ('1219', '111', 'MONSTER 600');
INSERT INTO `modelo` VALUES ('1220', '111', 'MONSTER 620');
INSERT INTO `modelo` VALUES ('1221', '111', 'MONSTER 695');
INSERT INTO `modelo` VALUES ('1222', '111', 'MONSTER 696');
INSERT INTO `modelo` VALUES ('1223', '111', 'MONSTER 900');
INSERT INTO `modelo` VALUES ('1224', '111', 'MONSTER S2-R 1000');
INSERT INTO `modelo` VALUES ('1225', '111', 'MONSTER S4 916');
INSERT INTO `modelo` VALUES ('1226', '111', 'MONSTER S4 R 996');
INSERT INTO `modelo` VALUES ('1227', '111', 'MONSTER S4 RS TESTASTRET');
INSERT INTO `modelo` VALUES ('1228', '111', 'MULTISTRADA 1000');
INSERT INTO `modelo` VALUES ('1229', '111', 'MULTISTRADA 1100');
INSERT INTO `modelo` VALUES ('1230', '111', 'MULTISTRADA 1200');
INSERT INTO `modelo` VALUES ('1231', '111', 'MULTISTRADA 1200S');
INSERT INTO `modelo` VALUES ('1232', '111', 'MULTISTRADA 620');
INSERT INTO `modelo` VALUES ('1233', '111', 'SPORT CLASSIC 1000');
INSERT INTO `modelo` VALUES ('1234', '111', 'SPORTCLASSIC PAUL SMART');
INSERT INTO `modelo` VALUES ('1235', '111', 'SS 900');
INSERT INTO `modelo` VALUES ('1236', '111', 'ST 2 900');
INSERT INTO `modelo` VALUES ('1237', '111', 'ST 4 900');
INSERT INTO `modelo` VALUES ('1238', '111', 'STONER');
INSERT INTO `modelo` VALUES ('1239', '111', 'STREETFIGHTER');
INSERT INTO `modelo` VALUES ('1240', '111', 'STREETFIGHTER S');
INSERT INTO `modelo` VALUES ('1241', '111', 'SUPERBIKE 1098');
INSERT INTO `modelo` VALUES ('1242', '111', 'SUPERBIKE 1098 DUPLICADO');
INSERT INTO `modelo` VALUES ('1243', '111', 'SUPERBIKE 1098 S');
INSERT INTO `modelo` VALUES ('1244', '111', 'SUPERBIKE 1198');
INSERT INTO `modelo` VALUES ('1245', '111', 'SUPERBIKE 1198 S');
INSERT INTO `modelo` VALUES ('1246', '111', 'SUPERBIKE 848');
INSERT INTO `modelo` VALUES ('1247', '112', 'DYNA CONVERTIBLE');
INSERT INTO `modelo` VALUES ('1248', '112', 'DYNA LOW RIDER');
INSERT INTO `modelo` VALUES ('1249', '112', 'DYNA LOW RIDER CONVERTIB');
INSERT INTO `modelo` VALUES ('1250', '112', 'DYNA SUPER GLIDE');
INSERT INTO `modelo` VALUES ('1251', '112', 'DYNA SUPER GLIDE CUSTOM');
INSERT INTO `modelo` VALUES ('1252', '112', 'DYNA WIDE GLIDE');
INSERT INTO `modelo` VALUES ('1253', '112', 'EG ROAD KING FUEL INJECT');
INSERT INTO `modelo` VALUES ('1254', '112', 'ELECTRA GLIDE SCREAMIN´');
INSERT INTO `modelo` VALUES ('1255', '112', 'ELECTRA GLIDE CLASSIC');
INSERT INTO `modelo` VALUES ('1256', '112', 'ELECTRA GLIDE SPECIAL');
INSERT INTO `modelo` VALUES ('1257', '112', 'ELECTRA GLIDE STANDARD');
INSERT INTO `modelo` VALUES ('1258', '112', 'ELECTRA GLIDE ULTRA FUEL');
INSERT INTO `modelo` VALUES ('1259', '112', 'FAT BOY SCREAMIN EAGLE');
INSERT INTO `modelo` VALUES ('1260', '112', 'FLSTF FB');
INSERT INTO `modelo` VALUES ('1261', '112', 'FX');
INSERT INTO `modelo` VALUES ('1262', '112', 'HERITAGE SOFTAIL CLASSIC');
INSERT INTO `modelo` VALUES ('1263', '112', 'HERITAGE SOFTAIL CUSTOM');
INSERT INTO `modelo` VALUES ('1264', '112', 'HERITAGE SOFTAIL SPRINGE');
INSERT INTO `modelo` VALUES ('1265', '112', 'NIGHT ROD SPECIAL');
INSERT INTO `modelo` VALUES ('1266', '112', 'NIGHT TRAIN');
INSERT INTO `modelo` VALUES ('1267', '112', 'ROAD KING');
INSERT INTO `modelo` VALUES ('1268', '112', 'ROAD KING CLASSIC');
INSERT INTO `modelo` VALUES ('1269', '112', 'ROAD KING CUSTOM');
INSERT INTO `modelo` VALUES ('1270', '112', 'ROAD KING POLICE FLHP');
INSERT INTO `modelo` VALUES ('1271', '112', 'ROAD KING SCREAMIN EAGLE');
INSERT INTO `modelo` VALUES ('1272', '112', 'ROCKER C');
INSERT INTO `modelo` VALUES ('1273', '112', 'SCREAMING EAGLE SOFTAIL');
INSERT INTO `modelo` VALUES ('1274', '112', 'SHOVELHEAD');
INSERT INTO `modelo` VALUES ('1275', '112', 'SOFTAIL CLASSIC');
INSERT INTO `modelo` VALUES ('1276', '112', 'SOFTAIL COVERTIBLE');
INSERT INTO `modelo` VALUES ('1277', '112', 'SOFTAIL CUSTOM');
INSERT INTO `modelo` VALUES ('1278', '112', 'SOFTAIL DELUXE');
INSERT INTO `modelo` VALUES ('1279', '112', 'SOFTAIL DEUCE');
INSERT INTO `modelo` VALUES ('1280', '112', 'SOFTAIL FAT BOY');
INSERT INTO `modelo` VALUES ('1281', '112', 'SOFTAIL FAT BOY SPECIAL');
INSERT INTO `modelo` VALUES ('1282', '112', 'SOFTAIL HERITAGE CUSTOM');
INSERT INTO `modelo` VALUES ('1283', '112', 'SOFTAIL ROCKER');
INSERT INTO `modelo` VALUES ('1284', '112', 'SOFTAIL SPECIAL');
INSERT INTO `modelo` VALUES ('1285', '112', 'SOFTAIL SPRINGER');
INSERT INTO `modelo` VALUES ('1286', '112', 'SOFTAIL STANDARD');
INSERT INTO `modelo` VALUES ('1287', '112', 'SPORTSTER 1200 XL CUSTOM');
INSERT INTO `modelo` VALUES ('1288', '112', 'SPORTSTER 1200 XL CUSTOM');
INSERT INTO `modelo` VALUES ('1289', '112', 'SPORTSTER 1200 XL SPORT');
INSERT INTO `modelo` VALUES ('1290', '112', 'SPORTSTER 1200 XL STANDA');
INSERT INTO `modelo` VALUES ('1291', '112', 'SPORTSTER 1200 XLH');
INSERT INTO `modelo` VALUES ('1292', '112', 'SPORTSTER 883 LOW');
INSERT INTO `modelo` VALUES ('1293', '112', 'SPORTSTER 883 XL STANDAR');
INSERT INTO `modelo` VALUES ('1294', '112', 'SPORTSTER 883 XLH HUGGER');
INSERT INTO `modelo` VALUES ('1295', '112', 'SPORTSTER XL 1200 N');
INSERT INTO `modelo` VALUES ('1296', '112', 'SPORTSTER XL 883');
INSERT INTO `modelo` VALUES ('1297', '112', 'SPORTSTER XL 883 CUSTOM');
INSERT INTO `modelo` VALUES ('1298', '112', 'SPORTSTER XL 883 R');
INSERT INTO `modelo` VALUES ('1299', '112', 'SPORTSTER XL 883N IRON');
INSERT INTO `modelo` VALUES ('1300', '112', 'SPOTSTER 883 ROADSTER');
INSERT INTO `modelo` VALUES ('1301', '112', 'SPOTSTER IRON 883');
INSERT INTO `modelo` VALUES ('1302', '112', 'SPRINGER SCREAMIN EAGLE');
INSERT INTO `modelo` VALUES ('1303', '112', 'STREET GLIDE');
INSERT INTO `modelo` VALUES ('1304', '112', 'STREET ROD');
INSERT INTO `modelo` VALUES ('1305', '112', 'U FLAT HEAD');
INSERT INTO `modelo` VALUES ('1306', '112', 'ULTRA ELECTRA GLIDE CLAS');
INSERT INTO `modelo` VALUES ('1307', '112', 'V-ROD');
INSERT INTO `modelo` VALUES ('1308', '112', 'V-ROD MUSCLE');
INSERT INTO `modelo` VALUES ('1309', '112', 'V-ROD SCREAMIN EAGLE');
INSERT INTO `modelo` VALUES ('1310', '112', 'V-ROD SPORT');
INSERT INTO `modelo` VALUES ('1311', '112', 'V-ROD X');
INSERT INTO `modelo` VALUES ('1312', '112', 'XR 1200');
INSERT INTO `modelo` VALUES ('1313', '112', 'XR 1200 X');
INSERT INTO `modelo` VALUES ('1314', '109', 'CITY 50');
INSERT INTO `modelo` VALUES ('1315', '109', 'CITY 90');
INSERT INTO `modelo` VALUES ('1316', '109', 'DAKAR 30.0');
INSERT INTO `modelo` VALUES ('1317', '109', 'ELEFANT 16.5');
INSERT INTO `modelo` VALUES ('1318', '109', 'ELEFANT 27.5');
INSERT INTO `modelo` VALUES ('1319', '109', 'ELEFANTRÉ 16.5');
INSERT INTO `modelo` VALUES ('1320', '109', 'ELEFANTRÉ 16.5 ES');
INSERT INTO `modelo` VALUES ('1321', '109', 'ELEFANTRÉ 30.0');
INSERT INTO `modelo` VALUES ('1322', '109', 'ELEFANTRÉ 30.0 ES');
INSERT INTO `modelo` VALUES ('1323', '109', 'EX 27.5');
INSERT INTO `modelo` VALUES ('1324', '109', 'SST 13.5');
INSERT INTO `modelo` VALUES ('1325', '109', 'SUPER CITY 125');
INSERT INTO `modelo` VALUES ('1326', '109', 'SXT 16.5');
INSERT INTO `modelo` VALUES ('1327', '109', 'SXT 27.5 E');
INSERT INTO `modelo` VALUES ('1328', '109', 'SXT 27.5 EX');
INSERT INTO `modelo` VALUES ('1329', '109', 'TCHAU 50');
INSERT INTO `modelo` VALUES ('1330', '114', 'COMET 150');
INSERT INTO `modelo` VALUES ('1331', '114', 'COMET 250');
INSERT INTO `modelo` VALUES ('1332', '114', 'COMET 650R');
INSERT INTO `modelo` VALUES ('1333', '114', 'COMET 650R EFI');
INSERT INTO `modelo` VALUES ('1334', '114', 'COMET EFI');
INSERT INTO `modelo` VALUES ('1335', '114', 'COMET GT 250 EFI');
INSERT INTO `modelo` VALUES ('1336', '114', 'COMET GT 250 R');
INSERT INTO `modelo` VALUES ('1337', '114', 'COMET GT 250R');
INSERT INTO `modelo` VALUES ('1338', '114', 'COMET GT 650R');
INSERT INTO `modelo` VALUES ('1339', '114', 'COMET GT-R');
INSERT INTO `modelo` VALUES ('1340', '114', 'COMET GTR EFI');
INSERT INTO `modelo` VALUES ('1341', '114', 'COMET PHASE 2');
INSERT INTO `modelo` VALUES ('1342', '114', 'CRUISE 125');
INSERT INTO `modelo` VALUES ('1343', '114', 'CRUISER II 125 CLASSIC');
INSERT INTO `modelo` VALUES ('1344', '114', 'CRZ 150');
INSERT INTO `modelo` VALUES ('1345', '114', 'CRZ 150 SM');
INSERT INTO `modelo` VALUES ('1346', '114', 'ERO PLUS 125');
INSERT INTO `modelo` VALUES ('1347', '114', 'FLASH 150');
INSERT INTO `modelo` VALUES ('1348', '114', 'FX 110 MIDAS');
INSERT INTO `modelo` VALUES ('1349', '114', 'GF 125 SPEED');
INSERT INTO `modelo` VALUES ('1350', '114', 'MAGIK 125');
INSERT INTO `modelo` VALUES ('1351', '114', 'MIRAGE 150');
INSERT INTO `modelo` VALUES ('1352', '114', 'MIRAGE 250 EFI');
INSERT INTO `modelo` VALUES ('1353', '114', 'MIRAGE 650');
INSERT INTO `modelo` VALUES ('1354', '114', 'MIRAGE 650 EFI');
INSERT INTO `modelo` VALUES ('1355', '114', 'MIRAGE POWER');
INSERT INTO `modelo` VALUES ('1356', '114', 'MIRAGE PREMIER');
INSERT INTO `modelo` VALUES ('1357', '114', 'MOTOKAR');
INSERT INTO `modelo` VALUES ('1358', '114', 'PRIMA 150');
INSERT INTO `modelo` VALUES ('1359', '114', 'PRIMA 150 GTS');
INSERT INTO `modelo` VALUES ('1360', '114', 'PRIMA 50');
INSERT INTO `modelo` VALUES ('1361', '114', 'PRIMA ELECTRA');
INSERT INTO `modelo` VALUES ('1362', '114', 'PRIMA RALLY 50');
INSERT INTO `modelo` VALUES ('1363', '114', 'RX 125');
INSERT INTO `modelo` VALUES ('1364', '114', 'SETA 125');
INSERT INTO `modelo` VALUES ('1365', '114', 'SETA 150');
INSERT INTO `modelo` VALUES ('1366', '114', 'SOFT 50');
INSERT INTO `modelo` VALUES ('1367', '114', 'SUPER CAB 100');
INSERT INTO `modelo` VALUES ('1368', '114', 'SUPER CAB 50');
INSERT INTO `modelo` VALUES ('1369', '114', 'TN 125');
INSERT INTO `modelo` VALUES ('1370', '114', 'WAY 125');
INSERT INTO `modelo` VALUES ('1371', '114', 'WIN 110');
INSERT INTO `modelo` VALUES ('1372', '115', 'Versys');
INSERT INTO `modelo` VALUES ('1373', '115', 'AVAJET 100');
INSERT INTO `modelo` VALUES ('1374', '115', 'CLASSIC 100');
INSERT INTO `modelo` VALUES ('1375', '115', 'CONCOURS 14');
INSERT INTO `modelo` VALUES ('1376', '115', 'D TRACKER 250 X');
INSERT INTO `modelo` VALUES ('1377', '115', 'ER 4N');
INSERT INTO `modelo` VALUES ('1378', '115', 'ER 5');
INSERT INTO `modelo` VALUES ('1379', '115', 'ER-6F');
INSERT INTO `modelo` VALUES ('1380', '115', 'ER-6N');
INSERT INTO `modelo` VALUES ('1381', '115', 'KDX 200');
INSERT INTO `modelo` VALUES ('1382', '115', 'KDX 220 R');
INSERT INTO `modelo` VALUES ('1383', '115', 'KDX 50');
INSERT INTO `modelo` VALUES ('1384', '115', 'KFX 450R');
INSERT INTO `modelo` VALUES ('1385', '115', 'KLR 250');
INSERT INTO `modelo` VALUES ('1386', '115', 'KLR 650');
INSERT INTO `modelo` VALUES ('1387', '115', 'KLX 110');
INSERT INTO `modelo` VALUES ('1388', '115', 'KLX 125');
INSERT INTO `modelo` VALUES ('1389', '115', 'KLX 125 L');
INSERT INTO `modelo` VALUES ('1390', '115', 'KLX 250');
INSERT INTO `modelo` VALUES ('1391', '115', 'KLX 300R');
INSERT INTO `modelo` VALUES ('1392', '115', 'KLX 400R');
INSERT INTO `modelo` VALUES ('1393', '115', 'KLX 400SR');
INSERT INTO `modelo` VALUES ('1394', '115', 'KLX 450R');
INSERT INTO `modelo` VALUES ('1395', '115', 'KLX 650');
INSERT INTO `modelo` VALUES ('1396', '115', 'KX 100');
INSERT INTO `modelo` VALUES ('1397', '115', 'KX 125');
INSERT INTO `modelo` VALUES ('1398', '115', 'KX 250');
INSERT INTO `modelo` VALUES ('1399', '115', 'KX 250F');
INSERT INTO `modelo` VALUES ('1400', '115', 'KX 450F');
INSERT INTO `modelo` VALUES ('1401', '115', 'KX 500');
INSERT INTO `modelo` VALUES ('1402', '115', 'KX 60');
INSERT INTO `modelo` VALUES ('1403', '115', 'KX 65');
INSERT INTO `modelo` VALUES ('1404', '115', 'KX 85');
INSERT INTO `modelo` VALUES ('1405', '115', 'KZ 1000');
INSERT INTO `modelo` VALUES ('1406', '115', 'KZ 1300');
INSERT INTO `modelo` VALUES ('1407', '115', 'MAXI II 100');
INSERT INTO `modelo` VALUES ('1408', '115', 'MOJAVE 250');
INSERT INTO `modelo` VALUES ('1409', '115', 'NINJA 250R');
INSERT INTO `modelo` VALUES ('1410', '115', 'NINJA 650R');
INSERT INTO `modelo` VALUES ('1411', '115', 'NINJA EX 500');
INSERT INTO `modelo` VALUES ('1412', '115', 'NINJA ZX-10');
INSERT INTO `modelo` VALUES ('1413', '115', 'NINJA ZX-10R');
INSERT INTO `modelo` VALUES ('1414', '115', 'NINJA ZX-11');
INSERT INTO `modelo` VALUES ('1415', '115', 'NINJA ZX-12R');
INSERT INTO `modelo` VALUES ('1416', '115', 'NINJA ZX-14');
INSERT INTO `modelo` VALUES ('1417', '115', 'NINJA ZX-6R 600cc');
INSERT INTO `modelo` VALUES ('1418', '115', 'NINJA ZX-6R 636 cc');
INSERT INTO `modelo` VALUES ('1419', '115', 'NINJA ZX-7R');
INSERT INTO `modelo` VALUES ('1420', '115', 'NINJA ZX-9R');
INSERT INTO `modelo` VALUES ('1421', '115', 'VERSYS');
INSERT INTO `modelo` VALUES ('1422', '115', 'VOYAGER 1200');
INSERT INTO `modelo` VALUES ('1423', '115', 'VOYAGER XII');
INSERT INTO `modelo` VALUES ('1424', '115', 'VULCAN 1500');
INSERT INTO `modelo` VALUES ('1425', '115', 'VULCAN 1500 CLASSIC');
INSERT INTO `modelo` VALUES ('1426', '115', 'VULCAN 1500 DRIFTER');
INSERT INTO `modelo` VALUES ('1427', '115', 'VULCAN 1500 MEAN STREAK');
INSERT INTO `modelo` VALUES ('1428', '115', 'VULCAN 1500 NOMAD FI');
INSERT INTO `modelo` VALUES ('1429', '115', 'VULCAN 1600 CLASSIC');
INSERT INTO `modelo` VALUES ('1430', '115', 'VULCAN 1600 MEAN STREAK');
INSERT INTO `modelo` VALUES ('1431', '115', 'VULCAN 1700');
INSERT INTO `modelo` VALUES ('1432', '115', 'VULCAN 2000');
INSERT INTO `modelo` VALUES ('1433', '115', 'VULCAN 750');
INSERT INTO `modelo` VALUES ('1434', '115', 'VULCAN 800');
INSERT INTO `modelo` VALUES ('1435', '115', 'VULCAN 800 CLASSIC');
INSERT INTO `modelo` VALUES ('1436', '115', 'VULCAN 800 DRIFTER');
INSERT INTO `modelo` VALUES ('1437', '115', 'VULCAN 900 CLASSIC');
INSERT INTO `modelo` VALUES ('1438', '115', 'VULCAN 900 CLASSIC LT');
INSERT INTO `modelo` VALUES ('1439', '115', 'VULCAN 900 CUSTOM');
INSERT INTO `modelo` VALUES ('1440', '115', 'VULCAN EN 500');
INSERT INTO `modelo` VALUES ('1441', '115', 'VULCAN LTD CLASS 500');
INSERT INTO `modelo` VALUES ('1442', '115', 'VULCAN NOMAD 1500');
INSERT INTO `modelo` VALUES ('1443', '115', 'Z1000');
INSERT INTO `modelo` VALUES ('1444', '115', 'Z750');
INSERT INTO `modelo` VALUES ('1445', '115', 'ZR-7');
INSERT INTO `modelo` VALUES ('1446', '115', 'ZR-7S');
INSERT INTO `modelo` VALUES ('1447', '115', 'ZZR 1200');
INSERT INTO `modelo` VALUES ('1448', '115', 'ZZR 1400');
INSERT INTO `modelo` VALUES ('1449', '116', 'AKROS 50');
INSERT INTO `modelo` VALUES ('1450', '116', 'AKROS 90');
INSERT INTO `modelo` VALUES ('1451', '116', 'ERGON 50');
INSERT INTO `modelo` VALUES ('1452', '116', 'FIFTY 50');
INSERT INTO `modelo` VALUES ('1453', '116', 'FUTURE');
INSERT INTO `modelo` VALUES ('1454', '116', 'FUTURE ELÉTRICA');
INSERT INTO `modelo` VALUES ('1455', '116', 'FUTURE SPORT');
INSERT INTO `modelo` VALUES ('1456', '116', 'HAWK 150');
INSERT INTO `modelo` VALUES ('1457', '116', 'HUNTER 100');
INSERT INTO `modelo` VALUES ('1458', '116', 'HUNTER 90');
INSERT INTO `modelo` VALUES ('1459', '116', 'HUNTER SE');
INSERT INTO `modelo` VALUES ('1460', '116', 'MAX SE');
INSERT INTO `modelo` VALUES ('1461', '116', 'MAX SED');
INSERT INTO `modelo` VALUES ('1462', '116', 'MOTARD');
INSERT INTO `modelo` VALUES ('1463', '116', 'MOTARD 200');
INSERT INTO `modelo` VALUES ('1464', '116', 'OUTLOOK 150');
INSERT INTO `modelo` VALUES ('1465', '116', 'PALIO 50');
INSERT INTO `modelo` VALUES ('1466', '116', 'PGO SPEED 50');
INSERT INTO `modelo` VALUES ('1467', '116', 'PGO SPEED 90');
INSERT INTO `modelo` VALUES ('1468', '116', 'STX');
INSERT INTO `modelo` VALUES ('1469', '116', 'STX 200');
INSERT INTO `modelo` VALUES ('1470', '116', 'SUPER FIFTY 50');
INSERT INTO `modelo` VALUES ('1471', '116', 'VBLADE');
INSERT INTO `modelo` VALUES ('1472', '116', 'WEB');
INSERT INTO `modelo` VALUES ('1473', '116', 'WEB EVO');
INSERT INTO `modelo` VALUES ('1474', '117', 'ADDRESS 100');
INSERT INTO `modelo` VALUES ('1475', '117', 'ADDRESS 50');
INSERT INTO `modelo` VALUES ('1476', '117', 'AE 50');
INSERT INTO `modelo` VALUES ('1477', '117', 'AG 100');
INSERT INTO `modelo` VALUES ('1478', '117', 'AN125 BURGMAN');
INSERT INTO `modelo` VALUES ('1479', '117', 'AN650 BURGMAN');
INSERT INTO `modelo` VALUES ('1480', '117', 'B-KING');
INSERT INTO `modelo` VALUES ('1481', '117', 'BANDIT 1200S');
INSERT INTO `modelo` VALUES ('1482', '117', 'BANDIT 1250');
INSERT INTO `modelo` VALUES ('1483', '117', 'BANDIT 1250F');
INSERT INTO `modelo` VALUES ('1484', '117', 'BANDIT 1250S');
INSERT INTO `modelo` VALUES ('1485', '117', 'BANDIT 600S');
INSERT INTO `modelo` VALUES ('1486', '117', 'BANDIT 650');
INSERT INTO `modelo` VALUES ('1487', '117', 'BANDIT 650S');
INSERT INTO `modelo` VALUES ('1488', '117', 'BANDIT N1200');
INSERT INTO `modelo` VALUES ('1489', '117', 'BANDIT N1250');
INSERT INTO `modelo` VALUES ('1490', '117', 'BANDIT N600');
INSERT INTO `modelo` VALUES ('1491', '117', 'BANDIT N650');
INSERT INTO `modelo` VALUES ('1492', '117', 'BOULEVARD M109R');
INSERT INTO `modelo` VALUES ('1493', '117', 'BOULEVARD M1500');
INSERT INTO `modelo` VALUES ('1494', '117', 'BOULEVARD M800');
INSERT INTO `modelo` VALUES ('1495', '117', 'BURGMAN 125 AUTOMATIC');
INSERT INTO `modelo` VALUES ('1496', '117', 'BURGMAN 400');
INSERT INTO `modelo` VALUES ('1497', '117', 'BURGMAN 650');
INSERT INTO `modelo` VALUES ('1498', '117', 'BURGMAN 650 EXECUTIVE');
INSERT INTO `modelo` VALUES ('1499', '117', 'C 1500 BOULEVARD');
INSERT INTO `modelo` VALUES ('1500', '117', 'DL 1000 V-STROM');
INSERT INTO `modelo` VALUES ('1501', '117', 'DL 650 V-STROM');
INSERT INTO `modelo` VALUES ('1502', '117', 'DR 350');
INSERT INTO `modelo` VALUES ('1503', '117', 'DR 350 SE');
INSERT INTO `modelo` VALUES ('1504', '117', 'DR 650 R');
INSERT INTO `modelo` VALUES ('1505', '117', 'DR 650 RE');
INSERT INTO `modelo` VALUES ('1506', '117', 'DR 650 RS');
INSERT INTO `modelo` VALUES ('1507', '117', 'DR 650 RSE');
INSERT INTO `modelo` VALUES ('1508', '117', 'DR 800 S');
INSERT INTO `modelo` VALUES ('1509', '117', 'DR-Z 400E');
INSERT INTO `modelo` VALUES ('1510', '117', 'EN 125 YES ED');
INSERT INTO `modelo` VALUES ('1511', '117', 'EN 125 YES ED CARGO');
INSERT INTO `modelo` VALUES ('1512', '117', 'FREEWIND XF 650');
INSERT INTO `modelo` VALUES ('1513', '117', 'GS500E');
INSERT INTO `modelo` VALUES ('1514', '117', 'GSX 1100 F');
INSERT INTO `modelo` VALUES ('1515', '117', 'GSX 1100 SBE KATANA');
INSERT INTO `modelo` VALUES ('1516', '117', 'GSX 1300 B-KING');
INSERT INTO `modelo` VALUES ('1517', '117', 'GSX 650 F');
INSERT INTO `modelo` VALUES ('1518', '117', 'GSX 750 F');
INSERT INTO `modelo` VALUES ('1519', '117', 'GSX R1000');
INSERT INTO `modelo` VALUES ('1520', '117', 'GSX R750');
INSERT INTO `modelo` VALUES ('1521', '117', 'GSX-R 750');
INSERT INTO `modelo` VALUES ('1522', '117', 'GSX-R1000');
INSERT INTO `modelo` VALUES ('1523', '117', 'GSX-R1100 W');
INSERT INTO `modelo` VALUES ('1524', '117', 'GSX-R600');
INSERT INTO `modelo` VALUES ('1525', '117', 'GSX-R750 W');
INSERT INTO `modelo` VALUES ('1526', '117', 'GSX-R750 W SRAD');
INSERT INTO `modelo` VALUES ('1527', '117', 'GSX1300R HAYABUSA');
INSERT INTO `modelo` VALUES ('1528', '117', 'GT 550');
INSERT INTO `modelo` VALUES ('1529', '117', 'INTRUDER 125');
INSERT INTO `modelo` VALUES ('1530', '117', 'INTRUDER 125 ED CARGO');
INSERT INTO `modelo` VALUES ('1531', '117', 'INTRUDER 250');
INSERT INTO `modelo` VALUES ('1532', '117', 'INTRUDER VS 1400 GLP');
INSERT INTO `modelo` VALUES ('1533', '117', 'INTRUDER VS 800 GL');
INSERT INTO `modelo` VALUES ('1534', '117', 'INTRUDER VS 800 GLP');
INSERT INTO `modelo` VALUES ('1535', '117', 'KATANA 125');
INSERT INTO `modelo` VALUES ('1536', '117', 'LC 1500 INTRUDER');
INSERT INTO `modelo` VALUES ('1537', '117', 'LETS II 50');
INSERT INTO `modelo` VALUES ('1538', '117', 'LS 650 SAVAGE');
INSERT INTO `modelo` VALUES ('1539', '117', 'LT 50 QUADRICICLO');
INSERT INTO `modelo` VALUES ('1540', '117', 'LT 80 QUADRICICLO');
INSERT INTO `modelo` VALUES ('1541', '117', 'LT F160 QUADRICICLO');
INSERT INTO `modelo` VALUES ('1542', '117', 'LT-A400F KINGQUAD');
INSERT INTO `modelo` VALUES ('1543', '117', 'LT-A450X KINGQUAD');
INSERT INTO `modelo` VALUES ('1544', '117', 'LT-A750X KINGQUAD');
INSERT INTO `modelo` VALUES ('1545', '117', 'LT-F250 OZARK');
INSERT INTO `modelo` VALUES ('1546', '117', 'LT-R450 QUADRACER');
INSERT INTO `modelo` VALUES ('1547', '117', 'M 800 BOULEVARD');
INSERT INTO `modelo` VALUES ('1548', '117', 'MARAUDER 800');
INSERT INTO `modelo` VALUES ('1549', '117', 'OZARK 250');
INSERT INTO `modelo` VALUES ('1550', '117', 'QUADRACER R450');
INSERT INTO `modelo` VALUES ('1551', '117', 'QUADRUNNER 160');
INSERT INTO `modelo` VALUES ('1552', '117', 'QUADSPORT Z400');
INSERT INTO `modelo` VALUES ('1553', '117', 'RF 600 R');
INSERT INTO `modelo` VALUES ('1554', '117', 'RF 900 R');
INSERT INTO `modelo` VALUES ('1555', '117', 'RM 125');
INSERT INTO `modelo` VALUES ('1556', '117', 'RM 250');
INSERT INTO `modelo` VALUES ('1557', '117', 'RM 80');
INSERT INTO `modelo` VALUES ('1558', '117', 'RM-Z 250');
INSERT INTO `modelo` VALUES ('1559', '117', 'RMX 250');
INSERT INTO `modelo` VALUES ('1560', '117', 'TL 1000 S');
INSERT INTO `modelo` VALUES ('1561', '117', 'V-STROM 650');
INSERT INTO `modelo` VALUES ('1562', '117', 'VX 800');
INSERT INTO `modelo` VALUES ('1563', '117', 'YES 125 ED');
INSERT INTO `modelo` VALUES ('1564', '117', 'YES SE');
INSERT INTO `modelo` VALUES ('1565', '117', 'YES SE CARGO');
INSERT INTO `modelo` VALUES ('1566', '117', 'ZX-14');
INSERT INTO `modelo` VALUES ('1567', '118', 'ADVENTURER');
INSERT INTO `modelo` VALUES ('1568', '118', 'BONNEVILE');
INSERT INTO `modelo` VALUES ('1569', '118', 'BONNEVILLE T 100 790');
INSERT INTO `modelo` VALUES ('1570', '118', 'BONNEVILLE T 100 865');
INSERT INTO `modelo` VALUES ('1571', '118', 'DAYTONA 1200');
INSERT INTO `modelo` VALUES ('1572', '118', 'DAYTONA 675');
INSERT INTO `modelo` VALUES ('1573', '118', 'DAYTONA 900');
INSERT INTO `modelo` VALUES ('1574', '118', 'DAYTONA 955i');
INSERT INTO `modelo` VALUES ('1575', '118', 'DAYTONA SUPER III');
INSERT INTO `modelo` VALUES ('1576', '118', 'DAYTONA T 955');
INSERT INTO `modelo` VALUES ('1577', '118', 'LEGEND 900');
INSERT INTO `modelo` VALUES ('1578', '118', 'ROCKET III');
INSERT INTO `modelo` VALUES ('1579', '118', 'ROCKET III CLASSIC');
INSERT INTO `modelo` VALUES ('1580', '118', 'ROCKET III TOURING');
INSERT INTO `modelo` VALUES ('1581', '118', 'SCRAMBLER');
INSERT INTO `modelo` VALUES ('1582', '118', 'SPEED TRIPLE');
INSERT INTO `modelo` VALUES ('1583', '118', 'SPEED TRIPLE 1050');
INSERT INTO `modelo` VALUES ('1584', '118', 'SPEED TRIPLE 1050 SE');
INSERT INTO `modelo` VALUES ('1585', '118', 'SPEED TRIPLE 1050i');
INSERT INTO `modelo` VALUES ('1586', '118', 'SPEED TRIPLE 900');
INSERT INTO `modelo` VALUES ('1587', '118', 'SPEED TRIPLE 955i');
INSERT INTO `modelo` VALUES ('1588', '118', 'SPEED TRIPLE T 509 900');
INSERT INTO `modelo` VALUES ('1589', '118', 'SPRINT 900');
INSERT INTO `modelo` VALUES ('1590', '118', 'SPRINT RS 955');
INSERT INTO `modelo` VALUES ('1591', '118', 'SPRINT ST');
INSERT INTO `modelo` VALUES ('1592', '118', 'SPRINT ST 1050');
INSERT INTO `modelo` VALUES ('1593', '118', 'STREET TRIPLE');
INSERT INTO `modelo` VALUES ('1594', '118', 'STREET TRIPLE 675');
INSERT INTO `modelo` VALUES ('1595', '118', 'THUNDERBIRD 900');
INSERT INTO `modelo` VALUES ('1596', '118', 'THUNDERBIRD 900 SPORT');
INSERT INTO `modelo` VALUES ('1597', '118', 'TIGER 1050');
INSERT INTO `modelo` VALUES ('1598', '118', 'TIGER 1050 SE');
INSERT INTO `modelo` VALUES ('1599', '118', 'TIGER 750');
INSERT INTO `modelo` VALUES ('1600', '118', 'TIGER 900');
INSERT INTO `modelo` VALUES ('1601', '118', 'TIGER 955i');
INSERT INTO `modelo` VALUES ('1602', '118', 'TIGER S');
INSERT INTO `modelo` VALUES ('1603', '118', 'TIGER T');
INSERT INTO `modelo` VALUES ('1604', '118', 'TIGER X');
INSERT INTO `modelo` VALUES ('1605', '118', 'TRIDENT 750');
INSERT INTO `modelo` VALUES ('1606', '118', 'TRIDENT 900');
INSERT INTO `modelo` VALUES ('1607', '118', 'TROPHY 1200');
INSERT INTO `modelo` VALUES ('1608', '118', 'TROPHY 900');
INSERT INTO `modelo` VALUES ('1609', '118', 'TT 600');
INSERT INTO `modelo` VALUES ('1610', '119', 'AXIS 90');
INSERT INTO `modelo` VALUES ('1611', '119', 'BWS 50');
INSERT INTO `modelo` VALUES ('1612', '119', 'CRYPTON 100');
INSERT INTO `modelo` VALUES ('1613', '119', 'CRYPTON ED');
INSERT INTO `modelo` VALUES ('1614', '119', 'CRYPTON K');
INSERT INTO `modelo` VALUES ('1615', '119', 'DRAG STAR 1100');
INSERT INTO `modelo` VALUES ('1616', '119', 'DRAG STAR 650');
INSERT INTO `modelo` VALUES ('1617', '119', 'DT 180');
INSERT INTO `modelo` VALUES ('1618', '119', 'DT 180Z');
INSERT INTO `modelo` VALUES ('1619', '119', 'DT 200');
INSERT INTO `modelo` VALUES ('1620', '119', 'DT 200R');
INSERT INTO `modelo` VALUES ('1621', '119', 'FACTOR YBR 125 ED');
INSERT INTO `modelo` VALUES ('1622', '119', 'FAZER 250');
INSERT INTO `modelo` VALUES ('1623', '119', 'FAZER 250IE LIMITED EDIT');
INSERT INTO `modelo` VALUES ('1624', '119', 'FAZER 600');
INSERT INTO `modelo` VALUES ('1625', '119', 'FAZER 600S');
INSERT INTO `modelo` VALUES ('1626', '119', 'FAZER 8');
INSERT INTO `modelo` VALUES ('1627', '119', 'FAZER YS 250');
INSERT INTO `modelo` VALUES ('1628', '119', 'FJR 1300');
INSERT INTO `modelo` VALUES ('1629', '119', 'FZ1');
INSERT INTO `modelo` VALUES ('1630', '119', 'FZ1 FAZER');
INSERT INTO `modelo` VALUES ('1631', '119', 'FZ6N');
INSERT INTO `modelo` VALUES ('1632', '119', 'FZ6S');
INSERT INTO `modelo` VALUES ('1633', '119', 'FZ8');
INSERT INTO `modelo` VALUES ('1634', '119', 'FZR 1000');
INSERT INTO `modelo` VALUES ('1635', '119', 'FZR 600');
INSERT INTO `modelo` VALUES ('1636', '119', 'FZR 600N');
INSERT INTO `modelo` VALUES ('1637', '119', 'JOG 50');
INSERT INTO `modelo` VALUES ('1638', '119', 'JOG TEEN 50');
INSERT INTO `modelo` VALUES ('1639', '119', 'MIDNIGHT WARRIOR');
INSERT INTO `modelo` VALUES ('1640', '119', 'MT-01');
INSERT INTO `modelo` VALUES ('1641', '119', 'MT-03');
INSERT INTO `modelo` VALUES ('1642', '119', 'NEO AT 115');
INSERT INTO `modelo` VALUES ('1643', '119', 'NEO CVT 115');
INSERT INTO `modelo` VALUES ('1644', '119', 'RD 125');
INSERT INTO `modelo` VALUES ('1645', '119', 'RD 135');
INSERT INTO `modelo` VALUES ('1646', '119', 'RD 350');
INSERT INTO `modelo` VALUES ('1647', '119', 'RD 350 LC');
INSERT INTO `modelo` VALUES ('1648', '119', 'RD 350R');
INSERT INTO `modelo` VALUES ('1649', '119', 'RDZ 125');
INSERT INTO `modelo` VALUES ('1650', '119', 'RDZ 135');
INSERT INTO `modelo` VALUES ('1651', '119', 'ROAD STAR 1600');
INSERT INTO `modelo` VALUES ('1652', '119', 'ROYAL STAR 1300');
INSERT INTO `modelo` VALUES ('1653', '119', 'RX 125');
INSERT INTO `modelo` VALUES ('1654', '119', 'TDM 225');
INSERT INTO `modelo` VALUES ('1655', '119', 'TDM 850');
INSERT INTO `modelo` VALUES ('1656', '119', 'TDM 900');
INSERT INTO `modelo` VALUES ('1657', '119', 'TDR 180');
INSERT INTO `modelo` VALUES ('1658', '119', 'TMAX XP 500');
INSERT INTO `modelo` VALUES ('1659', '119', 'TRX 850');
INSERT INTO `modelo` VALUES ('1660', '119', 'TT 125');
INSERT INTO `modelo` VALUES ('1661', '119', 'TT-R 125E');
INSERT INTO `modelo` VALUES ('1662', '119', 'TT-R 125L');
INSERT INTO `modelo` VALUES ('1663', '119', 'TT-R 125LE');
INSERT INTO `modelo` VALUES ('1664', '119', 'TT-R 230');
INSERT INTO `modelo` VALUES ('1665', '119', 'TX 650');
INSERT INTO `modelo` VALUES ('1666', '119', 'V MAX 1200');
INSERT INTO `modelo` VALUES ('1667', '119', 'V MAX 1800');
INSERT INTO `modelo` VALUES ('1668', '119', 'V STAR 1100');
INSERT INTO `modelo` VALUES ('1669', '119', 'V STAR 650');
INSERT INTO `modelo` VALUES ('1670', '119', 'V STAR 650 CLASSIC');
INSERT INTO `modelo` VALUES ('1671', '119', 'WR 200');
INSERT INTO `modelo` VALUES ('1672', '119', 'WR 250F');
INSERT INTO `modelo` VALUES ('1673', '119', 'WR 426F');
INSERT INTO `modelo` VALUES ('1674', '119', 'WR 450F');
INSERT INTO `modelo` VALUES ('1675', '119', 'XJ6 F');
INSERT INTO `modelo` VALUES ('1676', '119', 'XJ6 N');
INSERT INTO `modelo` VALUES ('1677', '119', 'XJR 1200');
INSERT INTO `modelo` VALUES ('1678', '119', 'XJR 1300');
INSERT INTO `modelo` VALUES ('1679', '119', 'XT 225');
INSERT INTO `modelo` VALUES ('1680', '119', 'XT 600 E');
INSERT INTO `modelo` VALUES ('1681', '119', 'XT 600Z TÉNÉRÉ');
INSERT INTO `modelo` VALUES ('1682', '119', 'XT 660 R');
INSERT INTO `modelo` VALUES ('1683', '119', 'XT1200Z SUPER TÉNÉRÉ');
INSERT INTO `modelo` VALUES ('1684', '119', 'XTZ 125 E');
INSERT INTO `modelo` VALUES ('1685', '119', 'XTZ 125 K');
INSERT INTO `modelo` VALUES ('1686', '119', 'XTZ 125 X/E');
INSERT INTO `modelo` VALUES ('1687', '119', 'XTZ 125 X/K');
INSERT INTO `modelo` VALUES ('1688', '119', 'XTZ 250 LANDER');
INSERT INTO `modelo` VALUES ('1689', '119', 'XTZ 250 TÉNÉRÉ');
INSERT INTO `modelo` VALUES ('1690', '119', 'XTZ 250 X');
INSERT INTO `modelo` VALUES ('1691', '119', 'XTZ 750S TENERE');
INSERT INTO `modelo` VALUES ('1692', '119', 'XV 1100 VIRAGO');
INSERT INTO `modelo` VALUES ('1693', '119', 'XV 250 VIRAGO');
INSERT INTO `modelo` VALUES ('1694', '119', 'XV 535S VIRAGO');
INSERT INTO `modelo` VALUES ('1695', '119', 'XV VIRAGO 750');
INSERT INTO `modelo` VALUES ('1696', '119', 'XVS 950 MIDNIGHT STAR');
INSERT INTO `modelo` VALUES ('1697', '119', 'YBR 125 E');
INSERT INTO `modelo` VALUES ('1698', '119', 'YBR 125 ED');
INSERT INTO `modelo` VALUES ('1699', '119', 'YBR 125 FACTOR E');
INSERT INTO `modelo` VALUES ('1700', '119', 'YBR 125 FACTOR ED');
INSERT INTO `modelo` VALUES ('1701', '119', 'YBR 125 FACTOR ED CENTEN');
INSERT INTO `modelo` VALUES ('1702', '119', 'YBR 125 FACTOR K');
INSERT INTO `modelo` VALUES ('1703', '119', 'YBR 125K');
INSERT INTO `modelo` VALUES ('1704', '119', 'YFM 125 GRIZZLY');
INSERT INTO `modelo` VALUES ('1705', '119', 'YFM 250 BIG BEAR');
INSERT INTO `modelo` VALUES ('1706', '119', 'YFM 250R');
INSERT INTO `modelo` VALUES ('1707', '119', 'YFM 350 GRIZZLY');
INSERT INTO `modelo` VALUES ('1708', '119', 'YFM 350R');
INSERT INTO `modelo` VALUES ('1709', '119', 'YFM 660 GRIZZLY');
INSERT INTO `modelo` VALUES ('1710', '119', 'YFM 660R RAPTOR');
INSERT INTO `modelo` VALUES ('1711', '119', 'YFM 700 GRIZZLY');
INSERT INTO `modelo` VALUES ('1712', '119', 'YFM 700R');
INSERT INTO `modelo` VALUES ('1713', '119', 'YFM 80R');
INSERT INTO `modelo` VALUES ('1714', '119', 'YFS 200 BLASTER');
INSERT INTO `modelo` VALUES ('1715', '119', 'YFZ 450');
INSERT INTO `modelo` VALUES ('1716', '119', 'YP 250 MAJESTY');
INSERT INTO `modelo` VALUES ('1717', '119', 'YZ 125');
INSERT INTO `modelo` VALUES ('1718', '119', 'YZ 250');
INSERT INTO `modelo` VALUES ('1719', '119', 'YZ 250F');
INSERT INTO `modelo` VALUES ('1720', '119', 'YZ 450F');
INSERT INTO `modelo` VALUES ('1721', '119', 'YZ 80');
INSERT INTO `modelo` VALUES ('1722', '119', 'YZ 85 LW');
INSERT INTO `modelo` VALUES ('1723', '119', 'YZF 1000 THUNDERACE');
INSERT INTO `modelo` VALUES ('1724', '119', 'YZF 450');
INSERT INTO `modelo` VALUES ('1725', '119', 'YZF 600 THUNDERCAT');
INSERT INTO `modelo` VALUES ('1726', '119', 'YZF 750');
INSERT INTO `modelo` VALUES ('1727', '119', 'YZF-R1');
INSERT INTO `modelo` VALUES ('1728', '119', 'YZF-R125');
INSERT INTO `modelo` VALUES ('1729', '119', 'YZF-R6');
INSERT INTO `modelo` VALUES ('1730', '121', '350');
INSERT INTO `modelo` VALUES ('1731', '122', 'AMAZONAS');
INSERT INTO `modelo` VALUES ('1732', '123', '110 MIX');
INSERT INTO `modelo` VALUES ('1733', '123', '110 X');
INSERT INTO `modelo` VALUES ('1734', '123', '125 A');
INSERT INTO `modelo` VALUES ('1735', '123', '125 S');
INSERT INTO `modelo` VALUES ('1736', '123', '150 SC');
INSERT INTO `modelo` VALUES ('1737', '123', '150 TC');
INSERT INTO `modelo` VALUES ('1738', '123', '250 C1');
INSERT INTO `modelo` VALUES ('1739', '123', '250 CROSS FX');
INSERT INTO `modelo` VALUES ('1740', '123', 'AMAZON 150');
INSERT INTO `modelo` VALUES ('1741', '123', 'BIG BOY 50');
INSERT INTO `modelo` VALUES ('1742', '123', 'COUGAR 150');
INSERT INTO `modelo` VALUES ('1743', '123', 'ENDURANCE 250 RX');
INSERT INTO `modelo` VALUES ('1744', '123', 'LX 125/26');
INSERT INTO `modelo` VALUES ('1745', '123', 'SELVA 250');
INSERT INTO `modelo` VALUES ('1746', '123', 'SNAKE 110');
INSERT INTO `modelo` VALUES ('1747', '123', 'THUNDER 300');
INSERT INTO `modelo` VALUES ('1748', '124', '2009');
INSERT INTO `modelo` VALUES ('1749', '124', 'AREA-51');
INSERT INTO `modelo` VALUES ('1750', '124', 'CLASSIC');
INSERT INTO `modelo` VALUES ('1751', '124', 'DORSODURO 750');
INSERT INTO `modelo` VALUES ('1752', '124', 'LEONARDO');
INSERT INTO `modelo` VALUES ('1753', '124', 'MOTO');
INSERT INTO `modelo` VALUES ('1754', '124', 'PEGASO');
INSERT INTO `modelo` VALUES ('1755', '124', 'RALLY');
INSERT INTO `modelo` VALUES ('1756', '124', 'RS 125');
INSERT INTO `modelo` VALUES ('1757', '124', 'RS 250');
INSERT INTO `modelo` VALUES ('1758', '124', 'RS 50');
INSERT INTO `modelo` VALUES ('1759', '124', 'RSV 1000 R');
INSERT INTO `modelo` VALUES ('1760', '124', 'RSV MILLE');
INSERT INTO `modelo` VALUES ('1761', '124', 'RSV4 FACTORY');
INSERT INTO `modelo` VALUES ('1762', '124', 'RX 50');
INSERT INTO `modelo` VALUES ('1763', '124', 'SCARABEO 200');
INSERT INTO `modelo` VALUES ('1764', '124', 'SCARABEO 300');
INSERT INTO `modelo` VALUES ('1765', '124', 'SCARABEO 500');
INSERT INTO `modelo` VALUES ('1766', '124', 'SCARABEO BRIGTH');
INSERT INTO `modelo` VALUES ('1767', '124', 'SCARABEO CLASSIC');
INSERT INTO `modelo` VALUES ('1768', '124', 'SCARABEO DE LUXE');
INSERT INTO `modelo` VALUES ('1769', '124', 'SONIC');
INSERT INTO `modelo` VALUES ('1770', '124', 'SR RACING');
INSERT INTO `modelo` VALUES ('1771', '124', 'SR STEALTH');
INSERT INTO `modelo` VALUES ('1772', '124', 'SR WWW');
INSERT INTO `modelo` VALUES ('1773', '125', 'CLASSIC 150');
INSERT INTO `modelo` VALUES ('1774', '126', 'MIRAGE SPORT');
INSERT INTO `modelo` VALUES ('1775', '127', 'TNT 1130 CAFÉ RACER');
INSERT INTO `modelo` VALUES ('1776', '127', 'TNT 1130S');
INSERT INTO `modelo` VALUES ('1777', '127', 'TNT 899');
INSERT INTO `modelo` VALUES ('1778', '127', 'TNT 899S');
INSERT INTO `modelo` VALUES ('1779', '127', 'TRE 1130K');
INSERT INTO `modelo` VALUES ('1780', '127', 'TRE 1130K AMAZONAS');
INSERT INTO `modelo` VALUES ('1781', '128', 'DB5R');
INSERT INTO `modelo` VALUES ('1782', '128', 'DB6 DELIRIO');
INSERT INTO `modelo` VALUES ('1783', '128', 'DB6R DELIRIO');
INSERT INTO `modelo` VALUES ('1784', '128', 'DB7');
INSERT INTO `modelo` VALUES ('1785', '130', 'ELEGANT 50');
INSERT INTO `modelo` VALUES ('1786', '130', 'FT 125 FOSTI');
INSERT INTO `modelo` VALUES ('1787', '130', 'JT 100 JAGUAR');
INSERT INTO `modelo` VALUES ('1788', '130', 'JT 50 JAGUAR');
INSERT INTO `modelo` VALUES ('1789', '131', 'CAN AM SPYDER ROADSTER');
INSERT INTO `modelo` VALUES ('1790', '131', 'DS 250');
INSERT INTO `modelo` VALUES ('1791', '131', 'DS 450');
INSERT INTO `modelo` VALUES ('1792', '131', 'DS 450 X');
INSERT INTO `modelo` VALUES ('1793', '131', 'DS 650');
INSERT INTO `modelo` VALUES ('1794', '131', 'DS 70');
INSERT INTO `modelo` VALUES ('1795', '131', 'DS 90');
INSERT INTO `modelo` VALUES ('1796', '131', 'DS 90 X');
INSERT INTO `modelo` VALUES ('1797', '131', 'OUTLANDER 400');
INSERT INTO `modelo` VALUES ('1798', '131', 'OUTLANDER 400 H.O. EFI');
INSERT INTO `modelo` VALUES ('1799', '131', 'OUTLANDER 400 H.O. EFI X');
INSERT INTO `modelo` VALUES ('1800', '131', 'OUTLANDER 400 XT');
INSERT INTO `modelo` VALUES ('1801', '131', 'OUTLANDER 650');
INSERT INTO `modelo` VALUES ('1802', '131', 'OUTLANDER 650 XT');
INSERT INTO `modelo` VALUES ('1803', '131', 'OUTLANDER 800');
INSERT INTO `modelo` VALUES ('1804', '131', 'OUTLANDER 800 XT');
INSERT INTO `modelo` VALUES ('1805', '131', 'OUTLANDER MAX 400');
INSERT INTO `modelo` VALUES ('1806', '131', 'OUTLANDER MAX 400 H.O. E');
INSERT INTO `modelo` VALUES ('1807', '131', 'OUTLANDER MAX 400 H.O. E');
INSERT INTO `modelo` VALUES ('1808', '131', 'OUTLANDER MAX 400 XT');
INSERT INTO `modelo` VALUES ('1809', '131', 'OUTLANDER MAX 650');
INSERT INTO `modelo` VALUES ('1810', '131', 'OUTLANDER MAX 650 XT');
INSERT INTO `modelo` VALUES ('1811', '131', 'OUTLANDER MAX 800');
INSERT INTO `modelo` VALUES ('1812', '131', 'OUTLANDER MAX 800 LTD');
INSERT INTO `modelo` VALUES ('1813', '131', 'OUTLANDER MAX 800 XT');
INSERT INTO `modelo` VALUES ('1814', '131', 'RENEGADE 500 H.O. EFI');
INSERT INTO `modelo` VALUES ('1815', '131', 'RENEGADE 800 H.O. EFI');
INSERT INTO `modelo` VALUES ('1816', '131', 'RENEGADE 800 X');
INSERT INTO `modelo` VALUES ('1817', '132', '1125CR');
INSERT INTO `modelo` VALUES ('1818', '132', '1125R');
INSERT INTO `modelo` VALUES ('1819', '132', 'FIREBOLT XB12R');
INSERT INTO `modelo` VALUES ('1820', '132', 'LIGHTNING CITY CROSS XB9');
INSERT INTO `modelo` VALUES ('1821', '132', 'LIGHTNING XB12S');
INSERT INTO `modelo` VALUES ('1822', '132', 'LIGHTNING XB12SS');
INSERT INTO `modelo` VALUES ('1823', '132', 'LIGHTNING XB12STT');
INSERT INTO `modelo` VALUES ('1824', '132', 'LIGHTNING XB9SX');
INSERT INTO `modelo` VALUES ('1825', '132', 'ULYSSES XB12X');
INSERT INTO `modelo` VALUES ('1826', '132', 'ULYSSES XB12X TOURING');
INSERT INTO `modelo` VALUES ('1827', '132', 'ULYSSES XB12XP');
INSERT INTO `modelo` VALUES ('1828', '132', 'ULYSSES XB12XT');
INSERT INTO `modelo` VALUES ('1829', '132', 'ULYSSES XS 12X 1200');
INSERT INTO `modelo` VALUES ('1830', '133', '1.6 STANDARD');
INSERT INTO `modelo` VALUES ('1831', '133', 'SPORT');
INSERT INTO `modelo` VALUES ('1832', '133', 'STAR I');
INSERT INTO `modelo` VALUES ('1833', '133', 'STAR II');
INSERT INTO `modelo` VALUES ('1834', '133', 'STAR II TOP');
INSERT INTO `modelo` VALUES ('1835', '133', 'TOP');
INSERT INTO `modelo` VALUES ('1836', '134', 'CANYON 500');
INSERT INTO `modelo` VALUES ('1837', '134', 'CANYON 600');
INSERT INTO `modelo` VALUES ('1838', '134', 'ELEFANT 750');
INSERT INTO `modelo` VALUES ('1839', '134', 'ELEFANT 900');
INSERT INTO `modelo` VALUES ('1840', '134', 'GRAND CANYON 900');
INSERT INTO `modelo` VALUES ('1841', '134', 'MITO 125');
INSERT INTO `modelo` VALUES ('1842', '134', 'NAVIGATOR 1000');
INSERT INTO `modelo` VALUES ('1843', '134', 'PLANET 125');
INSERT INTO `modelo` VALUES ('1844', '134', 'RAPTOR 125');
INSERT INTO `modelo` VALUES ('1845', '134', 'RAPTOR 650');
INSERT INTO `modelo` VALUES ('1846', '134', 'ROADSTER 200');
INSERT INTO `modelo` VALUES ('1847', '134', 'SUPER CITY 125');
INSERT INTO `modelo` VALUES ('1848', '134', 'V-RAPTOR 1000');
INSERT INTO `modelo` VALUES ('1849', '134', 'W-16 600');
INSERT INTO `modelo` VALUES ('1850', '134', 'XTRA-RAPTOR 1000');
INSERT INTO `modelo` VALUES ('1851', '135', 'MOBILETE 50');
INSERT INTO `modelo` VALUES ('1852', '136', 'PHOENIX 200');
INSERT INTO `modelo` VALUES ('1853', '137', 'ALTINO 100');
INSERT INTO `modelo` VALUES ('1854', '137', 'MESSAGE 50');
INSERT INTO `modelo` VALUES ('1855', '137', 'VC 125 ADVANCED');
INSERT INTO `modelo` VALUES ('1856', '137', 'VF 125');
INSERT INTO `modelo` VALUES ('1857', '137', 'VL 125 DAYSTAR');
INSERT INTO `modelo` VALUES ('1858', '137', 'VS 125');
INSERT INTO `modelo` VALUES ('1859', '137', 'VT 125 MAGMA');
INSERT INTO `modelo` VALUES ('1860', '138', 'APACHE RTR 150');
INSERT INTO `modelo` VALUES ('1861', '138', 'CITYCOM 300 I');
INSERT INTO `modelo` VALUES ('1862', '138', 'KANSAS 150');
INSERT INTO `modelo` VALUES ('1863', '138', 'KANSAS 250');
INSERT INTO `modelo` VALUES ('1864', '138', 'LASER 150');
INSERT INTO `modelo` VALUES ('1865', '138', 'SMART 125');
INSERT INTO `modelo` VALUES ('1866', '138', 'SPEED 150');
INSERT INTO `modelo` VALUES ('1867', '138', 'SUPER 100');
INSERT INTO `modelo` VALUES ('1868', '138', 'SUPER 50');
INSERT INTO `modelo` VALUES ('1869', '138', 'ZIG 110');
INSERT INTO `modelo` VALUES ('1870', '139', 'DY 110-6');
INSERT INTO `modelo` VALUES ('1871', '139', 'DY 125-8');
INSERT INTO `modelo` VALUES ('1872', '139', 'DY 125T-10');
INSERT INTO `modelo` VALUES ('1873', '139', 'DY 150-7');
INSERT INTO `modelo` VALUES ('1874', '139', 'DY 150-9');
INSERT INTO `modelo` VALUES ('1875', '139', 'DY 150GY');
INSERT INTO `modelo` VALUES ('1876', '139', 'DY 150ZH');
INSERT INTO `modelo` VALUES ('1877', '139', 'DY 250-2');
INSERT INTO `modelo` VALUES ('1878', '140', 'ATLANTIS');
INSERT INTO `modelo` VALUES ('1879', '140', 'GPR 50R');
INSERT INTO `modelo` VALUES ('1880', '140', 'PREDADOR AR');
INSERT INTO `modelo` VALUES ('1881', '140', 'PREDATOR LC');
INSERT INTO `modelo` VALUES ('1882', '140', 'SENDA 125R');
INSERT INTO `modelo` VALUES ('1883', '142', 'MIRAGE');
INSERT INTO `modelo` VALUES ('1884', '142', 'ONE');
INSERT INTO `modelo` VALUES ('1885', '142', 'ONE RACING');
INSERT INTO `modelo` VALUES ('1886', '143', '150 ADVENTURE');
INSERT INTO `modelo` VALUES ('1887', '143', '250 ELITE');
INSERT INTO `modelo` VALUES ('1888', '143', '49 AIR');
INSERT INTO `modelo` VALUES ('1889', '144', 'FY 100-10A');
INSERT INTO `modelo` VALUES ('1890', '144', 'FY 125-19');
INSERT INTO `modelo` VALUES ('1891', '144', 'FY 125-20 SACHS');
INSERT INTO `modelo` VALUES ('1892', '144', 'FY 125EY-2');
INSERT INTO `modelo` VALUES ('1893', '144', 'FY 125Y-3');
INSERT INTO `modelo` VALUES ('1894', '144', 'FY 150-3');
INSERT INTO `modelo` VALUES ('1895', '144', 'FY 150T-18');
INSERT INTO `modelo` VALUES ('1896', '144', 'FY 250');
INSERT INTO `modelo` VALUES ('1897', '145', 'GR 08T4');
INSERT INTO `modelo` VALUES ('1898', '145', 'GR 125S');
INSERT INTO `modelo` VALUES ('1899', '145', 'GR 125ST');
INSERT INTO `modelo` VALUES ('1900', '145', 'GR 125T3');
INSERT INTO `modelo` VALUES ('1901', '145', 'GR 125T3 TINA');
INSERT INTO `modelo` VALUES ('1902', '145', 'GR 125U');
INSERT INTO `modelo` VALUES ('1903', '145', 'GR 125Z');
INSERT INTO `modelo` VALUES ('1904', '145', 'GR 150P');
INSERT INTO `modelo` VALUES ('1905', '145', 'GR 150ST');
INSERT INTO `modelo` VALUES ('1906', '145', 'GR 150T3');
INSERT INTO `modelo` VALUES ('1907', '145', 'GR 150TI');
INSERT INTO `modelo` VALUES ('1908', '145', 'GR 150U');
INSERT INTO `modelo` VALUES ('1909', '145', 'GR 250T3');
INSERT INTO `modelo` VALUES ('1910', '145', 'GR 250V');
INSERT INTO `modelo` VALUES ('1911', '145', 'GR 500ATV');
INSERT INTO `modelo` VALUES ('1912', '146', 'EC 125');
INSERT INTO `modelo` VALUES ('1913', '146', 'EC 200');
INSERT INTO `modelo` VALUES ('1914', '146', 'EC 200 HOBBY');
INSERT INTO `modelo` VALUES ('1915', '146', 'EC 250');
INSERT INTO `modelo` VALUES ('1916', '146', 'EC 250 F');
INSERT INTO `modelo` VALUES ('1917', '146', 'EC 300');
INSERT INTO `modelo` VALUES ('1918', '146', 'EC 400 FSE');
INSERT INTO `modelo` VALUES ('1919', '146', 'EC 450');
INSERT INTO `modelo` VALUES ('1920', '146', 'EC 50 ROOKIE');
INSERT INTO `modelo` VALUES ('1921', '146', 'FSE 450');
INSERT INTO `modelo` VALUES ('1922', '146', 'FSR 450');
INSERT INTO `modelo` VALUES ('1923', '146', 'MC 125');
INSERT INTO `modelo` VALUES ('1924', '146', 'MC 250');
INSERT INTO `modelo` VALUES ('1925', '146', 'PAMPERA 125');
INSERT INTO `modelo` VALUES ('1926', '146', 'PAMPERA 250');
INSERT INTO `modelo` VALUES ('1927', '146', 'PAMPERA 450');
INSERT INTO `modelo` VALUES ('1928', '146', 'SM 125');
INSERT INTO `modelo` VALUES ('1929', '146', 'SM 250');
INSERT INTO `modelo` VALUES ('1930', '146', 'SM 450');
INSERT INTO `modelo` VALUES ('1931', '146', 'SM 450 FSE');
INSERT INTO `modelo` VALUES ('1932', '146', 'SM 50');
INSERT INTO `modelo` VALUES ('1933', '146', 'TX BOY 50');
INSERT INTO `modelo` VALUES ('1934', '146', 'TXT 125 PRO');
INSERT INTO `modelo` VALUES ('1935', '146', 'TXT 200 Contact');
INSERT INTO `modelo` VALUES ('1936', '146', 'TXT 280');
INSERT INTO `modelo` VALUES ('1937', '146', 'TXT 280 Contact');
INSERT INTO `modelo` VALUES ('1938', '146', 'TXT 280 PRO');
INSERT INTO `modelo` VALUES ('1939', '146', 'TXT 321 Contact');
INSERT INTO `modelo` VALUES ('1940', '146', 'TXT 50 ROOKIE');
INSERT INTO `modelo` VALUES ('1941', '146', 'TXT BOY 50');
INSERT INTO `modelo` VALUES ('1942', '146', 'TXT PRO 250');
INSERT INTO `modelo` VALUES ('1943', '146', 'TXT PRO 300');
INSERT INTO `modelo` VALUES ('1944', '147', 'NEXUS 500');
INSERT INTO `modelo` VALUES ('1945', '148', 'EASY 110');
INSERT INTO `modelo` VALUES ('1946', '148', 'RUNNER 125');
INSERT INTO `modelo` VALUES ('1947', '148', 'SAFARI 150');
INSERT INTO `modelo` VALUES ('1948', '148', 'SPORT 150');
INSERT INTO `modelo` VALUES ('1949', '149', 'STELVIO 1200 ABS');
INSERT INTO `modelo` VALUES ('1950', '150', 'HB 110-3');
INSERT INTO `modelo` VALUES ('1951', '150', 'HB 125-9');
INSERT INTO `modelo` VALUES ('1952', '150', 'HB 150');
INSERT INTO `modelo` VALUES ('1953', '150', 'HB 150T');
INSERT INTO `modelo` VALUES ('1954', '150', 'HB 50Q');
INSERT INTO `modelo` VALUES ('1955', '152', 'LEGION 125');
INSERT INTO `modelo` VALUES ('1956', '154', 'FE 400');
INSERT INTO `modelo` VALUES ('1957', '154', 'FE 501');
INSERT INTO `modelo` VALUES ('1958', '154', 'FE 550E');
INSERT INTO `modelo` VALUES ('1959', '155', '510 CONTINENTAL');
INSERT INTO `modelo` VALUES ('1960', '155', 'CR 125');
INSERT INTO `modelo` VALUES ('1961', '155', 'CR 250');
INSERT INTO `modelo` VALUES ('1962', '155', 'HUSQY BOY J');
INSERT INTO `modelo` VALUES ('1963', '155', 'HUSQY BOY R');
INSERT INTO `modelo` VALUES ('1964', '155', 'HUSQY BOY S');
INSERT INTO `modelo` VALUES ('1965', '155', 'SM 125S');
INSERT INTO `modelo` VALUES ('1966', '155', 'SM 450R');
INSERT INTO `modelo` VALUES ('1967', '155', 'SM 450RR');
INSERT INTO `modelo` VALUES ('1968', '155', 'SM 510R');
INSERT INTO `modelo` VALUES ('1969', '155', 'SM 610');
INSERT INTO `modelo` VALUES ('1970', '155', 'SM 610S');
INSERT INTO `modelo` VALUES ('1971', '155', 'SMR 570R');
INSERT INTO `modelo` VALUES ('1972', '155', 'SMR 630');
INSERT INTO `modelo` VALUES ('1973', '155', 'TC 250');
INSERT INTO `modelo` VALUES ('1974', '155', 'TC 450');
INSERT INTO `modelo` VALUES ('1975', '155', 'TC 510');
INSERT INTO `modelo` VALUES ('1976', '155', 'TC 570');
INSERT INTO `modelo` VALUES ('1977', '155', 'TE 250');
INSERT INTO `modelo` VALUES ('1978', '155', 'TE 400');
INSERT INTO `modelo` VALUES ('1979', '155', 'TE 410');
INSERT INTO `modelo` VALUES ('1980', '155', 'TE 410E');
INSERT INTO `modelo` VALUES ('1981', '155', 'TE 450');
INSERT INTO `modelo` VALUES ('1982', '155', 'TE 510');
INSERT INTO `modelo` VALUES ('1983', '155', 'TE 570');
INSERT INTO `modelo` VALUES ('1984', '155', 'TE 610');
INSERT INTO `modelo` VALUES ('1985', '155', 'TE 610E');
INSERT INTO `modelo` VALUES ('1986', '155', 'WR 125');
INSERT INTO `modelo` VALUES ('1987', '155', 'WR 125 CUP');
INSERT INTO `modelo` VALUES ('1988', '155', 'WR 250');
INSERT INTO `modelo` VALUES ('1989', '155', 'WR 360');
INSERT INTO `modelo` VALUES ('1990', '155', 'WRE 125');
INSERT INTO `modelo` VALUES ('1991', '156', 'ACTION 100 ES');
INSERT INTO `modelo` VALUES ('1992', '156', 'ACTION 100 ESA');
INSERT INTO `modelo` VALUES ('1993', '156', 'BRAVE 150');
INSERT INTO `modelo` VALUES ('1994', '156', 'BRAVE 450R');
INSERT INTO `modelo` VALUES ('1995', '156', 'BRAVE 70');
INSERT INTO `modelo` VALUES ('1996', '156', 'MATRIX 150');
INSERT INTO `modelo` VALUES ('1997', '156', 'MOVING 125 ES');
INSERT INTO `modelo` VALUES ('1998', '156', 'MOVING 125 ESD');
INSERT INTO `modelo` VALUES ('1999', '156', 'ONE 125 ES');
INSERT INTO `modelo` VALUES ('2000', '156', 'ONE 125 ESD');
INSERT INTO `modelo` VALUES ('2001', '156', 'ONE 125 EX');
INSERT INTO `modelo` VALUES ('2002', '156', 'VINTAGE 150');
INSERT INTO `modelo` VALUES ('2003', '156', 'WARRIOR 250');
INSERT INTO `modelo` VALUES ('2004', '156', 'WARRIOR 400 4x4');
INSERT INTO `modelo` VALUES ('2005', '156', 'WARRIOR 700 4x4');
INSERT INTO `modelo` VALUES ('2006', '157', '350 CHOPPER');
INSERT INTO `modelo` VALUES ('2007', '158', 'JP 150');
INSERT INTO `modelo` VALUES ('2008', '159', 'ATLANTIC 150');
INSERT INTO `modelo` VALUES ('2009', '159', 'ATLANTIC 250');
INSERT INTO `modelo` VALUES ('2010', '159', 'HYPE 110');
INSERT INTO `modelo` VALUES ('2011', '159', 'HYPE 50');
INSERT INTO `modelo` VALUES ('2012', '159', 'JONNY 50');
INSERT INTO `modelo` VALUES ('2013', '159', 'ORBIT 150');
INSERT INTO `modelo` VALUES ('2014', '159', 'PEGASUS 150');
INSERT INTO `modelo` VALUES ('2015', '159', 'QUICK 150');
INSERT INTO `modelo` VALUES ('2016', '159', 'TR 150');
INSERT INTO `modelo` VALUES ('2017', '160', '125 K-LOG');
INSERT INTO `modelo` VALUES ('2018', '160', '125 K-TOP');
INSERT INTO `modelo` VALUES ('2019', '160', '125 TOP');
INSERT INTO `modelo` VALUES ('2020', '160', '250 DUAL');
INSERT INTO `modelo` VALUES ('2021', '160', 'LJ-16');
INSERT INTO `modelo` VALUES ('2022', '163', 'DJW 50');
INSERT INTO `modelo` VALUES ('2023', '163', 'M BOY 90');
INSERT INTO `modelo` VALUES ('2024', '163', 'PEOPLE 50');
INSERT INTO `modelo` VALUES ('2025', '163', 'ZING 150');
INSERT INTO `modelo` VALUES ('2026', '164', 'VIP 350 EL');
INSERT INTO `modelo` VALUES ('2027', '165', '990 ADVENTURE ABS DAKAR');
INSERT INTO `modelo` VALUES ('2028', '165', 'ADVENTURE 640');
INSERT INTO `modelo` VALUES ('2029', '165', 'ADVENTURE 950');
INSERT INTO `modelo` VALUES ('2030', '165', 'ADVENTURE 990');
INSERT INTO `modelo` VALUES ('2031', '165', 'ADVENTURE 990 DAKAR');
INSERT INTO `modelo` VALUES ('2032', '165', 'DUKE 640');
INSERT INTO `modelo` VALUES ('2033', '165', 'DUKE II 640');
INSERT INTO `modelo` VALUES ('2034', '165', 'ENDURO 690');
INSERT INTO `modelo` VALUES ('2035', '165', 'ENDURO 690R');
INSERT INTO `modelo` VALUES ('2036', '165', 'EXC 125');
INSERT INTO `modelo` VALUES ('2037', '165', 'EXC 200');
INSERT INTO `modelo` VALUES ('2038', '165', 'EXC 250');
INSERT INTO `modelo` VALUES ('2039', '165', 'EXC 250 2T');
INSERT INTO `modelo` VALUES ('2040', '165', 'EXC 250 4T');
INSERT INTO `modelo` VALUES ('2041', '165', 'EXC 250F');
INSERT INTO `modelo` VALUES ('2042', '165', 'EXC 300');
INSERT INTO `modelo` VALUES ('2043', '165', 'EXC 380');
INSERT INTO `modelo` VALUES ('2044', '165', 'EXC 400');
INSERT INTO `modelo` VALUES ('2045', '165', 'EXC 450');
INSERT INTO `modelo` VALUES ('2046', '165', 'EXC 450R');
INSERT INTO `modelo` VALUES ('2047', '165', 'EXC 520');
INSERT INTO `modelo` VALUES ('2048', '165', 'EXC 525');
INSERT INTO `modelo` VALUES ('2049', '165', 'EXC 530R');
INSERT INTO `modelo` VALUES ('2050', '165', 'LC4 640');
INSERT INTO `modelo` VALUES ('2051', '165', 'RC8 1190');
INSERT INTO `modelo` VALUES ('2052', '165', 'RC8 R');
INSERT INTO `modelo` VALUES ('2053', '165', 'SC 400');
INSERT INTO `modelo` VALUES ('2054', '165', 'SC 620');
INSERT INTO `modelo` VALUES ('2055', '165', 'SUPER ENDURO 950');
INSERT INTO `modelo` VALUES ('2056', '165', 'SUPERDUKE 990');
INSERT INTO `modelo` VALUES ('2057', '165', 'SUPERMOTO 690');
INSERT INTO `modelo` VALUES ('2058', '165', 'SUPERMOTO 950R');
INSERT INTO `modelo` VALUES ('2059', '165', 'SUPERMOTO 990T');
INSERT INTO `modelo` VALUES ('2060', '165', 'SX 125');
INSERT INTO `modelo` VALUES ('2061', '165', 'SX 150');
INSERT INTO `modelo` VALUES ('2062', '165', 'SX 250');
INSERT INTO `modelo` VALUES ('2063', '165', 'SX 250F');
INSERT INTO `modelo` VALUES ('2064', '165', 'SX 350F');
INSERT INTO `modelo` VALUES ('2065', '165', 'SX 450');
INSERT INTO `modelo` VALUES ('2066', '165', 'SX 450F');
INSERT INTO `modelo` VALUES ('2067', '165', 'SX 50');
INSERT INTO `modelo` VALUES ('2068', '165', 'SX 65');
INSERT INTO `modelo` VALUES ('2069', '165', 'SX 85');
INSERT INTO `modelo` VALUES ('2070', '165', 'SXC 520');
INSERT INTO `modelo` VALUES ('2071', '165', 'SXC 625');
INSERT INTO `modelo` VALUES ('2072', '165', 'XCF 250W');
INSERT INTO `modelo` VALUES ('2073', '166', 'AKROS 50');
INSERT INTO `modelo` VALUES ('2074', '166', 'AKROS 90');
INSERT INTO `modelo` VALUES ('2075', '166', 'ERGON 50');
INSERT INTO `modelo` VALUES ('2076', '166', 'NIX 50');
INSERT INTO `modelo` VALUES ('2077', '166', 'PALIO 50');
INSERT INTO `modelo` VALUES ('2078', '167', 'LD 150');
INSERT INTO `modelo` VALUES ('2079', '168', 'ML 200 CHOPPER');
INSERT INTO `modelo` VALUES ('2080', '169', 'LY 125T-5');
INSERT INTO `modelo` VALUES ('2081', '169', 'LY 150T-5');
INSERT INTO `modelo` VALUES ('2082', '169', 'LY 150T-6A');
INSERT INTO `modelo` VALUES ('2083', '169', 'LY 150T-7A');
INSERT INTO `modelo` VALUES ('2084', '169', 'ZN 151F DELUXE');
INSERT INTO `modelo` VALUES ('2085', '169', 'ZN 152F DELUXE');
INSERT INTO `modelo` VALUES ('2086', '170', 'CIAK MASTER 200');
INSERT INTO `modelo` VALUES ('2087', '170', 'FIREFOX F15');
INSERT INTO `modelo` VALUES ('2088', '170', 'GRIZZLY 12');
INSERT INTO `modelo` VALUES ('2089', '170', 'PASSWORD 250');
INSERT INTO `modelo` VALUES ('2090', '170', 'SPIDERMAX GT500');
INSERT INTO `modelo` VALUES ('2091', '171', 'MBW125');
INSERT INTO `modelo` VALUES ('2092', '171', 'MBW150');
INSERT INTO `modelo` VALUES ('2093', '171', 'MBW250');
INSERT INTO `modelo` VALUES ('2094', '171', 'MBW50');
INSERT INTO `modelo` VALUES ('2095', '171', 'MBWSUPER150');
INSERT INTO `modelo` VALUES ('2096', '172', 'DRAGOLJ150-7');
INSERT INTO `modelo` VALUES ('2097', '172', 'EASYLJ110-10');
INSERT INTO `modelo` VALUES ('2098', '172', 'FASTLJ150-2');
INSERT INTO `modelo` VALUES ('2099', '172', 'SKEMALJ125-6');
INSERT INTO `modelo` VALUES ('2100', '172', 'VITELJ150T-7');
INSERT INTO `modelo` VALUES ('2101', '173', 'H6');
INSERT INTO `modelo` VALUES ('2102', '174', 'BELLAGIO');
INSERT INTO `modelo` VALUES ('2103', '174', 'CALIFÓRNIAEV');
INSERT INTO `modelo` VALUES ('2104', '174', 'CALIFÓRNIAJACKAL');
INSERT INTO `modelo` VALUES ('2105', '174', 'CALIFÓRNIASPECIAL');
INSERT INTO `modelo` VALUES ('2106', '174', 'CALIFÓRNIASTONE');
INSERT INTO `modelo` VALUES ('2107', '174', 'QUOTA1100ES');
INSERT INTO `modelo` VALUES ('2108', '174', 'V11LEMANS');
INSERT INTO `modelo` VALUES ('2109', '174', 'V11SPORT');
INSERT INTO `modelo` VALUES ('2110', '175', 'CUB110');
INSERT INTO `modelo` VALUES ('2111', '175', 'S1000');
INSERT INTO `modelo` VALUES ('2112', '175', 'S500');
INSERT INTO `modelo` VALUES ('2113', '175', 'S800');
INSERT INTO `modelo` VALUES ('2114', '175', 'SCO150');
INSERT INTO `modelo` VALUES ('2115', '175', 'SCO50');
INSERT INTO `modelo` VALUES ('2116', '175', 'SS500');
INSERT INTO `modelo` VALUES ('2117', '175', 'SS800');
INSERT INTO `modelo` VALUES ('2118', '175', 'V1500');
INSERT INTO `modelo` VALUES ('2119', '175', 'V500');
INSERT INTO `modelo` VALUES ('2120', '176', 'MOTOVI 1200');
INSERT INTO `modelo` VALUES ('2121', '177', '230SUPERMOTARD');
INSERT INTO `modelo` VALUES ('2122', '177', '230RSUPERCROSS');
INSERT INTO `modelo` VALUES ('2123', '177', 'BLACKJACK');
INSERT INTO `modelo` VALUES ('2124', '178', 'BRUTALE910');
INSERT INTO `modelo` VALUES ('2125', '178', 'BRUTALE910R');
INSERT INTO `modelo` VALUES ('2126', '178', 'BRUTALE910S');
INSERT INTO `modelo` VALUES ('2127', '178', 'BRUTALEAMERICA');
INSERT INTO `modelo` VALUES ('2128', '178', 'BRUTALEGLADIO');
INSERT INTO `modelo` VALUES ('2129', '178', 'BRUTALES');
INSERT INTO `modelo` VALUES ('2130', '178', 'BRUTALESTARFIGHTER');
INSERT INTO `modelo` VALUES ('2131', '178', 'BRUTALESTARFIGHTERTITANIU');
INSERT INTO `modelo` VALUES ('2132', '178', 'F410001+1');
INSERT INTO `modelo` VALUES ('2133', '178', 'F41000R');
INSERT INTO `modelo` VALUES ('2134', '178', 'F41000R1+1');
INSERT INTO `modelo` VALUES ('2135', '178', 'F41000S');
INSERT INTO `modelo` VALUES ('2136', '178', 'F4312R');
INSERT INTO `modelo` VALUES ('2137', '178', 'F4312R1+1');
INSERT INTO `modelo` VALUES ('2138', '178', 'F4750');
INSERT INTO `modelo` VALUES ('2139', '178', 'F4750S');
INSERT INTO `modelo` VALUES ('2140', '178', 'F4CORSE');
INSERT INTO `modelo` VALUES ('2141', '178', 'F4MAMBA');
INSERT INTO `modelo` VALUES ('2142', '178', 'F4SENNA');
INSERT INTO `modelo` VALUES ('2143', '178', 'F4TAMBURINI');
INSERT INTO `modelo` VALUES ('2144', '178', 'F4VELTRO');
INSERT INTO `modelo` VALUES ('2145', '179', 'BIGFORCE250');
INSERT INTO `modelo` VALUES ('2146', '179', 'BIGFORCE250II');
INSERT INTO `modelo` VALUES ('2147', '179', 'BLACKJACK');
INSERT INTO `modelo` VALUES ('2148', '179', 'BLACKSTAR125');
INSERT INTO `modelo` VALUES ('2149', '179', 'BLACKSTAR150');
INSERT INTO `modelo` VALUES ('2150', '179', 'BRX140');
INSERT INTO `modelo` VALUES ('2151', '179', 'BRX250');
INSERT INTO `modelo` VALUES ('2152', '179', 'DUAL200');
INSERT INTO `modelo` VALUES ('2153', '179', 'FENIX200');
INSERT INTO `modelo` VALUES ('2154', '179', 'FENIXGOLD');
INSERT INTO `modelo` VALUES ('2155', '179', 'FOX100');
INSERT INTO `modelo` VALUES ('2156', '179', 'FOX110');
INSERT INTO `modelo` VALUES ('2157', '179', 'GO100');
INSERT INTO `modelo` VALUES ('2158', '179', 'HALLEY150');
INSERT INTO `modelo` VALUES ('2159', '179', 'HALLEY200');
INSERT INTO `modelo` VALUES ('2160', '179', 'JURASIC300');
INSERT INTO `modelo` VALUES ('2161', '179', 'MA1003B');
INSERT INTO `modelo` VALUES ('2162', '179', 'MA125GY');
INSERT INTO `modelo` VALUES ('2163', '179', 'SIMBA50');
INSERT INTO `modelo` VALUES ('2164', '179', 'SPORT110');
INSERT INTO `modelo` VALUES ('2165', '179', 'SPORT150');
INSERT INTO `modelo` VALUES ('2166', '179', 'SPYDER270');
INSERT INTO `modelo` VALUES ('2167', '179', 'SPYDER320');
INSERT INTO `modelo` VALUES ('2168', '179', 'STREET150');
INSERT INTO `modelo` VALUES ('2169', '179', 'SUPER125');
INSERT INTO `modelo` VALUES ('2170', '179', 'SUPERX125R');
INSERT INTO `modelo` VALUES ('2171', '179', 'WY50QT-3SCOOTER');
INSERT INTO `modelo` VALUES ('2172', '179', 'X-CAPE200');
INSERT INTO `modelo` VALUES ('2173', '179', 'XRT110');
INSERT INTO `modelo` VALUES ('2174', '180', 'AX100');
INSERT INTO `modelo` VALUES ('2175', '180', 'AX100-A');
INSERT INTO `modelo` VALUES ('2176', '180', 'JC125');
INSERT INTO `modelo` VALUES ('2177', '180', 'JC125-2');
INSERT INTO `modelo` VALUES ('2178', '180', 'JC125-8');
INSERT INTO `modelo` VALUES ('2179', '180', 'QM50');
INSERT INTO `modelo` VALUES ('2180', '181', 'BR III');
INSERT INTO `modelo` VALUES ('2181', '182', 'BUXY50');
INSERT INTO `modelo` VALUES ('2182', '182', 'ELYSEO125');
INSERT INTO `modelo` VALUES ('2183', '182', 'SCOOTELEC');
INSERT INTO `modelo` VALUES ('2184', '182', 'SPEEDAKE50');
INSERT INTO `modelo` VALUES ('2185', '182', 'SPEEDFIGHT100');
INSERT INTO `modelo` VALUES ('2186', '182', 'SPEEDFIGHT50');
INSERT INTO `modelo` VALUES ('2187', '182', 'SPEEDFIGHT50LC');
INSERT INTO `modelo` VALUES ('2188', '182', 'SQUAB50');
INSERT INTO `modelo` VALUES ('2189', '182', 'TREKKER100');
INSERT INTO `modelo` VALUES ('2190', '182', 'VIVACITY50');
INSERT INTO `modelo` VALUES ('2191', '182', 'ZENITH50');
INSERT INTO `modelo` VALUES ('2192', '183', 'BEVERLY200');
INSERT INTO `modelo` VALUES ('2193', '183', 'BEVERLY250');
INSERT INTO `modelo` VALUES ('2194', '183', 'BEVERLY500');
INSERT INTO `modelo` VALUES ('2195', '183', 'BEVERLYCRUISER250');
INSERT INTO `modelo` VALUES ('2196', '183', 'BEVERLYCRUISER500');
INSERT INTO `modelo` VALUES ('2197', '183', 'BEVERLYTOURER300');
INSERT INTO `modelo` VALUES ('2198', '183', 'LIBERTY200');
INSERT INTO `modelo` VALUES ('2199', '183', 'LIBERTYS200');
INSERT INTO `modelo` VALUES ('2200', '183', 'MODELOBERVELY500');
INSERT INTO `modelo` VALUES ('2201', '183', 'MP3400RL');
INSERT INTO `modelo` VALUES ('2202', '183', 'NRGMC3DD');
INSERT INTO `modelo` VALUES ('2203', '183', 'RUNNER50');
INSERT INTO `modelo` VALUES ('2204', '183', 'VESPAGTS250I.E');
INSERT INTO `modelo` VALUES ('2205', '183', 'VESPAGTS250I.EABS');
INSERT INTO `modelo` VALUES ('2206', '183', 'VESPAGTV250I.E');
INSERT INTO `modelo` VALUES ('2207', '183', 'VESPALX125');
INSERT INTO `modelo` VALUES ('2208', '183', 'VESPALX150');
INSERT INTO `modelo` VALUES ('2209', '183', 'VESPAPX150ORIGINALE');
INSERT INTO `modelo` VALUES ('2210', '183', 'VESPAPX200');
INSERT INTO `modelo` VALUES ('2211', '183', 'ZIP50');
INSERT INTO `modelo` VALUES ('2212', '184', 'TRAIL BROSS 325');
INSERT INTO `modelo` VALUES ('2213', '185', 'BLACKJACK');
INSERT INTO `modelo` VALUES ('2214', '185', 'FENIXGOLD');
INSERT INTO `modelo` VALUES ('2215', '185', 'GHOST');
INSERT INTO `modelo` VALUES ('2216', '185', 'GHOST320');
INSERT INTO `modelo` VALUES ('2217', '185', 'SPYDER270');
INSERT INTO `modelo` VALUES ('2218', '185', 'SPYDER320');
INSERT INTO `modelo` VALUES ('2219', '186', 'TRC01 VERSÃO I');
INSERT INTO `modelo` VALUES ('2220', '186', 'TRC01 VERSÃO II');
INSERT INTO `modelo` VALUES ('2221', '187', 'MADASS');
INSERT INTO `modelo` VALUES ('2222', '188', 'ENJOY50');
INSERT INTO `modelo` VALUES ('2223', '188', 'HUSKY150');
INSERT INTO `modelo` VALUES ('2224', '188', 'PASSING110');
INSERT INTO `modelo` VALUES ('2225', '189', 'WY150-EX');
INSERT INTO `modelo` VALUES ('2226', '189', 'XY50Q');
INSERT INTO `modelo` VALUES ('2227', '189', 'XY50QPHOENIX');
INSERT INTO `modelo` VALUES ('2228', '189', 'XY110-VWAVE');
INSERT INTO `modelo` VALUES ('2229', '189', 'XY125-14');
INSERT INTO `modelo` VALUES ('2230', '189', 'XY150-5SPEED');
INSERT INTO `modelo` VALUES ('2231', '189', 'XY150-STA');
INSERT INTO `modelo` VALUES ('2232', '189', 'XY150GY');
INSERT INTO `modelo` VALUES ('2233', '189', 'XY200lll');
INSERT INTO `modelo` VALUES ('2234', '189', 'XY200-5');
INSERT INTO `modelo` VALUES ('2235', '189', 'XY250STVI');
INSERT INTO `modelo` VALUES ('2236', '189', 'XY250-5');
INSERT INTO `modelo` VALUES ('2237', '189', 'XY50Q-2');
INSERT INTO `modelo` VALUES ('2238', '190', 'AKROS 50');
INSERT INTO `modelo` VALUES ('2239', '193', 'BESTJH125G');
INSERT INTO `modelo` VALUES ('2240', '193', 'CJ50F');
INSERT INTO `modelo` VALUES ('2241', '193', 'FLYJH125L');
INSERT INTO `modelo` VALUES ('2242', '193', 'JH50');
INSERT INTO `modelo` VALUES ('2243', '193', 'JH70');
INSERT INTO `modelo` VALUES ('2244', '193', 'JH70III');
INSERT INTO `modelo` VALUES ('2245', '193', 'JOTO125');
INSERT INTO `modelo` VALUES ('2246', '193', 'JOTO135');
INSERT INTO `modelo` VALUES ('2247', '193', 'MONTEZ250');
INSERT INTO `modelo` VALUES ('2248', '193', 'PRINCEJL110-11');
INSERT INTO `modelo` VALUES ('2249', '193', 'SHARK250');
INSERT INTO `modelo` VALUES ('2250', '193', 'SKYJL110-3');
INSERT INTO `modelo` VALUES ('2251', '193', 'SKYJL110-8');
INSERT INTO `modelo` VALUES ('2252', '193', 'STARJL50Q-2');
INSERT INTO `modelo` VALUES ('2253', '193', 'VICO');
INSERT INTO `modelo` VALUES ('2254', '193', 'WORK125');
INSERT INTO `modelo` VALUES ('2255', '195', 'PHANTERAGT5');
INSERT INTO `modelo` VALUES ('2256', '195', 'PHANTOMGT5');
INSERT INTO `modelo` VALUES ('2257', '195', 'PHANTOMR4');
INSERT INTO `modelo` VALUES ('2258', '195', 'REBELLIAN');
INSERT INTO `modelo` VALUES ('2259', '195', 'TRITONGT5');
INSERT INTO `modelo` VALUES ('2260', '195', 'TRITONR4');
INSERT INTO `modelo` VALUES ('2261', '195', 'WORKMAN');
INSERT INTO `modelo` VALUES ('2262', '195', 'ZIPGT5TURBOCAM');
INSERT INTO `modelo` VALUES ('2263', '195', 'ZIPR3ITURBOCAM');
INSERT INTO `modelo` VALUES ('2264', '196', 'WY125-ESD');
INSERT INTO `modelo` VALUES ('2265', '196', 'WY125-ESDPLUS');
INSERT INTO `modelo` VALUES ('2266', '196', 'WY150-EX');
INSERT INTO `modelo` VALUES ('2267', '198', 'YG200-9');
INSERT INTO `modelo` VALUES ('2268', '199', 'CHOPPER COBRA');
INSERT INTO `modelo` VALUES ('2269', '200', 'FOREST 150');
INSERT INTO `modelo` VALUES ('2270', '200', 'ZY 125 T5');

-- ----------------------------
-- Table structure for ordem_pedido
-- ----------------------------
DROP TABLE IF EXISTS `ordem_pedido`;
CREATE TABLE `ordem_pedido` (
  `ORDEM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FRANQUIA_ID` int(11) DEFAULT NULL,
  `PEDIDO_STATUS_ID` int(11) DEFAULT NULL,
  `DATA_PEDIDO` datetime DEFAULT NULL,
  `DATA_ENTREGA` datetime DEFAULT NULL,
  `CODIGO_PAGSEGURO` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ORDEM_ID`),
  KEY `FK_REL_FRANQUIA_ORDEM_PEDIDO` (`FRANQUIA_ID`),
  KEY `FK_REL_PEDIDOSTATUS_ORDEM_PEDIDO` (`PEDIDO_STATUS_ID`),
  CONSTRAINT `FK_REL_FRANQUIA_ORDEM_PEDIDO` FOREIGN KEY (`FRANQUIA_ID`) REFERENCES `franquia` (`FRANQUIA_ID`),
  CONSTRAINT `FK_REL_PEDIDOSTATUS_ORDEM_PEDIDO` FOREIGN KEY (`PEDIDO_STATUS_ID`) REFERENCES `pedido_status` (`PEDIDO_STATUS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ordem_pedido
-- ----------------------------
INSERT INTO `ordem_pedido` VALUES ('1', '3', '5', '2014-01-30 10:15:13', '2014-02-02 10:15:13', null);
INSERT INTO `ordem_pedido` VALUES ('2', '3', '2', '2014-01-31 10:39:32', '2014-02-03 10:39:32', null);
INSERT INTO `ordem_pedido` VALUES ('3', '3', '1', '2014-01-31 10:48:18', '2014-02-03 10:48:18', null);
INSERT INTO `ordem_pedido` VALUES ('4', '3', '1', '2014-01-31 11:01:55', '2014-02-03 11:01:55', null);
INSERT INTO `ordem_pedido` VALUES ('5', '3', '5', '2014-01-31 11:02:08', '2014-02-03 11:02:08', null);
INSERT INTO `ordem_pedido` VALUES ('6', '3', '1', '2014-01-31 14:55:06', '2014-02-03 14:55:06', null);
INSERT INTO `ordem_pedido` VALUES ('7', '3', '1', '2014-01-31 15:07:21', '2014-02-03 15:07:21', null);
INSERT INTO `ordem_pedido` VALUES ('8', '3', '1', '2014-01-31 15:22:20', '2014-02-03 15:22:20', null);
INSERT INTO `ordem_pedido` VALUES ('9', '3', '1', '2014-01-31 15:32:32', '2014-02-03 15:32:32', null);
INSERT INTO `ordem_pedido` VALUES ('10', '3', '1', '2014-01-31 15:48:07', '2014-02-03 15:48:07', null);
INSERT INTO `ordem_pedido` VALUES ('11', '3', '1', '2014-02-06 16:54:54', '2014-02-09 16:54:54', null);
INSERT INTO `ordem_pedido` VALUES ('12', '3', '1', '2014-02-06 17:11:52', '2014-02-09 17:11:52', null);
INSERT INTO `ordem_pedido` VALUES ('13', '3', '1', '2014-02-07 10:01:48', '2014-02-10 10:01:48', null);
INSERT INTO `ordem_pedido` VALUES ('14', '3', '1', '2014-02-07 10:22:09', '2014-02-10 10:22:09', null);
INSERT INTO `ordem_pedido` VALUES ('15', '3', '1', '2014-02-07 10:36:05', '2014-02-10 10:36:05', null);
INSERT INTO `ordem_pedido` VALUES ('16', '3', '1', '2014-02-07 10:49:37', '2014-02-10 10:49:37', null);
INSERT INTO `ordem_pedido` VALUES ('17', '3', '1', '2014-02-07 11:18:21', '2014-02-10 11:18:21', null);
INSERT INTO `ordem_pedido` VALUES ('18', '3', '4', '2014-02-13 13:36:40', '2014-02-13 13:36:42', null);
INSERT INTO `ordem_pedido` VALUES ('22', '3', '1', '2014-02-13 13:59:39', '2014-02-16 13:59:39', '87294A29C3C36EABB4647FA8FF85F8C0');

-- ----------------------------
-- Table structure for pedido_status
-- ----------------------------
DROP TABLE IF EXISTS `pedido_status`;
CREATE TABLE `pedido_status` (
  `PEDIDO_STATUS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `STATUS` varchar(25) NOT NULL,
  PRIMARY KEY (`PEDIDO_STATUS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pedido_status
-- ----------------------------
INSERT INTO `pedido_status` VALUES ('1', 'Aguardando Confirmação');
INSERT INTO `pedido_status` VALUES ('2', 'Cancelado Pelo Agencia ');
INSERT INTO `pedido_status` VALUES ('3', 'Cancelado');
INSERT INTO `pedido_status` VALUES ('4', 'Sendo  Despachado');
INSERT INTO `pedido_status` VALUES ('5', 'Confirmado');

-- ----------------------------
-- Table structure for permissao
-- ----------------------------
DROP TABLE IF EXISTS `permissao`;
CREATE TABLE `permissao` (
  `PERMISSAO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(30) DEFAULT NULL,
  `ROLE` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`PERMISSAO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permissao
-- ----------------------------
INSERT INTO `permissao` VALUES ('1', 'Admin', 'admin');
INSERT INTO `permissao` VALUES ('2', 'Cadastrado sem Franquia', 'cadsimples');
INSERT INTO `permissao` VALUES ('3', 'Franqueado', 'franqueado');
INSERT INTO `permissao` VALUES ('4', 'Inativo', 'inativo');
INSERT INTO `permissao` VALUES ('5', 'Desativado', 'desativado');

-- ----------------------------
-- Table structure for produto
-- ----------------------------
DROP TABLE IF EXISTS `produto`;
CREATE TABLE `produto` (
  `PRODUTO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FRANQUIA_ID` int(11) DEFAULT NULL,
  `NOME` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRICAO` text COLLATE utf8_unicode_ci NOT NULL,
  `IMAGEM1` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGEM2` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGEM3` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGEM4` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGEM5` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGEM6` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRECO` decimal(8,3) DEFAULT NULL,
  `DESCONTOPRODUTO` decimal(8,3) DEFAULT NULL,
  `DESCONTOCLIENTE` decimal(8,3) DEFAULT NULL,
  `DESCONTOFRANQUEADO` decimal(8,3) DEFAULT NULL,
  `LARGURA` float DEFAULT NULL,
  `ALTURA` float DEFAULT NULL,
  `COMPRIMENTO` float DEFAULT NULL,
  `PESO` float DEFAULT NULL,
  `PROMOCAO_ATIVA` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PRODUTO_ID`),
  KEY `FK_FRANQUIA_PRODUTO` (`FRANQUIA_ID`),
  CONSTRAINT `FK_FRANQUIA_PRODUTO` FOREIGN KEY (`FRANQUIA_ID`) REFERENCES `franquia` (`FRANQUIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of produto
-- ----------------------------
INSERT INTO `produto` VALUES ('1', null, 'Teste Produto 1', '            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.                  ', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c', '35.600', '0.000', '0.200', '0.300', '1', '1', '1', '1', null);
INSERT INTO `produto` VALUES ('2', null, 'Teste Produto 2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '', '47.950', '1.000', '0.200', '0.300', '1', '1', '1', '0.4', null);
INSERT INTO `produto` VALUES ('3', null, 'Teste Produto 3', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '', '12.500', '1.000', '0.200', '0.300', '1', '1', '1', '0.978', null);
INSERT INTO `produto` VALUES ('4', null, 'Teste Produto 4', 'Teste', '6f097d14a31b4ea65b098606f8f6d2b855e5bf2c.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '', '1.000', '0.000', '1.000', '1.000', '1', '1', '1', '1', null);
INSERT INTO `produto` VALUES ('5', null, 'Teste Produto 5', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c', '35.600', '0.000', '0.200', '0.300', '1', '1', '1', '0.7', null);
INSERT INTO `produto` VALUES ('6', null, 'Teste Produto 6', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '', '47.950', '1.000', '0.200', '0.300', '1', '1', '1', '0.4', null);
INSERT INTO `produto` VALUES ('7', null, 'Teste Produto 7', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '', '12.500', '1.000', '0.200', '0.300', '1', '1', '1', '40', null);
INSERT INTO `produto` VALUES ('8', null, 'Teste Produto 8', 'Teste', '6f097d14a31b4ea65b098606f8f6d2b855e5bf2c.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '', '1.000', '0.000', '1.000', '1.000', '1', '1', '1', '1', null);
INSERT INTO `produto` VALUES ('9', null, 'Teste Produto 9', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c', '35.600', '0.000', '0.200', '0.300', '1', '1', '1', '1', null);
INSERT INTO `produto` VALUES ('10', null, 'Teste Produto 10', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '', '47.950', '1.000', '0.200', '0.300', '1', '1', '1', '0.4', null);
INSERT INTO `produto` VALUES ('11', null, 'Teste Produto  11', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '', '12.500', '1.000', '0.200', '0.300', '1', '1', '1', '0.56', null);
INSERT INTO `produto` VALUES ('12', null, 'Teste Produto 12', 'Teste', '6f097d14a31b4ea65b098606f8f6d2b855e5bf2c.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '1f76c01516e842b485a2b7ed68faa704098145a0.jpg', '', '1.000', '0.000', '1.000', '1.000', '1', '1', '1', '1', null);
INSERT INTO `produto` VALUES ('13', null, '', '', '', '', '', '', '', '', '0.000', '0.000', '0.000', '0.000', '0', '0', '0', '0', null);
INSERT INTO `produto` VALUES ('14', null, 'Prod Teste', '455445', 'b46db7b2f493c31069bf87791eaf185fcbbecbb7.jpg', '2f95ffa782f940778abbe45fe0ac764d0ce2c117.jpg', '014228fe12faed1b991a8f9b693cb5845c2d7ec1.jpg', '83365cf90763db166f432d19bb40318388f360bb.jpg', 'ef2fbe36f2b0364dcb4da63b7a1cb8d8fe618549.jpg', 'd29de26fb4b86c9b917cc6402eec50da7c020a2d.jpg', '345.000', '0.000', '10.000', '34.000', '1', '1', '1', '1', null);
INSERT INTO `produto` VALUES ('15', null, 'Produto do teste', '  tester   ', 'f35fae54a5e0383242ac9ac6b92fcba21467b9a2.jpg', '8dde6e5f91413547c7a4333bebf9ff542b026a2d.jpg', '9b4160dc922c487e603e1a054d85a916008179b6.jpg', null, null, null, '1.000', '0.000', '0.200', '0.000', '1', '1', '1', '1', null);
INSERT INTO `produto` VALUES ('16', null, 'Produto Teste 100', 'Teste', '35da435aa97e29ab6deb780ba2b9321fe053b588.jpg', 'b322c40c320a6ea5281ff07eb0238a5d8cc30f56.jpg', null, null, null, null, '23.400', '0.000', '1.000', '1.000', '1', '1', '1', '1', null);

-- ----------------------------
-- Table structure for produto_categoria_subcategoria
-- ----------------------------
DROP TABLE IF EXISTS `produto_categoria_subcategoria`;
CREATE TABLE `produto_categoria_subcategoria` (
  `PRODUTO_ID` int(11) DEFAULT NULL,
  `SUBCATEGORIA_ID` int(11) DEFAULT NULL,
  `CATEGORIA_ID` int(11) DEFAULT NULL,
  KEY `FK_REL_CATEGORIA` (`CATEGORIA_ID`),
  KEY `FK_REL_PROD_CATEGORIA_SUBCATEGORIA` (`PRODUTO_ID`),
  KEY `FK_REL_SUBCATEGORIA_CATEGORIA` (`SUBCATEGORIA_ID`),
  CONSTRAINT `FK_REL_CATEGORIA` FOREIGN KEY (`CATEGORIA_ID`) REFERENCES `categoria` (`CATEGORIA_ID`),
  CONSTRAINT `FK_REL_PROD_CATEGORIA_SUBCATEGORIA` FOREIGN KEY (`PRODUTO_ID`) REFERENCES `produto` (`PRODUTO_ID`),
  CONSTRAINT `FK_REL_SUBCATEGORIA_CATEGORIA` FOREIGN KEY (`SUBCATEGORIA_ID`) REFERENCES `subcategoria` (`SUBCATEGORIA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of produto_categoria_subcategoria
-- ----------------------------
INSERT INTO `produto_categoria_subcategoria` VALUES ('1', '13', '3');
INSERT INTO `produto_categoria_subcategoria` VALUES ('15', '1', '3');
INSERT INTO `produto_categoria_subcategoria` VALUES ('16', '1', '3');

-- ----------------------------
-- Table structure for produto_ordem
-- ----------------------------
DROP TABLE IF EXISTS `produto_ordem`;
CREATE TABLE `produto_ordem` (
  `PRODUTO_ORDEM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDEM_ID` int(11) DEFAULT NULL,
  `QUANTIDADE` int(11) DEFAULT NULL,
  `REFERENCIA` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PRODUTO_ORDEM_ID`),
  KEY `FK_REL_ORDEM_PRODUTO` (`ORDEM_ID`),
  CONSTRAINT `FK_REL_ORDEM_PRODUTO` FOREIGN KEY (`ORDEM_ID`) REFERENCES `ordem_pedido` (`ORDEM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of produto_ordem
-- ----------------------------
INSERT INTO `produto_ordem` VALUES ('1', '1', '1', '0');
INSERT INTO `produto_ordem` VALUES ('2', '2', '1', '0');
INSERT INTO `produto_ordem` VALUES ('3', '2', '1', '0');
INSERT INTO `produto_ordem` VALUES ('4', '1', '1', '0');
INSERT INTO `produto_ordem` VALUES ('5', '1', '1', '0');
INSERT INTO `produto_ordem` VALUES ('6', '2', '1', '0');
INSERT INTO `produto_ordem` VALUES ('7', '3', '5', '0');
INSERT INTO `produto_ordem` VALUES ('8', '4', '1', '0');
INSERT INTO `produto_ordem` VALUES ('9', '5', '1', '0');
INSERT INTO `produto_ordem` VALUES ('10', '6', '2', '0');
INSERT INTO `produto_ordem` VALUES ('11', '6', '6', '0');
INSERT INTO `produto_ordem` VALUES ('12', '7', '5', '1');
INSERT INTO `produto_ordem` VALUES ('13', '8', '2', '1');
INSERT INTO `produto_ordem` VALUES ('14', '9', '1', '1');
INSERT INTO `produto_ordem` VALUES ('15', '10', '1', '0');
INSERT INTO `produto_ordem` VALUES ('16', '11', '23', '0');
INSERT INTO `produto_ordem` VALUES ('17', '12', '1', '0');
INSERT INTO `produto_ordem` VALUES ('18', '13', '4', '0');
INSERT INTO `produto_ordem` VALUES ('19', '14', '1', '0');
INSERT INTO `produto_ordem` VALUES ('20', '15', '1', '0');
INSERT INTO `produto_ordem` VALUES ('21', '15', '1', '0');
INSERT INTO `produto_ordem` VALUES ('22', '15', '1', '0');
INSERT INTO `produto_ordem` VALUES ('23', '16', '1', '0');
INSERT INTO `produto_ordem` VALUES ('24', '16', '6', '0');
INSERT INTO `produto_ordem` VALUES ('25', '17', '15', '0');
INSERT INTO `produto_ordem` VALUES ('31', '18', '1', '0');
INSERT INTO `produto_ordem` VALUES ('33', '22', '2', '0');

-- ----------------------------
-- Table structure for proprietario
-- ----------------------------
DROP TABLE IF EXISTS `proprietario`;
CREATE TABLE `proprietario` (
  `PROPRIETARIO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERMISSAO_ID` int(11) NOT NULL,
  `NOME` varchar(30) DEFAULT NULL,
  `SOBRENOME` varchar(70) DEFAULT NULL,
  `EMAIL_SECUNDARIO` varchar(60) DEFAULT NULL,
  `LOGIN` varchar(60) NOT NULL,
  `SENHA` varchar(64) NOT NULL,
  `CPF` varchar(12) NOT NULL,
  `ESTADO_CIVIL` smallint(6) DEFAULT NULL,
  `RG` varchar(12) NOT NULL,
  `DATA_REGISTRO` datetime NOT NULL,
  `TELCOMERCIAL` varchar(18) DEFAULT NULL,
  `TELRESIDENCIAL` varchar(18) DEFAULT NULL,
  `TELCELULAR` varchar(18) DEFAULT NULL,
  `SEXO` char(1) DEFAULT NULL,
  `DATA_NASCIMENTO` date NOT NULL,
  `CEPFRANQUIA` char(12) DEFAULT NULL,
  `COMPLEMENTO` varchar(12) DEFAULT NULL,
  `ENDERECO` char(255) DEFAULT NULL,
  `NUMERO` smallint(6) DEFAULT NULL,
  `BAIRRO` varchar(60) DEFAULT NULL,
  `CIDADE` char(30) DEFAULT NULL,
  `ESTADO` char(2) DEFAULT NULL,
  `PAIS` char(18) DEFAULT NULL,
  `CODIGOEMAIL` varchar(64) DEFAULT NULL,
  `CODIGO_CADASTRO` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`PROPRIETARIO_ID`),
  KEY `FK_PERMISSAO_PROPRIETARIO` (`PERMISSAO_ID`),
  CONSTRAINT `FK_PERMISSAO_PROPRIETARIO` FOREIGN KEY (`PERMISSAO_ID`) REFERENCES `permissao` (`PERMISSAO_ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of proprietario
-- ----------------------------
INSERT INTO `proprietario` VALUES ('1', '1', 'Nicholas', 'Arimateia', 'noemail@email.com', 'nicholasluis@gmail.com', '$2y$10$7f4nLOblZDI6SojwebxwdeQ1MzdXeGAi0NkJy3HJ83UsO3dcjN0Z.', '3408074885', '1', '410717241', '0000-00-00 00:00:00', '(34) 4545-6565', '', '(65) 6567-7676', 'F', '1986-06-07', '12422-230', 'Casa', 'Rua Soldado Francisco Rodrigues', null, 'Residencial Campos Maia', 'Pindamonhangaba', 'SP', null, '', null);
INSERT INTO `proprietario` VALUES ('3', '2', 'Joséaa', 'Vieira', 'oioioiuis@gmail.com', 'jose@gmail.com', '$2y$10$DVV11B7vrtFe6rdpizJbp.mS8DQlwuiTpkbCLtV4rxVz5bX9PXdha', '348080748', '1', '410717242', '0000-00-00 00:00:00', '(45) 4554-5454', '', '(54) 5445-4554', 'F', '1991-10-11', '12422-230', 'CASAAAAA', 'Rua Soldado Francisco Rodrigues', null, 'Residencial Campos Maia', 'Pindamonhangaba', 'AC', 'Brasil', '', null);
INSERT INTO `proprietario` VALUES ('33', '2', 'Nicholartrtrtrtrtrt', 'Oliveira', 'noemail@email.co', 'joaocarlos@gmail.com', '$2y$10$EMtlOvFbMy/CYbMNWrhzHe5PG703wosJFEDFlW0aMCNISB1Jr.FAO', '34808074885', '1', '410717243', '0000-00-00 00:00:00', '(34) 4545-6565', '(11) 2323-4344', '(65) 6567-7676', 'F', '1986-06-07', '12422230', 'Casa', 'Rua Soldado Francisco Rodrigues', null, 'Residencial Campos Maia', 'Pindamonhangaba', 'SP', 'Brasil', '', null);
INSERT INTO `proprietario` VALUES ('34', '2', 'Julio', 'Menezes', 'oioioiuis@gmail.com', '', '', '11122233345', '0', '465677678', '0000-00-00 00:00:00', '(45) 4556-6776', '', '(23) 3445-6566', 'F', '0000-00-00', '12422230', 'Casa', 'Rua Soldado Francisco Rodrigues', null, 'Residencial Campos Maia', 'Pindamonhangaba', 'AC', 'Brasil', '', null);
INSERT INTO `proprietario` VALUES ('38', '2', 'Franquia Testea', 'trtrrt', null, 'teste@abrasil.com', '$2a$08$NDQzNTk2NDI4NTMwY2NkM.HrnkCqYuZd2glu53XNntDZKYI5QPa9a', '54544545545', '1', '545445545', '2014-02-25 14:04:06', '', '(33) 4443-7676', '', 'M', '2014-02-05', '12422-230', 'casa', 'Rua Soldado Francisco Rodrigues', null, 'Residencial Campos Maia', 'Pindamonhangaba', 'RN', 'Brasil', null, null);

-- ----------------------------
-- Table structure for rel_cliente_ordem_pedido
-- ----------------------------
DROP TABLE IF EXISTS `rel_cliente_ordem_pedido`;
CREATE TABLE `rel_cliente_ordem_pedido` (
  `CLIENTE_ID` int(11) NOT NULL,
  `ORDEM_ID` int(11) NOT NULL,
  PRIMARY KEY (`CLIENTE_ID`,`ORDEM_ID`),
  KEY `FK_REL_CLIENTE_ORDEM_PEDIDO2` (`ORDEM_ID`),
  CONSTRAINT `FK_REL_CLIENTE_ORDEM_PEDIDO` FOREIGN KEY (`CLIENTE_ID`) REFERENCES `cliente` (`CLIENTE_ID`),
  CONSTRAINT `FK_REL_CLIENTE_ORDEM_PEDIDO2` FOREIGN KEY (`ORDEM_ID`) REFERENCES `ordem_pedido` (`ORDEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rel_cliente_ordem_pedido
-- ----------------------------
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '1');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '2');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '3');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '4');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '5');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '6');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '7');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '8');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '9');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '10');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '11');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '12');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '13');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '14');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '15');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '16');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('2', '17');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('1', '18');
INSERT INTO `rel_cliente_ordem_pedido` VALUES ('2', '22');

-- ----------------------------
-- Table structure for rel_endereco_cliente
-- ----------------------------
DROP TABLE IF EXISTS `rel_endereco_cliente`;
CREATE TABLE `rel_endereco_cliente` (
  `ENDERECO_ID` int(11) NOT NULL,
  `CLIENTE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ENDERECO_ID`,`CLIENTE_ID`),
  KEY `FK_REL_ENDERECO_CLIENTE2` (`CLIENTE_ID`),
  CONSTRAINT `FK_REL_ENDERECO_CLIENTE` FOREIGN KEY (`ENDERECO_ID`) REFERENCES `enderecos` (`ENDERECO_ID`),
  CONSTRAINT `FK_REL_ENDERECO_CLIENTE2` FOREIGN KEY (`CLIENTE_ID`) REFERENCES `cliente` (`CLIENTE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rel_endereco_cliente
-- ----------------------------
INSERT INTO `rel_endereco_cliente` VALUES ('1', '1');
INSERT INTO `rel_endereco_cliente` VALUES ('2', '1');
INSERT INTO `rel_endereco_cliente` VALUES ('4', '1');
INSERT INTO `rel_endereco_cliente` VALUES ('3', '2');

-- ----------------------------
-- Table structure for rel_produto_ordem_pedido
-- ----------------------------
DROP TABLE IF EXISTS `rel_produto_ordem_pedido`;
CREATE TABLE `rel_produto_ordem_pedido` (
  `PRODUTO_ID` int(11) NOT NULL,
  `PRODUTO_ORDEM_ID` int(11) NOT NULL,
  PRIMARY KEY (`PRODUTO_ID`,`PRODUTO_ORDEM_ID`),
  KEY `FK_REL_PRODUTO_ORDEM_PEDIDO2` (`PRODUTO_ORDEM_ID`),
  CONSTRAINT `FK_REL_PRODUTO_ORDEM_PEDIDO` FOREIGN KEY (`PRODUTO_ID`) REFERENCES `produto` (`PRODUTO_ID`),
  CONSTRAINT `FK_REL_PRODUTO_ORDEM_PEDIDO2` FOREIGN KEY (`PRODUTO_ORDEM_ID`) REFERENCES `produto_ordem` (`PRODUTO_ORDEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rel_produto_ordem_pedido
-- ----------------------------
INSERT INTO `rel_produto_ordem_pedido` VALUES ('2', '4');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('1', '5');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('1', '6');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('1', '7');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('1', '8');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('1', '9');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('1', '10');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('3', '11');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('2', '12');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('3', '13');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('2', '14');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('2', '15');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('2', '16');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('9', '17');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('4', '18');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('12', '19');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('11', '20');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('5', '21');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('8', '22');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('5', '23');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('4', '24');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('2', '25');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('3', '25');
INSERT INTO `rel_produto_ordem_pedido` VALUES ('1', '33');

-- ----------------------------
-- Table structure for subcategoria
-- ----------------------------
DROP TABLE IF EXISTS `subcategoria`;
CREATE TABLE `subcategoria` (
  `SUBCATEGORIA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOMEINTERNO` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`SUBCATEGORIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of subcategoria
-- ----------------------------
INSERT INTO `subcategoria` VALUES ('1', 'GPS Avançados', 'gps-avancados', 'gps-avancados');
INSERT INTO `subcategoria` VALUES ('2', 'GPS Simples', 'gps-simples', 'gps-simples');
INSERT INTO `subcategoria` VALUES ('3', 'Alarmes Teste ', 'alarme-teste-3', 'alarme-teste-3');
INSERT INTO `subcategoria` VALUES ('4', 'Cabos 2v', 'cabos', 'cabos');
INSERT INTO `subcategoria` VALUES ('5', 'Produto teste 1', '66', '');
INSERT INTO `subcategoria` VALUES ('6', 'rfgfg', 'fggf', '');
INSERT INTO `subcategoria` VALUES ('7', 'anca', 'teste', '');
INSERT INTO `subcategoria` VALUES ('8', 'Tetse', 'tr6565', '');
INSERT INTO `subcategoria` VALUES ('9', 'Produto teste 1', 'gps', '');
INSERT INTO `subcategoria` VALUES ('10', 'testa', 'testa', '');
INSERT INTO `subcategoria` VALUES ('11', 'Teste 3', 'teste-3', '');
INSERT INTO `subcategoria` VALUES ('12', 'Cabos 3v', 'cabos-3v', '');
INSERT INTO `subcategoria` VALUES ('13', 'Teste', 'teste', '');
INSERT INTO `subcategoria` VALUES ('14', 'Teste1', 'teste1', '');
INSERT INTO `subcategoria` VALUES ('15', 'Tetse Sub', 'teste-sub', '');

-- ----------------------------
-- Table structure for tipo
-- ----------------------------
DROP TABLE IF EXISTS `tipo`;
CREATE TABLE `tipo` (
  `TIPO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`TIPO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipo
-- ----------------------------
INSERT INTO `tipo` VALUES ('1', 'Carro');
INSERT INTO `tipo` VALUES ('2', 'Motociclet');

-- ----------------------------
-- Table structure for veiculo
-- ----------------------------
DROP TABLE IF EXISTS `veiculo`;
CREATE TABLE `veiculo` (
  `VEICULO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODELO_ID` int(11) DEFAULT NULL,
  `COR` varchar(12) DEFAULT NULL,
  `CHASSI` varchar(32) DEFAULT NULL,
  `CAPACIDADE` smallint(6) DEFAULT NULL,
  `PLACA` char(7) DEFAULT NULL,
  `PROCEDENCIA` int(11) DEFAULT NULL,
  `ZEROKM` tinyint(1) DEFAULT NULL,
  `RENAVAM` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`VEICULO_ID`),
  KEY `FK_MODELO_VEICULO` (`MODELO_ID`),
  CONSTRAINT `FK_MODELO_VEICULO` FOREIGN KEY (`MODELO_ID`) REFERENCES `modelo` (`MODELO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of veiculo
-- ----------------------------
INSERT INTO `veiculo` VALUES ('1', '8', 'AZUL', 'ree trtr', '5', 'CBB-666', '1', '0', '3234344334');
INSERT INTO `veiculo` VALUES ('2', '1', 'PRETO', 'r4rtrtrtt66556', '5', 'CVN1345', null, '0', '3234344334');
INSERT INTO `veiculo` VALUES ('5', '3', 'Prata', 'DRTRTRT65766778HG', '11', '', '0', '0', '3234344334');
INSERT INTO `veiculo` VALUES ('6', '3', 'Prata', 'DRTRTRT65766778HG', '11', '', '0', '0', '3234344334');
INSERT INTO `veiculo` VALUES ('7', '3', 'Prata', 'DRTRTRT65766778HG', '11', '', '0', '0', '3234344334');
INSERT INTO `veiculo` VALUES ('8', '3', 'Prata', 'DRTRTRT65766778HG', '11', '', '0', '0', '3234344334');
INSERT INTO `veiculo` VALUES ('9', '3', 'Prata', 'DRTRTRT65766778HG', '11', '', '0', '0', '3234344334');
INSERT INTO `veiculo` VALUES ('10', '3', 'Prata', 'DRTRTRT65766778HG', '11', '', '0', '0', '3234344334');
INSERT INTO `veiculo` VALUES ('11', '310', 'Azul', 'SYFSADYYSD6343478', '5', 'ERR4454', null, '0', '3234344334');
INSERT INTO `veiculo` VALUES ('12', '816', 'Azul Bebe', '4GFN4784578UIIRTA', '12', 'SDD4556', null, '0', '3234344334');
INSERT INTO `veiculo` VALUES ('13', '97', 'Azul Bebe', 'RTRTRTTRT', '9', 'TTR', '1', '0', '3234344334');
INSERT INTO `veiculo` VALUES ('14', '93', 'Azul Bebe', 'WEREERREERERERER', '5', 'SDS6565', '1', '0', '3234344334');
INSERT INTO `veiculo` VALUES ('15', '66', 'Azul Bebe', 'WEWRERERERERERER', '6', 'EWW2345', null, '0', '3234344334');
INSERT INTO `veiculo` VALUES ('16', '313', 'Azul Bebe', 'SEERWRTRTRTRTRTt', '9', 'DFD6567', null, '0', '3234344334');
INSERT INTO `veiculo` VALUES ('17', '19', 'Verde Musgo', 'REE RTRFR TR TRTTRr', '5', 'DFF-545', '1', '0', '4TRTRTR6556');
INSERT INTO `veiculo` VALUES ('18', null, 'dd', 'DFD DFDFF DD GFGFFG', '3', 'GFG-435', '1', '1', 'DFDFDFDDFDF');

-- ----------------------------
-- Table structure for veiculo_proprietario
-- ----------------------------
DROP TABLE IF EXISTS `veiculo_proprietario`;
CREATE TABLE `veiculo_proprietario` (
  `VEICULO_ID` int(11) NOT NULL,
  `PROPRIETARIO_ID` int(11) NOT NULL,
  PRIMARY KEY (`VEICULO_ID`,`PROPRIETARIO_ID`),
  KEY `FK_VEICULO_PROPRIETARIO2` (`PROPRIETARIO_ID`),
  CONSTRAINT `FK_VEICULO_PROPRIETARIO` FOREIGN KEY (`VEICULO_ID`) REFERENCES `veiculo` (`VEICULO_ID`),
  CONSTRAINT `FK_VEICULO_PROPRIETARIO2` FOREIGN KEY (`PROPRIETARIO_ID`) REFERENCES `proprietario` (`PROPRIETARIO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of veiculo_proprietario
-- ----------------------------
INSERT INTO `veiculo_proprietario` VALUES ('1', '3');
INSERT INTO `veiculo_proprietario` VALUES ('17', '3');
INSERT INTO `veiculo_proprietario` VALUES ('18', '3');
INSERT INTO `veiculo_proprietario` VALUES ('14', '33');
INSERT INTO `veiculo_proprietario` VALUES ('15', '33');
INSERT INTO `veiculo_proprietario` VALUES ('16', '33');
INSERT INTO `veiculo_proprietario` VALUES ('13', '34');
