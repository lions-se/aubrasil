<?

function URL($parse){
	$p = parse_url($parse);
	return $p['path'];	
}

function absoluteURL(){
	$server = "http";
	$host  = $_SERVER['HTTP_HOST'];
	$request_uri = $_SERVER['REQUEST_URI'];
	
	$url = sprintf("%s://%s%s", $server,$host,$request_uri);	
	return $url;
}




function rootURL(){
	
	$server = "http";
	$host  = $_SERVER['HTTP_HOST'];

	
	$url = sprintf("%s://%s", $server,$host);	
	return $url;
}


function toAscii($str, $replace=array(), $delimiter='-') {
	if( !empty($replace) ) {
		$str = str_replace((array)$replace, ' ', $str);
	}

	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	return $clean;
}


function checkSession(){
	if( !isset($_SESSION['franquia_id'])){
		//header("location: redirecionamento.php");
		return false;	
	}
	
	return true;
}

function formValue($data){
	
	return !empty($data) ? $data : '';	
}

function validateField($field){

	if(!isset($field) || empty($field) || is_null($field)){
		return false;
	}

	return true;
	//return empty($field) ? false : is_null($field) ? true : false;
}

function validateEmail($email){

	return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function formselect($id,$db_id){

	return ($id == $db_id) ? 'selected' : '';

}

function formValueDate($data){
	$d = preg_split("/\\-/",$data);
	
	return sprintf("%d-%d-%d",$d[2],$d[1],$d[0]);
}

function showPaises(){
	$paises = '<option value="África do Sul">África do Sul</option>
<option value="Albânia">Albânia</option>
<option value="Alemanha">Alemanha</option>
<option value="Andorra">Andorra</option>
<option value="Angola">Angola</option>
<option value="Anguilla">Anguilla</option>
<option value="Antigua">Antigua</option>
<option value="Arábia Saudita">Arábia Saudita</option>
<option value="Argentina">Argentina</option>
<option value="Armênia">Armênia</option>
<option value="Aruba">Aruba</option>
<option value="Austrália">Austrália</option>
<option value="Áustria">Áustria</option>
<option value="Azerbaijão">Azerbaijão</option>
<option value="Bahamas">Bahamas</option>
<option value="Bahrein">Bahrein</option>
<option value="Bangladesh">Bangladesh</option>
<option value="Barbados">Barbados</option>
<option value="Bélgica">Bélgica</option>
<option value="Benin">Benin</option>
<option value="Bermudas">Bermudas</option>
<option value="Botsuana">Botsuana</option>
<option value="Brasil" selected>Brasil</option>
<option value="Brunei">Brunei</option>
<option value="Bulgária">Bulgária</option>
<option value="Burkina Fasso">Burkina Fasso</option>
<option value="botão">botão</option>
<option value="Cabo Verde">Cabo Verde</option>
<option value="Camarões">Camarões</option>
<option value="Camboja">Camboja</option>
<option value="Canadá">Canadá</option>
<option value="Cazaquistão">Cazaquistão</option>
<option value="Chade">Chade</option>
<option value="Chile">Chile</option>
<option value="China">China</option>
<option value="Cidade do Vaticano">Cidade do Vaticano</option>
<option value="Colômbia">Colômbia</option>
<option value="Congo">Congo</option>
<option value="Coréia do Sul">Coréia do Sul</option>
<option value="Costa do Marfim">Costa do Marfim</option>
<option value="Costa Rica">Costa Rica</option>
<option value="Croácia">Croácia</option>
<option value="Dinamarca">Dinamarca</option>
<option value="Djibuti">Djibuti</option>
<option value="Dominica">Dominica</option>
<option value="EUA">EUA</option>
<option value="Egito">Egito</option>
<option value="El Salvador">El Salvador</option>
<option value="Emirados Árabes">Emirados Árabes</option>
<option value="Equador">Equador</option>
<option value="Eritréia">Eritréia</option>
<option value="Escócia">Escócia</option>
<option value="Eslováquia">Eslováquia</option>
<option value="Eslovênia">Eslovênia</option>
<option value="Espanha">Espanha</option>
<option value="Estônia">Estônia</option>
<option value="Etiópia">Etiópia</option>
<option value="Fiji">Fiji</option>
<option value="Filipinas">Filipinas</option>
<option value="Finlândia">Finlândia</option>
<option value="França">França</option>
<option value="Gabão">Gabão</option>
<option value="Gâmbia">Gâmbia</option>
<option value="Gana">Gana</option>
<option value="Geórgia">Geórgia</option>
<option value="Gibraltar">Gibraltar</option>
<option value="Granada">Granada</option>
<option value="Grécia">Grécia</option>
<option value="Guadalupe">Guadalupe</option>
<option value="Guam">Guam</option>
<option value="Guatemala">Guatemala</option>
<option value="Guiana">Guiana</option>
<option value="Guiana Francesa">Guiana Francesa</option>
<option value="Guiné-bissau">Guiné-bissau</option>
<option value="Haiti">Haiti</option>
<option value="Holanda">Holanda</option>
<option value="Honduras">Honduras</option>
<option value="Hong Kong">Hong Kong</option>
<option value="Hungria">Hungria</option>
<option value="Iêmen">Iêmen</option>
<option value="Ilhas Cayman">Ilhas Cayman</option>
<option value="Ilhas Cook">Ilhas Cook</option>
<option value="Ilhas Curaçao">Ilhas Curaçao</option>
<option value="Ilhas Marshall">Ilhas Marshall</option>
<option value="Ilhas Turks & Caicos">Ilhas Turks & Caicos</option>
<option value="Ilhas Virgens (brit.)">Ilhas Virgens (brit.)</option>
<option value="Ilhas Virgens(amer.)">Ilhas Virgens(amer.)</option>
<option value="Ilhas Wallis e Futuna">Ilhas Wallis e Futuna</option>
<option value="Índia">Índia</option>
<option value="Indonésia">Indonésia</option>
<option value="Inglaterra">Inglaterra</option>
<option value="Irlanda">Irlanda</option>
<option value="Islândia">Islândia</option>
<option value="Israel">Israel</option>
<option value="Itália">Itália</option>
<option value="Jamaica">Jamaica</option>
<option value="Japão">Japão</option>
<option value="Jordânia">Jordânia</option>
<option value="Kuwait">Kuwait</option>
<option value="Latvia">Latvia</option>
<option value="Líbano">Líbano</option>
<option value="Liechtenstein">Liechtenstein</option>
<option value="Lituânia">Lituânia</option>
<option value="Luxemburgo">Luxemburgo</option>
<option value="Macau">Macau</option>
<option value="Macedônia">Macedônia</option>
<option value="Madagascar">Madagascar</option>
<option value="Malásia">Malásia</option>
<option value="Malaui">Malaui</option>
<option value="Mali">Mali</option>
<option value="Malta">Malta</option>
<option value="Marrocos">Marrocos</option>
<option value="Martinica">Martinica</option>
<option value="Mauritânia">Mauritânia</option>
<option value="Mauritius">Mauritius</option>
<option value="México">México</option>
<option value="Moldova">Moldova</option>
<option value="Mônaco">Mônaco</option>
<option value="Montserrat">Montserrat</option>
<option value="Nepal">Nepal</option>
<option value="Nicarágua">Nicarágua</option>
<option value="Niger">Niger</option>
<option value="Nigéria">Nigéria</option>
<option value="Noruega">Noruega</option>
<option value="Nova Caledônia">Nova Caledônia</option>
<option value="Nova Zelândia">Nova Zelândia</option>
<option value="Omã">Omã</option>
<option value="Palau">Palau</option>
<option value="Panamá">Panamá</option>
<option value="Papua-nova Guiné">Papua-nova Guiné</option>
<option value="Paquistão">Paquistão</option>
<option value="Peru">Peru</option>
<option value="Polinésia Francesa">Polinésia Francesa</option>
<option value="Polônia">Polônia</option>
<option value="Porto Rico">Porto Rico</option>
<option value="Portugal">Portugal</option>
<option value="Qatar">Qatar</option>
<option value="Quênia">Quênia</option>
<option value="Rep. Dominicana">Rep. Dominicana</option>
<option value="Rep. Tcheca">Rep. Tcheca</option>
<option value="Reunion">Reunion</option>
<option value="Romênia">Romênia</option>
<option value="Ruanda">Ruanda</option>
<option value="Rússia">Rússia</option>
<option value="Saipan">Saipan</option>
<option value="Samoa Americana">Samoa Americana</option>
<option value="Senegal">Senegal</option>
<option value="Serra Leone">Serra Leone</option>
<option value="Seychelles">Seychelles</option>
<option value="Singapura">Singapura</option>
<option value="Síria">Síria</option>
<option value="Sri Lanka">Sri Lanka</option>
<option value="St. Kitts & Nevis">St. Kitts & Nevis</option>
<option value="St. Lúcia">St. Lúcia</option>
<option value="St. Vincent">St. Vincent</option>
<option value="Sudão">Sudão</option>
<option value="Suécia">Suécia</option>
<option value="Suiça">Suiça</option>
<option value="Suriname">Suriname</option>
<option value="Tailândia">Tailândia</option>
<option value="Taiwan">Taiwan</option>
<option value="Tanzânia">Tanzânia</option>
<option value="Togo">Togo</option>
<option value="Trinidad & Tobago">Trinidad & Tobago</option>
<option value="Tunísia">Tunísia</option>
<option value="Turquia">Turquia</option>
<option value="Ucrânia">Ucrânia</option>
<option value="Uganda">Uganda</option>
<option value="Uruguai">Uruguai</option>
<option value="Venezuela">Venezuela</option>
<option value="Vietnã">Vietnã</option>
<option value="Zaire">Zaire</option>
<option value="Zâmbia">Zâmbia</option>
<option value="Zimbábue">Zimbábue</option>';	

return $paises;
}


function estadosBrasileiros($select = NULL){

	$estados = array("AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins");

	$strEstados = '';
	foreach ($estados as $key => $value) {
		
		if($select == $key) {
			$strEstados .= sprintf('<option selected value="%s">%s</option>',$key,$value);
		}else {

			$strEstados .= sprintf('<option value="%s">%s</option>',$key,$value);

		}
	}

	return $strEstados;
}

function urlbase(){
$protocol = (strstr('https',$_SERVER['SERVER_PROTOCOL']) === false)?'http':'https';
$url = $protocol.'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

return $url;	
}

function request_self(){
return $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];	
}

function checkQueryString($field, $type = 'd'){
	$pattern = sprintf("/(?|&)%s=\%s+/", $field,$type);
	$match = preg_match($pattern,request_self());
	
	if(!$match){
		return false;
	}
	
	return true;
	
}

function SaltString($str){
	return SESSION_SALT.$str;	
}

function palavras($texto,$num){

	$txt = explode(" ", $texto);
	$result = array();
	
	for($i = 0; $i < $num; ++$i){
		$result[$i] = $txt[$i];
	}

	return implode(' ', $result)."...";

}


function saibamais($texto,$num,$link, $linktexto){

	$saida = palavras($texto,$num);


	return sprintf('%s </br><a href="%s">%s</a>', $saida, $link, $linktexto);

}


define ('ALERTA_FALHA',0);
define('ALERTA_SUCESSO',1);
define ('ALERTA_AVISO',2);


function alert($titulo, $texto, $tipo = ALERTA_SUCESSO){

	if(session_status() == PHP_SESSION_ACTIVE){
		$class = '';

		switch($tipo){
			case ALERTA_SUCESSO:
				$class = 'alert-success';
				break;

			case ALERTA_AVISO:
				$class = 'alert-warning';
				break;

			case ALERTA_FALHA:
				$class = 'alert-danger';
				break;


		}

		$_SESSION['flash'] = sprintf('<div class="alert %s" data-dismiss="alert"  ><strong>%s: </strong> %s</div>', $class, $titulo, $texto);
	}

}


function flash(){
	print $_SESSION['flash'];
}

function kill_alert(){
	if(session_status() == PHP_SESSION_ACTIVE){
		unset($_SESSION['flash']);
	}
}


function checkPost($campo){

	if(!isset($campo)){

		return false;
	}else if(empty($campo)){
		return false;
	}
	
	return true;
}


function matchEmail($email){

	$pattern = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 

	return preg_match($pattern, $email) ? true : false;
}


// convert  DD-MM-YY pra  YY-MM-DD
function date_convert($date){

	$ex = explode('-',$date);


	return sprintf("%d-%d-%d",$ex[2],$ex[1],$ex[0]);
}

//converte YY-MM-DD pra DD-MM-YY
function date_revert($date){
	$e = explode('-', $date);

	return sprintf("%d-%d-%d",$ex[0],$ex[1],$ex[2]);

}

function expiresSession($time){

	if(isset($_SESSION['expires'])){

		$timediff = (mktime() -  $_SESSION['expires']) - $time * 60;

		if( $timediff > 0){

			unset($_SESSION['expires']);
		}else {

			

			$_SESSION['expires'] =  mktime();
		}

	}
}


function sexo($select = NULL){

	$sexo = array('M' => 'Masculino', 'F' => 'Feminino');
	$str = "";

	foreach ($sexo as $key => $value) {
			
		if($key == $select){

			$str .= sprintf('<option value="%s" selected>%s</option>', $key, $value);

		}else{
			$str .= sprintf('<option value="%s" >%s</option>', $key, $value);
		}

	}

	return $str;
	
}


function estadoCivil($select = NULL){

	$e = array(1 => 'Solteiro (a)', 2 => 'Casado (a)', 3 => 'Divorciado (a)', 4 => 'Viuvo (a)');

	$str = '';
	foreach($e as $key => $value){

		if($key == $select ){
			$str .= sprintf('<option value="%d" selected>%s</option>'.PHP_EOL, $key, $value);
		}else {
			$str .= sprintf('<option value="%d">%s</option>'.PHP_EOL, $key, $value);
		}

	}

	return $str;
}


function mostraMarca($sel= NULL){

	$db = Flight::get('db');

	$q = $db->query2("SELECT
marca.MARCA_ID,
marca.TIPO_ID,
marca.NOMEMARCA
FROM
marca");



	$tag = "";


	if($sel == NULL){
		$tag .= sprintf('<option value="-1" selected>Selecione a Marca</option>');	
	}


	foreach ($q->querydata as $key => $value) {
		
		if($sel !== NULL && $sel == $value['MARCA_ID']){

			$tag .= sprintf('<option value="%d" selected>%s</option>', $value['MARCA_ID'],  $value['NOMEMARCA']);

		}else {

			$tag .= sprintf('<option value="%d" >%s</option>', $value['MARCA_ID'],  $value['NOMEMARCA']);

		}

	}

	print($tag);

}


function href($href, $text, $class = NULL){

	$c = $class != NULL ? sprintf('class="%s"', $class) : '';

	return sprintf('<a href="%s" %s>%s</a>', $href, $c, $text);
}


function make_seed()
{
    list($usec, $sec) = explode(' ', microtime());
    return (float) $sec + ((float) $usec * 100000);
}



function genConfirmationCode($num=6){

	$code = genEmailConfirmation();

	$newcode = array();

	for($i = 0; $i < strlen($code); $i++){

		$newcode[$i] = $code[$i];

	}

	//print "original: ".implode('',$newcode);
	shuffle($newcode);


	return substr(implode('',$newcode),0,$num);
	

}


function categoria($id){

	$db = Flight::get('db');



	$categorias_query = "SELECT
categoria.CATEGORIA_ID,
categoria.NOME,
categoria.NOMEINTERNO,
categoria.URL
FROM
categoria";

	$all_categorias = $db->query2($categorias_query, array());

	$html ='';


	foreach($all_categorias->querydata as $key => $value){

		if($value['CATEGORIA_ID'] == $id){
			$html .= sprintf('<option selected value="%s">%s</option>', $value['CATEGORIA_ID'], $value['NOME'] );
		}else{
			$html .= sprintf('<option value="%s">%s</option>', $value['CATEGORIA_ID'], $value['NOME'] );
		}
	}

	$html .= '';


	return $html;

}


function subcategoria($produto_id){

	$db = Flight::get('db');
	$html = "";

	$query = "SELECT
produto.PRODUTO_ID,
subcategoria.SUBCATEGORIA_ID,
categoria.CATEGORIA_ID,
categoria.NOME,
subcategoria.NOME
FROM
produto
INNER JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
INNER JOIN categoria ON produto_categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
INNER JOIN subcategoria ON produto_categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE produto.PRODUTO_ID = :id";

	$q  = $db->query($query, array(':id' => $produto_id));

	$dados = new ArrayIterator($q->querydata);


	$query = "SELECT
subcategoria.NOME as SUBCAT_NOME,
categoria_subcategoria.SUBCATEGORIA_ID,
categoria_subcategoria.CATEGORIA_ID,
categoria.NOME as CAT_NOME
FROM
subcategoria
INNER JOIN categoria_subcategoria ON categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
INNER JOIN categoria ON categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
WHERE categoria.CATEGORIA_ID = :cat_id";

	$all_subcats = $db->query($query, array(":cat_id" => $q->querydata[0]['CATEGORIA_ID']));

	$subcategorias = new ArrayIterator($all_subcats->querydata);

		foreach ($subcategorias as $key => $value) {
			
			if($value['SUBCATEGORIA_ID'] == $q->querydata[0]['SUBCATEGORIA_ID']){

					$html .= sprintf('<option selected value="%s">%s</option>', $value['SUBCATEGORIA_ID'], $value['SUBCAT_NOME']);
			}else {

					$html .= sprintf('<option  value="%s">%s</option>', $value['SUBCATEGORIA_ID'], $value['SUBCAT_NOME']);

			}
		
		}

	return $html;
}

function calcula_frete($servico,$CEPorigem,$CEPdestino,$peso,$altura='4',$largura='12',$comprimento='16',$valor='1.00'){
    ////////////////////////////////////////////////
    // Código dos Serviços dos Correios
    // 41106 PAC
    // 40010 SEDEX
    // 40045 SEDEX a Cobrar
    // 40215 SEDEX 10
    ////////////////////////////////////////////////
    // URL do WebService
    $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=".$CEPorigem."&sCepDestino=".$CEPdestino."&nVlPeso=".$peso."&nCdFormato=1&nVlComprimento=".$comprimento."&nVlAltura=".$altura."&nVlLargura=".$largura."&sCdMaoPropria=n&nVlValorDeclarado=".$valor."&sCdAvisoRecebimento=n&nCdServico=".$servico."&nVlDiametro=0&StrRetorno=xml";
    // Carrega o XML de Retorno
    $xml = simplexml_load_file($correios);
    // Verifica se não há erros
    if($xml->cServico->Erro == '0'){
        return $xml->cServico->Valor;
    }else{
        return false;
    }
}


function limpa_medidas($medida){

	$medidas = array(' ', 'cm','kg','mg');

	return floatval(str_replace($medidas, '', $medida));

}

function format_medidas($medida, $dec = 2){

		return number_format($medida, $dec,'.','');
}

function print_medidas($medida,$dec=2, $suffix){

	return format_medidas($medida,$dec).' '.$suffix;

}

function slug($str)
{
	$str = strtolower(trim($str));

	if(strstr($str, 'ç')){
		$str= str_replace('ç', 'c', $str);
	}
	$str = preg_replace('/[^a-z0-9-]/', '-', $str);
	$str = preg_replace('/-+/', "-", $str);
	return $str;
}


function  genEmailConfirmation(){

	return rtrim(base64_encode(md5(microtime())),"=");

}
/**
 * Generates links   
 * @param  mixed $links [description]
 * @return [type]        [description]
 */
function genLinks($links) {

	$base = rootURL();
	$slash = '/';

	$link  = array();

	foreach ($links as $key => $value) {
		
		array_push($link, $value);
	}


	return implode('/', $link);


}


function simul_post ($url, $data, $optional_headers = null)
{
	$params = array('http' => array
	(
		'method' => 'POST',
		'content' => http_build_query($data, "", "&")
	));
	if ($optional_headers !== null):
		$params['http']['header'] = $optional_headers;
	endif;
	$ctx = stream_context_create($params);
	$fp = @fopen($url, 'rb', false, $ctx);
	if (!$fp):
		throw new Exception("Problem with $url, $php_errormsg");
	endif;
	$response = @stream_get_contents($fp);
	if ($response === false):
		throw new Exception("Problem reading data from $url, $php_errormsg");
	endif;
	return $response;
}




function dinheiroBR($valor,$digitos)
{

		return sprintf("%.2f", $valor);


}



function converteKilos($valor)
{
	return sprintf("%.2fkg", $valor);
}



function extensao($file){

	return pathinfo($file, PATHINFO_EXTENSION);
}


function get_conteudo_estatico($nome){

	$db = Flight::get('db');

	$query = "SELECT conteudo_id, ".$nome."  FROM conteudo_estatico WHERE conteudo_id = 1";

	$q = $db->query2($query);

	return $q->querydata[0][$nome];

}


function bytesToHuman($bytes, $decimals=2){

	 $sz = 'BKMGTP';
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}


function calculateFilesize($filepath){

	if(is_file($filepath) && file_exists($filepath)){

		$fp = fopen($filepath,'rb');
		fseek($fp, 0 , SEEK_END);
		$size = ftell($fp);
		fclose($fp);

		return $size;

	}
}

function getPedidoStatus(){

	$db = Flight::get('db');


	$query = "SELECT pedido_status_id,status FROM  pedido_status";

	$q = $db->query2($query);

	return $q->querydata;
}





?>

