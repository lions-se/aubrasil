<?php
ob_start();
include_once("config.php");
session_start();


Flight::path(APP_CLASSPATH.DIR);

define('FLIGHT_FRAMEWORK', true);


Flight::register('db', 'DatabasePDO', array($config['hostname'],  $config['username'], $config['passwd'], 3306, DatabasePDO::DRIVER_MYSQL,$config['db'],  false ), function($db){
    });


Flight::register('carrinho', 'Cart', array(), function($d){

//});
});

Flight::register('loginSession', 'UserSession', array(), function($d){

//});
});

Flight::register('Debug', 'Debug', array(), function($debug){

});

Flight::Debug()->EnableDebug(true);


Flight::register('buscaCep','CepWebService', array(), function($cep){

});


Flight::register('PHPMailer','PHPMailer', array(), function($cep){

});



Flight::register('Pagina', 'Pagination', array($db, 15), function($pag){

});

Flight::set('db',Flight::db());
Flight::set('cart', Flight::carrinho());
Flight::set('debug', Flight::Debug());

$id =  explode('/',Flight::request()->url);

Flight::set('actualFranquia', $id[1]);


Flight::set('captcha_pubkey', $config['public_key']);
Flight::set('captcha_privkey', $config['private_key']);

Flight::set('pagseg_token', TOKEN);
Flight::set('pagseg_email', EMAIL_PAGSEGURO);

if(!isset($_SESSION['carrinho'])){



	//Flight::set('carrinho',  Flight::carrinho());
	$_SESSION['carrinho'] = Flight::get('cart');

}else {

	Flight::set('cart',  $_SESSION['carrinho'] );
	//Flight::set('carrinho', $_SESSION['carrinho']);
	

	
}


//Flight::set('carrinho',$carrinho);
Flight::set('flight.views.path', 'paginas');
Flight::set('fullpage', false);
Flight::set('franquiaDados', Util::getFranquia( Flight::get('actualFranquia')  ));
Flight::set('showParallax', false);
Flight::set('titulo', "");

/*
Flight::map('franquia', function($id){

	$db = Flight::get('db');

			$franquia = $db->query2("SELECT
proprietario.NOME,
proprietario.SOBRENOME,
franquia.NOMEFRANQUIA,
franquia.SLOGAN,
franquia.TELFRANQUIA,
franquia.FRANQUIA_ID
FROM
proprietario
INNER JOIN franquia ON franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
WHERE franquia.FRANQUIA_ID = :id", array(':id'=>$id));




			//var_dump($franquia);
			return $franquia->querydata;

});

*/

include_once(APP_CLASSPATH.DIR.'Carrinho.php');
include_once(APP_CLASSPATH.DIR.'Home.php');

//var_dump(Flight::get('franquiaDados'));

Flight::render('header-footer/header.php', NULL, 'header_content');
Flight::render('header-footer/footer.php',NULL, 'footer_content');

Flight::render('layout_base/top.php', array('franquiaData'=> Flight::get('franquiaDados') ) , 'topo_content');
Flight::render('layout_base/menu_horizontal.php', NULL, 'menu_cima');
Flight::render('layout_base/menu_categorias.php', NULL, 'menu_lateral');
Flight::render('layout_base/rodape_site.php', NULL, 'rodape');
Flight::render('layout_base/parallax.php', NULL, 'parallax');
Flight::render('layout_base/classificados.php', NULL, 'classificados');



Flight::route('/@id:[0-9]+(/home)', array('Home','index'), 'conteudo');
//Flight::route('/@id/(.*)', array('Home','index'), 'conteudo');

Flight::route('GET /@id:[0-9]+/produto/@produto_id:[0-9]+(-@nome.html)', array('ExibeProduto','getProduto'), 'conteudo');

/**
 * Ações e caminho do carrinho de compras
 * pelo amor  de cthulhu  nao  mexer nessa parte a nao ser que tenha uma boa razao
 */

Flight::route('GET /@id:[0-9]+/carrinho', array('Carrinho','index'), 'conteudo');
Flight::route('GET /@id:[0-9]+/carrinho/apagar-referencias', array('Carrinho','apagarReferencias'), 'conteudo');
Flight::route('/@id:[0-9]+/carrinho/checkout', array('Carrinho','checkout'), 'conteudo');

Flight::route('POST /@id:[0-9]+/carrinho/atualiza-produtos', array('Carrinho','updateCart'), 'conteudo');
Flight::route('POST /@id:[0-9]+/carrinho/remove-produto/@produtoid', array('Carrinho','removeProduct'), 'conteudo');
Flight::route('POST /@id:[0-9]+/carrinho/calcula-frete', array('Carrinho','calculaFrete'), 'conteudo');
Flight::route('POST /@id:[0-9]+/carrinho/finalizar', array('Carrinho','finalizarCompra'), 'conteudo');
Flight::route('POST /@id:[0-9]+/carrinho/recomendacao', array('Carrinho','recomendacao'), 'conteudo');

Flight::route('GET /@id:[0-9]+/carrinho/transacao_concluida', array('Carrinho','transacaoConcluida'));

/**
 *  FIM  AÇÕES DO CARRINHO
 */

Flight::route('GET /@id:[0-9]+/produto/add-carrinho/@produto_id:[0-9]+(-@nome)', array('Carrinho','get'), 'conteudo');


Flight::route('GET|POST /@id:[0-9]+/cadastro_cliente', array('Usuario', 'index'), 'conteudo');
Flight::route('/@id:[0-9]+/cadastro_cliente/sucesso', array('Usuario', 'signupSuccess'), 'conteudo');
Flight::route('GET /@id:[0-9]+/checkcpf/@cpf', array('Util', 'checkCPF'), 'conteudo');

Flight::route('POST /@id:[0-9]+/do_cadastro', array('Usuario', 'sendForm'), 'conteudo');


/**
 * Relaciocnado ao Login
 */

Flight::route('GET /@id:[0-9]+/login', array('Login', 'index'), 'conteudo');
Flight::route('POST /@id:[0-9]+/do_login', array('Login', 'dologin'), 'conteudo');
Flight::route('POST /@id:[0-9]+/login/login_facebook', array('Login', 'loginFacebook'), 'conteudo');

/**
 * Cadastro de Perfis
 */

Flight::route('GET /@id:[0-9]+/editar-perfil', array('Usuario', 'editProfile'), 'conteudo');
Flight::route('GET /@id:[0-9]+/editar-perfil/user', array('Usuario', 'editUser'), 'conteudo');
Flight::route('GET /@id:[0-9]+/editar-perfil/credentials', array('Usuario', 'editCredentials'), 'conteudo');

Flight::route('POST /@id:[0-9]+/editar-perfil/do_edit_user', array('Usuario', 'doEditUser'), 'conteudo');
Flight::route('POST /@id:[0-9]+/editar-perfil/do_edit_credentials', array('Usuario', 'doEditCredentials'), 'conteudo');

/**
 * Cadastro de Endereços PARA AMANHA
 */
Flight::route('GET /@id:[0-9]+/editar-perfil/cadastra_endereco', array('Usuario', 'cadastraEndereco'), 'conteudo');
Flight::route('GET /@id:[0-9]+/editar-perfil/cadastra_endereco/endereco/@id_endereco:[0-9]+', array('Usuario', 'cadastraEndereco'), 'conteudo');
Flight::route('GET /@id:[0-9]+/editar-perfil/cadastra_endereco/do_edita_endereco/@endereco_id:[0-9]+', array('Usuario', 'doEditaEndereco'), 'conteudo');
Flight::route('GET /@id:[0-9]+/editar-perfil/cadastra_endereco/do_apaga_endereco/@endereco_id:[0-9]+', array('Usuario', 'apagaEndereco'), 'conteudo');

Flight::route('POST /@id:[0-9]+/editar-perfil/cadastra_endereco/atu_endereco', array('Usuario', 'updateEndereco'), 'conteudo');
Flight::route('POST /@id:[0-9]+/editar-perfil/cadastra_endereco/del_endereco', array('Usuario', 'delEndereco'), 'conteudo');

Flight::route('POST /@id:[0-9]+/editar-perfil/cadastra_endereco/do_cadastra_endereco', array('Usuario', 'cadastraNovoEndereco'), 'conteudo');


/**
 * ORDEM DE PEDIDOS
 */

Flight::route('GET /@id:[0-9]+/editar-perfil/order', array('Pedidos', 'visualizaPedidos'), 'conteudo');
Flight::route('GET /@id:[0-9]+/editar-perfil/order/view_order/@order_id:[0-9]+', array('Pedidos', 'visualizarNotaVirtual'), 'conteudo');




/**
 * CATEGORIAS
 */
Flight::route('GET /@id:[0-9]+/categoria/@categoria_id:[0-9]+(-@nome)', array('Categorias', 'index'), 'conteudo');




/**
 * CONTEUDO ESTATICO DO SITE
 */

Flight::route('GET /@id:[0-9]+/a-empresa', function($email){
		
		Flight::render('conteudo/aempresa.php',   array('dados' => get_conteudo_estatico('conteudo_aempresa') ), 'conteudo');
		Flight::render('home');

});


Flight::route('GET /@id:[0-9]+/a-franquia', function($email){
		
		Flight::render('conteudo/afranquia.php', array('dados' => get_conteudo_estatico('conteudo_afranquia') ), 'conteudo');
		Flight::render('home');

});

Flight::route('GET /@id:[0-9]+/treinamentos', function($email){
		
		Flight::render('conteudo/treinamentos.php',array('dados' => get_conteudo_estatico('conteudo_treina') ), 'conteudo');
		Flight::render('home');

});

Flight::route('GET /@id:[0-9]+/nossos-servicos', function($email){
		
		Flight::render('conteudo/servicos.php',array('dados' => get_conteudo_estatico('conteudo_servicos') ), 'conteudo');
		Flight::render('home');

});

Flight::route('GET /@id:[0-9]+/sua-franquia', function($email){
		
		Flight::render('conteudo/suafranquia.php', array('dados' => get_conteudo_estatico('conteudo_suafranquia') ), 'conteudo');
		Flight::render('home');

});



Flight::route('GET /@id:[0-9]+/contato', function($email){
		
		Flight::render('conteudo/contato.php', array(), 'conteudo');
		Flight::render('home');

});


Flight::route('POST /@id:[0-9]+/contato', function($email){
		
		if(!Util::ContatoEmail(Flight::get('actualFranquia'))){

			alert('Houve um Problema', "Envio de Contato Não pôde ser enviado, por favor tente mais tarde", ALERTA_ERRO);
			Flight::render('conteudo/contato.php', array(), 'conteudo');
			Flight::render('home');

		}


			alert('Sucesso', "Enviado com Sucesso");
			Flight::render('conteudo/contato.php', array(), 'conteudo');
			Flight::render('home');


});


// TESTE DE EMAIL (REMOVER MAIS TARDE)

Flight::route('GET /@id:[0-9]+/email/', function($id){

	echo Util::sendConfirmationEmail($id);


});



Flight::route('POST /@id:[0-9]+/contato', function($franquia_id){
		
		if(isset($_POST['recaptcha_response_field'])){
				$result = recaptcha_check_answer (Flight::get('captcha_privkey'),
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);


		}


		if(!$result->is_valid){

			alert('Houve um Problema', "Precisa Digitar o Codigo  corretamente", ALERTA_ERRO);
			Flight::render('conteudo/contato.php', array('dados' => $_POST), 'conteudo');
			Flight::render('home');
		}


		if(!isset($_POST['nome']) || empty($_POST['nome'])  ||  !isset($_POST['email']) || empty($_POST['email']) || !isset($_POST['texto']) || empty($_POST['texto']) ){

			alert('Houve um Problema', "Preencha os Campos Necessarios Corretamente", ALERTA_ERRO);
			Flight::render('conteudo/contato.php', array('dados' => $_POST), 'conteudo');
			Flight::render('home');
		}else {

		/**
		 * AQUI CODIGO DE ENVIAR E-MAIL
		 */
			alert('Sucesso!', "Contato Enviado Com Sucesso!");
			Flight::render('conteudo/contato.php', array('dados' => $_POST, 'formSend' => true), 'conteudo');
			Flight::render('home');

		}

});




Flight::route('GET /@id:[0-9]+/confirma_email/@codigo', function($franquia_id, $codigo){

	$db = Flight::get('db');

	$query = "SELECT EXISTS(SELECT
cliente.CLIENTE_ID,
cliente.NOME,
cliente.CODIGO_CADASTRO,
cliente.CONFIRMA_CADASTRO,
cliente.FRANQUIA_ID
FROM
cliente
WHERE cliente.CODIGO_CADASTRO LIKE :codigo AND cliente.FRANQUIA_ID = :franquia_id) AS EXISTE";

	$q = $db->query2($query, array(':codigo' => $codigo, ':franquia_id' => $franquia_id));


	$interno_query = "SELECT
cliente.CLIENTE_ID,
cliente.NOME,
cliente.CODIGO_CADASTRO,
cliente.CONFIRMA_CADASTRO,
cliente.FRANQUIA_ID
FROM
cliente
WHERE cliente.CODIGO_CADASTRO LIKE :codigo AND cliente.FRANQUIA_ID = :franquia_id ";


	$interno_data = $db->query2($interno_query, array(':codigo' => $codigo, ':franquia_id' => $franquia_id));


	if($q->querydata[0]['EXISTE'] == 1){

		if( $interno_data->querydata[0]['CONFIRMA_CADASTRO'] ==  0){

			if($db->update2('cliente', array('CONFIRMA_CADASTRO' => 1),
				' cliente.CODIGO_CADASTRO = :codigo ', array(':codigo' => $codigo))){

				alert('Parabens', sprintf("Cadastro: %s foi confirmado com sucesso!", $q->querydata['NOME']));

			}
		}else {

			alert('Houve um problema', 'Esta conta ja esta ativa',ALERTA_ERRO);
		}
	}else {

		alert('Houve um problema', 'Esta conta Não Existe', ALERTA_ERRO);
	}


	Flight::set('fullpage', true);
	Flight::render('email/confirma_email.php', array(), 'conteudo');
	Flight::render('home.php');

});
	




/**
 * BUSCA DE CEP
 */
Flight::route('GET|POST /@id:[0-9]+/cep/@endcep', function($franquia_id,$cep){

	$req_cep = "";

	if($_SERVER['REQUEST_METHOD'] == "POST" ){

		$req_cep = $_POST['cep'];

	}else {

		$req_cep = $cep;
	}
	
	Util::buscarCep($req_cep);
});	



Flight::route('GET /@id:[0-9]/logout', function($id){

	unset($_SESSION['credentials']);
	unset($_SESSION['redirBack']);
	Flight::redirect(sprintf("/%d/",$id));
});


Flight::route('GET /@id:[0-9]/notificacoes', function($id){

if(count($_POST) > 0){



} 

});




Flight::start();
ob_end_clean();

?>