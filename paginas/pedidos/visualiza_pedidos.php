<div class="container">
	
	<? if(empty($data->querydata)){ ?>
		
		<div class="row">
			<div class="col col-md-12">
						<div class="msg_sem_pedidos">
							<h1>Não há pedidos cadastrados</h1>
						</div>
			</div>
		</div>

	<? }else{ ?>

	<h1>Visualizar Pedidos</h1>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Pedido:</th>
				<th>Visualização:</th>
				
			</tr>
		</thead>
		<tbody>

			<? foreach($data->querydata as $key => $value){ ?>
			<tr>
				<td>Pedido #<? printf("%08d", $value['ORDEM_ID']);  ?></td>
				<td><a href="<?php echo Util::URL(sprintf("view_order/%d", $value['ORDEM_ID'])); ?>" class="btn btn-lg btn-primary">Ver Pedido</a> </td>
				
			</tr>
			<? } ?>
		</tbody>
	</table>
	<? } ?>
</div>
