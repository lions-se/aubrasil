<?
ini_set('default_charset', 'UTF-8');
header('Content-Type: text/html; charset=utf-8');
// Adds X-Frame-Options to HTTP header, so that page cannot be shown in an iframe.
header('X-Frame-Options: DENY');

// Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
header('X-Frame-Options: SAMEORIGIN');

header("X-Content-Security-Policy: allow 'self'");

// Adds the Content-Security-Policy to the HTTP header.
// JavaScript will be restricted to the same domain as the page itself, but
// is allowed inside the HTML page (no separate *.js file required).
header("X-Content-Security-Policy: allow 'self'; options inline-script");


header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sun, 11 Apr 2010 00:00:00 GMT");

?>
<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
	<meta name="identifier-url" content="http://www.autobrasilfranquias.com.br" />
	<meta name="title" content="Auto Brasil Franquias" />
	<meta name="description" content="Venda de Franquias" />
	<meta name="abstract" content="Venda de Franquias" />
	<meta name="keywords" content="gps, alarmes, produtos, veicular, venda, franquias" />
	<meta name="author" content="Lions Soluções Empresariais" />
	<meta name="revisit-after" content="15" />
	<meta name="language" content="pt-br" />
	<meta name="copyright" content="© 2014 Lions Soluções Empresariais" />

    <link rel="shortcut icon" href="img/logo_icone.png">
    
	
    <!--Css geral com IMPORT URL()-->
    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap-theme.css" />
    <link href="css/ui-darkness/jquery-ui-1.10.4.custom.css" />
    <link rel="stylesheet" type="text/css" href="/css/autobrasil.css" />
	<link rel="stylesheet" type="text/css" href="/css/geral.css" />
	<link rel="stylesheet" type="text/css" href="/css/paginacao.css" />
	<link rel="stylesheet" type="text/css" href="/creditos/creditos.css" />
	<link rel="stylesheet" type="text/css" href="/css/ui-lightness/jquery-ui-1.10.4.custom.min.css" />
  <link rel="stylesheet" href="/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/style_carousel.css">
  <link rel="stylesheet" href="/css/parallax_style2.css">

	
<script src="/js/jquery-1.10.js"></script>
<script src="/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>


<link rel="stylesheet" type="text/css" href="/css/liquidcarousel.css">
	<!--ESSE SCRIPT E CHAMADAS DEVE PERMANECER NESTA PÁGINA-->
	<title>Auto Brasil Franquia - <?php echo Flight::get('titulo'); ?></title>
</head>
<body>


<div class="container-fluid main_layout">

  <div class="container-fluid align_page">
  


<? if(defined(FACEBOOK_JS_SDK)){ ?>
<div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '198244937050473',
          status     : true,
          xfbml      : true,
          email		 : true
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/all.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    <? } ?>
    



  <link rel="stylesheet" href="/resources/demos/style.css">
