<?


$login = $dados['login'];
//$login = $dados['senha'];
$emailsecundario  = $dados['emailsecundario'];
$nome  = $dados['nome'];
$sobrenome  = $dados['sobrenome'];
$rg  = $dados['rg'];
$cpf  = $dados['cpf'];

$telefone  = $dados['telefone'];
$telefonecomercial = $dados['telefonecomercial'];
$telefonecelular  = $dados['telefonecelular'];

$dob = $dados['dob'];


?>

<div class="container-fluid ">

      <?
      if(isset($_SESSION['flash'])){
        echo flash();
        kill_alert();
      }

     ?>

  <div class="container fill">



		<h1>Cadastro:</h1>
        
       <div class="row">
      <form class="form-horizontal" action="/3/do_cadastro" id="formEdit"  method="post" enctype="application/x-www-form-urlencoded" role="form">
      
      <legend>Credenciais</legend>

       <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">E-Mail (login):</label>
    <div class="col-sm-10">
      <input type="text" name="login" value="<?php echo formValue($login); ?>" class="form-control campotexto" id="cpEnd" placeholder="E-Mail">
    </div>
  </div>

    <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Senha:</label>
    <div class="col-sm-10">
      <input type="password" name="senha" class="form-control campotexto" id="cpEnd" placeholder="Senha">
    </div>
  </div>


   <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Repetir a Senha:</label>
    <div class="col-sm-10">
      <input type="password" name="repete_senha" class="form-control campotexto"  id="cpEnd" placeholder="Repetir Senha">
    </div>
  </div>

               <div class="form-group ">
    <label for="cpBairro" class="col-sm-2 control-label">E-Mail Secundario (Opcional)</label>
    <div class="col-sm-10">
      <input type="text" name="emailsecundario" class="form-control campotexto" id="cpBairro" value="<?php echo formValue($emailsecundario); ?>" placeholder="E-Mail Secundario">
    </div>
  </div>


 <legend>Informações:</legend>

  <div class="form-group ">
    <label for="cpNome" class="col-sm-2 control-label">Nome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control campotexto" name="nome" value="<?php echo formValue($nome); ?>"  id="cpNome" placeholder="Nome">
    </div>
  </div>
  <div class="form-group">
    <label for="cpSobrenome" class="col-sm-2 control-label">Sobrenome</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="sobrenome" value="<?php echo formValue($sobrenome); ?>" id="cpSobrenome" placeholder="Seu Sobrenome">
    </div>
  </div>
  
   <div id="cpfField" class="form-group">
    <label for="cpCPF" class="col-sm-2 control-label">CPF</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="cpf" value="<?php echo formValue($cpf); ?>" data-title="CPF" data-content="Erro. CPF Ja Cadastrado no sistema. por favor verificar tente logar"  id="cpCPF" placeholder="Seu CPF">
    </div>
  </div>
  
  
   <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">RG</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="rg" value="<?php echo formValue($rg); ?>" id="cpRg" placeholder="Numero do RG">
    </div>
  </div>


     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">Telefone:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="telefone" value="<?php echo formValue($telefone); ?>" id="cpRg" placeholder="Telefone">
    </div>
  </div>


     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">Telefone Comercial:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="telefonecomercial" value="<?php echo formValue($telefonecomercial); ?>" id="cpRg" placeholder="Telefone Comercial">
    </div>
  </div>


     <div class="form-group">
    <label for="cpRg" class="col-sm-2 control-label">Telefone Celular:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control  campotexto" name="telefonecelular" value="<?php echo formValue($telefonecelular); ?>" id="cpRg" placeholder="Telefone Celular">
    </div>
  </div>
  
  
   <div class="form-group">
    <label for="cpDOB" class="col-sm-2 control-label">Data de Nascimento</label>
    <div class="col-sm-10">
		<div class="campodocs">
			<input class="span2   campotexto" readonly date-calendar="true" id="nascimento"  name="dob" size="16" value="<?php echo formValueDate($dob); ?>"   type="text">
		</div>
        
    </div>
  	</div> 


    <!-- <div class="form-group">
    <label for="cpDOB" class="col-sm-2 control-label">Endereço</label>
    <div class="col-sm-10">
    <div class="campodocs">
        <a  href="#modalCadEndereco" data-toggle="modal" data-target="#modalCadEndereco" class="btn btn-primary">Cadastrar Endereço</a>
    </div>
        
    </div>
    </div>-->

                    <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">Sexo:</label>
    <div class="col-sm-10">
      <select id="cpPais" name="sexo" class="form-control  campotexto">
          <option value="M" selected>Masculino</option>
          <option value="F">Feminino</option>
        </select>

    </div>
  </div>

                <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">Estado Civil</label>
    <div class="col-sm-10">
      <select id="cpPais" name="estadocivil" class="form-control  campotexto">
          <option value="1" selected>Solteiro (a)</option>
          <option value="2">Casado (a)</option>
          <option value="3">Divorciado (a)</option>
          <option value="4">Viuvo(a)</option>
        </select>

    </div>
  </div>
    
    

  
        <div class="form-group">
    <label for="cpPais" class="col-sm-2 control-label">País</label>
    <div class="col-sm-10">
    	<select id="cpPais" name="pais" class="form-control  campotexto">
        	<?php echo showPaises(); ?>
        </select>

    </div>
  </div>
  


 
       <div class="form-group">
    <label for="cpCidade" class="col-sm-2 control-label">CAPTCHA</label>
    <div class="col-sm-10">
      <?php echo recaptcha_get_html(Flight::get('captcha_pubkey'), $error); ?>
    </div>
  </div>




        <div class="form-group">
    <label for="cpCidade" class="col-sm-2 control-label"></label>
    <div class="col-sm-10">
        <div class="checkbox center">
    <label>
      <input name="termosservico" type="checkbox"> Li e Aceito os <a href="#termos" data-toggle="modal" data-target="#termos">Termos De Serviços</a>
    </label>
  </div>
    </div>
  </div>

    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" id="btSendForm" name="SendForm" class="btn btn-default">Atualizar</button>
    </div>
  </div>
</form>
</div>
        
	</div>
</div>



<!-- Modal -->
<div class="modal fade" id="termos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Termos  e Serviços Autobrasil Franquia</h4>
      </div>
      <div class="modal-body">
        <? include("paginas/termos/termos.php"); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

  $(function(){

    is_expired = 0;

    var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();



    
    function doAjaxCpf(){

            var end_cpf = '<?php echo sprintf("%s/%d/checkcpf/",rootURL(),Flight::get("actualFranquia")) ?>' + $("input[name=cpf]").val(); 

            $.ajax({

              url: end_cpf,
              dataType: 'json',
              type:'get',

              success: function(data){

                    console.log(data);
                    if(data.existe == true){
                      $("#cpCPF").attr('data-content', "CPF Existente no Sistema");
                      $("#cpCPF").popover('show');
                    }else {
                        $("#cpCPF").attr('data-content', "CPF Validado COm Sucesso");
                        $("#cpCPF").popover('show');

                    }

                  


              }, error: function (xchr,state,error){


              }

          });


    }


    $('#cpCPF').on('change',function(){

        delay(function(){
          doAjaxCpf();
        },900);

    });








    $("#btBuscaCep").on('click',function(e){
    
   
    //$.post()


      
    $(document).ajaxStart(function() {
            $("#buscaCepLoading").show(0.5);

        }).ajaxStop(function() {
            $("#buscaCepLoading").hide(0,5);
        });
      
    var cep = $("#cpCEP").val();
    
    var cep_end = '<?php echo rootURL()."/cep/" ?>' + cep;
      
    $.ajax({
        url: cep_end,
        type: "get",
    dataType:"json",
      
    
    
    
        success: function(dados){
            
        //console.log(dados);
        $('input[name=endereco').val(dados.logradouro);
        $('input[name=bairro').val(dados.bairro);
         $('input[name=cidade').val(dados.cidade);
        //$("#cpBairro").val(dados.bairro);
        //$("#cpCidade").val(dados.cidade);
        //$("#status").html(dados);
        $("#cpEstadoBr option[value=" + dados.estado + "]").prop('selected', true);
        //console.log(dados.logradouro);
       
        },
        error:function(xchr, status, error){
            //alert("failure");
            console.log(xchr + ' ' + error);
            //$("#result").html('There is error while submit');
        }
    }); // fimajax
  
    });


  });
</script>


  
   

  <!-- Modal -->
<div class="modal fade" id="modalCadEndereco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Cadastrar um Novo Endereço</h4>
      </div>
      <div class="modal-body">

          <div id="validaCep" class="form-group">
    <label for="cpCep" class="col-sm-2 control-label">CEP</label>
    <div class="col-sm-10">
      <div class="campodocs">
        <input class="span2  campocep" name="cep" id="cpCEP" value="<?php echo formValue($cep); ?>"  size="16" type="text">
                <a id="btBuscaCep" class="btn btn-primary btn-sm">Buscar CEP</a>
      
        </div>

    </div>
  </div>
  
        

          <div class="form-group ">
    <label for="cpEnd" class="col-sm-2 control-label">Endereço</label>
    <div class="col-sm-10">
      <input type="text" name="endereco" class="form-control campotexto" value="<?php echo formValue($end); ?>" id="cpEnd" placeholder="Endereço">
    </div>
  </div>
  
       <div class="form-group ">
    <label for="cpBairro" class="col-sm-2 control-label">Bairro</label>
    <div class="col-sm-10">
      <input type="text" name="bairro" class="form-control campotexto" id="cpBairro" value="<?php echo formValue($bairro); ?>" placeholder="Bairro">
    </div>
  </div>
       <div class="form-group">
    <label for="cpCidade" class="col-sm-2 control-label">Cidade</label>
    <div class="col-sm-10">
      <input type="text" name="cidade" class="form-control  campotexto"   value="<?php echo formValue($cidade); ?>" id="cpCidade" placeholder="Cidade">
    </div>
  </div>

       <div class="form-group">
    <label for="cpEstadoBr" class="col-sm-2 control-label">Estado</label>
    <div class="col-sm-10">
      <select id="cpEstadoBr" name="estado" class="form-control  campotexto">
          <?php echo estadosBrasileiros($uf); ?>
        </select>

    </div>
  </div>
  



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar Alterações</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->