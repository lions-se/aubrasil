
<? 

		$upload = '/uploads/thumbs/';
		$test_path = 'uploads/thumbs/';
	   	$nome  = $prodquery->querydata[0]['NOME'];
		$img1  = $prodquery->querydata[0]['IMAGEM1'];
		$img2  = $prodquery->querydata[0]['IMAGEM2'];
		$img3  = $prodquery->querydata[0]['IMAGEM3'];
		$img4  = $prodquery->querydata[0]['IMAGEM4'];
		$img5  = $prodquery->querydata[0]['IMAGEM5'];
		$img6  = $prodquery->querydata[0]['IMAGEM6'];
		$preco = $prodquery->querydata[0]['PRECO'];
		$produtoid = $prodquery->querydata[0]['PRODUTO_ID'];
		$peso = $prodquery->querydata[0]['PESO'];
		$altura = $prodquery->querydata[0]['ALTURA'];
		$largura = $prodquery->querydata[0]['LARGURA'];
		$comprimento = $prodquery->querydata[0]['COMPRIMENTO'];
		$descr = $prodquery->querydata[0]['DESCRICAO'];




		
		$imgfinal1 = file_exists( realpath($test_path.$img1)) ? $upload.$img1 : '/imagens/sem_foto.jpg';
		$imgfinal2 = file_exists( realpath($test_path.$img2)) ? $upload.$img2 : '/imagens/sem_foto.jpg';
		$imgfinal3 = file_exists( realpath($test_path.$img4)) ? $upload.$img3 : '/imagens/sem_foto.jpg';
		$imgfinal4 = file_exists( realpath($test_path.$img4)) ? $upload.$img4 : '/imagens/sem_foto.jpg';
		$imgfinal5 = file_exists( realpath($test_path.$img5)) ? $upload.$img5 : '/imagens/sem_foto.jpg';
		$imgfinal6 = file_exists( realpath($test_path.$img6)) ? $upload.$img6 : '/imagens/sem_foto.jpg';
		
		/*
		$imgfinal2 = file_exists($upload.$img2) ? $upload.$img2 : '/imagens/sem_foto.jpg';
		$imgfinal3 = file_exists($upload.$img3) ? $upload.$img3 : '/imagens/sem_foto.jpg';
		$imgfinal4 = file_exists($upload.$img4) ? $upload.$img4 : '/imagens/sem_foto.jpg';
		$imgfinal5 = file_exists($upload.$img5) ? $upload.$img5 : '/imagens/sem_foto.jpg';
		$imgfinal6 = file_exists($upload.$img6) ? $upload.$img6 : '/imagens/sem_foto.jpg';
		*/

?>


	<h1 class="text-center"> <?php echo  $nome;  ?></h1>



	<div class="ajuste-conteudo">

	<? if(isset($_SESSION['flash'])){

		flash();
		kill_alert();

	} ?>


			<div class="galeria">
				<img src="<?php  echo  $imgfinal1; ?>" class="img-thumbnail img-responsive">
				<img src="<?php  echo  $imgfinal2; ?>" class="img-thumbnail img-responsive">
				<img src="<?php  echo  $imgfinal3; ?>" class="img-thumbnail img-responsive">
				
			</div>
			<div class="galeria">
				<img src="<?php  echo  $imgfinal4; ?>" class="img-thumbnail img-responsive">
				<img src="<?php  echo  $imgfinal5; ?>" class="img-thumbnail img-responsive">
				<img src="<?php  echo  $imgfinal6; ?>" class="img-thumbnail img-responsive">
			</div>

		<p class="preco-texto"><strong>Preço:</strong> <?php echo "R$ ".number_format($preco,2,'.',''); ?></p>

		<ul class="nav nav-tabs">
			<li class="active"><a href="#descricao" data-toggle="tab" >Descrição do Produto</a></li>
			<li><a href="#mensagens" data-toggle="tab">Especificações Técnicas</a></li>
		
		</ul>
	<!-- Tab panes -->
		<div class="tab-content">
  			<div class="tab-pane active format-descricao" id="descricao">
  				<p><?php echo $descr; ?></p> 
  			</div>
  			<div class="tab-pane" id="mensagens">

  				<div class="row">
  					<div class="col col-sm-3">
  						<strong>Largura: </strong>
  					</div>

  					<div class="col col-sm-4">
  						<p><?php echo  sprintf("%.2f",$largura * 1000); ?> cm </p>
  					</div>
  				</div>



  				<div class="row">
  					<div class="col col-sm-3">
  						<strong>Altura: </strong>
  					</div>

  					<div class="col col-sm-4">
  						<p><?php echo  sprintf("%.2f",$altura * 1000); ?> cm </p>
  					</div>
  				</div>

  					<div class="row">
  					<div class="col col-sm-3">
  						<strong>Comprimento: </strong>
  					</div>

  					<div class="col col-sm-4">
  						<p><?php echo  sprintf("%.2f",$comprimento * 1000); ?> cm </p>
  					</div>
  				</div>



  				<div class="row">
  					<div class="col col-sm-3">
  						<strong>Peso: </strong>
  					</div>

  					<div class="col col-sm-2">
  						<p><?php echo  sprintf("%.2f",$peso); ?>kg </p>
  					</div>
  				</div>


  			</div>
		</div>
		<div class="btComprar">
			
			<?php echo href('add-carrinho/'.$produto_id.'-'.slug($nome) ,'<span class="glyphicon glyphicon-shopping-cart"></span>
				Colocar no Carrinho', 'btn btn-success btn-lg btn-block'); ?>
			
		</div>
	</div>
	


   <script>
	  $(document).ready(function(e) {
       $("img[data-gallery=true]").elevateZoom({
  			zoomType				: "inner",
  			cursor: "crosshair"
		});
    });
	</script>

