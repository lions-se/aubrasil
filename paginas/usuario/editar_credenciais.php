
<div class="container">
	<form method="post" action="<?php printf("/%d/editar-perfil/do_edit_credentials",Flight::get('actualFranquia')); ?>" id="formCredenciais">
		<legend>Alterar Credenciais</legend>		
			<div class="form-group"><label for="" class="col-sm-2 control-label">Login</label><input disabled type="text" class="form-control campotexto" value="<?php echo $user['LOGIN']; ?>" ></div>
			<div class="form-group"><label for="" class="col-sm-2 control-label">Senha Antiga:</label><input type="password" name="passwd" class="form-control campotexto"></div>
			<div class="form-group"><label for="" class="col-sm-2 control-label">Nova Senha</label><input type="password" name="newpasswd" class="form-control campotexto"></div>
			<button type="submit" class="btn btn-lg btn-success">Atualizar Credenciais</button>
	</form>
</div>