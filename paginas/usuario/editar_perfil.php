<div class="container">
	      <?
      /**
       * Mostra Mensagens de Erro e etc..
       */
      if(isset($_SESSION['flash'])){
        echo flash();
        kill_alert();
      }

     ?>
	<div class="row">

		<div class="col col-sm-7">

	<li class="media">
    <a class="pull-left" href="#">
      <img class="media-object" src="..." alt="...">
    </a>
    <div class="media-body">
      <h4 class="media-heading">Codigo de Referencia:</h4>
     	<p>Seu Codigo de Referência: <strong> <?php echo $user['CODIGO_REFERENCIA']; ?></strong> </p>
    </div>
  </li>
  

  <li class="media">
    <a class="pull-left" href="#">
      <img class="media-object" src="..." alt="...">
    </a>
    <div class="media-body">
      <h4 class="media-heading">Visualizar Pedidos</h4>
      <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, officia, quod, repudiandae consequuntur deleniti optio placeat quisquam recusandae vero rerum eos necessitatibus non ad dolorum in autem ex nobis laborum?</small>
      <a class="btn btn-sm btn-default"  href="<?php printf('/%s/editar-perfil/order', Flight::get('actualFranquia') ); ?>">Clique Aqui</a>
    </div>
  </li>



<li class="media">
    <a class="pull-left" href="#">
      <img class="media-object" src="..." alt="...">
    </a>
    <div class="media-body">
      <h4 class="media-heading">Alterar Perfil de Usuario</h4>
      <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, voluptatem, hic, quidem maxime assumenda ratione ex magnam excepturi impedit natus soluta optio eligendi laudantium possimus quibusdam dolorum nihil. Commodi, fugiat.</small>
      		 
      <a class="btn btn-sm btn-default" href="<?php printf('/%s/editar-perfil/user', Flight::get('actualFranquia') ); ?>">Clique Aqui</a>
    </div>
  </li>



  <li class="media">
    <a class="pull-left" href="#">
      <img class="media-object" src="..." alt="...">
    </a>
    <div class="media-body">
      <h4 class="media-heading">Alterar Credenciais</h4>
      <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, illum, aperiam est quis asperiores dolorum quod ab unde dicta quisquam veritatis perferendis et obcaecati eos dolores porro suscipit at temporibus.</small>
      <a class="btn btn-sm btn-default"  href="<?php printf('/%s/editar-perfil/credentials', Flight::get('actualFranquia') ); ?>">Clique Aqui</a>
    </div>
  </li>

  <li class="media">
    <a class="pull-left" href="#">
      <img class="media-object" src="..." alt="...">
    </a>
    <div class="media-body">
      <h4 class="media-heading">Cadastrar Endereços</h4>
      <small>Cadastre os Endereços para Realizar a Entrega dos Produtos com mais conforto</small>
      <a class="btn btn-sm btn-default"  href="<?php printf('/%s/editar-perfil/cadastra_endereco', Flight::get('actualFranquia') ); ?>">Clique Aqui</a>
    </div>
  </li>


</ul>

		</div>



		<div class="col col-sm-4">

			<div class="panel panel-default">
  				<div class="panel-heading">Ajuda</div>
  				<div class="panel-body">
    				Panel content
  				</div>
			</div>


		</div>



	</div>

</div>