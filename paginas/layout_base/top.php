<? 

//$franquia = Flight::franquia(); 
$carrinho = $_SESSION['carrinho'];

//var_dump(Flight::request());

$id = Flight::get('actualFranquia');

//var_dump($parse); 

$cred = $_SESSION['credentials'];

$login = (isset($_SESSION['credentials'])) ? "Editar Perfil (".$cred->nome.")" : 'Login';
$link  = (isset($_SESSION['credentials'])) ? sprintf("/%d/editar-perfil/",$id) : sprintf("/%d/login", Flight::get('actualFranquia') );



if($carrinho->count() === 0){

  $prod_texto = sprintf("%s", 'Sem Produtos') ;
}else {

  $prod_texto = ($carrinho->count() > 1 ) ? sprintf("%d %s", $carrinho->count(), 'Produtos') : sprintf("%d %s", $carrinho->count(), 'Produto');
}




$nomefranquia = $franquiaData[0]['NOMEFRANQUIA'];
$slogan =  $franquiaData[0]['SLOGAN'];
$telefone =  $franquiaData[0]['TELFRANQUIA'];
?>
	
	<div class="page_top">

		<div class="align_top">
			<div class="row">
				<div class="col col-md-2">
					<div class="logo_franquia"><a href="<?php echo sprintf("%s/%d", rootURL(), Flight::get('actualFranquia')); ?>"><img src="<?php echo rootURL(); ?>/imagens/extruturais/logo_base.png" alt=""></a></div>
				</div>
				<div class="col col-md-6" style="width:550px;">
					<div class="titulo_franquia">
						<ul>
							<li><h1><?php echo $nomefranquia; ?><h1></li>
							<li><span><?php echo $slogan; ?></span></li>
						</ul>
					</div>

				</div>
				<div class="col col-md-4" style="width:255px; float:right;">
				<div class="bt_login_menu">
					
					<div class="btn-group">

    					<button type="button" class="btn btn-login dropdown-toggle" data-toggle="dropdown" id="login">
      						 
      						 <? 
      						 	if(!isset($_SESSION['credentials'])){
      						 		echo "<span class='glyphicon glyphicon-lock'></span> Login";
      						 	}else {
      						 		
      						 		echo "<span class='glyphicon glyphicon-lock'> ".$_SESSION['credentials']->nome;
      						 	}
      						  ?>
      						
    					</button>
    					<ul class="dropdown-menu">
    						 <? 
      						 	if(isset($_SESSION['credentials'])){ ?>
									<li><a href="<?php echo rootURL().'/'.Flight::get('actualFranquia').'/editar-perfil' ?>"><i class="fa fa-user"></i> Editar Perfil</a></li>
									 <li><a href="<?php echo rootURL().'/'.Flight::get('actualFranquia').'/editar-perfil/order' ?>" class="logout"><i class="fa fa-truck"></i> Ver Pedidos</a></li>
									<li class="divider"></li>
									<li><a href="<?php echo rootURL().'/'.Flight::get('actualFranquia').'/logout' ?>"><span class='glyphicon glyphicon-lock'> Logout</a></li>
      						 	<? }else { ?>
      						<li><a href="<?php echo rootURL().'/'.Flight::get('actualFranquia').'/login' ?>"><span class='glyphicon glyphicon-lock'>  Fazer Login</a></li>
      						<li><a href="<?php echo rootURL().'/'.Flight::get('actualFranquia').'/cadastro_cliente' ?>"><span class='glyphicon glyphicon-lock'>  Cadastrar</a></li>
      							<? } ?>
                             
    					</ul>
                        
                        
                    <div class="col col-md-4">
                    <a id="carrinho" href="<?php echo rootURL().'/'.Flight::get('actualFranquia').'/carrinho' ?>" class="btn btn-login">
                    <span class="glyphicon glyphicon-shopping-cart"> </span>
                    <?php echo $prod_texto; ?>
					</a>
				    </div>
                        
                        
                        
  					</div>

				</div>
				</div>


			</div>
		</div>
	
	</div>
