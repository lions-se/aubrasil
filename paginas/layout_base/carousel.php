<div class="produtos_carousel">
	<div class="wrapper">
		<div class="list_carousel responsive">
		<ul id="carousel_slide">
			<? foreach ($produtos->querydata as $key => $value) { 

				$imgpath = rootURL().'/uploads/thumbs/';
				$image = $value['IMAGEM1'];

				$imgfinal= !is_file($imgpath.$image) ? $imgpath.$image : '/imagens/sem_foto.gif';


			?>
			<li><a href="/<? echo genLinks(array(Flight::get('actualFranquia'), 'produto', $value['PRODUTO_ID'].'-'.slug($value['NOME']).'.html')); ?>"><img class="img-thumbnail img-responsive"  src="<?php echo $imgfinal; ?>" width="88" height="126" alt="image"/></a></li>
			<? }  ?>
		</ul>
		<div class="clearfix"></div>
</div>
</div>
</div>

<script type="text/javascript">
	
	$(function(){

		$('#carousel_slide').carouFredSel({
					width: '100%',
		

				});
	});

</script>
