<?

setlocale(LC_MONETARY, 'pt_BR');

$carrinho = Flight::get('cart');


$total = 0.0;


$url = explode('/',Flight::request()->url);
$id = $url[1];



$cartId = array();



//$req = simul_post("http://local.autobrasil/3/carrinho/atualiza-produtos", array('ids' => array(1,2)));
?>

<div class="container-fluid">

    <h3> Carrinho de Compras</h3>

    

<form id="formCarrinho" method="post">
<table class="table table-condensed table-striped table-bordered">
    <thead>
        <tr>
            <th class="col col-sm-1">&nbsp;</th>
            <th class="col col-sm-1">Imagem</th>
            <th class="col col-sm-1">Nome</th>
            <th class="col col-sm-1">Quantidade</th>
            <th class="col col-sm-1">Preço Unitario</th>
            <th class="col col-sm-1">Desconto:</th>
            <th class="col col-sm-1">Peso</th>
            <th class="col col-sm-1">Subtotal</th>
        </tr>
    </thead>

    <tbody>

        <? foreach($carrinho->all_products() as $val){ 


            $exibe_total = $val->price * $val->qty -  ($val->desc_produto * $val->qty );
            array_push($cartId, $val->id);
            $total += ($val->price * $val->qty) - ($val->desc_produto * $val->qty );
            $peso +=  ($val->peso * $val->qty);

            $soma_desconto += $val->desc_produto * $val->qty;




        ?>
        <tr row-id="<?php echo $val->id; ?>">
            <td  class="col col-sm-1">
                <button data-id="<?php echo $val->id; ?>"   type="button" class="btn btn-xs">
                    <span class="glyphicon glyphicon-remove"></span>
                    Apagar
                </a>
            </td>
            <td class="col col-sm-1"><img src="/uploads/thumbs/<?php echo $val->img; ?>" /> </td>
            <td class="col col-sm-1"><?php echo $val->name; ?></td>
             <td class="col col-sm-1">
                <input type="hidden" name="ids[]" value="<?php echo $val->id ?>">
                <input class="campocep" name="qty[]" data-spinner="true" type="text" value="<?php echo $val->qty; ?>" />

             </td>
            <td class="col col-sm-1">R$ <?php echo number_format($val->price,2,',','.'); ?></td>
            <td class="col col-sm-1">R$ <? echo dinheiroBR($soma_desconto,2);  ?></td>
            <td class="col col-sm-1"><? echo converteKilos($val->peso); ?></td>
            <td class="col col-sm-1"><?php echo "R$ ".number_format($exibe_total,2,',','.');  ?></td>
        </tr>
        <?


         } ?>

        <tr>
            
            <? if($peso > 30.00){ 

            alert('Aviso', 'As Agencias dos Correios tem uma limitação de 30kg por encomenda', ALERTA_AVISO);

            }

            ?>

            <td colspan="4">Peso Total: <small><?php echo number_format($peso,2,',','.'); ?>kg (max 30kg) </small></td>
            <td colspan="4">Total:  <small>R$ <?php echo number_format($total,2,',','.'); ?></small></td>

        </tr>

    </tbody>
</table>
</div>










<div class="container">


    <div class="row">
        <div class="col col-sm-5">
            <button type="button" name="btAtualiza" class="btn btn-default btn-md" > Atualizar Carrinho </button>

        </div>


        <div class="col col-sm-5">
            <a  href="/<?php echo $id.'/carrinho/checkout/' ?>" name="btAtualiza" class="btn btn-success btn-md" >Prosseguir Compra </a>
        </div>
    </div>
  <div class="clearfix">&nbsp;</div>
    <div class="row">
        
        <div class="col col-sm-6">
            <legend>Simulação de Preço de frete</legend>
<p class="text-muted">Esses valores serão adicionados automaticamente</p>
<!-- INICIO CODIGO PAGSEGURO -->
<a href="https://pagseguro.uol.com.br/desenvolvedor/simulador_de_frete.jhtml?CepOrigem=<?php echo $franquia['CEPFRANQUIA']; ?>&amp;Peso=0.300&amp;Valor=0,00" id="ps_freight_simulator" target="_blank"><img src="https://p.simg.uol.com.br/out/pagseguro/i/user/imgCalculoFrete.gif" id="imgCalculoFrete" alt="Cálculo automático de frete" border="0" /></a>
<script type="text/javascript" src="https://p.simg.uol.com.br/out/pagseguro/j/simulador_de_frete.js"></script>
<!-- FINAL CODIGO PAGSEGURO -->
        </div>

        


    </div>



    
</form>






</div>

<script type="text/javascript">
    
    /*
    function removeProduct(id){
        $(function(){
            $("button[data-id="+ id + "]").on('click', function(){

                var row = $("tr[row-id=" + id + "]");

               row.hide('slow');
       
            });
        
        });

    }




    */


        $("input[data-spinner=true").spinner({
            min: 1,
            max: 999
        });

    $(":button[data-id]").on('click',function(){

        var id = $(this).attr('data-id');

       



        $.ajax({

            url: '<?php echo rootURL() ?>/<?php echo $id; ?>/carrinho/remove-produto/' + id,
            type:'post',
            dataType:'json',
            data: {'product_id':id},
            success: function(data){

                if(data.result == true){
                    $('tr[row-id=' + id + ']').hide('slow');
                }
                console.log(data);
                //location.reload();
            }, error: function(xchr,obj,error){

                console.error(xchr + ' ' + error);
            },
            complete: function(d){

                location.reload();
            }

        });
       

    });


    
    
    $("button[name=btAtualiza]").on('click', function(){

        event.preventDefault();

        $.ajax({

            url: '<?php echo rootURL() ?>/<?php echo $id; ?>/carrinho/atualiza-produtos',
            type:'post',
            dataType:'text',
            data: $("#formCarrinho").serialize(),
            success: function(data){

                console.log(data);
                location.reload();
            }

        });

    });
      


</script>
