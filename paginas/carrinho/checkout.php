<?

setlocale(LC_MONETARY, 'pt_BR');


$url = explode('/',Flight::request()->url);
$id = Flight::get('actualFranquia');

if($_SESSION['cpfChecked']){
  $_SESSION['cpfChecked'] = false;
}else if(!isset($_SESSION['cpfChecked'] )){
  $_SESSION['cpfChecked'] = false;
}



?>
<div class="container-fluid">
	    <?
        if(isset($_SESSION['flash'])){

            flash();
            kill_alert();
        }

    ?>
  <h1>Confirmação de Compra</h1>



<form id="formCarrinho" action="<? printf("/%d/carrinho/finalizar",$id); ?>"  method="post">
<table class="table table-condensed table-striped">
    <thead>
        <tr>

            <th class="col col-sm-1">Imagem</th>
            <th class="col col-sm-1">Nome</th>
            <th class="col col-sm-1">Preço Unitario</th>
            <th class="col col-sm-1">Quantidade</th>
            <th class="col col-sm-1">Peso</th>
            <th class="col col-sm-1">Desconto do Produto:</th>
            <th class="col col-sm-1">Subtotal</th>
        </tr>
    </thead>

    <tbody>

        <? foreach($querydata as  $key => $val){ 

            $soma_desconto += $val->desc_cliente *  $val->qty;

            $total_bruto += ($val->price * $val->qty) - ($val->desc_produto * $val->qty);
            $produto_calculo = ($val->price * $val->qty) - ($val->desc_produto * $val->qty);





            //var_dump($total_bruto);


            $val_referencia = isset($_SESSION['descontoFator']) ? sprintf("R$ %.2f - R$ %.2f", dinheiroBR($produto_calculo,2), ($val->desc_cliente * $val->qty) ) : sprintf("R$ %.2f", $produto_calculo);

             $resultado_total = isset($_SESSION['descontoFator']) ? $total_bruto - $soma_desconto : $total_bruto;

             $url_img = "uploads/thumbs/";
             $real_path = realpath($url_img.$val->img);

             $teste_dir = file_exists($real_path) ? true : false;

             $imagem_nome = ($teste_dir == true) ? '/'.$url_img.$val->img : '/imagens/sem_foto.jpg';
            

        ?>
        <tr row-id="<?php echo $val->id; ?>">
            


           <td class="span6"><img src="<?php echo  $imagem_nome; ?>" width="50" height="50" /></td>
           <td class="span6"><?php echo $val->name; ?></td>
           <td class="span6"><?php echo dinheiroBR($val->price,2); ?></td>
           <td class="span6"><?php echo $val->qty; ?></td>
           <td class="span6"><?php printf("%.2f", $val->peso) ?>kg </td>
           <td class="span6"><p class="text-muted">R$<?php echo number_format($val->desc_produto * $val->qty,2,",",".");  ?></p></td>
           <td class="span6"><?php  echo $val_referencia;  ?></td>

        </tr>
        <?
            

         
         } ?>






          <tr>
            <td class="col col-sm-1"> &nbsp;</td>
            <td class="col col-sm-1">&nbsp;</td>
            <td class="col col-sm-1">&nbsp;</td>
            <td class="col col-sm-1">&nbsp;</td>
            <td class="col col-sm-1">&nbsp;</td>
            <td class="col col-sm-1">&nbsp;</td>
            <td class="col col-sm-1">Total: <?php echo number_format($resultado_total,2,',','.'); ?> </a></td>
        </tr>


    </tbody>
</table>




</form>
 <form id="formComprador"  action="<? printf("/%d/carrinho/finalizar",$id); ?>" method="post" class="form-horizontal">
                <legend>Dados do Comprador</legend>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nome:</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><? echo $comprador['NOME'] ?></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sobrenome:</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><? echo $comprador['SOBRENOME'] ?></p>
                        </div>
                    </div>


                       <div class="form-group">
                        <label class="col-sm-2 control-label">CPF:</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><? echo $comprador['CPF'] ?></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">RG:</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><? echo $comprador['RG'] ?></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email Primario</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><? echo $comprador['LOGIN'] ?></p>
                        </div>
                    </div>


                             <legend>Selecione o Metodo de Entrega</legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="freteOpcao" id="optionsRadios-2" value="1" checked>
                                         PAC
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="freteOpcao" id="optionsRadios-3" value="2">
                                         SEDEX
                                    </label>
                                </div>
                            </div>
                        </div>
                       

                  

                   

                   <legend>Escolha Endereço  para Entrega</legend>



                    <? 
                    if(empty($enderecos)){

                        print <<<PRINTA

                        <p>Você Não possui nenhum endereço cadastrado. por favor adicione pelo menos um<p>

PRINTA;

                    }


                    if(is_null($enderecos) || empty($enderecos)){ ?>



                    <? } ?>


                     <? if(!is_null($enderecos)){ 

                         $primeiro = true;


                        ?>

                       

                        <?  foreach ($enderecos as $value) {         ?>
                   
                        
                        <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-10">
                                
                            <? $escolhido = $primeiro ? 'checked' : ''; ?>

                            <div class="radio">
                                    
                                    <? if($primeiro){ ?>

                                     <label>
                                    <input type="radio" name="endOpcao" id="optionsRadios-<? echo $value['ENDERECO_ID']; ?>" value="<? echo $value['ENDERECO_ID']; ?>" checked>
                                         <? echo $value['ENDERECO'] ?>,  <? echo $value['NUMERO']  ?> - <? echo $value['CIDADE']  ?>
                                    </label>


                                   <? 

                                   $primeiro = false;
                                    }else { ?>

                                <label>
                                    <input type="radio" name="endOpcao" id="optionsRadios=<? echo $value['ENDERECO_ID']; ?>" value="<? echo $value['ENDERECO_ID']; ?>" >
                                         <? echo $value['ENDERECO'] ?>,  <? echo $value['NUMERO']  ?> - <? echo $value['CIDADE']  ?>
                                </label>
                                <? }  ?>
                            </div>

                           
                        </div>
                    </div>
                       <? } ?>

                    <? } ?>
                    


                    <a class="btn btn-default btn-sm" href="<?php echo sprintf("/%d/editar-perfil/cadastra_endereco", Flight::get('actualFranquia')); ?>">Cadastrar Novo Endereço</a>
                </form>





<form action="<?php printf("/%d/carrinho/recomendacao/",$id); ?>" method="post" class="form-horizontal">
      <legend> Recomendação </legend>
                        <p>Adicione  codigo de um outro cliente que recomendou para obter descontos nos produtos</p>
                        <div class="form-group ">
                            <? 

                                $class = isset($_SESSION['descontoFator']) ? 'disabled' : ''
                            ?>

                            <label for="cpNome" class="col-sm-2 control-label">Codigo de Referencia:</label>
                                <div class="col-sm-10">
                                    <input type="text" <?php echo $class; ?> class="form-control campotexto" name="recomendacao" value="<?php echo $_SESSION['codReferencia']; ?>"  id="cpLogin" placeholder="Coloque o Codigo de Referencia">         
                                </div>
                        </div>

                        <?php if(isset($_SESSION[descontoFator])) {?>
                            <a href="<?php printf("/%d/carrinho/apagar-referencias",$id); ?>" class="btn  btn-sm" href="#">
                                <span class="glyphicon glyphicon-remove"></span> Apagar Referencia</a>
                        <?php }else { ?>

                        <button type="submit" class="btn btn-success btn-sm" href="#"> Checar Referencia</button>
                        <? } ?>


               

                       




</form>




                   <div class="row-fluid">
                        <div class="col col-md-6">&nbsp;</div>
                        <div class="col col-md-3"><a class="btn btn-default" href="/<?php echo $id ?>/carrinho">Voltar</a></div>
                        <div class="col col-md-3"><button id="btFinalizaCompra" type="type" class="btn btn-success btn-lg">Finalizar Compra</button></div>
                    </div>




</div>

<?
    
 ?>

<script>

    $("#btFinalizaCompra").on('click',function(){

        event.preventDefault();
        $("#formCarrinho").submit();
        $("#formComprador").submit();

    });
	
	$("#btCalcFrete").on('click',function(){
		var end = '/<?php echo $id; ?>/carrinho/calcula-frete';
		var cep = $('#cpCEP').val();

			$.post(end, {'cep': cep , 'servico': $("input[name=inputWalls]:checked").val(), 'peso': <?php echo number_format($peso,2,'.',''); ?>  }, function(data){
			   
          console.log(data);

				if(data.result == 1){
					var total = <?php echo $total_bruto - $soma_desconto ?>;
					var tot = (total +  data.frete) 

			//console.log(tot);


			
					$(".custo").html('<p class="text-center frete">Subtotal: R$ ' + total.toFixed(2) + ' +  R$ ' + data.frete.toFixed(2)  + ' (frete) </p> ');
					$(".total").html('<p class="text-center total">Total: R$ ' +  tot.toFixed(2) +' </p> ');
			//$('.frete').val("+ " + data.frete);
			//console.log(data);

		

			}


		});
		
	});
</script>
    

