
<div class="container">
	<div class="alert alert-danger">Atenção: Esta Alteração é irreversivel</div>
	<h1>Apagar Endereço:</h1>
	<form action="<?php printf("/%d/editar-perfil/cadastra_endereco/del_endereco", Flight::get('actualFranquia')); ?>" method="post" class="form-horizontal">
			
		<input type="hidden" value="<?php echo $data['ENDERECO_ID']?>" name="endereco_id">

    	<div id="validaCep" class="form-group">
    		<label for="cpCep" class="col-sm-2 control-label">CEP</label>
    		<div class="col-sm-3">
     			<div class="form-inline">
					<input class="span2 form-control  campotexto" name="cep" id="cpCEP" value="<?php echo formValue($data['CEP']); ?>"  size="16" type="text">
					<button type="button" id="btBuscaCep" class="btn btn-sm btn-primary">Busca CEP</button>

			  </div>
    		</div>
  		</div>


			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Endereço</label>
				<div class="col-sm-10">
					<input type="text" id="cpEndereco" name="endereco"  value="<?php echo formValue($data['ENDERECO']); ?>" class="form-control campocep">
				</div>
			</div>

						<div class="form-group">
				<label for="" class="col-sm-2 control-label">Numero</label>
				<div class="col-sm-10">
					<input type="text" id="cpNumero" name="numero" value="<?php echo formValue($data['NUMERO']); ?>"  class="form-control campotel">
				</div>
			</div>


						<div class="form-group">
				<label for="" class="col-sm-2 control-label">Bairro</label>
				<div class="col-sm-10">
					<input type="text" id="cpBairro" name="bairro" value="<?php echo formValue($data['BAIRRO']); ?>"  class="form-control campocep">
				</div>
			</div>


			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Cidade:</label>
				<div class="col-sm-10">
					<input type="text" id="cpCidade" name="cidade" value="<?php echo formValue($data['CIDADE']); ?>" class="form-control campotel">
				</div>
			</div>

			
			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Estado</label>
				<div class="col-sm-10">
					<select name="selectEstado" id="cpEstado">
							
						<?php echo estadosBrasileiros($data['ESTADO']);  ?>

					</select>
				</div>
			</div>


		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
      			<button type="submit" id="btSendForm" name="SendForm" class="btn btn-default">Apagar Endereço</button> 
    		</div>
  		</div>


	</form>

</div>


</div>


<script type="text/javascript">
	$("#btBuscaCep").on('click', function(){

		var cep = $("#cpCEP").val();


	


		$.ajax({

			url: "<?php echo rootURL().'/'.Flight::get('actualFranquia').'/cep/'; ?>" + cep,
			type: 'get',
			dataType:'json',

			success: function(data){
					
				console.log(data);
				$("#cpEndereco").val(data.logradouro);
				$("#cpBairro").val(data.bairro);
				$("#cpCidade").val(data.cidade);
				$("#cpEstado option[value=" + data.estado + "]").prop('selected', true);

			}, error: function(xchr,status,error){

			} 
		})
		

	});
</script>