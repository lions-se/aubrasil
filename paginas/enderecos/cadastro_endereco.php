<div class="container">
	      <?
      if(isset($_SESSION['flash'])){
        echo flash();
        kill_alert();
      }

     ?>

     <h1>Lista de Endereços <small>Voce Pode Cadastrar até 5 Endereços</small></h1>

	<table class="table table-striped table-bordered">
		<thead>
				<tr>
					<th>Endereço:</th>
					<th>CEP:</th>
					<th>Ações</th>
				</tr>
			</thead>
		<tbody>

			<? foreach($enderecos->querydata as $key => $value){ ?>
				<tr>
					<td><p><?php echo $value['ENDERECO'] ?>,<?php echo $value['NUMERO'] ?> -  <?php echo $value['CIDADE'] ?> </p></td>
					<td><?php echo $value['CEP'] ?></td>
					<td>
						<div class="row">
							<div class="col col-sm-6"><a href="<? echo Util::URL(sprintf("%s/%d", 'do_edita_endereco', $value['ENDERECO_ID'] )); ?>" class="btn btn-lg btn-warning"><span class="glyphicon glyphicon-pencil"></span>  Editar</a></div>
							<div class="col col-sm-6"><a href="<? echo Util::URL(sprintf("%s/%d", 'do_apaga_endereco', $value['ENDERECO_ID'] )); ?>" class="btn btn-lg btn-danger"><span class="glyphicon glyphicon-remove"></span> Apagar</a></div>
						</div>
					</td>
				</tr>
			<? } ?>

				<tr colspan="3">
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><small>Numero de Cadastro de Endereços Disponiveis: <strong><?php echo ( 5 - $enderecos->rowsAffected()); ?>  Endereços<strong></small></td>
				</tr>
			</tbody>	
	</table>



	
	<form action="<?php printf("/%d/editar-perfil/cadastra_endereco/do_cadastra_endereco", Flight::get('actualFranquia')); ?>" method="post" class="form-horizontal">
			


    	<div id="validaCep" class="form-group">
    		<label for="cpCep" class="col-sm-2 control-label">CEP</label>
    		<div class="col-sm-3">
     			<div class="form-inline">
					<input class="span2 form-control  campotexto" name="cep" id="cpCEP" value="<?php echo formValue($cep); ?>"  size="16" type="text">
					<button type="button" id="btBuscaCep" class="btn btn-sm btn-primary">Busca CEP</button>

			  </div>
    		</div>
  		</div>


			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Endereço</label>
				<div class="col-sm-10">
					<input type="text" id="cpEndereco" name="endereco"  class="form-control campocep">
				</div>
			</div>

						<div class="form-group">
				<label for="" class="col-sm-2 control-label">Numero</label>
				<div class="col-sm-10">
					<input type="text" id="cpNumero" name="numero"  class="form-control campotel">
				</div>
			</div>


						<div class="form-group">
				<label for="" class="col-sm-2 control-label">Bairro</label>
				<div class="col-sm-10">
					<input type="text" id="cpBairro" name="bairro"  class="form-control campocep">
				</div>
			</div>


			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Cidade:</label>
				<div class="col-sm-10">
					<input type="text" id="cpCidade" name="cidade"  class="form-control campotel">
				</div>
			</div>

			
			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Estado</label>
				<div class="col-sm-10">
					<select name="selectEstado" id="cpEstado">
							
						<?php echo estadosBrasileiros();  ?>

					</select>
				</div>
			</div>


		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
      			<button type="submit" id="btSendForm" name="SendForm" class="btn btn-default">Cadastrar Novo Endereço</button> 
    		</div>
  		</div>


	</form>

</div>


<script type="text/javascript">
	$("#btBuscaCep").on('click', function(){

		var cep = $("#cpCEP").val();


	


		$.ajax({

			url: "<?php echo rootURL().'/'.Flight::get('actualFranquia').'/cep/' ?>" + cep,
			type: 'get',
			dataType:'json',

			success: function(data){
					
				console.log(data);
				$("#cpEndereco").val(data.logradouro);
				$("#cpBairro").val(data.bairro);
				$("#cpCidade").val(data.cidade);
				$("#cpEstado option[value=" + data.estado + "]").prop('selected', true);

			}, error: function(xchr,status,error){

				console.log(xchr+' '+error);

			} 
		})
		

	});
</script>