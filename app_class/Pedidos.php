<?

class Pedidos {


	public static function visualizaPedidos($franquia_id){
		if(!Util::checaUsuario($franquia_id)){
			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(sprintf("/%d/login/",$franquia_id));
		}


		$db = Flight::get('db');

		$query_pedidos = "SELECT
cliente.CLIENTE_ID,
ordem_pedido.DATA_PEDIDO,
ordem_pedido.DATA_ENTREGA,
pedido_status.`STATUS`,
ordem_pedido.ORDEM_ID,
cliente.NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG,
cliente.TELEFONE
FROM
cliente
LEFT JOIN rel_cliente_ordem_pedido ON rel_cliente_ordem_pedido.CLIENTE_ID = cliente.CLIENTE_ID
LEFT JOIN ordem_pedido ON rel_cliente_ordem_pedido.ORDEM_ID = ordem_pedido.ORDEM_ID
LEFT JOIN pedido_status ON ordem_pedido.PEDIDO_STATUS_ID = pedido_status.PEDIDO_STATUS_ID
WHERE cliente.CLIENTE_ID = :cliente_id AND  NOT ISNULL(ordem_pedido.ORDEM_ID)";


	
	$q = $db->query2($query_pedidos, array(':cliente_id' => $_SESSION['credentials']->id));

	Flight::set('fullpage', true);
	Flight::render('pedidos/visualiza_pedidos.php', array('data' => $q), 'conteudo');
	Flight::render('home');
	

	}


	public static function visualizarNotaVirtual($franquia_id, $ordem_id){
			if(!Util::checaUsuario($franquia_id)){
				$_SESSION['redirBack'] = Flight::request()->url;
				Flight::redirect(sprintf("/%d/login/",$franquia_id));
			}

			$db = Flight::get('db');

		/*
		$query = "SELECT
produto.NOME AS PRODUTO_NOME,
produto_ordem.ORDEM_ID,
ordem_pedido.PEDIDO_STATUS_ID,
ordem_pedido.DATA_PEDIDO,
ordem_pedido.DATA_ENTREGA,
cliente.NOME AS CLIENTE_NOME,
rel_cliente_ordem_pedido.CLIENTE_ID,
produto_ordem.QUANTIDADE,
produto_ordem.REFERENCIA,
produto.PRECO,
produto.DESCONTOPRODUTO,
produto.DESCONTOCLIENTE,
produto.DESCONTOFRANQUEADO,
produto.PESO,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG,
cliente.LOGIN,
enderecos.CEP,
enderecos.ENDERECO,
produto.IMAGEM1
FROM
produto
LEFT JOIN rel_produto_ordem_pedido ON rel_produto_ordem_pedido.PRODUTO_ID = produto.PRODUTO_ID
LEFT JOIN produto_ordem ON rel_produto_ordem_pedido.PRODUTO_ORDEM_ID = produto_ordem.PRODUTO_ORDEM_ID
LEFT JOIN ordem_pedido ON produto_ordem.ORDEM_ID = ordem_pedido.ORDEM_ID
LEFT JOIN rel_cliente_ordem_pedido ON rel_cliente_ordem_pedido.ORDEM_ID = ordem_pedido.ORDEM_ID
LEFT JOIN cliente ON rel_cliente_ordem_pedido.CLIENTE_ID = cliente.CLIENTE_ID
LEFT JOIN rel_endereco_cliente ON rel_endereco_cliente.CLIENTE_ID = cliente.CLIENTE_ID
LEFT JOIN enderecos ON rel_endereco_cliente.ENDERECO_ID = enderecos.ENDERECO_ID
WHERE produto_ordem.ORDEM_ID = :ordem_id AND cliente.CLIENTE_ID = :cliente_id";
*/

	$query = "SELECT
produto_ordem.PRODUTO_ORDEM_ID,
rel_produto_ordem_pedido.PRODUTO_ID,
rel_produto_ordem_pedido.PRODUTO_ORDEM_ID,
produto.NOME AS PRODUTO_NOME,
produto.IMAGEM1,
produto.PRECO,
produto.DESCONTOPRODUTO,
produto.DESCONTOCLIENTE,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
rel_cliente_ordem_pedido.CLIENTE_ID,
rel_cliente_ordem_pedido.ORDEM_ID,
produto_ordem.QUANTIDADE,
ordem_pedido.PEDIDO_STATUS_ID,
ordem_pedido.DATA_PEDIDO,
ordem_pedido.DATA_ENTREGA,
ordem_pedido.CODIGO_PAGSEGURO,
cliente.NOME as CLIENTE_NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG
FROM
ordem_pedido
LEFT JOIN produto_ordem ON produto_ordem.ORDEM_ID = ordem_pedido.ORDEM_ID
INNER JOIN rel_produto_ordem_pedido ON rel_produto_ordem_pedido.PRODUTO_ORDEM_ID = produto_ordem.PRODUTO_ORDEM_ID
INNER JOIN produto ON rel_produto_ordem_pedido.PRODUTO_ID = produto.PRODUTO_ID
INNER JOIN rel_cliente_ordem_pedido ON rel_cliente_ordem_pedido.ORDEM_ID = ordem_pedido.ORDEM_ID
INNER JOIN cliente ON rel_cliente_ordem_pedido.CLIENTE_ID = cliente.CLIENTE_ID
WHERE produto_ordem.ORDEM_ID = :ordem_id AND rel_cliente_ordem_pedido.CLIENTE_ID = :cliente_id

";
	


	$q = $db->query2($query,array(':ordem_id' => $ordem_id, ':cliente_id' => $_SESSION['credentials']->id));

	if($q->rowsAffected() == 0){
		Flight::notFound();
	}

	Flight::set('fullpage', true);
	Flight::render('pedidos/ver_pedido.php', array('data' => $q), 'conteudo');
	Flight::render('home.php');

		
	}

}

?>