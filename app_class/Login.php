<?php

class Login {

	public static  function  index($id){

		if(array_key_exists('credentials', $_SESSION)){

				Flight::redirect(sprintf("/%d/", $id));

			}

	
		Flight::set('fullpage', true);

		Flight::render('login/loginform.php',NULL,'conteudo');
		Flight::render('home.php');

	}

	public static function doLogin($id){
		$db = Flight::get('db');


		if(is_null($_POST['login']) || !isset($_POST['login'])|| is_null($_POST['senha']) || !isset($_POST['senha']) ){

			alert('Houve um Problema', "Preencha os Campos de Login e Senha Corretamente", ALERT_ERRO);
			Flight::redirect(sprintf("/%d/login",$id));
		}

		$query = "SELECT
cliente.CLIENTE_ID,
cliente.LOGIN,
cliente.SENHA,
cliente.CONFIRMA_CADASTRO,
cliente.NOME,
cliente.FRANQUIA_ID
FROM
			cliente
WHERE cliente.LOGIN  = :email AND cliente.CONFIRMA_CADASTRO = 1 AND cliente.FRANQUIA_ID = :franquia_id
LIMIT 1
";


		$login = $_POST['login'];
		$senha = $_POST['senha']; 
		$senha_hash =  password_hash($_POST['senha'], PASSWORD_BCRYPT,array('cost'=>10));
		

		$q =  $db->query2($query,array(':email'=>$login, ':franquia_id' => $id ) );

		$senha_db = $q->querydata[0]['SENHA'];

		if(!password_verify($senha, $senha_db)){

			alert("Houve um Problema", "Login e Senha invalidos", ALERTA_ERRO);
			Flight::redirect(sprintf("/%d/login",$id));
		}


		$_SESSION['userSession'] = session_id();


		//Flight::set('loginSession', Flight::loginSession() );

		$sess = Flight::loginSession();

		$sess->id = $q->querydata[0]['CLIENTE_ID'];
		$sess->nome = $q->querydata[0]['NOME'];
		$sess->franquia_id = $q->querydata[0]['FRANQUIA_ID'];


		Flight::set('loginCredentials', $sess);
		$_SESSION['credentials'] = Flight::get('loginCredentials');

		if(isset($_SESSION['redirBack'])){

			$redir = $_SESSION['redirBack'];
			unset($_SESSION['redirBack']);
			Flight::redirect($redir);
			
		}else {

		Flight::redirect(sprintf("/%d/",$id));
		
		}


	}


	public static function loginFacebook($franquia_id){

		//$fb = new Facebook();
		//
		//

	}

}

?>