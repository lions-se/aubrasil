<?php


class Util {


	/**
	 * Pega o cep da pessoa e retorna com o logradouro, bairro, cidade e estado 
	 * @param  [type] $cep
	 * @return [type]
	 */
	public static function getCep($franquia_id,$cep){

		$remove = array('-','.');

		$parseCep = str_replace($remove, '', $cep);

		$busca = Flight::buscaCep();

		//var_dump($busca);
		$info = $busca->setCep($parseCep)->setUser('ryonagana')->setPass('cb3252518')->find();


		$dados['logradouro'] = $info[0];
		$dados['bairro']     = $info[1];
		$dados['cidade']     = $info[2];
		$dados['estado']     = strtoupper($info[3]);

		print Flight::json($dados);



	}

	public static function EmailExistente($email){
		$db = Flight::db();

		$query= "SELECT EXISTS(SELECT
cliente.CLIENTE_ID,
cliente.LOGIN
FROM
cliente
WHERE cliente.LOGIN LIKE :email
) as EXISTE";

		
		$q = $db->query2($query, array(':email' => $email));


		if($q->querydata[0]['EXISTE'] == 1){

			return true;
		}


	return false;

	}


	public static function getFranquia($id){
			$db = Flight::get('db');

			$franquiaExiste = $db->query2("SELECT EXISTS(SELECT
				proprietario.NOME,
				proprietario.SOBRENOME,
				franquia.NOMEFRANQUIA,
				franquia.SLOGAN,
				franquia.TELFRANQUIA,
franquia.FRANQUIA_ID
FROM
proprietario
RIGHT JOIN franquia ON franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
WHERE franquia.FRANQUIA_ID = :id) AS EXISTE", array(':id'=>$id));

			//var_dump($franquiaExiste->querydata);


			if($franquiaExiste->querydata[0]['EXISTE'] == 0){

				//redirecione aqui para qualquer lugar
				//

				//Flight::redirect(sprintf("/%d/",$id));
				Flight::notFound();


			}

			



			$gatherFranquia = "SELECT
proprietario.NOME,
proprietario.SOBRENOME,
franquia.NOMEFRANQUIA,
franquia.SLOGAN,
franquia.TELFRANQUIA,
franquia.FRANQUIA_ID,
franquia.CEPFRANQUIA,
franquia.ENDERECO,
franquia.BAIRRO,
franquia.ESTADO,
franquia.CIDADE,
franquia.EMAIL,
franquia.ENDERECO_SMTP,
franquia.LOGIN_SMTP,
franquia.SENHA_SMTP
FROM
proprietario
RIGHT JOIN franquia ON franquia.PROPRIETARIO_ID = proprietario.PROPRIETARIO_ID
WHERE franquia.FRANQUIA_ID = :id
";

			
				
				$pegaDadosFranquia = $db->query2($gatherFranquia,array(':id'=>$id));




			//var_dump($franquia);
			return $pegaDadosFranquia->querydata;

	}

	/**
	 * Checa  qual franquia o usuario esta conectado evitando colisoes de ID  caso seja igual de outra franquia
	 * prevenindo acesso
	 * @param  Integer $franquia_id  numero da franquia a checar
	 * @return Boolean               retorn se verdadeiro ou falso a existencia de registros
	 */
	public static function checaUsuario($franquia_id){

			$db = Flight::get('db');
			

			$user_query = "SELECT EXISTS(SELECT
				cliente.CLIENTE_ID,
				cliente.FRANQUIA_ID,
				cliente.NOME,
				cliente.SOBRENOME
				FROM
					cliente
				LEFT JOIN franquia ON cliente.FRANQUIA_ID = franquia.FRANQUIA_ID
				WHERE cliente.CLIENTE_ID  =  :cliente_id  AND cliente.FRANQUIA_ID = :franquia_id) AS EXISTE";


			$q = $db->query2($user_query, array(':cliente_id' => $_SESSION['credentials']->id, ':franquia_id' => $franquia_id ));





			return $q->querydata[0]['EXISTE'] == 1 ? true : false;

	}

	/**
	 * Verifica  a Sessão
	 * @param  int $franquia_id valor 
	 * @return [type]              [description]
	 */
	public static function checkSession($franquia_id){
		if(!array_key_exists('credentials', $_SESSION)){
			return false;
		}else if(!Util::checaUsuario($franquia_id)) {
			
			alert('Acesso Negado', "Credenciais Não pertence a essa franquia",  ALERTA_ERRO);
			Flight::redirect(rootURL().'/'.Flight::get('actualFranquia') );
			//Flight::notFound();
			//return false;
		}

		return true;
	}




	/**
	 * Checa se o CPF da pessoa ja existe no sistema
	 * @param  int $id
	 * @param  string $cpf
	 * @return string
	 */
	public static function checkCPF($id, $cpf){

		$db = Flight::get('db');

		$cpf_query = "SELECT EXISTS(SELECT
franquia.FRANQUIA_ID,
cliente.CLIENTE_ID,
cliente.CPF
FROM
cliente
LEFT JOIN franquia ON cliente.FRANQUIA_ID = franquia.FRANQUIA_ID
WHERE cliente.CPF = :cpf AND cliente.FRANQUIA_ID = :franquia_id) AS EXISTE";
		header("content-type application/json");
		$clean = array('-','.');
		$cpf_clean = str_replace($clean,'', $cpf);
		
		$q = $db->query2($cpf_query, array(':cpf' => $cpf_clean, ':franquia_id' => $id));

		$result = ($q->querydata[0]['EXISTE'] === 1) ? true : false;

		if(strlen($cpf_clean) != 11){
			return Flight::json(array('erro' => 'true'));
		}
		

		//var_dump($result);
		
		return Flight::json(array('existe' => $result));

	
	}

	public static function getUserById($id){
		$db = Flight::db();


		$query = "SELECT
cliente.CLIENTE_ID,
cliente.FRANQUIA_ID,
cliente.NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG,
cliente.ESTADOCIVIL,
cliente.SEXO,
cliente.LOGIN,
cliente.DATA_NASCIMENTO,
cliente.DESCRICAO,
cliente.PAIS,
cliente.CODIGO_CADASTRO,
cliente.TELEFONE,
cliente.TELEFONECOMERCIAL,
cliente.TELEFONECELULAR
FROM
cliente
WHERE cliente.CLIENTE_ID = :codigo";
	
	$q = $db->query2($query, array(':codigo' => $id));

	return $q->querydata[0];

	}


	public static function getFranquiaEmail($id){
		$db = Flight::db();

		$query ="SELECT
franquia.FRANQUIA_ID,
franquia.EMAIL
FROM
franquia
WHERE franquia.FRANQUIA_ID = :id";
	
	$q = $db->query($query, array(':id' => $id));

	return $q->querydata[0]['EMAIL'];

	}


	public static function getUserByConfirmation($confirmation_code){

		$db = Flight::db();

		$query = "SELECT
cliente.CLIENTE_ID,
cliente.FRANQUIA_ID,
cliente.NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG,
cliente.ESTADOCIVIL,
cliente.SEXO,
cliente.DATA_NASCIMENTO,
cliente.DESCRICAO,
cliente.PAIS,
cliente.CODIGO_CADASTRO,
cliente.TELEFONE,
cliente.TELEFONECOMERCIAL,
cliente.TELEFONECELULAR
FROM
cliente
WHERE cliente.CODIGO_CADASTRO LIKE :codigo";
	
	$q = $db->query2($query, array(':codigo' => $confirmation_code));

	return $q->querydata[0];

	}


	public static function ContatoEmail($id){


		$temp = file_get_contents("paginas/email/email_contato.php");
		//Flight::render('email/email_template');
		
		//$email_franqueado = Util::getFranquiaEmail($id);
		//$usuario = Util::getUserById($id);
		$franquia = Util::getFranquia(Flight::get('actualFranquia'))[0];


	

		$find = array('{NOME}','{ASSUNTO}','{TEXTO}', '{EMAIL}' , '{TELEFONE}');
		
		//$link = rootURL().'/'.Flight::get('actualFranquia').'/confirma_email/'.$usuario['CODIGO_CADASTRO'];
		//
		
		switch($_POST['assunto']){
			case '1':
				$assunto = "Duvidas";
				break;
			case '2':
				$assunto = "Financeiro";
				break;
			case '3':
				$assunto  = "Outros";
				break;
			case '4':
				$assunto = "Criticas e Sugestoes";
				break;
			default:
				$assunto = "Duvidas";
				break;

		}


		
		$replace = array($_POST['nome'],$assunto, $_POST['texto'], $_POST['email'], $_POST['telefone']);
		

		$template =  str_replace($find, $replace, $temp);

		//var_dump($franquia['LOGIN_SMTP']);
		
		
		// Inicia a classe PHPMailer
		// 
			if(isset($_POST['recaptcha_response_field'])){
				$result = recaptcha_check_answer (Flight::get('captcha_privkey'),
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
		}


		if(!$result->is_valid){
			alert('Ocorreu um Problema', 'Captcha Precisa ser digitado corretamente', ALERTA_ERRO);
			Flight::render('conteudo/contato.php', array('dados' => $_POST), 'conteudo');
			Flight::render('home');
		}

		

		if(is_null($franquia['LOGIN_SMTP']) || empty($franquia['LOGIN_SMTP'])){
			echo "Erro Login Vazio";
			return;
		}else if(is_null($franquia['ENDERECO_SMTP']) || empty($franquia['ENDERECO_SMTP'])){
			echo "Erro Endereço Vazio";
			return;
		}else if(is_null($franquia['SENHA_SMTP']) || empty($franquia['SENHA_SMTP'])) {
			echo "Erro senha Vazio";
			return;
		}

		$mail = Flight::PHPMailer();
// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=	
		$mail->IsSMTP(); // Define que a mensagem será SMTP
		$mail->Host = $franquia['ENDERECO_SMTP']; // Endereço do servidor SMTP
		$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
		$mail->Username = $franquia['LOGIN_SMTP']; // Usuário do servidor SMTP
		$mail->Password = $franquia['SENHA_SMTP']; // Senha do servidor SMTP
		//$mail->SMTPDebug = 0;

// Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

		$mail->From =    $franquia['EMAIL']; // Seu e-mail
		$mail->FromName = "Administrador"; // Seu nome

// Define os destinatário(s)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// 
// 
	$mail->AddAddress($franquia['EMAIL'],$_POST['nome']);
//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

// Define os dados técnicos da Mensagem
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
//$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)

// Define a mensagem (Texto e Assunto)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		//$mail->Subject  = "Mensagem Teste"; // Assunto da mensagem
	//	$mail->Body = "Este é o corpo da mensagem de teste, em <b>HTML</b>! <br /> <img src="http://i2.wp.com/blog.thiagobelem.net/wp-includes/images/smilies/icon_smile.gif?w=625" alt=":)" class="wp-smiley" width="15" height="15"> ";
	//	$mail->AltBody = "Este é o corpo da mensagem de teste, em Texto Plano! \r\n <img src="http://i2.wp.com/blog.thiagobelem.net/wp-includes/images/smilies/icon_smile.gif?w=625" alt=":)" class="wp-smiley" width="15" height="15"> ";
		$mail->Subject = "[".strtoupper($assunto)."] Formulario de Contato: ".$_POST['nome'];
		$mail->Body = $template;

// Define os anexos (opcional)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo

// Envia o e-mail
	$enviado = $mail->Send();

// Limpa os destinatários e os anexos
	$mail->ClearAllRecipients();
	$mail->ClearAttachments();

// Exibe uma mensagem de resultado
	if ($enviado) {
		return true;
	} 

	return false;

	}


	public static function sendConfirmationEmail($id){

		$temp = file_get_contents("paginas/email/email_template.php");
		//Flight::render('email/email_template');
		
		$email_franqueado = Util::getFranquiaEmail($id);
		$usuario = Util::getUserById($id);
		$franquia = Util::getFranquia(Flight::get('actualFranquia'))[0];


	

		$find = array('{NOME}','{LINK}','{LOGIN}');
		
		$link = rootURL().'/'.Flight::get('actualFranquia').'/confirma_email/'.$usuario['CODIGO_CADASTRO'];


		
		$replace = array($usuario['NOME'] ,$link, $usuario['LOGIN']);
		

		$template =  str_replace($find, $replace, $temp);

		//var_dump($franquia['LOGIN_SMTP']);
		
		
		// Inicia a classe PHPMailer
		

		if(is_null($franquia['LOGIN_SMTP']) || empty($franquia['LOGIN_SMTP'])){
			echo "Erro Login Vazio";
			return;
		}else if(is_null($franquia['ENDERECO_SMTP']) || empty($franquia['ENDERECO_SMTP'])){
			echo "Erro Endereço Vazio";
			return;
		}else if(is_null($franquia['SENHA_SMTP']) || empty($franquia['SENHA_SMTP'])) {
			echo "Erro senha Vazio";
			return;
		}

		$mail = Flight::PHPMailer();
// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=	
		$mail->IsSMTP(); // Define que a mensagem será SMTP
		$mail->Host = $franquia['ENDERECO_SMTP']; // Endereço do servidor SMTP
		$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
		$mail->Username = $franquia['LOGIN_SMTP']; // Usuário do servidor SMTP
		$mail->Password = $franquia['SENHA_SMTP']; // Senha do servidor SMTP
		//$mail->SMTPDebug = 1;

// Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

		$mail->From =    $franquia['EMAIL']; // Seu e-mail
		$mail->FromName = "Administrador"; // Seu nome

// Define os destinatário(s)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// 
// 
	$mail->AddAddress($usuario['LOGIN'],$usuario['NOME']);
//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

// Define os dados técnicos da Mensagem
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
//$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)

// Define a mensagem (Texto e Assunto)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		//$mail->Subject  = "Mensagem Teste"; // Assunto da mensagem
	//	$mail->Body = "Este é o corpo da mensagem de teste, em <b>HTML</b>! <br /> <img src="http://i2.wp.com/blog.thiagobelem.net/wp-includes/images/smilies/icon_smile.gif?w=625" alt=":)" class="wp-smiley" width="15" height="15"> ";
	//	$mail->AltBody = "Este é o corpo da mensagem de teste, em Texto Plano! \r\n <img src="http://i2.wp.com/blog.thiagobelem.net/wp-includes/images/smilies/icon_smile.gif?w=625" alt=":)" class="wp-smiley" width="15" height="15"> ";
		$mail->Subject = "Cadastro Auto Brasil";
		$mail->Body = $template;

// Define os anexos (opcional)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo

// Envia o e-mail
	$enviado = $mail->Send();

// Limpa os destinatários e os anexos
	$mail->ClearAllRecipients();
	$mail->ClearAttachments();

// Exibe uma mensagem de resultado
	if ($enviado) {
		echo "E-mail enviado com sucesso!";
	} else {
		echo "Não foi possível enviar o e-mail.<br /><br />";
		echo "<b>Informações do erro:</b> <br />" . $mail->ErrorInfo;
	}

	}

/**
 * Realiza a atualização das credenciais quando o cliente altera seu proprio usuario
 * ele modifica as credenciais ja existente na sessao
 * @param int $userid
 * @param array $data
 */
	public static function UpdateCredentials($userid, $data){

		$cred = $_SESSION['credentials'];

		if(is_array($data)){

			$cred->id = $userid;

			if(array_key_exists('nome', $data)){
				$cred->name = $data['nome'];

			}else if(array_key_exists('franquia_id', $data)){
				$cred->franquia_id = $data['franquia_id']; 
			}else if(array_key_exists('login', $data)){
				$cred->login = $data['login'];
			}

			
			$cred->session =  session_id();

			
			
			Flight::set('loginCredentials', $cred);
			$_SESSION['credentials'] = Flight::get('loginCredentials');


			



		}


	}

	public static function URL($end){

		$url = Flight::request()->url.'/'.$end;

		return str_replace('//', '/', $url);

	}

	public static function buscarCep($cep_raw){

		header('Content-Type: application/json');

		if(empty($cep_raw)){
			die("HUEEHUHUAHUEHHAUHHUEHUAUHA");
		}

	$pattern = "/^[0-9]{8,8}$/";
	$reg = preg_match($pattern,$cep,$matches);

	$remove = array('.','-',',');
		
	$cep = str_replace($remove, '', $cep_raw);

	if(!empty($reg) || $reg != NULL){
		$dados['erro'] = 0;	
	}else {
		$dados['erro'] = 1;
	}

	

	$cURL = curl_init('http://republicavirtual.com.br/web_cep.php?cep='.$cep.'&formato=json');
	curl_setopt($cURL, CURLOPT_RETURNTRANSFER,true);

	// Seguir qualquer redirecionamento que houver na URL
	curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, false);

	$resultado = curl_exec($cURL);

	// Pega o código de resposta HTTP
	$resposta = curl_getinfo($cURL, CURLINFO_HTTP_CODE);

	curl_close($cURL);

	if ($resposta != '404') {
		
		echo $resultado;
		
	} else {
		echo 'Site Esta Fora do Ar';
	}	

	} 


}


?>