<?
class Usuario{

	public static function index($id){


		Flight::set('fullpage', true);

		if($_SERVER['REQUEST_METHOD'] == "POST"){

		$nome = $_POST['nome'];
		$sobrenome = $_POST['sobrenome'];
		$login = $_POST['login'];

		if(empty($nome) || empty($sobrenome) || empty($login)){

			alert('Houve um Problema', "Preencha o Formulario Corretamente", ALERTA_ERRO);
			Flight::redirect(sprintf("/%d/login",$id));
		}

		Flight::render('cadastroUsuario.php', array('nome' => $nome, 'sobrenome' => $sobrenome, 'login' => $login), 'conteudo');
		Flight::render('home.php');

		}else{
		Flight::render('cadastroUsuario.php', array(), 'conteudo');
		Flight::render('home.php');
		}
	}


	public static function sendForm($id){


		//var_dump($_POST);
		$pass = $_POST['senha'];
		$passhash = password_hash($_POST['senha'], PASSWORD_BCRYPT,array('cost'=>10));
		$repete =  password_hash($_POST['repete_senha'],PASSWORD_BCRYPT,array('cost'=>10));


		$dados = array();

		$result = NULL;

		if(isset($_POST['recaptcha_response_field'])){
				$result = recaptcha_check_answer (Flight::get('captcha_privkey'),
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
		}


		//var_dump($result->is_valid);
		$retrieve = array('nome'=>$_POST['nome']);


		if(!$result->is_valid){
			alert('Ocorreu um Problema', 'Captcha Precisa ser digitado corretamente', ALERTA_ERRO);
			Flight::render('CadastroUsuario', array('dados' => $_POST), 'conteudo');
			 Flight::render('home');
		}


		if( empty($_POST['login']) || empty($_POST['senha'])  ){
			
			alert('Ocorreu um Problema', 'Insira nome de Usuario e Senha', ALERTA_ERRO);
			  Flight::render('CadastroUsuario', array('dados' => $_POST), 'conteudo');
			  Flight::render('home');
			//Flight::redirect(sprintf("/%d/cadastro_cliente",$id));
		
		}else if(!password_verify($pass,$repete)){
			
			alert('Ocorreu um Problema', 'As senhas devem  coincidir com o campo de repetir senha', ALERTA_ERRO);
			 Flight::render('CadastroUsuario', array('dados' => $_POST), 'conteudo');
			  Flight::render('home');

		}else if(empty($_POST['nome']) || empty($_POST['sobrenome'])){
			
			alert('Ocorreu um Problema', 'Preencha os Campos de Nome e Sobrenome', ALERTA_ERRO);
			 Flight::render('CadastroUsuario', array('dados' => $_POST), 'conteudo');
			  Flight::render('home');
		
		}else if(empty($_POST['cpf']) ||  empty($_POST['rg']) ){
		
			alert('Ocorreu um Problema', 'Preencha Com os seus documentos', ALERTA_ERRO);
			 Flight::render('CadastroUsuario', array('dados' => $_POST), 'conteudo');
			  Flight::render('home');
		
		}else if($_POST['dob'] === '0-0-0'){
		
			alert('Ocorreu um Problema', 'Preencha o campo Data de Nascimento', ALERTA_ERRO);
			 Flight::render('CadastroUsuario', array('dados' => $_POST), 'conteudo');
			  Flight::render('home');
		
		}else if(!$_POST['termosservico']){
			alert('Ocorreu um Problema', 'Precisa Aceitar os Termos e Serviços Prestados', ALERTA_ERRO);
			 Flight::render('CadastroUsuario', array('dados' => $_POST), 'conteudo');
			  Flight::render('home');
		}else if(Util::EmailExistente($_POST['login'])) {

			alert('Ocorreu um Problema', 'E-Mail já Existente no Sistema', ALERTA_ERRO);
			 Flight::render('CadastroUsuario', array('dados' => $_POST), 'conteudo');
			  Flight::render('home');


		}else {
		
	
	

			date_default_timezone_set('America/Sao_Paulo');


			$db = Flight::get('db');

			var_dump($_POST['estadocivil']);

			$registro = new DateTime('NOW');

			$confirmacao =  genEmailConfirmation();
			$cod_referencia = genConfirmationCode(6);


			if( is_null($_POST['nome']) || is_null($_POST['sobrenome'])   ){

				alert("Falha","Preencha seu nome");
				Flight::redirect(sprintf("/%d/cadastro_cliente",$id));

			}else {


				//$senha = password_hash($_POST['senha'], PASSWORD_BCRYPT, array('cost'=> 10));


				$data = array('FRANQUIA_ID'=> intval($id),
					   'NOME' => $_POST['nome'],
					   'SOBRENOME' => $_POST['sobrenome'],
					   'CPF' => $_POST['cpf'],
					   'RG' => $_POST['rg'],
					   'ESTADOCIVIL' => $_POST['estadocivil'],
					   'SEXO'  => $_POST['sexo'],
					   'DATA_NASCIMENTO' => $_POST['dob'],
					   'LOGIN' => $_POST['login'],
					    'SENHA'=> $passhash,
					    'EMAILSECUNDARIO' => $_POST['emailsecundario'],
					    'CODIGO_CADASTRO' => $confirmacao,
					    'DATA_REGISTRO' => $registro->format('c'),
					    'CODIGO_REFERENCIA' => $cod_referencia
					   	);


				$usuario_id = $db->insert('cliente', $data);


				/*
				FIX
				$email = new PHPMailer();
				$email->isSendMail();
				$email->setFrom('contato@autobrasilfranquia.com.br', 'Contato Autobrasil');
				$email->addAddress($_POST['login'], $_POST['nome']);
				$email->Subject = "Confirmação de Cadastro AutoBrasil";
				$email->msgHTML = ("

						Codigo de Confirmação: ".$confirmacao);


				$email->send();
				*/
				//alert('SUCESSO', 'Foi Cadastrado com sucesso. sera enviado um e-mail  de confirmação em breve');
				//


			

				//var_dump($confirmacao);

				Util::sendConfirmationEmail($usuario_id);
				//Flight::redirect(sprintf("/%d/cadastro_cliente/sucesso",$id));
		}
	}


	}

	public static function signupSuccess($id){
			Flight::set('fullpage', true);
			Flight::render('cadastro_paginas/cadastro_sucesso.php', NULL, 'conteudo');
			Flight::render('home.php');
	}


	public static function EditProfile($franquia_id){
			if(!Util::checkSession($franquia_id)){

				$_SESSION['redirBack'] = Flight::request()->url;
				Flight::redirect(sprintf("/%d/login/",$franquia_id));

			}

			$db = Flight::get('db');


			$sql_user = "SELECT
cliente.CODIGO_REFERENCIA,
cliente.CLIENTE_ID,
cliente.FRANQUIA_ID,
cliente.NOME,
cliente.SOBRENOME
FROM
cliente
LEFT JOIN franquia ON cliente.FRANQUIA_ID = franquia.FRANQUIA_ID
WHERE cliente.CLIENTE_ID = :cliente_id AND cliente.FRANQUIA_ID = :franquia_id";
		
			$q = $db->query2($sql_user,array(':cliente_id'=> $_SESSION['credentials']->id, ':franquia_id' => $franquia_id));
			



			Flight::set('fullpage', true);
			Flight::render('usuario/editar_perfil.php', array('user' => $q->querydata[0]), 'conteudo');
			Flight::render('home.php');


	}

	public static function editUser($franquia_id){
		if(!Util::checkSession($franquia_id)){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(sprintf("/%d/login/",$franquia_id));
		}

		$db = Flight::get('db');

		$query_sql = "SELECT
cliente.CODIGO_REFERENCIA,
cliente.CLIENTE_ID,
cliente.FRANQUIA_ID,
cliente.NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG,
cliente.ESTADOCIVIL,
cliente.SEXO,
cliente.DATA_NASCIMENTO,
cliente.LOGIN,
cliente.PAIS,
cliente.TELEFONE,
cliente.TELEFONECOMERCIAL,
cliente.TELEFONECELULAR
FROM
cliente
WHERE cliente.CLIENTE_ID =  :cliente_id AND cliente.FRANQUIA_ID = :franquia_id";

		
		$q = $db->query2($query_sql,array(':cliente_id' => $_SESSION['credentials']->id, ':franquia_id' => $franquia_id));
		


		Flight::set('fullpage', true);
		Flight::render('usuario/editar_user.php', array('user' => $q->querydata[0]), 'conteudo');
		Flight::render('home.php');

	}

	public static function doEditUser($franquia_id){

		if(!Util::checkSession($franquia_id)){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(sprintf("/%d/login/",$franquia_id));

		}
		$db = Flight::get('db');

		$getSenha_query = "SELECT
cliente.CLIENTE_ID,
cliente.SENHA
FROM
cliente
WHERE cliente.CLIENTE_ID = :cliente_id";

		$getSenha = $db->query2($getSenha_query, array(':cliente_id'=>$_SESSION['credentials']->id));

		$senha = $getSenha->querydata[0]['SENHA'];

		$erros = array();
		$num_erros = 0;
		$str_erros = "";


		//$repete_senha = password_hash($_POST['repetesenha'], PASSWORD_BCRYPT, array('cost' => 10));


		if(empty($_POST['nome']) || empty($_POST['sobrenome'])){

			$erros[] = "nome e Sobrenome Não podem estar vazios";
			$num_erros += 1;
		}else if( empty($_POST['rg']) ){
			$erros[] = "Campo RG Não Pode Estar Vazio";
			$num_erros += 1;
		}else if( empty($_POST['telefone'])){
			$erros[] = "Deve Ter Pelo Menos um telefone para Contato";
			$num_erros += 1;
		}else if( $_POST['dob'] == '0-0-0'){
			$erros[] = "Coloque a Data de Nascimento Correta";
			$num_erros += 1;
		}else {

			if(!password_verify($_POST['senha'],$senha)){

				alert('Problemas', 'Senha ou login Inválido' , ALERTA_ERRO);
				Flight::redirect(sprintf("/%d/editar-perfil/user",$franquia_id));	
			}

			$cleaner = array('(',')','-');

			$telefone = str_replace( $cleaner,'',$_POST['telefone']);
			$telefonecelular = str_replace( $cleaner,'',$_POST['telefonecelular']);
			$telefonecomercial = str_replace( $cleaner,'',$_POST['telefonecomercial']);


			$db->update2('cliente',
				array('NOME' => $_POST['nome'],
					  'SOBRENOME' => $_POST['sobrenome'],
					  'RG' => $_POST['rg'],
					  'TELEFONE' => $telefone,
					  'TELEFONECELULAR' => $telefonecelular,
					  'TELEFONECOMERCIAL' => $telefonecomercial,
					  'SEXO' => $_POST['sexo'],
					  'ESTADOCIVIL' => $_POST['estadocivil'],
					  'PAIS' => $_POST['pais']

					 )
				,'cliente.CLIENTE_ID = :cliente_id ', array(':cliente_id' => $_SESSION['credentials']->id));
			
				Util::UpdateCredentials($_SESSION['credentials']->id, array('nome' => $_POST['nome'], 'franquia_id' => $franquia_id));

				alert('Sucesso', 'Seus Dados Foram Editados Com Sucesso!');
				Flight::redirect(sprintf("/%d/editar-perfil",$franquia_id));	

		}

		if($num_erros != 0){

			foreach($erros as $key => $value){

				$str_erros .= $value;
			}

			
			alert('Problemas', $str_erros, ALERTA_ERRO);
			Flight::redirect(sprintf("/%d/editar-perfil/user",$franquia_id));	
		}

	


	}

	public static function editCredentials($franquia_id){	

			$db = Flight::get('db');
			Flight::set('fullpage', true);

			if(!Util::checkSession($franquia_id)){

				$_SESSION['redirBack'] = Flight::request()->url;
				Flight::redirect(sprintf("/%d/login/",$franquia_id));
			}

			$query = "SELECT
cliente.CLIENTE_ID,
cliente.LOGIN
FROM
cliente
WHERE cliente.CLIENTE_ID = :cliente_id";
		
		$q = $db->query2($query, array(':cliente_id'=> $_SESSION['credentials']->id));


			Flight::render('usuario/editar_credenciais.php', array('user' => $q->querydata[0]), 'conteudo');
			Flight::render('home.php');


	}

	public static function doEditCredentials($franquia_id){

			$db = Flight::get('db');

			if(!Util::checkSession($franquia_id)){

				$_SESSION['redirBack'] = Flight::request()->url;
				Flight::redirect(sprintf("/%d/login/",$franquia_id));
			}

			$query = "SELECT
				cliente.CLIENTE_ID,
				cliente.LOGIN,
				cliente.SENHA
				FROM
				cliente
				WHERE cliente.CLIENTE_ID = :cliente_id";

			$q = $db->query2($query, array(':cliente_id' => $_SESSION['credentials']->id));

			$senha = $q->querydata[0]['SENHA'];


			if(password_verify($_POST['passwd'], $senha)){

				$newpass = password_hash($_POST['newpasswd'], PASSWORD_BCRYPT, array('cost' => 10));

				$db->update2('cliente', array('SENHA' => $newpass ), 'cliente.CLIENTE_ID = :id', array(':id' => $_SESSION['credentials']->id ));
				alert('Sucesso', "Sua Senha Foi Alterada com Sucesso");
				Flight::redirect(sprintf('/%d/editar-perfil',$franquia_id));

			}else {

				alert('Houve Problema', "Prenchimento  Invalido de campo de senhas",ALERTA_ERRO);
				Flight::redirect(sprintf('/%d/editar-perfil', $franquia_id));
			}

	}


	public static function cadastraEndereco($franquia_id){
			
			if(!Util::checkSession($franquia_id)){
				$_SESSION['redirBack'] = Flight::request()->url;
				Flight::redirect(sprintf("/%d/login/",$franquia_id));
			}



			$db = Flight::get('db');


			$query = "SELECT
enderecos.CEP,
enderecos.ENDERECO,
enderecos.BAIRRO,
enderecos.NUMERO,
enderecos.CIDADE,
cliente.CLIENTE_ID,
enderecos.ENDERECO_ID
FROM
cliente
LEFT JOIN rel_endereco_cliente ON rel_endereco_cliente.CLIENTE_ID = cliente.CLIENTE_ID
LEFT JOIN enderecos ON rel_endereco_cliente.ENDERECO_ID = enderecos.ENDERECO_ID
WHERE cliente.CLIENTE_ID = :cliente_id AND  NOT ISNULL(enderecos.ENDERECO_ID) LIMIT 5";

			//$pag = Flight::Pagina();
			$q = $db->query2($query, array(':cliente_id'=> $_SESSION['credentials']->id));

			//$q = $pag->retrieveData($query, array(':cliente_id' => $_SESSION['credentials']->id ));



			Flight::set('fullpage', true);
			Flight::render("enderecos/cadastro_endereco.php", array('enderecos' => $q), 'conteudo');
			Flight::render('home.php');

			
	}


	public static function cadastraNovoEndereco($franquia_id){

		if(!Util::checaUsuario($franquia_id)){

			$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(sprintf("/%d/login/",$franquia_id));

		}

		$db = Flight::get('db');


		$end 	= $_POST['endereco'];
		$bairro = $_POST['bairro'];
		$cidade = $_POST['cidade'];
		$estado = $_POST['selectEstado'];
		$numero = $_POST['numero'];
		$cep = $_POST['cep'];


		if(!empty($end) || !empty($bairro) || !empty($cidade) || !empty($estado) || !empty($numero) || !empty($cep) ){


			$cep_existe_query = "SELECT EXISTS(SELECT
enderecos.ENDERECO_ID,
enderecos.CEP,
cliente.CLIENTE_ID
FROM
enderecos
LEFT JOIN rel_endereco_cliente ON rel_endereco_cliente.ENDERECO_ID = enderecos.ENDERECO_ID
LEFT JOIN cliente ON rel_endereco_cliente.CLIENTE_ID = cliente.CLIENTE_ID
WHERE enderecos.CEP LIKE :cep AND cliente.CLIENTE_ID = :cliente_id) as EXISTE";

			
			$q = $db->query2($cep_existe_query, array(':cep'=> $cep, ':cliente_id' => $_SESSION['credentials']->id));


			if($q->querydata[0]['EXISTE'] == 1){
				alert('Houve um Problema', 'CEP Ja cadastrado', ALERTA_ERRO);
				Flight::redirect(sprintf("/%d/editar-perfil/cadastra_endereco",$franquia_id));
			}


			$lastid = $db->insert('enderecos', array(

				'CEP' => $cep,
				'ENDERECO' => $end,
				'BAIRRO' => $bairro,
				'CIDADE' => $cidade,
				'NUMERO' => $numero,
				'ESTADO' => $estado
			));


			$db->insert('rel_endereco_cliente', array('ENDERECO_ID' => $lastid ,
													  'CLIENTE_ID' => $_SESSION['credentials']->id));


			alert('Sucesso', 'Endereço Cadastrado  com Sucesso');
			Flight::redirect(sprintf("/%d/editar-perfil/cadastra_endereco",$franquia_id));

		}else {
			print 'falha';
		}



	}


	public static function doEditaEndereco($franquia_id, $end_id){
			if(!Util::checaUsuario($franquia_id)){

				$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(sprintf("/%d/login/",$franquia_id));

			}
			Flight::set('fullpage', true);
			$db = Flight::get('db');

			$query ="SELECT
cliente.CLIENTE_ID,
enderecos.CEP,
enderecos.ENDERECO,
enderecos.BAIRRO,
enderecos.NUMERO,
enderecos.CIDADE,
enderecos.ESTADO,
enderecos.ENDERECO_ID
FROM
enderecos
LEFT JOIN rel_endereco_cliente ON rel_endereco_cliente.ENDERECO_ID = enderecos.ENDERECO_ID
LEFT JOIN cliente ON rel_endereco_cliente.CLIENTE_ID = cliente.CLIENTE_ID
WHERE cliente.CLIENTE_ID = :cliente_id AND enderecos.ENDERECO_ID = :endereco_id  AND NOT ISNULL(enderecos.ENDERECO_ID)";
		
		$q = $db->query2($query, array(':cliente_id' => $_SESSION['credentials']->id, ':endereco_id' => $end_id));


		//var_dump($q->querydata);
		Flight::render('enderecos/edita_endereco.php', array('data' => $q->querydata[0]), 'conteudo' );
		Flight::render('home.php');


	}

	public static function updateEndereco($franquia_id){
			if(!Util::checaUsuario($franquia_id)){
				$_SESSION['redirBack'] = Flight::request()->url;
			Flight::redirect(sprintf("/%d/login/",$franquia_id));

			}

			$db = Flight::get('db');


		$end 	= $_POST['endereco'];
		$bairro = $_POST['bairro'];
		$cidade = $_POST['cidade'];
		$estado = $_POST['selectEstado'];
		$numero = $_POST['numero'];
		$cep = $_POST['cep'];
		$id =  $_POST['endereco_id'];



		$db->update2('enderecos', array(
				'ENDERECO' => $end,
				'BAIRRO' => $bairro,
				'CIDADE' => $cidade,
				'ESTADO' => $estado,
				'NUMERO' => $numero,
				'CEP' => $cep
			),



			' enderecos.ENDERECO_ID = :endereco_id', array(':endereco_id' => $id )	);

			//var_dump($_POST);
			alert('Sucesso', 'Endereço Editado  com Sucesso');
			Flight::redirect(sprintf("/%d/editar-perfil/cadastra_endereco",$franquia_id));


	}


	public static function apagaEndereco($franquia_id, $endereco_id){
			
			if(!Util::checaUsuario($franquia_id)){
					$_SESSION['redirBack'] = Flight::request()->url;
					Flight::redirect(sprintf("/%d/login/",$franquia_id));
			}

			Flight::set('fullpage', true);
			$db = Flight::get('db');


						$query ="SELECT
cliente.CLIENTE_ID,
enderecos.CEP,
enderecos.ENDERECO,
enderecos.BAIRRO,
enderecos.NUMERO,
enderecos.CIDADE,
enderecos.ESTADO,
enderecos.ENDERECO_ID
FROM
enderecos
LEFT JOIN rel_endereco_cliente ON rel_endereco_cliente.ENDERECO_ID = enderecos.ENDERECO_ID
LEFT JOIN cliente ON rel_endereco_cliente.CLIENTE_ID = cliente.CLIENTE_ID
WHERE cliente.CLIENTE_ID = :cliente_id AND enderecos.ENDERECO_ID = :endereco_id  AND NOT ISNULL(enderecos.ENDERECO_ID)";

			$q = $db->query2($query, array(':cliente_id' => $_SESSION['credentials']->id, ':endereco_id' => $endereco_id));




			Flight::render('enderecos/apaga_endereco.php', array('data' => $q->querydata[0]), 'conteudo' );
			Flight::render('home.php');

	}


	public static function delEndereco($franquia_id){
			if(!Util::checaUsuario($franquia_id)){
					$_SESSION['redirBack'] = Flight::request()->url;
					Flight::redirect(sprintf("/%d/login/",$franquia_id));
			}

			$db = Flight::get('db');


			$db->delete2('enderecos', ' enderecos.ENDERECO_ID = :end_id ', array(':end_id' => $_POST['endereco_id']));
			$db->delete2('rel_endereco_cliente', ' ENDERECO_ID = :end_id ', array(':end_id' => $_POST['endereco_id']));
			alert('Sucesso', 'Endereço Apagado  com Sucesso');
			Flight::redirect(sprintf("/%d/editar-perfil/cadastra_endereco",$franquia_id));

	}

}

?>