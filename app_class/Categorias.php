<?

class Categorias {

	public static function index ($franquia_id, $subcat_id){

		$db = Flight::get('db');
		$pag = new Pagination($db,9);

		$query = "SELECT
produto_categoria_subcategoria.PRODUTO_ID,
produto_categoria_subcategoria.SUBCATEGORIA_ID,
produto_categoria_subcategoria.CATEGORIA_ID,
produto.PRODUTO_ID,
produto.NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.PRECO,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
produto.DESCONTOCLIENTE,
produto.DESCONTOPRODUTO
FROM
produto
INNER JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
WHERE produto_categoria_subcategoria.SUBCATEGORIA_ID = :subcat_id";
		$pag->setPages('produto');
		$q = $pag->retrieveData($query, array(':subcat_id' => (int) $subcat_id));

		$categoria_nome = $q->querydata[0]['CAT_NOME'];
		$subcategoria_nome = $q->querydata[0]['SUBCAT_NOME'];
		

		//Flight::render('layout_base/carousel.php',array('produtos' => $q), 'carousel');
		Flight::render('produtos/produtos_home.php',  array('q'=>$q, 'franquia_id' => $id, 'categoria' => $categoria_nome, 'subcategoria'=>$subcategoria_nome, 'pag' => $pag  ) ,'conteudo');
		Flight::render('home');

		//var_dump($q->querydata);

	}
}

?>