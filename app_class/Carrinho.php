<?php


class Carrinho {


	public static function index($franquia_id){
			if(!Util::checaUsuario($franquia_id)){

				$_SESSION['redirBack'] = Flight::request()->url;

				Flight::redirect(sprintf("/%d/login/",$franquia_id));

			}

			/*
			if($id == Flight::get('actualFranquia')){



			}
			*/
		
			$db = Flight::get('db');

			$query = "SELECT
franquia.FRANQUIA_ID,
franquia.ENDERECO,
franquia.CEPFRANQUIA
FROM
franquia
WHERE franquia.FRANQUIA_ID = :franquia_id";

			$q = $db->query2($query, array(':franquia_id' => $franquia_id));


			Flight::set('fullpage', true);
			Flight::render('carrinho/carrinho.php', array('franquia' => $q->querydata[0]), 'conteudo');
			Flight::render('home');
			
			//var_dump(Flight::db());
	}

	public static function get($id, $idproduto){
			if(!Util::checaUsuario($id)){

				$_SESSION['redirBack'] = Flight::request()->url;
				alert('Houve um Problema', "Necessita realizar Login.", ALERTA_ERRO);
				Flight::redirect(sprintf("/%d/login/",$id));

			}
				


				$carrinho = $_SESSION['carrinho'];
				$db = Flight::db();



				$query = "SELECT
produto_categoria_subcategoria.PRODUTO_ID,
produto_categoria_subcategoria.SUBCATEGORIA_ID,
produto_categoria_subcategoria.CATEGORIA_ID,
produto.PRODUTO_ID,
produto.NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.PRECO,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
produto.DESCONTOCLIENTE,
produto.DESCONTOPRODUTO,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PROMOCAO_ATIVA,
categoria.NOME as CAT_NOME,
subcategoria.NOME as SUBCAT_NOME
FROM
produto
LEFT JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
LEFT JOIN categoria ON produto_categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
LEFT JOIN subcategoria ON produto_categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID

					WHERE produto.PRODUTO_ID = :prod_id";


					$q =  $db->query2($query, array('prod_id'=>$idproduto));

					//var_dump($q->querydata);

					if($q->rowsAffected()  == 1){




						$prod = new Produto();	

						$prod->franquia_id = intval($id);
						$prod->id = intval($idproduto);
						$prod->name = $q->querydata[0]['NOME'];	
						$prod->price =  $q->querydata[0]['PRECO'];
						$prod->descr = $q->querydata[0]['DESCRICAO'];
						$prod->peso = $q->querydata[0]['PESO'];
						$prod->img = $q->querydata[0]['IMAGEM1'];
						$prod->desc_produto = $q->querydata[0]['DESCONTOPRODUTO']; 
						$prod->desc_franqueado = $q->querydata[0]['DESCONTOFRANQUEADO']; 
						$prod->desc_cliente = $q->querydata[0]['DESCONTOCLIENTE']; 


						if($carrinho->xsearch($q->querydata[0]['PRODUTO_ID'])){

							alert('Sucesso', 'Foi Acrescentado a Quantidade deste produto');
							$carrinho->addProduct($prod);
							$carrinho->setFranquiaId($id);
							Flight::redirect(sprintf("/%d/produto/%d-%s.html",$id, $idproduto, slug($q->querydata[0]['NOME']) ));
						}else {

							$carrinho->addProduct($prod);
							$carrinho->setFranquiaId($id);
							alert('Sucesso', 'Produto Adicionado com Sucesso.');
							Flight::redirect(sprintf("/%d/produto/%d-%s.html",$id, $idproduto, slug($q->querydata[0]['NOME']) ));
						}
					//$_SESSION['carrinho']  = $carrinho;

					//$carrinho->showCartData();
					//print 'aaa';
					
					//$_SESSION['carrinho'] = $carrinho;
					//Flight::set('carrinho', $_SESSION['carrinho']);
					//Flight::redirect('')
					}else {

						Flight::notFound();

					}
					

	}

	public static function updateCart($id){

		if(empty($_POST['ids'])){

			return true;
		}

		/*
			
			essa funcao foi melhorada em que quando a pessoa atualiza o carrinho tambem atualiza com 
			dados do banco de dados evitando problema caso opreço do produto seja trocado

		*/

		$cart = Flight::get('cart');
		$db = Flight::get('db');


		$ids = $_POST['ids'];
		$qty = $_POST['qty'];
		$count = $cart->count();
		$all = $cart->all_products();

		$query = "SELECT
produto_categoria_subcategoria.PRODUTO_ID,
produto_categoria_subcategoria.SUBCATEGORIA_ID,
produto_categoria_subcategoria.CATEGORIA_ID,
produto.PRODUTO_ID,
produto.NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.PRECO,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
produto.DESCONTOCLIENTE,
produto.DESCONTOPRODUTO,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PROMOCAO_ATIVA,
categoria.NOME as CAT_NOME,
subcategoria.NOME as SUBCAT_NOME
FROM
produto
LEFT JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
LEFT JOIN categoria ON produto_categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
LEFT JOIN subcategoria ON produto_categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE produto.PRODUTO_ID = :prod_id
";


			foreach ($ids as $value) {
				$q = $db->query2($query, array(':prod_id'=>$value));
				$produto = $cart->search($value);
				$index = $cart->indexSearch($value);


				$produto->name = $q->querydata[0]['NOME'];
				$produto->price = $q->querydata[0]['PRECO'];
				$produto->descr = $q->querydata[0]['DESCRICAO'];
				$produto->peso = $q->querydata[0]['PESO'];
				$produto->img = $q->querydata[0]['IMAGEM1'];
				$produto->desc_produto = $q->querydata[0]['DESCONTOPRODUTO']; 
				$produto->desc_franqueado = $q->querydata[0]['DESCONTOFRANQUEADO']; 
				$produto->desc_cliente = $q->querydata[0]['DESCONTOCLIENTE']; 

				$cart->all_products()[$index] = $produto;
			}

	
		// atualiza as quantidades
		
		for($i = 0; $i < $cart->count(); $i++){

			if($cart->xsearch($ids[$i])){
				$all[$i]->setQty($qty[$i]);
			}
			
		}


			



		


		//Flight::redirect(sprintf("/%d/produto/carrinho"))
		//Flight::notFound();
	}

	public static function removeProduct($id,$productid){

		$cart = $_SESSION['carrinho'];

		$id = $_POST['product_id'];
		

		$cart->removeProduct($id);

		//Flight::set('cart', $cart);
		return Flight::json(array('object_id'=> $id, 'result'=> true, 'count' => $cart->count()));



	}

	public static function checkout($id){
		
		if(!Util::checaUsuario($id)){

			$_SESSION['redirBack'] = Flight::request()->url;

			Flight::redirect(sprintf("/%d/login/",$id));

		}

		/**
		 * Calcula peso antes de prosseguir no checout
		 * 
		 */
		foreach ($_SESSION['carrinho']->all_products() as $key => $value) {
			
			$calc_peso += $value->peso * $value->qty;
			
		}

		if($calc_peso > 30.0){
			 alert('Aviso', sprintf('As Agencias dos Correios tem uma limitação de 30kg por encomenda, voce ultrapassou %.2f kg do limite',($calc_peso - 30))  , ALERTA_AVISO);
			 Flight::redirect(sprintf("/%d/carrinho/",$id));
		}



		$cart = $_SESSION['carrinho'];
		$db = Flight::get('db');

		Flight::set('fullpage', true);

		if($cart->count() == 0){

			alert('Houve um Problema', "Carrinho de Compras Vazio", ALERTA_ERRO);
			Flight::redirect(sprintf("/%d/carrinho/",$id));
		}

		$query = "SELECT
produto_categoria_subcategoria.PRODUTO_ID,
produto_categoria_subcategoria.SUBCATEGORIA_ID,
produto_categoria_subcategoria.CATEGORIA_ID,
produto.PRODUTO_ID,
produto.NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.PRECO,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
produto.DESCONTOCLIENTE,
produto.DESCONTOPRODUTO,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PROMOCAO_ATIVA,
categoria.NOME as CAT_NOME,
subcategoria.NOME as SUBCAT_NOME
FROM
produto
LEFT JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
LEFT JOIN categoria ON produto_categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
LEFT JOIN subcategoria ON produto_categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE produto.PRODUTO_ID = :prod_id";

			$store = array();
			
			

			for($i = 0; $i < $cart->count(); $i++){

				$product_id = $cart->all_products()[$i]->id;
				$product = $cart->search($product_id);
				$index = $cart->indexSearch($product_id);

				$q = $db->query2($query,array(':prod_id'=>$product_id));
				

				$product->name = $q->querydata[0]['NOME'];
				$product->price = $q->querydata[0]['PRECO'];
				$product->descr = $q->querydata[0]['DESCRICAO'];
				$product->peso = $q->querydata[0]['PESO'];
				$product->img = $q->querydata[0]['IMAGEM1'];

				$cart->all_products()[$index] = $product;
				$store[$i] = $product;
			}


			$query_comprador = "SELECT
cliente.CLIENTE_ID,
cliente.FRANQUIA_ID,
cliente.NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG,
cliente.ESTADOCIVIL,
cliente.SEXO,
cliente.DATA_NASCIMENTO,
cliente.EMAILSECUNDARIO,
cliente.DESCRICAO,
cliente.PAIS,
cliente.DATA_REGISTRO,
cliente.CONFIRMA_CADASTRO,
cliente.LOGIN
FROM
cliente
LEFT JOIN franquia ON cliente.FRANQUIA_ID = franquia.FRANQUIA_ID
WHERE CLIENTE_ID = :userid AND cliente.CONFIRMA_CADASTRO = 1  AND cliente.FRANQUIA_ID  = :franquia_id"; 


	$enderecos_query = "SELECT
cliente.CLIENTE_ID,
enderecos.ENDERECO_ID,
enderecos.CEP,
enderecos.BAIRRO,
enderecos.ENDERECO,
enderecos.NUMERO,
enderecos.CIDADE
FROM
cliente
LEFT JOIN rel_endereco_cliente ON rel_endereco_cliente.CLIENTE_ID = cliente.CLIENTE_ID
LEFT JOIN enderecos ON rel_endereco_cliente.ENDERECO_ID = enderecos.ENDERECO_ID
WHERE cliente.CLIENTE_ID = :cliente_id AND  NOT ISNULL(enderecos.ENDERECO_ID) ";


		$end = $db->query2($enderecos_query, array(':cliente_id'=>  $_SESSION['credentials']->id));

		//$data_endereco = $db->query2($enderecos_query, array(':cliente_id' => $_SESSION['credentials']->id  )  );	


		$query_c = $db->query2($query_comprador, array(':userid' => $_SESSION['credentials']->id, ':franquia_id' => $_SESSION['credentials']->franquia_id));

		$usuario = array();

		foreach($query_c->querydata[0] as $key => $value){
			$usuario[$key] = $value;
		}

		Flight::render('carrinho/checkout.php', array('querydata'=>$store, 'conteudo', 'comprador' => $usuario,  'enderecos'=>$end->querydata), 'conteudo' );
		Flight::render('home');


	}


	public static function calculaFrete($id){

		if(!isset($_POST['cep']) || empty($_POST['cep'])){

			return Flight::json(array('result'=>0));
		}

		$db = Flight::get('db');


		
		//var_dump($_POST);


		$query = "SELECT
franquia.CEPFRANQUIA
FROM
franquia
WHERE
franquia.FRANQUIA_ID = :id";
		
		$q = $db->query2($query, array(":id" => $id));


		$clean = array('-', ' ', '.');



		$cep_origem =  str_replace($clean, '', $q->querydata[0]['CEPFRANQUIA']);
		$cep_destino =  str_replace($clean, '', $_POST['cep']); //$_POST['cep'];
		$servico = $_POST['servico'];
		$peso = floatval($_POST['peso']);
		
		$val = calcula_frete($servico, $cep_origem, $cep_destino ,$peso);


		/*
			bem vindo a gambiarra onde p PHP te obriga a fazer
			simplemente pra pegar um valor decimal sem perder as cas
			terei que  trocar  virgula por ponto  e depois converter
		*/
	
		if(!is_bool($val)){
			$num = (float) str_replace(',','.', $val->__toString());
			$res = Flight::json(array('frete' =>   $num, 'result' => 1));

			$_SESSION['cpfChecked'] = true;
		
			return $servico;
		}else {

			return Flight::json(array('result'=>0));
		}
		


	}




	public static function recomendacao($franquia_id){

			$franquia_id = Flight::get('actualFranquia');


			if(!Util::checaUsuario($franquia_id)){

				$_SESSION['redirBack'] = Flight::request()->url;
				Flight::redirect(sprintf("/%d/login/",$franquia_id));

			}

		if(!isset($_POST['recomendacao']) ||  empty($_POST['recomendacao'])){

			alert("Houve um Problema", "Preencha o campo com o codigo de referencia de  um usuario válido",ALERTA_ERRO);
			Flight::redirect(sprintf("/%d/carrinho/checkout/",$franquia_id));

		}

		$db = Flight::get('db');
		$cart = $_SESSION['carrinho'];

		$recomendacao  = substr($_POST['recomendacao'],0,6);


		$cod_query = "  SELECT
							cliente.NOME,
							cliente.SOBRENOME,
							cliente.CLIENTE_ID,
							cliente.CODIGO_REFERENCIA
						FROM
							cliente
							WHERE cliente.CODIGO_REFERENCIA LIKE :codigo";

		$q = $db->query2($cod_query, array(':codigo'=> $recomendacao));


		$id = $q->querydata[0]['CLIENTE_ID'];
		$nome = $q->querydata[0]['NOME'];
		$sobrenome = $q->querydata[0]['SOBRENOME'];


		//somente para repassar para outra´s paginas
		//,mas nao tem nenhum peso consideravel se caso nao existir
		//ela sera reescrita de volta
		$_SESSION['codReferencia'] = $q->querydata[0]['CODIGO_REFERENCIA'];


		if($id == $_SESSION['credentials']->id){

			alert("Houve um Problema", "Proibido usar seu proprio codigo de referencia",ALERTA_ERRO);
			Flight::redirect(sprintf("/%d/carrinho/checkout/",$franquia_id));
		}

		if($q->rowsAffected() > 0){

				$query_prod = "SELECT
produto_categoria_subcategoria.PRODUTO_ID,
produto_categoria_subcategoria.SUBCATEGORIA_ID,
produto_categoria_subcategoria.CATEGORIA_ID,
produto.PRODUTO_ID,
produto.NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.PRECO,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
produto.DESCONTOCLIENTE,
produto.DESCONTOPRODUTO,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PROMOCAO_ATIVA,
categoria.NOME as CAT_NOME,
subcategoria.NOME as SUBCAT_NOME
FROM
produto
LEFT JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
LEFT JOIN categoria ON produto_categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
LEFT JOIN subcategoria ON produto_categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE produto.PRODUTO_ID = :prod_id";

				$fator = array();

				for($i = 0; $i < $cart->count(); $i++){
					
					$prod_id = $cart->all_products()[$i]->id;
					$product = $cart->search($prod_id); 
					$index   = $cart->indexSearch($prod_id);

					$q = $db->query2($query_prod ,array(':prod_id'=>$prod_id));

					$product->desconto = $q->querydata[0]['DESCONTOCLIENTE']  * $product->qty;
					//$product->price = $q->querydata[0]['PRECO'] - $q->querydata[0]['DESCONTOCLIENTE'];
					array_push($fator, array('id' =>$prod_id, 'fator' => $q->querydata[0]['DESCONTOCLIENTE'] ));

					$cart->all_products()[$index] = $product;
					

					//$_SESSION['carrinho'] = $cart;
					

				}



				self::updateCart($id);

				$_SESSION['descontoFator'] = $fator;
 
				alert("Sucesso", "Sua Referencia foi Aceita Com Sucesso");
				//Flight::render("carrinho/checkout.php", $fator,'conteudo');
				//Flight::render("home");
				Flight::redirect(sprintf("/%d/carrinho/checkout/",$franquia_id));


		}else {


			alert("Houve um Problema", "Código Invalido",ALERTA_ERRO);
			Flight::redirect(sprintf("/%d/carrinho/checkout/",$franquia_id));

		}

		//var_dump($q->querydata);

	}


	public static function apagarReferencias($id){
		
		if(!Util::checaUsuario($id)){

				$_SESSION['redirBack'] = Flight::request()->url;
				Flight::redirect(sprintf("/%d/login/",$id));

		}

		$cart = $_SESSION['carrinho'];

		if(isset($_SESSION['descontoFator'])){

			unset($_SESSION['descontoFator']);
			unset($_SESSION['codReferencia']);
			$cart->desconto = null;

		} 	

			self::updateCart($id);

			alert("Sucesso", "Referencias Apagadas com Sucesso");
			Flight::redirect(sprintf("/%d/carrinho/checkout/",$id));

	}

	public static function finalizarCompra($id){
		
		if(!Util::checaUsuario($id)){

				$_SESSION['redirBack'] = Flight::request()->url;
				Flight::redirect(sprintf("/%d/login/",$id));

		}

		if(!array_key_exists('credentials', $_SESSION)){


			alert("Houve um Problema", "Não Tem Permissao de Acesso a Esta Pagina",ALERTA_ERRO);
			Flight::redirect(sprintf("/%d/carrinho/checkout/",$id));

		}

		$db = Flight::db();
		$credential = $_SESSION['credentials'];

		/* VERIFICA SE A PESSOA CADASTROU ENDEREÇOS */
		$end_query = "SELECT EXISTS(
SELECT
cliente.CLIENTE_ID,
enderecos.ENDERECO_ID,
enderecos.ENDERECO,
enderecos.BAIRRO,
enderecos.NUMERO,
enderecos.CIDADE,
enderecos.ESTADO,
enderecos.CEP
FROM
cliente
LEFT JOIN rel_endereco_cliente ON rel_endereco_cliente.CLIENTE_ID = cliente.CLIENTE_ID
LEFT JOIN enderecos ON rel_endereco_cliente.ENDERECO_ID = enderecos.ENDERECO_ID
WHERE
cliente.CLIENTE_ID = :cliente_id AND NOT ISNULL(enderecos.ENDERECO_ID)
) as EXISTE";


		$q = $db->query2($end_query, array(':cliente_id'=>$credential->id));

		//var_dump($q->querydata[0]);

		if($q->querydata[0]['EXISTE'] == 0){


			alert("Houve um Problema", "Necessita de pelo menos um endereço cadastrado",ALERTA_ERRO);
			Flight::redirect(sprintf("/%d/carrinho/checkout/",$id));

		}



		$client_query = "SELECT
cliente.CLIENTE_ID,
cliente.FRANQUIA_ID,
cliente.NOME,
cliente.SOBRENOME,
cliente.CPF,
cliente.RG,
cliente.ESTADOCIVIL,
cliente.SEXO,
cliente.DATA_NASCIMENTO,
cliente.EMAILSECUNDARIO,
cliente.DESCRICAO,
cliente.PAIS,
cliente.LOGIN
FROM
cliente
WHERE cliente.CLIENTE_ID = :cliente_id";


	
		$cliente = $db->query2($client_query, array(':cliente_id'=>$credential->id));


		$nome = $cliente->querydata[0]['NOME'];
		$sobrenome = $cliente->querydata[0]['SOBRENOME'];
		$numero = $cliente->querydata[0]['NUMERO'];
		$complemento = "casa";
		$email = $cliente->querydata[0]['LOGIN'];
		$telefone = $cliente->querydata[0]['TELEFONE'];
		$area = $cliente->querydata[0]['NOME'];


		$seleciona_end_query = "SELECT
cliente.CLIENTE_ID,
cliente.NOME,
cliente.SOBRENOME,
enderecos.ENDERECO_ID,
enderecos.ENDERECO,
enderecos.BAIRRO,
enderecos.NUMERO,
enderecos.CIDADE,
enderecos.CEP,
enderecos.ESTADO
FROM
cliente
LEFT JOIN rel_endereco_cliente ON rel_endereco_cliente.CLIENTE_ID = cliente.CLIENTE_ID
LEFT JOIN enderecos ON rel_endereco_cliente.ENDERECO_ID = enderecos.ENDERECO_ID
WHERE
cliente.CLIENTE_ID = :cliente_id AND NOT ISNULL(enderecos.ENDERECO_ID) AND enderecos.ENDERECO_ID = :end_id";

	
		$pega_end = $db->query2($seleciona_end_query, array(':cliente_id' => $credential->id, ':end_id' => $_POST['endOpcao'] ));

		$nome =  $pega_end->querydata[0]['NOME'];
		$sobrenome =  $pega_end->querydata[0]['SOBRENOME'];
		$end_selecionado= $pega_end->querydata[0]['ENDERECO'];
		$num_selecionado= $pega_end->querydata[0]['NUMERO'];
		$bairro_selecionado =  $pega_end->querydata[0]['BAIRRO'];
		$cidade_selecionado =  $pega_end->querydata[0]['CIDADE'];
		$estado_selecionado =  $pega_end->querydata[0]['ESTADO'];

		$cep =  $pega_end->querydata[0]['CEP'];

		$conf = array('email' => 'nicholasluis@gmail.com', 'token' => '626AF57A32EF4BE5863AB61D13249065');

	
		$xml = $_SESSION['carrinho']-> generateXMLPagSeguro(array('name'=> $nome.' '.$sobrenome,
											 'email' => $email
											 //'phone' => array('areaCode'=>12, 'number' => '551236426818')

											),

									  array('address'=> array('street'=>$end_selecionado,
									  						  'number' => $num_selecionado,
									  						  'complement'=> 'casa',
									  						  'district'=> $bairro_selecionado,
									  						  'postalCode'=> $cep,
									  						  'city'=> $cidade_selecionado,
									  						  'state'=> $estado_selecionado,
									  						  'country'=>'BRA'),
									  		'type'=> $_POST['freteOpcao']),

									  array('redirectURL' => sprintf("%s/%d/carrinho/transacao_concluida",rootURL(), $id),
									  		'email' => $conf['email'],
									  		'token' => $conf['token'])
									 );


		
		
		$url =  'https://ws.pagseguro.uol.com.br/v2/checkout/?email='.$conf['email'].'&token='.$conf['token'];
		//print $xml;
	


		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, Array('Content-Type: application/xml; charset=UTF-8'));
		curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
		
		$xml= curl_exec($curl);

		if($xml == "Unauthorized"){
			Flight::render('carrinho/retorno.php', array('status'=> false), 'conteudo' );
			Flight::render('home.php');
			exit();
		}

		curl_close($curl);


		//header("Content-Type: application/xml");
		//print $xml;



		$parseXML = simplexml_load_string($xml);

		if( count($parseXML->error) > 0){

			alert('Falha', "O Sistema Esta Com Problemas Desculpe", ALERTA_ERRO);
			Flight::render('carrinho/retorno.php', array('status'=> false), 'conteudo' );
			Flight::render('home.php');

		}


		//header("Content-Type: text/xml");
		//echo $xml;
				
	
		
		//GRAVA TODOS OS DADOS DA COMPRA NO BANCO DE DADOS P'RA GUARDAR PEDIDOS
		 
	

	
		$dataHoje = new DateTime('NOW', new DateTimeZone('America/Sao_Paulo'));
		$dataEntrega = new DateTime('NOW', new DateTimeZone('America/Sao_Paulo'));

		$dataEntrega->add(new DateInterval('P3D'));

		/*
		$ordem_id =  $db->insert('ordem_pedido', array('PEDIDO_STATUS_ID' => '1',
														'FRANQUIA_ID' => $id,
														'DATA_PEDIDO' => $dataHoje->format("Y-m-d H:i:s"),
														'DATA_ENTREGA' => $dataEntrega->format("Y-m-d H:i:s"),
														'CODIGO_PAGSEGURO' => $parseXML->code));

		*/
	

		$codigo = $parseXML->code;
	
		$ordem_id = $db->insert('ordem_pedido',  array('PEDIDO_STATUS_ID' => 1,
													   'FRANQUIA_ID'  => Flight::get('actualFranquia'),
													   'DATA_PEDIDO' => $dataHoje->format("Y-m-d H:i:s"),
													   'DATA_ENTREGA' =>  $dataEntrega->format("Y-m-d H:i:s")
													    ));




		$_SESSION['ultimoPedido'] = $ordem_id;



		
	   foreach ($_SESSION['carrinho']->all_products()  as $key => $value) {
	   		
	   		$ref = isset($_SESSION['descontoFator']) ? 1 : 0;




	   		$produto_ordem_id = $db->insert('produto_ordem', array('ORDEM_ID' => $ordem_id,
	   											'QUANTIDADE' => $value->qty,
	   											'REFERENCIA' => $ref));



	   		$produto_pedido = $db->insert('rel_produto_ordem_pedido', array(
	   							'PRODUTO_ID' => $value->id,
	   							'PRODUTO_ORDEM_ID' => $produto_ordem_id

	   			));
	   		

	   		

	   }


	   $db->insert('rel_cliente_ordem_pedido', array('CLIENTE_ID' => $_SESSION['credentials']->id, 'ORDEM_ID' => $ordem_id));


	
	   unset($_SESSION['carrinho']);
	   $_SESSION['carrinho'] = Flight::carrinho();


		Flight::redirect('https://pagseguro.uol.com.br/v2/checkout/payment.html?code='.$parseXML->code);
				//FIM





		/*
		$requestPayment = new PagSeguroPaymentRequest();

		foreach($_SESSION['carrinho']->all_products() as $key => $value ){
				$qtd =  $value->qty;
				$fator_desconto =  isset($_SESSION['descontoFator']) ? $value->desc_cliente * $value->qty : 0;
				$price = ($value->price * $value->qty) - $fator_desconto;
				$requestPayment->addItem($value->id, $value->nome , $value->qty , sprintf("%.2f", $price / $value->qty));  

		}

		$nome_completo = $nome.' '.$sobrenome;

		$requestPayment->setSender($nome_completo, $end_selecionado, $num_selecionado, 'casa', $bairro_selecionado, $cidade_selecionado, $estado_selecionado, 'BRA' );
		$requestPayment->setCurrency("BRL"); 
		$requestPayment->setShippingType($_POST['freteOpcao']);

		$requestPayment->addParameter('redirectURL', rootURL().'/'.$franquia_id.'/carrinho/transacao_concluida');

		$pagSeguroCredentials = new PagSeguroAccountCredentials('nicholasluis@gmail.com','626AF57A32EF4BE5863AB61D13249065');

		$url = $requestPayment->register($pagSeguroCredentials);

		*/
		
		

	}


	public static function transacaoConcluida($franquia_id){

		
		$id= $_SESSION['ultimoPedido'];
		$codigo = $_GET['transaction_id'];

		$db = Flight::get('db');


		$db->update('ordem_pedido', array('CODIGO_PAGSEGURO' => strval($codigo)), 'ORDEM_ID = '.$id);
		unset($_SESSION['ultimoPedido']);

		Flight::render('carrinho/transacao_concluida.php', array(), 'conteudo');
		Flight::render('home');

	}


}

?>