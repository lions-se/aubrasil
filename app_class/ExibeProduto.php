<?

class ExibeProduto {

	public static function index(){
		//Flight::render("home.php");
	}

	public static function getProduto($franquia_id,$produto_id,$nome){

		$query = "SELECT
produto.PRODUTO_ID,
produto.NOME,
produto.DESCRICAO,
produto.IMAGEM1,
produto.IMAGEM2,
produto.IMAGEM3,
produto.IMAGEM4,
produto.IMAGEM5,
produto.IMAGEM6,
produto.PRECO,
produto.DESCONTOPRODUTO,
produto.DESCONTOCLIENTE,
produto.DESCONTOFRANQUEADO,
produto.LARGURA,
produto.ALTURA,
produto.COMPRIMENTO,
produto.PESO,
produto.PROMOCAO_ATIVA,
produto_categoria_subcategoria.SUBCATEGORIA_ID,
produto_categoria_subcategoria.CATEGORIA_ID
FROM
produto
INNER JOIN produto_categoria_subcategoria ON produto_categoria_subcategoria.PRODUTO_ID = produto.PRODUTO_ID
INNER JOIN categoria ON produto_categoria_subcategoria.CATEGORIA_ID = categoria.CATEGORIA_ID
INNER JOIN subcategoria ON produto_categoria_subcategoria.SUBCATEGORIA_ID = subcategoria.SUBCATEGORIA_ID
WHERE produto.PRODUTO_ID = :produto_id

";


			$db = Flight::get('db');

			$q = $db->query2($query, array(':produto_id'=> $produto_id));


			//var_dump($franquia_id);

			//var_dump($produto_id);
			//var_dump($q->querydata);



			Flight::render("exibe-produto.php", array('prodquery'=>$q, 'franquia_id'=> $franquia_id, 'produto_id'=>$produto_id), 'conteudo');
			Flight::render('home.php');
	}

}

?>